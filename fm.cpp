/*****************************************************************************
*                                                                            *
*  COPYRIGHT 2018 ACCEL FLIGHT SIMULATION. ALL RIGHTS RESERVED               *
*                                                                            *
*  This file is part of A320 Neo Sim FMGC simulation                         *
*                                                                            *
*  All information and content in this document is the Property              *
*  to ACCEL (Tianjin) Flight Simulation Co., Ltd. and shall not              *
*  be disclosed, disseminated, copied, or used except for purposes           *
*  expressly authorized in writing by ACCEL. Any unauthorized use            *
*  of the content will be considered an infringement of                      *
*  ACCEL's intellectual property rights.                                     *
*                                                                            *
*                                                                            *
*  @file     fm.cpp                                                          *
*  @brief    Entry point for the NetVdn Application.                         *
*                                                                            *
*            This is a Sim FM example of how to use the NetVdnClient to      *
*            publish and subscribe to data on the Virtual Data Network.      *
*                                                                            *
*            This example includes publishing and subscribing to variables   *
*            as well as sending and receiving requests.                      *
*                                                                            *
*  @author   Shi, Junjie                                                     *
*  @email    junjie.shi@accelflightsimulation.com                            *
*  @version  1.0.0.0(version)                                                *
*  @date     2018/07/21                                                      *
*                                                                            *
*----------------------------------------------------------------------------*
*  Remark         : Description                                              *
*----------------------------------------------------------------------------*
*  Change History :                                                          *
*  <Date>     | <Version> | <Author>       | <Description>                   *
*----------------------------------------------------------------------------*
*  1/15/2019  | 1.0.0.0   | Shi, Junjie   | Create file                      *
*----------------------------------------------------------------------------*
*                                                                            *
*****************************************************************************/
#include "fm.h"

FM::FM()
{
}

FM::~FM()
{
}
/*There is no need to subscribe variables here, FG subscribe the variables from simulator,send them to FGA320, 
recv from FGA320 on port 100184 and send the parameters necessary to FM.*/
CoreSim::Osal::Bool FM::subscribeInputsFM(CoreSim::NetVdn::NetVdnClient& vdn)
{  
   //FMGC Power published by FMGC SE. FMGC powered if CB power and not malfunction and not FMGS reset active
   vdn.subscribe(m_inputs.LB02_Power_28vdc[0],"Fmgc","LB02_Power_28vdc[0]");
   vdn.subscribe(m_inputs.LB02_Power_28vdc[1],"Fmgc","LB02_Power_28vdc[1]");
   //FMGC Resets
   vdn.subscribe(m_inputs.Fmgs_Reset,"Fmgc","Fmgs_Reset", "Reset");
   //mcdu bus
   /*
   vdn.subscribe(m_inputs.MCDU1_Bus.Label270,"A429_MCDU1","Label_0270");
   vdn.subscribe(m_inputs.MCDU1_Bus.Label271,"A429_MCDU1","Label_0271");
   vdn.subscribe(m_inputs.MCDU1_Bus.Label272,"A429_MCDU1","Label_0272");
   vdn.subscribe(m_inputs.MCDU1_Bus.Label300,"A429_MCDU1","Label_0300");
   vdn.subscribe(m_inputs.MCDU1_Bus.Label301,"A429_MCDU1","Label_0301");
   vdn.subscribe(m_inputs.MCDU1_Bus.Label302,"A429_MCDU1","Label_0302");
   vdn.subscribe(m_inputs.MCDU1_Bus.Label303,"A429_MCDU1","Label_0303");
   vdn.subscribe(m_inputs.MCDU1_Bus.Label304,"A429_MCDU1","Label_0304");
   vdn.subscribe(m_inputs.MCDU1_Bus.Label307,"A429_MCDU1","Label_0307");
   vdn.subscribe(m_inputs.MCDU1_Bus.Label350,"A429_MCDU1","Label_0350");
   vdn.subscribe(m_inputs.MCDU1_Bus.Label352,"A429_MCDU1","Label_0352");
   vdn.subscribe(m_inputs.MCDU1_Bus.Label354,"A429_MCDU1","Label_0354");
   vdn.subscribe(m_inputs.MCDU1_Bus.Label377,"A429_MCDU1","Label_0377");

   vdn.subscribe(m_inputs.MCDU2_Bus.Label270,"A429_MCDU2","Label_0270");
   vdn.subscribe(m_inputs.MCDU2_Bus.Label271,"A429_MCDU2","Label_0271");
   vdn.subscribe(m_inputs.MCDU2_Bus.Label272,"A429_MCDU2","Label_0272");
   vdn.subscribe(m_inputs.MCDU2_Bus.Label300,"A429_MCDU2","Label_0300");
   vdn.subscribe(m_inputs.MCDU2_Bus.Label301,"A429_MCDU2","Label_0301");
   vdn.subscribe(m_inputs.MCDU2_Bus.Label302,"A429_MCDU2","Label_0302");
   vdn.subscribe(m_inputs.MCDU2_Bus.Label303,"A429_MCDU2","Label_0303");
   vdn.subscribe(m_inputs.MCDU2_Bus.Label304,"A429_MCDU2","Label_0304");
   vdn.subscribe(m_inputs.MCDU2_Bus.Label307,"A429_MCDU2","Label_0307");
   vdn.subscribe(m_inputs.MCDU2_Bus.Label350,"A429_MCDU2","Label_0350");
   vdn.subscribe(m_inputs.MCDU2_Bus.Label352,"A429_MCDU2","Label_0352");
   vdn.subscribe(m_inputs.MCDU2_Bus.Label354,"A429_MCDU2","Label_0354");
   vdn.subscribe(m_inputs.MCDU2_Bus.Label377,"A429_MCDU2","Label_0377");
   */
   vdn.subscribe(m_inputs.Mcdu_bus[0],"A429_MCDU1","");
   vdn.subscribe(m_inputs.Mcdu_bus[1],"A429_MCDU2","");

   vdn.subscribe(m_inputs.Mcdu_Eis_bus[0],"A429_MCDU1_EIS_BGN","");
   vdn.subscribe(m_inputs.Mcdu_Eis_bus[1],"A429_MCDU2_EIS_BGN","");

   //gps bus
   vdn.subscribe(m_inputs.GPS1_Bus.Label057,"A429_MMR1_GNSS_1","Label_0057");
   vdn.subscribe(m_inputs.GPS1_Bus.Label076,"A429_MMR1_GNSS_1","Label_0076");
   vdn.subscribe(m_inputs.GPS1_Bus.Label101,"A429_MMR1_GNSS_1","Label_0101");
   vdn.subscribe(m_inputs.GPS1_Bus.Label102,"A429_MMR1_GNSS_1","Label_0102");
   vdn.subscribe(m_inputs.GPS1_Bus.Label103,"A429_MMR1_GNSS_1","Label_0103");
   vdn.subscribe(m_inputs.GPS1_Bus.Label110,"A429_MMR1_GNSS_1","Label_0110");
   vdn.subscribe(m_inputs.GPS1_Bus.Label111,"A429_MMR1_GNSS_1","Label_0111");
   vdn.subscribe(m_inputs.GPS1_Bus.Label112,"A429_MMR1_GNSS_1","Label_0112");
   vdn.subscribe(m_inputs.GPS1_Bus.Label120,"A429_MMR1_GNSS_1","Label_0120");
   vdn.subscribe(m_inputs.GPS1_Bus.Label121,"A429_MMR1_GNSS_1","Label_0121");
   vdn.subscribe(m_inputs.GPS1_Bus.Label125,"A429_MMR1_GNSS_1","Label_0125");
   vdn.subscribe(m_inputs.GPS1_Bus.Label130,"A429_MMR1_GNSS_1","Label_0130");
   vdn.subscribe(m_inputs.GPS1_Bus.Label133,"A429_MMR1_GNSS_1","Label_0133");
   vdn.subscribe(m_inputs.GPS1_Bus.Label136,"A429_MMR1_GNSS_1","Label_0136");
   vdn.subscribe(m_inputs.GPS1_Bus.Label140,"A429_MMR1_GNSS_1","Label_0140");
   vdn.subscribe(m_inputs.GPS1_Bus.Label141,"A429_MMR1_GNSS_1","Label_0141");
   vdn.subscribe(m_inputs.GPS1_Bus.Label150,"A429_MMR1_GNSS_1","Label_0150");
   vdn.subscribe(m_inputs.GPS1_Bus.Label165,"A429_MMR1_GNSS_1","Label_0165");
   vdn.subscribe(m_inputs.GPS1_Bus.Label166,"A429_MMR1_GNSS_1","Label_0166");
   vdn.subscribe(m_inputs.GPS1_Bus.Label174,"A429_MMR1_GNSS_1","Label_0174");
   vdn.subscribe(m_inputs.GPS1_Bus.Label247,"A429_MMR1_GNSS_1","Label_0247");
   vdn.subscribe(m_inputs.GPS1_Bus.Label260,"A429_MMR1_GNSS_1","Label_0260");
   vdn.subscribe(m_inputs.GPS1_Bus.Label273,"A429_MMR1_GNSS_1","Label_0273");
   vdn.subscribe(m_inputs.GPS1_Bus.Label377,"A429_MMR1_GNSS_1","Label_0377");
      
   vdn.subscribe(m_inputs.GPS2_Bus.Label057,"A429_MMR1_GNSS_2","Label_0057");
   vdn.subscribe(m_inputs.GPS2_Bus.Label076,"A429_MMR1_GNSS_2","Label_0076");
   vdn.subscribe(m_inputs.GPS2_Bus.Label101,"A429_MMR1_GNSS_2","Label_0101");
   vdn.subscribe(m_inputs.GPS2_Bus.Label102,"A429_MMR1_GNSS_2","Label_0102");
   vdn.subscribe(m_inputs.GPS2_Bus.Label103,"A429_MMR1_GNSS_2","Label_0103");
   vdn.subscribe(m_inputs.GPS2_Bus.Label110,"A429_MMR1_GNSS_2","Label_0110");
   vdn.subscribe(m_inputs.GPS2_Bus.Label111,"A429_MMR1_GNSS_2","Label_0111");
   vdn.subscribe(m_inputs.GPS2_Bus.Label112,"A429_MMR1_GNSS_2","Label_0112");
   vdn.subscribe(m_inputs.GPS2_Bus.Label120,"A429_MMR1_GNSS_2","Label_0120");
   vdn.subscribe(m_inputs.GPS2_Bus.Label121,"A429_MMR1_GNSS_2","Label_0121");
   vdn.subscribe(m_inputs.GPS2_Bus.Label125,"A429_MMR1_GNSS_2","Label_0125");
   vdn.subscribe(m_inputs.GPS2_Bus.Label130,"A429_MMR1_GNSS_2","Label_0130");
   vdn.subscribe(m_inputs.GPS2_Bus.Label133,"A429_MMR1_GNSS_2","Label_0133");
   vdn.subscribe(m_inputs.GPS2_Bus.Label136,"A429_MMR1_GNSS_2","Label_0136");
   vdn.subscribe(m_inputs.GPS2_Bus.Label140,"A429_MMR1_GNSS_2","Label_0140");
   vdn.subscribe(m_inputs.GPS2_Bus.Label141,"A429_MMR1_GNSS_2","Label_0141");
   vdn.subscribe(m_inputs.GPS2_Bus.Label150,"A429_MMR1_GNSS_2","Label_0150");
   vdn.subscribe(m_inputs.GPS2_Bus.Label165,"A429_MMR1_GNSS_2","Label_0165");
   vdn.subscribe(m_inputs.GPS2_Bus.Label166,"A429_MMR1_GNSS_2","Label_0166");
   vdn.subscribe(m_inputs.GPS2_Bus.Label174,"A429_MMR1_GNSS_2","Label_0174");
   vdn.subscribe(m_inputs.GPS2_Bus.Label247,"A429_MMR1_GNSS_2","Label_0247");
   vdn.subscribe(m_inputs.GPS2_Bus.Label260,"A429_MMR1_GNSS_2","Label_0260");
   vdn.subscribe(m_inputs.GPS2_Bus.Label273,"A429_MMR1_GNSS_2","Label_0273");
   vdn.subscribe(m_inputs.GPS2_Bus.Label377,"A429_MMR1_GNSS_2","Label_0377");
    
   //clock
   vdn.subscribe(m_inputs.Clock_bus.Label125,"A429_Clock","Label_0125"); 
   vdn.subscribe(m_inputs.Clock_bus.Label150,"A429_Clock","Label_0150"); 
   vdn.subscribe(m_inputs.Clock_bus.Label260,"A429_Clock","Label_0260"); 
   vdn.subscribe(m_inputs.Clock_bus.Label377,"A429_Clock","Label_0377"); 

   //dme bus--one shot
   vdn.subscribe(m_inputs.Dme_bus[0],"A429_DME1_1","");
   vdn.subscribe(m_inputs.Dme_bus[1],"A429_DME2_1","");
   //one shot test
   //vdn.subscribe(m_inputs.Dme_bus[0],"test_bus","");

   // A429 data loading buses from data loader SE
   vdn.subscribe(m_inputs.Dldr_bus[0], "A429_DLDR_FMGC1","");   //A429_DLDR_FMGC1SE
   vdn.subscribe(m_inputs.Dldr_bus[1], "A429_DLDR_FMGC2","");   //A429_DLDR_FMGC2SE

   vdn.subscribe(m_inputs.Atsu_acars[0],"A429_ATSU_SYS_3_OUT","");   
   vdn.subscribe(m_inputs.Atsu_acars[1],"A429_ATSU_SYS_4_OUT","");   

   //the buses are linked with hardwire currently. May not one-shot type. 
   vdn.subscribe(m_inputs.Print_1,"A429_PRINTER","");   //topic name to be modified later according to the busname of in the db

   return true;
}

template <typename T>
void FM::packMsgBufHeader(T& bus,CoreSim::Osal::UInt32 m_numBytes,CoreSim::Osal::UInt16 m_numWORDS,CoreSim::Osal::UInt16 m_numMSGS)
{
   if(m_numBytes <= C_MAX_NUM_BYTES)
   bus.NUM_BYTES = m_numBytes;
   bus.NUM_WORDS = m_numWORDS;
   bus.NUM_MSGS  = m_numMSGS;
   return;
}

template <typename T>
void FM::packMsg(T& bus,CoreSim::Osal::UChar m_msgType,CoreSim::Osal::UChar m_CAB,CoreSim::Osal::UChar m_BusNum1,CoreSim::Osal::UChar m_numWords1,CoreSim::Osal::UChar m_numWords2,CoreSim::Osal::UChar m_BusNum2)
{
   bus.MSG_Header.MSG_TYPE   = m_msgType;
   bus.MSG_Header.CAB        = m_CAB;
   bus.MSG_Header.BusNum1    = m_BusNum1;
   bus.MSG_Header.NUM_WORDS1 = m_numWords1;
   bus.MSG_Header.NUM_WORDS2 = m_numWords2;
   bus.MSG_Header.BusNum2    = m_BusNum2;
   return;
}

template <typename T>
int FM::setBusNumber(T& bus,CoreSim::Osal::UInt32 m_busNum)
{
   bus.MSG_Header.BusNum1 = m_busNum&0xff;
   bus.MSG_Header.BusNum2 = (m_busNum >> 8) & 0x0f;
   int busNum = ( bus.MSG_Header.BusNum2 <<8) | bus.MSG_Header.BusNum1;

   return busNum;
}

template <typename T>
void FM::setMsg(T& bus,CoreSim::Osal::UInt32 m_msgType)
{
   bus.MSG_Header.MSG_TYPE = m_msgType;
   switch (m_msgType)
   {
   case 3:
       setNumWords(bus,1);
       setCab(bus,0);
       setBusNumber(bus,0);
       break;
   case 26:
       setNumWords(bus,2);
       break;
   case 27:
       setNumWords(bus,2);
       break;     
   case 28:
       setNumWords(bus,2);
       break;
   case 29:
      setNumWords(bus,2);
      setBusNumber(bus,0);
      break;
   case 30:
      setCab(bus,0);
      setBusNumber(bus,0);
      break;
   case 31:
      setCab(bus,0);
      setBusNumber(bus,0);
      break;
   default:
      break;
   }
   return;
}

template <typename T>
void FM::setCab(T& bus,CoreSim::Osal::UChar m_CAB)
{
   bus.MSG_Header.CAB = m_CAB;

   return;
}

template <typename T>
void FM::setBusData(T& bus,CoreSim::Osal::UInt32 bus_data)
{
   bus.BUS_DATA = bus_data;

   return;
}

template <typename T>
void FM::setNumWords(T& bus,CoreSim::Osal::UInt32 m_numWords)
{
   bus.MSG_Header.NUM_WORDS1 = m_numWords&0xff;
   bus.MSG_Header.NUM_WORDS2 = (m_numWords >> 8) & 0x0f;

   return;
}

//Connect to FM use TCP socket and send Initial Message.
//initial protocol. send Initial Connection String,echo the orientation message back after receive it from FMS2.
//send all initial discrete information.
int FM::FMConnAndSendInitial(FG* fg,CoreSim::Osal::String fm_server_ip,CoreSim::Osal::UInt16 fm_server_port)
{
   CoreSim::Osal::Int32 bytes_Received = 0;
   CoreSim::Osal::Int32 recMsgType     = 0;

   //TCP Socket used for sending.
   CoreSim::Osal::String send_ip       = fm_server_ip;   //"192.168.1.55";
   CoreSim::Osal::UInt16 send_port     = fm_server_port; //18300
   m_sendSocketAddress = Osal::Net::SocketAddress(send_ip, send_port);

   try
   {
      //m_txTcpSocket.setRecvTimeout(30);
      m_txTcpSocket.connect(m_sendSocketAddress);
      
      /*Initial Connection String*/
      //char buffs[]   = "FMS2 -aircraft a320 -dual -show_dbg -protocol3 -mapping_addr 80790000 �issdll -brief_trace -priority 3";
      char buffs[]   = "FMS2 -aircraft a320 -dual -show_dbg -protocol3 -mapping_addr 80790000 -issdll";
      //char buffs[]   = "FMS2 -brief_trace -pid 1616 -aircraft a320 -dual -show_dbg -issdll -mapping_addr 80790000";
      int bytes_Send = static_cast<int>(m_txTcpSocket.send(buffs,sizeof(buffs)));
      if (bytes_Send > 0)
      {
         std::cout << "SEND <" << buffs << ">" << std::endl;
      }
      else
      {
         std::cout << "Send Error:!" <<  std::endl;
         //return 0;
      }

      /*After the initial Connection String, XSIM receive the orientation message, and must transmit the same pattern to the FMS2.*/
      /*orientation message:08 00 00 00 03 02 01 00*/
      memset(&m_RevBufSize,0,sizeof(m_RevBufSize));
      do
      {    
         bytes_Received = m_txTcpSocket.recv(&m_RevBufSize,sizeof(m_RevBufSize));
         if ((bytes_Received > 0)&&(m_RevBufSize.NUM_BYTES == 8) && (m_RevBufSize.NUM_WORDS == 0x0203) && (m_RevBufSize.NUM_MSGS == 0x01))
         {
            m_txTcpSocket.send(&m_RevBufSize,sizeof(m_RevBufSize));
         }
      } while (bytes_Received <= 0);

      recMsgType = RecvDataFromFM(fg);
      if (C_P_END_FRAME == recMsgType)
      {  
         m_locals.fm_puprdy_fm = TRUE;
         //Sends discrete Data after receives the first P_END_FRMAE and before P_X_END_FRAME
         packDisDatas(m_disData);
         m_txTcpSocket.send(&m_disData,sizeof(m_disData));
      }

      return 0;
   }
   catch (...)
   {
      std::cerr << "Could not connect to TCP Server" << send_ip << ":" << send_port << " or connection was lost." << std::endl;
      return 1;
   }  
}

template <typename T>
CoreSim::Osal::Int16 FM::packDisData(T& input,FILE *fm)
{
   CoreSim::Osal::UInt16 m_ByteNum = 0;

   if((fm = fopen("fmlog.txt","a+")) == NULL)
   {
      std::wcout << "Failed to open fmlog.txt! packDisData" <<std::endl;
      return 0;
   }
   //memset(&input,0,sizeof(input));

   //DIS words-Discrete word0.Bus 66-FMS1/2
   setMsg(input.m_bus_66,C_P_BUS_DIS); 
   setCab(input.m_bus_66,C_FMS_1_2);
   setBusNumber(input.m_bus_66,Bus_DIS_WD_0);
   //setBusData(input.m_bus_66,bytes4ToInt(0x00b489d0)); //0x88762200
   setBusData(input.m_bus_66,bytes4ToInt(di_word0_bus66));   //modified in integrate stadge
   m_ByteNum += 8;
   
   //DIS words-Discrete word1.Bus 68-FMS1/2
   setMsg(input.m_bus_68,C_P_BUS_DIS); 
   setCab(input.m_bus_68,C_FMS_1_2);
   setBusNumber(input.m_bus_68,Bus_DIS_WD_1);
   //setBusData(input.m_bus_68,bytes4ToInt(0x00000400));  //0x00000400 
   setBusData(input.m_bus_68,bytes4ToInt(di_word1_bus68));   //modified in integrate stadge
   m_ByteNum += 8;

   //DIS words-cfm-engine.Bus 464(0x1d0)-FMS1/2
   setMsg(input.m_bus_464,C_P_BUS_DIS); 
   setCab(input.m_bus_464,C_FMS_1_2);
   setBusNumber(input.m_bus_464,Bus_CFM_ENG);
   //setBusData(input.m_bus_464,bytes4ToInt(0x00000000)); 
   setBusData(input.m_bus_464,bytes4ToInt(cfm_engine));   //modified in integrate stadge
   m_ByteNum += 8;

   //DIS words-iae-engine.Bus 465(0x1d1)-FMS1/2
   setMsg(input.m_bus_465,C_P_BUS_DIS); 
   setCab(input.m_bus_465,C_FMS_1_2);
   setBusNumber(input.m_bus_465,Bus_IAE_ENG);
   //setBusData(input.m_bus_465,bytes4ToInt(0x000000FF)); 
   setBusData(input.m_bus_465,bytes4ToInt(iae_engine));   //modified in integrate stadge
   m_ByteNum += 8;

   //DIS words-pw-engine.Bus 473(0x1d9)-FMS1/2
   setMsg(input.m_bus_473,C_P_BUS_DIS); 
   setCab(input.m_bus_473,C_FMS_1_2);
   setBusNumber(input.m_bus_473,Bus_PW_ENG);
   //setBusData(input.m_bus_473,bytes4ToInt(0x00000000));
   setBusData(input.m_bus_473,bytes4ToInt(pw_engine));   //modified in integrate stadge
   m_ByteNum += 8;
   
   //DIS words-Program Pins.Bus 461(0x1cd)-FMS1/2
   setMsg(input.m_bus_461,C_P_BUS_DIS); 
   setCab(input.m_bus_461,C_FMS_1_2);
   setBusNumber(input.m_bus_461,Bus_AL_PINS);
   //setBusData(input.m_bus_461,bytes4ToInt(0x00000018)); //0x00000862
   setBusData(input.m_bus_461,bytes4ToInt(ac_Pins_Dis));
   m_ByteNum += 8;
   
   //DIS words-Program Pins.Bus 462(0x1ce)-FMS1/2
   setMsg(input.m_bus_462,C_P_BUS_DIS); 
   setCab(input.m_bus_462,C_FMS_1_2);
   setBusNumber(input.m_bus_462,Bus_AL_PIN1);
   //setBusData(input.m_bus_462,bytes4ToInt(0x00000000));   //0x00003000
   setBusData(input.m_bus_462,bytes4ToInt(al_Pins_Dis1));   //AL
   m_ByteNum += 8;
  
   //DIS words-Program Pins 2.Bus 463(0x1cf)-FMS1/2
   setMsg(input.m_bus_463,C_P_BUS_DIS); 
   setCab(input.m_bus_463,C_FMS_1_2);
   setBusNumber(input.m_bus_463,Bus_AL_PIN2);
   //setBusData(input.m_bus_463,bytes4ToInt(0x00000000));  //0x00000020
   setBusData(input.m_bus_463,bytes4ToInt(al_Pins_Dis2));  
   m_ByteNum += 8;

   //DIS words-code comp1.Bus 466(0x1d2)-FMS1/2
   setMsg(input.m_bus_466,C_P_BUS_DIS); 
   setCab(input.m_bus_466,C_FMS_1_2);
   setBusNumber(input.m_bus_466,Bus_CODE_CMP_WD1);
   //setBusData(input.m_bus_466,bytes4ToInt(0x00004141)); 
   setBusData(input.m_bus_466,bytes4ToInt(code_cmp_1));     //modify later 20190508
   m_ByteNum += 8;
   
   //DIS words-code comp2.Bus 467(0x1d3)-FMS1/2
   setMsg(input.m_bus_467,C_P_BUS_DIS); 
   setCab(input.m_bus_467,C_FMS_1_2);
   setBusNumber(input.m_bus_467,Bus_CODE_CMP_WD2);
   //setBusData(input.m_bus_467,bytes4ToInt(0x00004141));  
   setBusData(input.m_bus_467,bytes4ToInt(code_cmp_2));
   m_ByteNum += 8;
   
   //DIS words-code comp3.Bus 468(0x1d4)-FMS1/2
   setMsg(input.m_bus_468,C_P_BUS_DIS); 
   setCab(input.m_bus_468,C_FMS_1_2);
   setBusNumber(input.m_bus_468,Bus_CODE_CMP_WD3);
   //setBusData(input.m_bus_468,bytes4ToInt(0x00004141)); 
   setBusData(input.m_bus_468,bytes4ToInt(code_cmp_3));
   m_ByteNum += 8;

   //DIS words-code comp4.Bus 469(0x1d5)-FMS1/2
   setMsg(input.m_bus_469,C_P_BUS_DIS); 
   setCab(input.m_bus_469,C_FMS_1_2);
   setBusNumber(input.m_bus_469,Bus_CODE_CMP_WD4);
   //setBusData(input.m_bus_469,bytes4ToInt(0x00004141));   
   setBusData(input.m_bus_469,bytes4ToInt(code_cmp_4)); 
   m_ByteNum += 8;
   
   //DIS words-code comp5.Bus 470(0x1d6)-FMS1/2
   setMsg(input.m_bus_470,C_P_BUS_DIS); 
   setCab(input.m_bus_470,C_FMS_1_2);
   setBusNumber(input.m_bus_470,Bus_CODE_CMP_WD5);
   //setBusData(input.m_bus_470,bytes4ToInt(0x00004141));
   setBusData(input.m_bus_470,bytes4ToInt(code_cmp_5));
   m_ByteNum += 8;
   
   //DIS words-code comp6.Bus 471(0x1d7)-FMS1/2
   setMsg(input.m_bus_471,C_P_BUS_DIS); 
   setCab(input.m_bus_471,C_FMS_1_2);
   setBusNumber(input.m_bus_471,Bus_CODE_CMP_WD6);
   //setBusData(input.m_bus_471,bytes4ToInt(0x00004141));   
   setBusData(input.m_bus_471,bytes4ToInt(code_cmp_6)); 
   m_ByteNum += 8;

   //429 words.Bus 100,fg->fm Label 260 fg_healthy -FMS1/2
   setMsg(input.m_bus_100,C_P_BUS_DIS); 
   setCab(input.m_bus_100,C_FMS_1_2);
   setBusNumber(input.m_bus_100,Bus_FGFM_Protocol);
   setBusData(input.m_bus_100,bytes4ToInt(0x0003FD0D));   //TODO: change the value in integrated stadge after FG_Healthy works normal
   //setBusData(input.m_bus_100,bytes4ToInt(fg_hlty));   //modify later 20190508
   m_ByteNum += 8;

#if 0
   //pack other datas here
   //Bus 19,Label 312-FMS1
   setMsg(input.m_bus_19_1,C_P_BUS_429); 
   setCab(input.m_bus_19_1,C_FMS_1);
   setBusNumber(input.m_bus_19_1,19);
   //setBusData(input.m_bus_19_1,bytes4ToInt(0x61000053)); 
   //packMsg(input.m_bus_19_1,26,1,19,2,0,0);
   //input.m_bus_19_1.BUS_DATA = 0x53000061;     //61000053 reverse. 
   m_ByteNum += 8;
   fprintf(fm,"SEND P_BUS 429 %04X  %04X  %04X\n",input.m_bus_19_1.MSG_Header,((input.m_bus_19_1.MSG_Header.BusNum1)|(input.m_bus_19_1.MSG_Header.BusNum2<<8)),bytes4ToInt(input.m_bus_19_1.BUS_DATA));
   fflush(fm);

   //Bus 19,Label 312-FMS2
   setMsg(input.m_bus_19_2,C_P_BUS_429); 
   setCab(input.m_bus_19_2,C_FMS_2);
   setBusNumber(input.m_bus_19_2,19);
   //setBusData(input.m_bus_19_2,bytes4ToInt(0x61000053));
   //packMsg(input.m_bus_19_2,26,2,19,2,0,0);
   //input.m_bus_19_2.BUS_DATA = 0x53000061;
   m_ByteNum += 8;
   fprintf(fm,"SEND P_BUS 429 %04X  %04X  %04X\n",input.m_bus_19_2.MSG_Header,((input.m_bus_19_2.MSG_Header.BusNum1)|(input.m_bus_19_2.MSG_Header.BusNum2<<8)),bytes4ToInt(input.m_bus_19_2.BUS_DATA));
   fflush(fm);
#endif
   //429 words.Bus 100,Label 277-FMS1/2
   //429 words.Bus 100,Label 360 crossload,FMGC 2G only -FMS1/2
   
   //429 words.Bus 108,Label 250-FMS1/2
   setMsg(input.m_bus_108,C_P_BUS_429); 
   setCab(input.m_bus_108,C_FMS_1_2);
   setBusNumber(input.m_bus_108,Bus_Dataloader);
//   setBusData(input.m_bus_108,bytes4ToInt(0x60000415));      //Label 250(0x15)
   m_ByteNum += 8;

#if DEBUG_PRT
   fprintf(fm,"SEND P_BUS DIS %04X  %04X  %04X\n",input.m_bus_66.MSG_Header,((input.m_bus_66.MSG_Header.BusNum1)|(input.m_bus_66.MSG_Header.BusNum2<<8)), bytes4ToInt(input.m_bus_66.BUS_DATA));
   fprintf(fm,"SEND P_BUS DIS %04X  %04X  %04X\n",input.m_bus_68.MSG_Header,((input.m_bus_68.MSG_Header.BusNum1)|(input.m_bus_68.MSG_Header.BusNum2<<8)),bytes4ToInt(input.m_bus_68.BUS_DATA));
   fprintf(fm,"SEND P_BUS DIS %04X  %04X  %04X\n",input.m_bus_464.MSG_Header,((input.m_bus_464.MSG_Header.BusNum1)|(input.m_bus_464.MSG_Header.BusNum2<<8)),bytes4ToInt(input.m_bus_464.BUS_DATA));
   fprintf(fm,"SEND P_BUS DIS %04X  %04X  %04X\n",input.m_bus_465.MSG_Header,((input.m_bus_465.MSG_Header.BusNum1)|(input.m_bus_465.MSG_Header.BusNum2<<8)),bytes4ToInt(input.m_bus_465.BUS_DATA));
   fprintf(fm,"SEND P_BUS DIS %04X  %04X  %04X\n",input.m_bus_473.MSG_Header,((input.m_bus_473.MSG_Header.BusNum1)|(input.m_bus_473.MSG_Header.BusNum2<<8)),bytes4ToInt(input.m_bus_473.BUS_DATA));
   fprintf(fm,"SEND P_BUS DIS %04X  %04X  %04X\n",input.m_bus_461.MSG_Header,((input.m_bus_461.MSG_Header.BusNum1)|(input.m_bus_461.MSG_Header.BusNum2<<8)),bytes4ToInt(input.m_bus_461.BUS_DATA));
   fprintf(fm,"SEND P_BUS DIS %04X  %04X  %04X\n",input.m_bus_462.MSG_Header,((input.m_bus_462.MSG_Header.BusNum1)|(input.m_bus_462.MSG_Header.BusNum2<<8)),bytes4ToInt(input.m_bus_462.BUS_DATA));
   fprintf(fm,"SEND P_BUS DIS %04X  %04X  %04X\n",input.m_bus_463.MSG_Header,((input.m_bus_463.MSG_Header.BusNum1)|(input.m_bus_463.MSG_Header.BusNum2<<8)),bytes4ToInt(input.m_bus_463.BUS_DATA));
   fprintf(fm,"SEND P_BUS DIS %04X  %04X  %04X\n",input.m_bus_466.MSG_Header,((input.m_bus_466.MSG_Header.BusNum1)|(input.m_bus_466.MSG_Header.BusNum2<<8)),bytes4ToInt(input.m_bus_466.BUS_DATA));
   fprintf(fm,"SEND P_BUS DIS %04X  %04X  %04X\n",input.m_bus_467.MSG_Header,((input.m_bus_467.MSG_Header.BusNum1)|(input.m_bus_467.MSG_Header.BusNum2<<8)),bytes4ToInt(input.m_bus_467.BUS_DATA));
   fprintf(fm,"SEND P_BUS DIS %04X  %04X  %04X\n",input.m_bus_468.MSG_Header,((input.m_bus_468.MSG_Header.BusNum1)|(input.m_bus_468.MSG_Header.BusNum2<<8)),bytes4ToInt(input.m_bus_468.BUS_DATA));
   fprintf(fm,"SEND P_BUS DIS %04X  %04X  %04X\n",input.m_bus_469.MSG_Header,((input.m_bus_469.MSG_Header.BusNum1)|(input.m_bus_469.MSG_Header.BusNum2<<8)),bytes4ToInt(input.m_bus_469.BUS_DATA));
   fprintf(fm,"SEND P_BUS DIS %04X  %04X  %04X\n",input.m_bus_470.MSG_Header,((input.m_bus_470.MSG_Header.BusNum1)|(input.m_bus_470.MSG_Header.BusNum2<<8)),bytes4ToInt(input.m_bus_470.BUS_DATA));
   fprintf(fm,"SEND P_BUS DIS %04X  %04X  %04X\n",input.m_bus_471.MSG_Header,((input.m_bus_471.MSG_Header.BusNum1)|(input.m_bus_471.MSG_Header.BusNum2<<8)),bytes4ToInt(input.m_bus_471.BUS_DATA));
   fprintf(fm,"SEND P_BUS 429 %04X  %04X  %04X\n",input.m_bus_100.MSG_Header,((input.m_bus_100.MSG_Header.BusNum1)|(input.m_bus_100.MSG_Header.BusNum2<<8)),bytes4ToInt(input.m_bus_100.BUS_DATA));
   fprintf(fm,"SEND P_BUS 429 %04X  %04X  %04X\n",input.m_bus_108.MSG_Header,((input.m_bus_108.MSG_Header.BusNum1)|(input.m_bus_108.MSG_Header.BusNum2<<8)),bytes4ToInt(input.m_bus_108.BUS_DATA));
   fflush(fm);
#endif

   fclose(fm);
   return m_ByteNum;
}

void FM::packInitialDatas(Common_Data& m_commoms)
{
   /*BUS_DATA should be modified later.subscribe from VDN and may need reverse byte order. shijj*/
   CoreSim::Osal::UInt16 m_ByteNum = 0;
   CoreSim::Osal::UInt16 m_WordNum = 0;
   CoreSim::Osal::UInt16 m_MsgNum  = 0;
   CoreSim::Osal::UInt32 label     = 0;
   CoreSim::Osal::Int32 Num_A429_MCDU1 = 0;
   std::vector <CoreSim::Osal::UInt32> gpsDataList[2];
   std::vector <CoreSim::Osal::UInt32> mcduDataList[2];
   std::vector <CoreSim::Osal::UInt32> clockDataList;
   //std::vector <CoreSim::Osal::UInt32> vorDataList[2];

   if((fmlog = fopen("fmlog.txt","a+")) == NULL)
   {
      std::wcout << "Failed to open fmlog.txt! packInitialDatas" <<std::endl;
      return;
   }
   memset(&m_commoms,0,sizeof(m_commoms));

   m_ByteNum = packDisData(m_commoms,fmlog);
   
#if 0
   //pack other datas here
   //Bus 19,Label 312-FMS1
   setMsg(m_commoms.m_bus_19_1,C_P_BUS_429); 
   setCab(m_commoms.m_bus_19_1,C_FMS_1);
   setBusNumber(m_commoms.m_bus_19_1,19);
   //setBusData(m_commoms.m_bus_19_1,bytes4ToInt(0x61000053)); 
   //packMsg(m_commoms.m_bus_19_1,26,1,19,2,0,0);
   //m_commoms.m_bus_19_1.BUS_DATA = 0x53000061;     //61000053 reverse. 
   m_ByteNum += 8;
   fprintf(fm,"SEND P_BUS 429 %04X  %04X  %04X\n",m_commoms.m_bus_19_1.MSG_Header,((m_commoms.m_bus_19_1.MSG_Header.BusNum1)|(m_commoms.m_bus_19_1.MSG_Header.BusNum2<<8)),bytes4ToInt(m_commoms.m_bus_19_1.BUS_DATA));
   fflush(fm);

   //Bus 19,Label 312-FMS2
   setMsg(m_commoms.m_bus_19_2,C_P_BUS_429); 
   setCab(m_commoms.m_bus_19_2,C_FMS_2);
   setBusNumber(m_commoms.m_bus_19_2,19);
   //setBusData(m_commoms.m_bus_19_2,bytes4ToInt(0x61000053));
   //packMsg(m_commoms.m_bus_19_2,26,2,19,2,0,0);
   //m_commoms.m_bus_19_2.BUS_DATA = 0x53000061;
   m_ByteNum += 8;
   fprintf(fm,"SEND P_BUS 429 %04X  %04X  %04X\n",m_commoms.m_bus_19_2.MSG_Header,((m_commoms.m_bus_19_2.MSG_Header.BusNum1)|(m_commoms.m_bus_19_2.MSG_Header.BusNum2<<8)),bytes4ToInt(m_commoms.m_bus_19_2.BUS_DATA));
   fflush(fm);
#endif

#if 0   
   //429 words.Bus 108,Label 250-FMS1/2
   setMsg(m_commoms.m_bus_108,C_P_BUS_429); 
   setCab(m_commoms.m_bus_108,C_FMS_1_2);
   setBusNumber(m_commoms.m_bus_108,108);
   setBusData(m_commoms.m_bus_108,bytes4ToInt(0x60000415));   
   //packMsg(m_commoms.m_bus_108,26,0,108,2,0,0);
   //m_commoms.m_bus_108.BUS_DATA = 0x15040060;   //Label 250(0x15)
   m_ByteNum += 8;
   fprintf(fm,"SEND P_BUS 429 %04X  %04X  %04X\n",m_commoms.m_bus_108.MSG_Header,((m_commoms.m_bus_108.MSG_Header.BusNum1)|(m_commoms.m_bus_108.MSG_Header.BusNum2<<8)),bytes4ToInt(m_commoms.m_bus_108.BUS_DATA));
   fflush(fm);
#endif
  
   //reverseLabel();
   //gps data  
   gpsDataList[0].push_back(m_inputs.GPS1_Bus.Label057);
   gpsDataList[0].push_back(m_inputs.GPS1_Bus.Label076);
   gpsDataList[0].push_back(m_inputs.GPS1_Bus.Label101);
   gpsDataList[0].push_back(m_inputs.GPS1_Bus.Label102);
   gpsDataList[0].push_back(m_inputs.GPS1_Bus.Label103);
   gpsDataList[0].push_back(m_inputs.GPS1_Bus.Label110);
   gpsDataList[0].push_back(m_inputs.GPS1_Bus.Label111);
   gpsDataList[0].push_back(m_inputs.GPS1_Bus.Label112);
   gpsDataList[0].push_back(m_inputs.GPS1_Bus.Label120);
   gpsDataList[0].push_back(m_inputs.GPS1_Bus.Label121);
   gpsDataList[0].push_back(m_inputs.GPS1_Bus.Label125);
   gpsDataList[0].push_back(m_inputs.GPS1_Bus.Label130);
   gpsDataList[0].push_back(m_inputs.GPS1_Bus.Label133);
   gpsDataList[0].push_back(m_inputs.GPS1_Bus.Label136);
   gpsDataList[0].push_back(m_inputs.GPS1_Bus.Label140);
   gpsDataList[0].push_back(m_inputs.GPS1_Bus.Label141);
   gpsDataList[0].push_back(m_inputs.GPS1_Bus.Label150);
   gpsDataList[0].push_back(m_inputs.GPS1_Bus.Label165);
   gpsDataList[0].push_back(m_inputs.GPS1_Bus.Label166);
   gpsDataList[0].push_back(m_inputs.GPS1_Bus.Label174);
   gpsDataList[0].push_back(m_inputs.GPS1_Bus.Label247);
   gpsDataList[0].push_back(m_inputs.GPS1_Bus.Label260);
   gpsDataList[0].push_back(m_inputs.GPS1_Bus.Label273);
   gpsDataList[0].push_back(m_inputs.GPS1_Bus.Label377);
        
   gpsDataList[1].push_back(m_inputs.GPS2_Bus.Label057);
   gpsDataList[1].push_back(m_inputs.GPS2_Bus.Label076);
   gpsDataList[1].push_back(m_inputs.GPS2_Bus.Label101);
   gpsDataList[1].push_back(m_inputs.GPS2_Bus.Label102);
   gpsDataList[1].push_back(m_inputs.GPS2_Bus.Label103);
   gpsDataList[1].push_back(m_inputs.GPS2_Bus.Label110);
   gpsDataList[1].push_back(m_inputs.GPS2_Bus.Label111);
   gpsDataList[1].push_back(m_inputs.GPS2_Bus.Label112);
   gpsDataList[1].push_back(m_inputs.GPS2_Bus.Label120);
   gpsDataList[1].push_back(m_inputs.GPS2_Bus.Label121);
   gpsDataList[1].push_back(m_inputs.GPS2_Bus.Label125);
   gpsDataList[1].push_back(m_inputs.GPS2_Bus.Label130);
   gpsDataList[1].push_back(m_inputs.GPS2_Bus.Label133);
   gpsDataList[1].push_back(m_inputs.GPS2_Bus.Label136);
   gpsDataList[1].push_back(m_inputs.GPS2_Bus.Label140);
   gpsDataList[1].push_back(m_inputs.GPS2_Bus.Label141);
   gpsDataList[1].push_back(m_inputs.GPS2_Bus.Label150);
   gpsDataList[1].push_back(m_inputs.GPS2_Bus.Label165);
   gpsDataList[1].push_back(m_inputs.GPS2_Bus.Label166);
   gpsDataList[1].push_back(m_inputs.GPS2_Bus.Label174);
   gpsDataList[1].push_back(m_inputs.GPS2_Bus.Label247);
   gpsDataList[1].push_back(m_inputs.GPS2_Bus.Label260);
   gpsDataList[1].push_back(m_inputs.GPS2_Bus.Label273);
   gpsDataList[1].push_back(m_inputs.GPS2_Bus.Label377);
   //mcdu data
   while(m_inputs.Mcdu_bus[0].pop(label))
   {  
      switch(label&0xff)
      {
      case 0XBA:  //272
         m_inputs.MCDU1_Bus.Label272.setWord(reverseBit8(label));
         break;
      case 0XC0: //300
         m_inputs.MCDU1_Bus.Label300.setWord(reverseBit8(label));
         break;
      case 0XC1: //301
         m_inputs.MCDU1_Bus.Label301.setWord(reverseBit8(label));
         break;
      case 0XC2: //301
         m_inputs.MCDU1_Bus.Label301.setWord(reverseBit8(label));
         break;
      case 0XC3: //303
         m_inputs.MCDU1_Bus.Label303.setWord(reverseBit8(label));
         break;
      case 0XFF: //377
         m_inputs.MCDU2_Bus.Label377.setWord(reverseBit8(label));
         break;
      }
      //Num_A429_MCDU1++;
      //if(Num_A429_MCDU1 >= 14)
      //{break;}
      //mcduDataList[0].push_back(label);
   }

   //Num_A429_MCDU1 = 0;
   while(m_inputs.Mcdu_bus[1].pop(label))
   {
      switch(label&0xff)
      {
      case 0XBA:  //272
         m_inputs.MCDU2_Bus.Label272.setLabel(reverseBit8(label));
         break;
      case 0XC0: //300
         m_inputs.MCDU2_Bus.Label300.setLabel(reverseBit8(label));
         break;
      case 0XC1: //301
         m_inputs.MCDU2_Bus.Label301.setLabel(reverseBit8(label));
         break;
      case 0XC2: //301
         m_inputs.MCDU2_Bus.Label301.setLabel(reverseBit8(label));
         break;
      case 0XC3: //303
         m_inputs.MCDU2_Bus.Label303.setLabel(reverseBit8(label));
         break;
      case 0XFF: //377
         m_inputs.MCDU2_Bus.Label377.setLabel(reverseBit8(label));
         break;

      }
      //Num_A429_MCDU1++;
      //if(Num_A429_MCDU1 >= 14)
      //{break;}
      //mcduDataList[1].push_back(label);
   }
   
   mcduDataList[0].push_back(m_inputs.MCDU1_Bus.Label270);
   mcduDataList[0].push_back(m_inputs.MCDU1_Bus.Label271);
   mcduDataList[0].push_back(m_inputs.MCDU1_Bus.Label272);
   mcduDataList[0].push_back(m_inputs.MCDU1_Bus.Label300);
   mcduDataList[0].push_back(m_inputs.MCDU1_Bus.Label301);
   mcduDataList[0].push_back(m_inputs.MCDU1_Bus.Label302);  
   mcduDataList[0].push_back(m_inputs.MCDU1_Bus.Label303);
   mcduDataList[0].push_back(m_inputs.MCDU1_Bus.Label304);
   mcduDataList[0].push_back(m_inputs.MCDU1_Bus.Label307);
   mcduDataList[0].push_back(m_inputs.MCDU1_Bus.Label350);
   mcduDataList[0].push_back(m_inputs.MCDU1_Bus.Label352);
   mcduDataList[0].push_back(m_inputs.MCDU1_Bus.Label354);
   mcduDataList[0].push_back(m_inputs.MCDU1_Bus.Label377);
   

   mcduDataList[1].push_back(m_inputs.MCDU2_Bus.Label270);
   mcduDataList[1].push_back(m_inputs.MCDU2_Bus.Label271);
   mcduDataList[1].push_back(m_inputs.MCDU2_Bus.Label272);
   mcduDataList[1].push_back(m_inputs.MCDU2_Bus.Label300);
   mcduDataList[1].push_back(m_inputs.MCDU2_Bus.Label301);
   mcduDataList[1].push_back(m_inputs.MCDU2_Bus.Label302);  
   mcduDataList[1].push_back(m_inputs.MCDU2_Bus.Label303);
   mcduDataList[1].push_back(m_inputs.MCDU2_Bus.Label304);
   mcduDataList[1].push_back(m_inputs.MCDU2_Bus.Label307);
   mcduDataList[1].push_back(m_inputs.MCDU2_Bus.Label350);
   mcduDataList[1].push_back(m_inputs.MCDU2_Bus.Label352);
   mcduDataList[1].push_back(m_inputs.MCDU2_Bus.Label354);
   mcduDataList[1].push_back(m_inputs.MCDU2_Bus.Label377);
  
   //clock data
   clockDataList.push_back(m_inputs.Clock_bus.Label125);
   clockDataList.push_back(m_inputs.Clock_bus.Label150);
   clockDataList.push_back(m_inputs.Clock_bus.Label260);
   clockDataList.push_back(m_inputs.Clock_bus.Label377);

   /*pack direct data here.*/
   //pack gps data
   for (int i = 0; i < C_DataGps_Size; i++)
   {
      //gps own
      setMsg(m_commoms.gps_own[i],C_P_BUS_429);
      setCab(m_commoms.gps_own[i],C_FMS_1_2);
      setBusNumber(m_commoms.gps_own[i],Bus_GpsOwn);
      setBusData(m_commoms.gps_own[i],bytes4ToInt(gpsDataList[0][i])); 
      //setBusData(m_commoms.gps_own[i],bytes4ToInt(m_inputs.Gps.Label057)); 
      m_ByteNum += 8;

      //gps opp
      setMsg(m_commoms.gps_opp[i],C_P_BUS_429);
      setCab(m_commoms.gps_opp[i],C_FMS_1_2);
      setBusNumber(m_commoms.gps_opp[i],Bus_GpsOpp);
      setBusData(m_commoms.gps_opp[i],bytes4ToInt(gpsDataList[1][i]));
      m_ByteNum += 8;

#if DEBUG_PRT
      fprintf(fmlog,"SEND P_BUS 429 %04X  %04X  %04X\n",m_commoms.gps_own[i].MSG_Header,((m_commoms.gps_own[i].MSG_Header.BusNum1)|(m_commoms.gps_own[i].MSG_Header.BusNum2<<8)),bytes4ToInt(m_commoms.gps_own[i].BUS_DATA));
      fprintf(fmlog,"SEND P_BUS 429 %04X  %04X  %04X\n",m_commoms.gps_opp[i].MSG_Header,((m_commoms.gps_opp[i].MSG_Header.BusNum1)|(m_commoms.gps_opp[i].MSG_Header.BusNum2<<8)),bytes4ToInt(m_commoms.gps_opp[i].BUS_DATA));
      fflush(fmlog);
#endif
   }
   
   for (int i = 0; i < C_DataMcdu_Size/*mcduDataList[0].size()*/ ; i++)
   {
      //mcdu own
      setMsg(m_commoms.mcdu_own[i],C_P_BUS_429);
      setCab(m_commoms.mcdu_own[i],C_FMS_1_2);
      setBusNumber(m_commoms.mcdu_own[i],Bus_McduOwn);
      setBusData(m_commoms.mcdu_own[i],bytes4ToInt(mcduDataList[0][i])); 
      m_ByteNum += 8;

      //mcdu opp
      setMsg(m_commoms.mcdu_opp[i],C_P_BUS_429);
      setCab(m_commoms.mcdu_opp[i],C_FMS_1_2);
      setBusNumber(m_commoms.mcdu_opp[i],Bus_McduOpp);
      setBusData(m_commoms.mcdu_opp[i],bytes4ToInt(mcduDataList[1][i]));
      m_ByteNum += 8;

#if DEBUG_PRT
      fprintf(fmlog,"SEND P_BUS 429 MCDU: %04X  %04X  %04X\n",m_commoms.mcdu_own[i].MSG_Header,((m_commoms.mcdu_own[i].MSG_Header.BusNum1)|(m_commoms.mcdu_own[i].MSG_Header.BusNum2<<8)),bytes4ToInt(m_commoms.mcdu_own[i].BUS_DATA));
      fprintf(fmlog,"SEND P_BUS 429 MCDU: %04X  %04X  %04X\n",m_commoms.mcdu_opp[i].MSG_Header,((m_commoms.mcdu_opp[i].MSG_Header.BusNum1)|(m_commoms.mcdu_opp[i].MSG_Header.BusNum2<<8)),bytes4ToInt(m_commoms.mcdu_opp[i].BUS_DATA));
      fflush(fmlog);

#endif
   }
   
   //pack clock data
   for (int i = 0; i < C_DataClock_Size ; i++)
   {
      setMsg(m_commoms.clock[i],C_P_BUS_429);
      setCab(m_commoms.clock[i],C_FMS_1_2);
      setBusNumber(m_commoms.clock[i],Bus_Clock);
      setBusData(m_commoms.clock[i],bytes4ToInt(clockDataList[i])); 
      m_ByteNum += 8;
#if DEBUG_PRT
      fprintf(fmlog,"SEND P_BUS 429 %04X  %04X  %04X\n",m_commoms.clock[i].MSG_Header,((m_commoms.clock[i].MSG_Header.BusNum1)|(m_commoms.clock[i].MSG_Header.BusNum2<<8)),bytes4ToInt(m_commoms.clock[i].BUS_DATA));
      fflush(fmlog);
#endif
   }

   //DME OWN, bus 43
   /*
   setMsg(m_commoms.dme_1,C_P_BUS_429);
   setCab(m_commoms.dme_1,C_FMS_1_2);
   setBusNumber(m_commoms.dme_1,43);
   setBusData(m_commoms.dme_1,bytes4ToInt(m_outputs.label_Dme1));
   m_ByteNum += 8;

   //DME OPP, bus 30
   setMsg(m_commoms.dme_2,C_P_BUS_429);
   setCab(m_commoms.dme_2,C_FMS_1_2);
   setBusNumber(m_commoms.dme_2,30);
   setBusData(m_commoms.dme_2,bytes4ToInt(m_outputs.label_Dme2));
   m_ByteNum += 8;

   //PRINTER, bus 102
   setMsg(m_commoms.print_1,C_P_BUS_429);
   setCab(m_commoms.print_1,C_FMS_1_2);
   setBusNumber(m_commoms.print_1,102);
   setBusData(m_commoms.print_1,bytes4ToInt(m_outputs.label_Dme2));   //change the data
   m_ByteNum += 8;
   */

   /*pack CM data here*/
    //ADC
   m_ByteNum += packCMDatas(m_inputsA429.in_CMIO.Adc_T.BAROC_HPa_1,m_commoms.adc_own[0],4,fmlog,Bus_ADC);
   m_ByteNum += packCMDatas(m_inputsA429.in_CMIO.Adc_E.PRESS_ALT,m_commoms.adc_own[4],7,fmlog,Bus_ADC);
   setMsg(m_commoms.adc_own[11],C_P_BUS_429);
   setCab(m_commoms.adc_own[11],C_FMS_1_2);
   setBusNumber(m_commoms.adc_own[11],45);
   setBusData(m_commoms.adc_own[11],bytes4ToInt(m_inputsA429.in_CMIO.Adc_E.ADCSOURCE|(m_inputsA429.in_CMIO.Adc_E.ADCFGVAL << 16)));
   m_ByteNum += 8;
	//m_commoms.adc_own[12].BUS_DATA = m_inputsA429.in_CMIO.Adc_E.ADCSOURCE|(m_inputsA429.in_CMIO.Adc_E.ADCFGVAL << 16);
	
   //IRS OWN
   m_ByteNum += packCMDatas(m_inputsA429.in_CMIO.Irs_Own.HYB_GS,m_commoms.irs_own[0],9,fmlog,Bus_IRS_GPS_Hybrid_Own);
   m_ByteNum += packCMDatas(m_inputsA429.in_CMIO.Irs_Own_2.PPOS_LAT,m_commoms.irs_own[9],12,fmlog,Bus_IRS_GPS_Hybrid_Own);
   m_ByteNum += packCMDatas(m_inputsA429.in_CMIO.Irs_Own_End.GPS_ALTITUDE_AMSL,m_commoms.irs_own[21],12,fmlog,Bus_IRS_GPS_Hybrid_Own);
   //IRS OPP
   m_ByteNum += packCMDatas(m_inputsA429.in_CMIO.Irs_Opp.HYB_GS,m_commoms.irs_opp[0],9,fmlog,Bus_IRS_GPS_Hybrid_Opp);
   m_ByteNum += packCMDatas(m_inputsA429.in_CMIO.Irs_Opp_2.PPOS_LAT,m_commoms.irs_opp[9],12,fmlog,Bus_IRS_GPS_Hybrid_Opp);
   m_ByteNum += packCMDatas(m_inputsA429.in_CMIO.Irs_Opp_End.GPS_ALTITUDE_AMSL,m_commoms.irs_opp[21],12,fmlog,Bus_IRS_GPS_Hybrid_Opp);
   //IRS 3
   m_ByteNum += packCMDatas(m_inputsA429.in_CMIO.Irs_3.HYB_GS,m_commoms.irs_3[0],9,fmlog,Bus_IRS_GPS_Hybrid_3);
   m_ByteNum += packCMDatas(m_inputsA429.in_CMIO.Irs_3_2.PPOS_LAT,m_commoms.irs_3[9],12,fmlog,Bus_IRS_GPS_Hybrid_3);
   m_ByteNum += packCMDatas(m_inputsA429.in_CMIO.Irs_3_End.GPS_ALTITUDE_AMSL,m_commoms.irs_3[21],12,fmlog,Bus_IRS_GPS_Hybrid_3);

   //FCU
   m_ByteNum += packCMDatas(m_inputsA429.in_CMIO.Fcu.SEL_HDG,m_commoms.fcu_a[0],9,fmlog,Bus_FCUA);	//FCU_A is different from FCU_B,check how to deal with this. TODO	
   setMsg(m_commoms.fcu_a[9],C_P_BUS_429);
   setCab(m_commoms.fcu_a[9],C_FMS_1_2);
   setBusNumber(m_commoms.fcu_a[9],Bus_FCUA);
   setBusData(m_commoms.fcu_a[9],bytes4ToInt(m_inputsA429.in_CMIO.Fcu.SEL_AS_VAL|(m_inputsA429.in_CMIO.Fcu.SEL_VS_VAL << 16)));
   m_ByteNum += 8;

   setMsg(m_commoms.fcu_a[10],C_P_BUS_429);
   setCab(m_commoms.fcu_a[10],C_FMS_1_2);
   setBusNumber(m_commoms.fcu_a[10],Bus_FCUA);
   setBusData(m_commoms.fcu_a[10],bytes4ToInt(m_inputsA429.in_CMIO.Fcu.FCU_FGVAL|(m_inputsA429.in_CMIO.Fcu.SEL_MACH_VAL << 16)));
   m_ByteNum += 8;

   setMsg(m_commoms.fcu_a[11],C_P_BUS_429);
   setCab(m_commoms.fcu_a[11],C_FMS_1_2);
   setBusNumber(m_commoms.fcu_a[11],Bus_FCUA);
   setBusData(m_commoms.fcu_a[11],bytes4ToInt((m_inputsA429.in_CMIO.Fcu.SEL_FPA_VAL << 16)));
   m_ByteNum += 8;

   m_ByteNum += packCMDatas(m_inputsA429.in_CMIO.Fcu.SEL_FPA,m_commoms.fcu_a[12],2,fmlog,Bus_FCUA);
   //FCU B
   m_ByteNum += packCMDatas(m_inputsA429.in_CMIO.Fcu.SEL_HDG,m_commoms.fcu_b[0],14,fmlog,Bus_FCUB);

   //FAC
   m_ByteNum += packCMDatas(m_inputsA429.in_CMIO.Fac.VMAXOP,m_commoms.fac[0],15,fmlog,Bus_FAC);        //TODO: has 2 UINT16 variables, check if it is send right.
   setMsg(m_commoms.fac[12],C_P_BUS_429);
   setCab(m_commoms.fac[12],C_FMS_1_2);
   setBusNumber(m_commoms.fac[12],Bus_FAC);
   setBusData(m_commoms.fac[12],bytes4ToInt(m_inputsA429.in_CMIO.Fac.FAC_OPP_SELECT|(m_inputsA429.in_CMIO.Fac.FAC_FG_VAL << 16)));
   //m_ByteNum += 8;
	
   //VOR own
   m_ByteNum += packCMDatas(m_inputsA429.in_CMIO.Vor.VOR_OWN_BRG,m_commoms.vor_own[0],4,fmlog,Bus_VOROwn); 
   m_ByteNum += packCMDatas(m_inputsA429.in_CMIO.Vor.VOR_OWN_SELCRS,m_commoms.vor_own[4],1,fmlog,Bus_VOROwn); 	
   setMsg(m_commoms.vor_own[5],C_P_BUS_429);
   setCab(m_commoms.vor_own[5],C_FMS_1_2);
   setBusNumber(m_commoms.vor_own[5],Bus_VOROwn);
   setBusData(m_commoms.vor_own[5],bytes4ToInt((m_inputsA429.in_CMIO.Vor.VOR_OWN_FGVAL<<16)|m_inputsA429.in_CMIO.Vor.VOR_OWN_BRG_NCD));
   m_ByteNum += 8;	
   //VOR opp
   m_ByteNum += packCMDatas(m_inputsA429.in_CMIO.Vor.VOR_OPP_BRG,m_commoms.vor_opp[0],5,fmlog,Bus_VOROpp);

   //200 adcfgval 
   //100 adc3sel 

   //P-X-END-FRAME
   packMsg(m_commoms.m_x_end,3,0,0,1,0,0);
   m_ByteNum += 4;    
   m_MsgNum = (m_ByteNum + 4)/8;


#if DEBUG_PRT
   fprintf(fmlog,"SEND P_BUS 429 %04X  %04X  %04X\n",m_commoms.adc_own[11].MSG_Header,((m_commoms.adc_own[11].MSG_Header.BusNum1)|(m_commoms.adc_own[11].MSG_Header.BusNum2<<8)),bytes4ToInt(m_commoms.adc_own[11].BUS_DATA));
   fprintf(fmlog,"SEND P_BUS 429 %04X  %04X  %04X\n",m_commoms.fcu_a[9].MSG_Header,((m_commoms.fcu_a[9].MSG_Header.BusNum1)|(m_commoms.fcu_a[9].MSG_Header.BusNum2<<8)),bytes4ToInt(m_commoms.fcu_a[9].BUS_DATA));
   fprintf(fmlog,"SEND P_BUS 429 %04X  %04X  %04X\n",m_commoms.fcu_a[10].MSG_Header,((m_commoms.fcu_a[10].MSG_Header.BusNum1)|(m_commoms.fcu_a[10].MSG_Header.BusNum2<<8)),bytes4ToInt(m_commoms.fcu_a[10].BUS_DATA));
   fprintf(fmlog,"SEND P_BUS 429 %04X  %04X  %04X\n",m_commoms.fcu_a[11].MSG_Header,((m_commoms.fcu_a[11].MSG_Header.BusNum1)|(m_commoms.fcu_a[11].MSG_Header.BusNum2<<8)),bytes4ToInt(m_commoms.fcu_a[11].BUS_DATA));
   fprintf(fmlog,"SEND P_BUS 429 %04X  %04X  %04X\n",m_commoms.fac[12].MSG_Header,((m_commoms.fac[12].MSG_Header.BusNum1)|(m_commoms.fac[12].MSG_Header.BusNum2<<8)),bytes4ToInt(m_commoms.fac[12].BUS_DATA));
   fprintf(fmlog,"SEND P_BUS 429 %04X  %04X  %04X\n",m_commoms.vor_own[5].MSG_Header,((m_commoms.vor_own[5].MSG_Header.BusNum1)|(m_commoms.vor_own[5].MSG_Header.BusNum2<<8)),bytes4ToInt(m_commoms.vor_own[5].BUS_DATA));
   fprintf(fmlog,"SEND P_XEN %04X\n",m_commoms.m_x_end);
   fflush(fmlog);
#endif
   //pack Message Buffer Header
   m_ByteNum += 8;
   m_WordNum = (m_ByteNum - 4)/4;   //26
   m_WordNum = bytes2ToInt(m_WordNum); //0x2600
   m_MsgNum  = bytes2ToInt(m_MsgNum);
   packMsgBufHeader(m_commoms.m_msgBufHeader,m_ByteNum,m_WordNum,m_MsgNum);  //to be modified  

   fclose(fmlog);
   return;
}

void FM::packInitialDatas2(Common_Data2& m_commoms2)
{
   /*BUS_DATA should be modified later.subscribe from VDN and may need reverse byte order. shijj*/
   CoreSim::Osal::Int16 m_ByteNum = 0;
   CoreSim::Osal::Int16 m_WordNum = 0;
   CoreSim::Osal::Int16 m_MsgNum  = 0;
   if((fmlog = fopen("fmlog.txt","a+")) == NULL)
   {
      std::wcout << "Failed to open fmlog.txt! packInitialDatas2" <<std::endl;
      return;
   }
   memset(&m_commoms2,0,sizeof(m_commoms2));
     
   m_ByteNum = packDisData(m_commoms2,fmlog);
   //reverseLabel();
   //FG/FM PROTOCOL
   //m_ByteNum += packCMDatas(in_FG2FM.MEM_BUSY,m_commoms2.m_bus_100[0],25,fm,In_FGFM_Protocol);
	//FQI
	m_ByteNum += packCMDatas(m_inputsA429.in_CMIO.Fqi_T.FUEL_QUANT,m_commoms2.fqi_Top,1,fmlog,Bus_FQI);        //TODO: has 2 UINT16 variables, check if it is send right.
	m_ByteNum += packCMDatas(m_inputsA429.in_CMIO.Fqi_E.FUEL_QTY_left_outer_tank,m_commoms2.fqi_End[0],12,fmlog,Bus_FQI);

	//FADEC
	m_ByteNum += packCMDatas(m_inputsA429.in_CMIO.FadecOwn.ENG_SN_1,m_commoms2.fadec_1[0],11,fmlog,Bus_FadecOwn);	
	m_ByteNum += packCMDatas(m_inputsA429.in_CMIO.FadecOpp.N1_EPR_LIMIT,m_commoms2.fadec_2[0],7,fmlog,Bus_FadecOpp);	

   //P-X-END-FRAME
   packMsg(m_commoms2.m_x_end,3,0,0,1,0,0);
   m_ByteNum += 4;    
   m_MsgNum = (m_ByteNum + 4)/8;

#if DEBUG_PRT
   fprintf(fmlog,"SEND P_XEN %04X\n",m_commoms2.m_x_end);
   fflush(fmlog);
#endif

   //pack Message Buffer Header
   m_ByteNum += 8;
   m_WordNum = (m_ByteNum - 4)/4;   //26
   m_WordNum = bytes2ToInt(m_WordNum); //0x2600
   m_MsgNum  = bytes2ToInt(m_MsgNum);
   packMsgBufHeader(m_commoms2.m_msgBufHeader,m_ByteNum,m_WordNum,m_MsgNum);  //to be modified  

   //std::cout << "Send  Initial Msg:"<< m_ByteNum << std::endl;   
   fclose(fmlog);
   return;
}

/*
void FM::packFPSaveData(FP_SAVE_Data& m_fpsave,CoreSim::Osal::String filename)
{
   if((fmlog = fopen("fmlog.txt","a+")) == NULL)
   {
      std::wcout << "Failed to open fmlog.txt! packDisDatas" <<std::endl;
      return;
   }
   memset(&m_fpsave,0,sizeof(m_fpsave));

   //P_POWER message		  		  
   setMsg(m_fpsave.p_fpsave,C_P_FP_SAVE); 
   setCab(m_fpsave.p_fpsave,C_FMS_1_2); 
   filename.copy(m_fpsave.p_fpsave.DATA,sizeof(filename),0);    
   CoreSim::Osal::UInt32 num =  ceil(sizeof(m_fpsave.p_fpsave.DATA)/32);
   setNumWords(m_fpsave.p_fpsave,128);
   string str = filename;
   num = sizeof(str);
   num = str.size();
   //P-X-END-FRAME
   setMsg(m_fpsave.m_x_end,C_P_X_END_FRAME);

   fprintf(fmlog,"SEND P_FPS  %04X  %04X  \n",m_fpsave.p_fpsave.MSG_Header,((m_fpsave.p_fpsave.MSG_Header.BusNum1)|(m_fpsave.p_fpsave.MSG_Header.BusNum2<<8)));
   int k = 0;
   while(m_fpsave.p_fpsave.DATA[k] != '\0')
   {
      fprintf(fmlog,"%c",m_fpsave.p_fpsave.DATA[k]);
      //fflush(fm);
      k++;
   }
   fprintf(fmlog,"\n");
   fprintf(fmlog,"SEND P_XEN %04X\n",m_fpsave.m_x_end);
   fflush(fmlog);
   
   packMsgBufHeader(m_fpsave.m_msgBufHeader,(128+2+1)*4,bytes2ToInt(128+1+1),bytes2ToInt(2));  //to be modified 

	fclose(fmlog);
	return;
};
*/

void FM::packPwrOffData(Pwr_Data& m_pwrData,CoreSim::Osal::UChar m_CAB)
{
   if((fmlog = fopen("fmlog.txt","a+")) == NULL)
   {
      std::wcout << "Failed to open fmlog.txt! packDisDatas" <<std::endl;
      return;
   }
   memset(&m_pwrData,0,sizeof(m_pwrData));

   //P_POWER message		  		  
   setMsg(m_pwrData.p_power,C_P_POWER); 
   setCab(m_pwrData.p_power,m_CAB);//C_FMS_1
   setBusNumber(m_pwrData.p_power,(PWR_TRS|(DATALOADER<<1)|(GRND_CND<<2)));
   setBusData(m_pwrData.p_power,bytes4ToInt(PWR_OFF));

   //P-X-END-FRAME
   setMsg(m_pwrData.m_x_end,C_P_X_END_FRAME);

   fprintf(fmlog,"SEND P_PWR  %04X  %04X  %04X\n",m_pwrData.p_power.MSG_Header,((m_pwrData.p_power.MSG_Header.BusNum1)|(m_pwrData.p_power.MSG_Header.BusNum2<<8)),bytes4ToInt(m_pwrData.p_power.BUS_DATA));
   fprintf(fmlog,"SEND P_XEN %04X\n",m_pwrData.m_x_end);
   fflush(fmlog);
   
   packMsgBufHeader(m_pwrData.m_msgBufHeader,20,bytes2ToInt(16/4),bytes2ToInt(2));  //to be modified 

	fclose(fmlog);
	return;
};

void FM::packPwrOnData(Pwr_Data& m_pwrData,CoreSim::Osal::UChar m_CAB)
{
   if((fmlog = fopen("fmlog.txt","a+")) == NULL)
   {
      std::wcout << "Failed to open fmlog.txt! packDisDatas" <<std::endl;
      return;
   }
   memset(&m_pwrData,0,sizeof(m_pwrData));

   //P_POWER message		  		  
   setMsg(m_pwrData.p_power,C_P_POWER); 
   setCab(m_pwrData.p_power,m_CAB);//C_FMS_1
   setBusNumber(m_pwrData.p_power,(PWR_TRS|(DATALOADER<<1)|(GRND_CND<<2)));
   setBusData(m_pwrData.p_power,bytes4ToInt(PWR_ON));
   
   //P-X-END-FRAME
   setMsg(m_pwrData.m_x_end,C_P_X_END_FRAME);

   fprintf(fmlog,"SEND P_PWR  %04X  %04X  %04X\n",m_pwrData.p_power.MSG_Header,((m_pwrData.p_power.MSG_Header.BusNum1)|(m_pwrData.p_power.MSG_Header.BusNum2<<8)),bytes4ToInt(m_pwrData.p_power.BUS_DATA));
   fflush(fmlog);
   fprintf(fmlog,"SEND P_XEN %04X\n",m_pwrData.m_x_end);   

   packMsgBufHeader(m_pwrData.m_msgBufHeader,20,bytes2ToInt(16/4),bytes2ToInt(2));  //to be modified 

   fclose(fmlog);
   return;
};

void FM::packDisDatas(Dis_Data& m_disData)
{
   CoreSim::Osal::UInt16 m_ByteNum = 0;
   CoreSim::Osal::UInt16 m_WordNum = 0;
   CoreSim::Osal::UInt16 m_MsgNum  = 0;
   if((fmlog = fopen("fmlog.txt","a+")) == NULL)
   {
      std::wcout << "Failed to open fmlog.txt! packDisDatas" <<std::endl;
      return;
   }
   memset(&m_disData,0,sizeof(m_disData));

   //Message Buffer Header
   //packMsgBufHeader(m_commoms.m_msgBufHeader,0x9c,0x2600,0x1300);  //to be modified 
   //m_ByteNum = packDisData(m_disData,fm);

#if 0
   //Bus 19(0x13),Label 312-FMS1
   setMsg(m_disData.m_bus_19_1,C_P_BUS_429); 
   setCab(m_disData.m_bus_19_1,C_FMS_1);
   setBusNumber(m_disData.m_bus_19_1,19);
   setBusData(m_disData.m_bus_19_1,bytes4ToInt(0x61000053));  
   m_ByteNum += 8;
   m_MsgNum  += 1;
   fprintf(fm,"SEND P_BUS 429 %04X  %04X  %04X\n",m_disData.m_bus_19_1.MSG_Header,((m_disData.m_bus_19_1.MSG_Header.BusNum1)|(m_disData.m_bus_19_1.MSG_Header.BusNum2<<8)),bytes4ToInt(m_disData.m_bus_19_1.BUS_DATA));
   fflush(fm);

   //Bus 19(0x13),Label 312-FMS2
   setMsg(m_disData.m_bus_19_2,C_P_BUS_429); 
   setCab(m_disData.m_bus_19_2,C_FMS_2);
   setBusNumber(m_disData.m_bus_19_2,19);
   setBusData(m_disData.m_bus_19_2,bytes4ToInt(0x61000053));
   m_ByteNum += 8;
   m_MsgNum  += 1;
   fprintf(fm,"SEND P_BUS 429 %04X  %04X  %04X\n",m_disData.m_bus_19_2.MSG_Header,((m_disData.m_bus_19_2.MSG_Header.BusNum1)|(m_disData.m_bus_19_2.MSG_Header.BusNum2<<8)),bytes4ToInt(m_disData.m_bus_19_2.BUS_DATA));
   fflush(fm);
#endif

   //DIS words-Discrete word0.Bus 66(0x42)-FMS1/2
   setMsg(m_disData.m_bus_66,C_P_BUS_DIS); 
   setCab(m_disData.m_bus_66,C_FMS_1_2);
   setBusNumber(m_disData.m_bus_66,Bus_DIS_WD_0);
#if FM_DEBUG
   setBusData(m_disData.m_bus_66,bytes4ToInt(0x00b489d0)); //  //0x88762200
#endif
#if FG_DEBUG
   setBusData(m_disData.m_bus_66,bytes4ToInt(di_word0_bus66));   //modified in integrate stadge
#endif
   m_ByteNum += 8;
   m_MsgNum  += 1;
     
   //DIS words-Discrete word1.Bus 68(0x44)-FMS1/2
   setMsg(m_disData.m_bus_68,C_P_BUS_DIS); 
   setCab(m_disData.m_bus_68,C_FMS_1_2);
   setBusNumber(m_disData.m_bus_68,Bus_DIS_WD_1);
#if FM_DEBUG
   setBusData(m_disData.m_bus_68,bytes4ToInt(0x00000400));   //0x000004C5
#endif
#if FG_DEBUG
   setBusData(m_disData.m_bus_68,bytes4ToInt(di_word1_bus68));   //modified in integrate stadge
#endif
   m_ByteNum += 8;
   m_MsgNum  += 1;  

   //DIS words-cfm-engine.Bus 464(0x1d0)-FMS1/2
   setMsg(m_disData.m_bus_464,C_P_BUS_DIS); 
   setCab(m_disData.m_bus_464,C_FMS_1_2);
   setBusNumber(m_disData.m_bus_464,Bus_CFM_ENG);
#if FM_DEBUG
   setBusData(m_disData.m_bus_464,bytes4ToInt(0x000000FF));//0x00000000
#endif
#if FG_DEBUG
   setBusData(m_disData.m_bus_464,bytes4ToInt(cfm_engine));  
#endif
   m_ByteNum += 8;
   m_MsgNum  += 1;  

   //DIS words-iae-engine.Bus 465(0x1d1)-FMS1/2
   setMsg(m_disData.m_bus_465,C_P_BUS_DIS); 
   setCab(m_disData.m_bus_465,C_FMS_1_2);
   setBusNumber(m_disData.m_bus_465,Bus_IAE_ENG);
#if FM_DEBUG
   setBusData(m_disData.m_bus_465,bytes4ToInt(0x00000000)); //0x000000FF
#endif
#if FG_DEBUG
   setBusData(m_disData.m_bus_465,bytes4ToInt(iae_engine)); 
#endif
   m_ByteNum += 8;
   m_MsgNum  += 1;  

   //DIS words-pw-engine.Bus 473(0x1d9)-FMS1/2
   setMsg(m_disData.m_bus_473,C_P_BUS_DIS); 
   setCab(m_disData.m_bus_473,C_FMS_1_2);
   setBusNumber(m_disData.m_bus_473,Bus_PW_ENG);
#if FM_DEBUG
   setBusData(m_disData.m_bus_473,bytes4ToInt(0x00000000));
#endif
#if FG_DEBUG
   setBusData(m_disData.m_bus_473,bytes4ToInt(pw_engine));
#endif
   m_ByteNum += 8;
   m_MsgNum  += 1;  
   
   //DIS words-Program Pins.Bus 461(0x1cd)-FMS1/2
   setMsg(m_disData.m_bus_461,C_P_BUS_DIS); 
   setCab(m_disData.m_bus_461,C_FMS_1_2);
   setBusNumber(m_disData.m_bus_461,Bus_AL_PINS);
#if FM_DEBUG
   setBusData(m_disData.m_bus_461,bytes4ToInt(0x00000018)); //0x0862
#endif
#if FG_DEBUG
   setBusData(m_disData.m_bus_461,bytes4ToInt(ac_Pins_Dis));   //modified in integrate stadge
#endif
   m_ByteNum += 8;
   m_MsgNum  += 1;   
 
   //DIS words-Program Pins1.Bus 462(0x1ce)-FMS1/2
   setMsg(m_disData.m_bus_462,C_P_BUS_DIS); 
   setCab(m_disData.m_bus_462,C_FMS_1_2);
   setBusNumber(m_disData.m_bus_462,Bus_AL_PIN1);
#if FM_DEBUG
   setBusData(m_disData.m_bus_462,bytes4ToInt(0x00000000));  //0x3000
#endif
#if FG_DEBUG
   setBusData(m_disData.m_bus_462,bytes4ToInt(al_Pins_Dis1));   //modified in integrate stadge
#endif
   m_ByteNum += 8;
   m_MsgNum  += 1;  
  
   //DIS words-Program Pins 2.Bus 463(0x1cf)-FMS1/2
   setMsg(m_disData.m_bus_463,C_P_BUS_DIS); 
   setCab(m_disData.m_bus_463,C_FMS_1_2);
   setBusNumber(m_disData.m_bus_463,Bus_AL_PIN2);
#if FM_DEBUG
   setBusData(m_disData.m_bus_463,bytes4ToInt(0x00000000));    //0x0020
#endif
#if FG_DEBUG
   setBusData(m_disData.m_bus_463,bytes4ToInt(al_Pins_Dis2));   //modified in integrate stadge
#endif
   m_ByteNum += 8;
   m_MsgNum  += 1;   

   //DIS words-code comp1.Bus 466(0x1d2)-FMS1/2
   setMsg(m_disData.m_bus_466,C_P_BUS_DIS); 
   setCab(m_disData.m_bus_466,C_FMS_1_2);
   setBusNumber(m_disData.m_bus_466,Bus_CODE_CMP_WD1);
   //setBusData(m_disData.m_bus_466,bytes4ToInt(0x00004141));  
   setBusData(m_disData.m_bus_466,bytes4ToInt(code_cmp_1));     //modify later 20190508
   m_ByteNum += 8;
   m_MsgNum  += 1; 
   
   //DIS words-code comp2.Bus 467(0x1d3)-FMS1/2
   setMsg(m_disData.m_bus_467,C_P_BUS_DIS); 
   setCab(m_disData.m_bus_467,C_FMS_1_2);
   setBusNumber(m_disData.m_bus_467,Bus_CODE_CMP_WD2);
   //setBusData(m_disData.m_bus_467,bytes4ToInt(0x00004141)); 
   setBusData(m_disData.m_bus_467,bytes4ToInt(code_cmp_2));
   m_ByteNum += 8;
   m_MsgNum  += 1;
   
   //DIS words-code comp3.Bus 468(0x1d4)-FMS1/2
   setMsg(m_disData.m_bus_468,C_P_BUS_DIS); 
   setCab(m_disData.m_bus_468,C_FMS_1_2);
   setBusNumber(m_disData.m_bus_468,Bus_CODE_CMP_WD3);
   //setBusData(m_disData.m_bus_468,bytes4ToInt(0x00004141));  
   setBusData(m_disData.m_bus_468,bytes4ToInt(code_cmp_3));
   m_ByteNum += 8;
   m_MsgNum  += 1; 

   //DIS words-code comp4.Bus 469(0x1d5)-FMS1/2
   setMsg(m_disData.m_bus_469,C_P_BUS_DIS); 
   setCab(m_disData.m_bus_469,C_FMS_1_2);
   setBusNumber(m_disData.m_bus_469,Bus_CODE_CMP_WD4);
   //setBusData(m_disData.m_bus_469,bytes4ToInt(0x00004141));
   setBusData(m_disData.m_bus_469,bytes4ToInt(code_cmp_4));
   m_ByteNum += 8;
   m_MsgNum  += 1; 
   
   //DIS words-code comp5.Bus 470(0x1d6)-FMS1/2
   setMsg(m_disData.m_bus_470,C_P_BUS_DIS); 
   setCab(m_disData.m_bus_470,C_FMS_1_2);
   setBusNumber(m_disData.m_bus_470,Bus_CODE_CMP_WD5);
   //setBusData(m_disData.m_bus_470,bytes4ToInt(0x00004141));  
   setBusData(m_disData.m_bus_470,bytes4ToInt(code_cmp_5));  
   m_ByteNum += 8;
   m_MsgNum  += 1;  
   
   //DIS words-code comp6.Bus 471(0x1d7)-FMS1/2
   setMsg(m_disData.m_bus_471,C_P_BUS_DIS); 
   setCab(m_disData.m_bus_471,C_FMS_1_2);
   setBusNumber(m_disData.m_bus_471,Bus_CODE_CMP_WD6);
   //setBusData(m_disData.m_bus_471,bytes4ToInt(0x00004141));  
   setBusData(m_disData.m_bus_471,bytes4ToInt(code_cmp_6));
   m_ByteNum += 8;
   m_MsgNum  += 1;  

   //429 words.Bus 100(0x64),fg->fm Label 260 fg_healthy -FMS1/2
   setMsg(m_disData.m_bus_100,C_P_BUS_429); 
   setCab(m_disData.m_bus_100,C_FMS_1_2);
   setBusNumber(m_disData.m_bus_100,100);
   setBusData(m_disData.m_bus_100,bytes4ToInt(0x0003FD0D));    //TODO: change the value in integrated stadge after FG_Healthy works normal
   //setBusData(m_disData.m_bus_100,bytes4ToInt(fg_hlty));     //modify later 20190508
   m_ByteNum += 8;
   m_MsgNum  += 1;  

   //429 words.Bus 108(0x6c)  
   setMsg(m_disData.m_bus_108,C_P_BUS_429); 
   setCab(m_disData.m_bus_108,C_FMS_1_2);
   setBusNumber(m_disData.m_bus_108,108);
//   setBusData(m_disData.m_bus_108,bytes4ToInt(0x60000415));
   m_ByteNum += 8;
   m_MsgNum  += 1;

   //P-X-END-FRAME
   setMsg(m_disData.m_x_end,C_P_X_END_FRAME);
   m_ByteNum += 4;    //156
   m_MsgNum  += 1;

#if DEBUG_PRT
   fprintf(fmlog,"SEND P_BUS DIS %04X  %04X  %04X\n",m_disData.m_bus_66.MSG_Header,((m_disData.m_bus_66.MSG_Header.BusNum1)|(m_disData.m_bus_66.MSG_Header.BusNum2<<8)),bytes4ToInt(m_disData.m_bus_66.BUS_DATA));
   fprintf(fmlog,"SEND P_BUS DIS %04X  %04X  %04X\n",m_disData.m_bus_68.MSG_Header,((m_disData.m_bus_68.MSG_Header.BusNum1)|(m_disData.m_bus_68.MSG_Header.BusNum2<<8)),bytes4ToInt(m_disData.m_bus_68.BUS_DATA));
   fprintf(fmlog,"SEND P_BUS DIS %04X  %04X  %04X\n",m_disData.m_bus_464.MSG_Header,((m_disData.m_bus_464.MSG_Header.BusNum1)|(m_disData.m_bus_464.MSG_Header.BusNum2<<8)),bytes4ToInt(m_disData.m_bus_464.BUS_DATA));
   fprintf(fmlog,"SEND P_BUS DIS %04X  %04X  %04X\n",m_disData.m_bus_465.MSG_Header,((m_disData.m_bus_465.MSG_Header.BusNum1)|(m_disData.m_bus_465.MSG_Header.BusNum2<<8)),bytes4ToInt(m_disData.m_bus_465.BUS_DATA));
   fprintf(fmlog,"SEND P_BUS DIS %04X  %04X  %04X\n",m_disData.m_bus_473.MSG_Header,((m_disData.m_bus_473.MSG_Header.BusNum1)|(m_disData.m_bus_473.MSG_Header.BusNum2<<8)),bytes4ToInt(m_disData.m_bus_473.BUS_DATA));
   fprintf(fmlog,"SEND P_BUS DIS %04X  %04X  %04X\n",m_disData.m_bus_461.MSG_Header,((m_disData.m_bus_461.MSG_Header.BusNum1)|(m_disData.m_bus_461.MSG_Header.BusNum2<<8)),bytes4ToInt(m_disData.m_bus_461.BUS_DATA));
   fprintf(fmlog,"SEND P_BUS DIS %04X  %04X  %04X\n",m_disData.m_bus_463.MSG_Header,((m_disData.m_bus_463.MSG_Header.BusNum1)|(m_disData.m_bus_463.MSG_Header.BusNum2<<8)),bytes4ToInt(m_disData.m_bus_463.BUS_DATA));
   fprintf(fmlog,"SEND P_BUS DIS %04X  %04X  %04X\n",m_disData.m_bus_462.MSG_Header,((m_disData.m_bus_462.MSG_Header.BusNum1)|(m_disData.m_bus_462.MSG_Header.BusNum2<<8)),bytes4ToInt(m_disData.m_bus_462.BUS_DATA));
   fprintf(fmlog,"SEND P_BUS DIS %04X  %04X  %04X\n",m_disData.m_bus_466.MSG_Header,((m_disData.m_bus_466.MSG_Header.BusNum1)|(m_disData.m_bus_466.MSG_Header.BusNum2<<8)),bytes4ToInt(m_disData.m_bus_466.BUS_DATA));
   fprintf(fmlog,"SEND P_BUS DIS %04X  %04X  %04X\n",m_disData.m_bus_467.MSG_Header,((m_disData.m_bus_467.MSG_Header.BusNum1)|(m_disData.m_bus_467.MSG_Header.BusNum2<<8)),bytes4ToInt(m_disData.m_bus_467.BUS_DATA));
   fprintf(fmlog,"SEND P_BUS DIS %04X  %04X  %04X\n",m_disData.m_bus_468.MSG_Header,((m_disData.m_bus_468.MSG_Header.BusNum1)|(m_disData.m_bus_468.MSG_Header.BusNum2<<8)),bytes4ToInt(m_disData.m_bus_468.BUS_DATA));
   fprintf(fmlog,"SEND P_BUS DIS %04X  %04X  %04X\n",m_disData.m_bus_469.MSG_Header,((m_disData.m_bus_469.MSG_Header.BusNum1)|(m_disData.m_bus_469.MSG_Header.BusNum2<<8)),bytes4ToInt(m_disData.m_bus_469.BUS_DATA));
   fprintf(fmlog,"SEND P_BUS DIS %04X  %04X  %04X\n",m_disData.m_bus_470.MSG_Header,((m_disData.m_bus_470.MSG_Header.BusNum1)|(m_disData.m_bus_470.MSG_Header.BusNum2<<8)),bytes4ToInt(m_disData.m_bus_470.BUS_DATA));
   fprintf(fmlog,"SEND P_BUS DIS %04X  %04X  %04X\n",m_disData.m_bus_471.MSG_Header,((m_disData.m_bus_471.MSG_Header.BusNum1)|(m_disData.m_bus_471.MSG_Header.BusNum2<<8)),bytes4ToInt(m_disData.m_bus_471.BUS_DATA));
   fprintf(fmlog,"SEND P_BUS 429 %04X  %04X  %04X\n",m_disData.m_bus_100.MSG_Header,((m_disData.m_bus_100.MSG_Header.BusNum1)|(m_disData.m_bus_100.MSG_Header.BusNum2<<8)),bytes4ToInt(m_disData.m_bus_100.BUS_DATA));
   fprintf(fmlog,"SEND P_BUS 429 %04X  %04X  %04X\n",m_disData.m_bus_108.MSG_Header,((m_disData.m_bus_108.MSG_Header.BusNum1)|(m_disData.m_bus_108.MSG_Header.BusNum2<<8)),bytes4ToInt(m_disData.m_bus_108.BUS_DATA));
   fprintf(fmlog,"SEND P_XEN %04X\n",m_disData.m_x_end);
   fflush(fmlog);
#endif
   //pack Message Buffer Header
   m_ByteNum += 8;
   m_WordNum = (m_ByteNum - 4)/4 ;   //
   m_WordNum = bytes2ToInt(m_WordNum);    //((m_WordNum&0XFF)<<8)|((m_WordNum>>8)&0XFF)    (m_WordNum<<8)&0xff00; //0x2600
   m_MsgNum  = bytes2ToInt(m_MsgNum);
   packMsgBufHeader(m_disData.m_msgBufHeader,m_ByteNum,m_WordNum,m_MsgNum);  //to be modified 

   //std::cout << "Send  Dis Msg:"<< m_ByteNum << std::endl;   
   fclose(fmlog);
   return;
}

int FM::RecvDataFromFM(FG* fg)
{
   CoreSim::Osal::Int32 bytes_Received = 0;
   CoreSim::Osal::Int32 dataLen        = 0;
   CoreSim::Osal::Int32 dataType       = 0;
   CoreSim::Osal::Int32 num_msgs       = 0;
   char *p                             = NULL;
   //RecvData *dataBuf                   = NULL;
   RecvDataMsg *dataMsg                = NULL;

   do
   {
      /*first receive 8 bytes.The first 8 bytes describe data length to be received.*/
      memset(&m_RevBufSize,0,sizeof(m_RevBufSize));
      bytes_Received = m_txTcpSocket.recv(&m_RevBufSize,sizeof(m_RevBufSize));
      if(bytes_Received < 0)
      {
         std::cout << "Receive error from RecvDataFromFM!" << bytes_Received << std::endl;
      }
      /*Message buffers sized from 8 to 16384.*/
      /*The NUM_BYTES word can be transmitted as a big or little endian byte stream (MSB first, or MSB last). 
      It is the responsibility of the receiver of this data to examine and adjust the endian.*/
      dataLen = bytes4ToInt(m_RevBufSize.NUM_BYTES);
      if ((dataLen > 16384) || ( dataLen < 8))
      {
          dataLen = m_RevBufSize.NUM_BYTES;
      }
      if ((dataLen < 8) || (dataLen > 16384))
      {
         std::cout << "Error: Receive msg length error! Length: " << dataLen << std::endl;
         fprintf(fmlog,"MSG :%X\n",m_RevBufSize,sizeof(m_RevBufSize));
         return 0;
      }
      num_msgs  = bytes2ToInt(m_RevBufSize.NUM_MSGS);

      //the structure to save the received datas
      /*
      dataBuf = (RecvData *)malloc(dataLen);
      memset(dataBuf,0,sizeof(dataBuf));
      memcpy((char *)(&dataBuf->m_RecMsgBufHeader),&m_RevBufSize,sizeof(m_RevBufSize)); 
      */
         
      //Dynamic allocate spaces according to the first 8 bytes.
      if (dataLen > 8)
      {     
         dataMsg =  (RecvDataMsg *)malloc(dataLen - 8);
         memset(dataMsg,0,sizeof(dataMsg));

         p = new char[dataLen - 8];
         memset(p,0,dataLen-8);
         if( m_txTcpSocket.recv(p,dataLen-8) > 0)
         {
            memcpy((char *)(&dataMsg->m_RecMsgHeader),p,dataLen-8);
         }
         else
         {
            std::cout << "Abnormal Receive! " <<"\n";
            return 0;
         }         
      }
        
      dataType = unpackDatas(dataMsg,num_msgs,fg);
#if 0
	  //for test only
	  if((ftest = fopen("test.txt","a+")) == NULL)
	  {
		  std::wcout << "Failed to open test.txt! RecvDataFromFM" <<std::endl;
		  return -1;
	  }
      int k=0;
      CoreSim::Osal::UInt16 *p = NULL;
      p = (CoreSim::Osal::UInt16 *)&m_comMemoryFMout;
      fprintf(ftest,"RECV: "); 
      while ( k!= sizeof(m_comMemoryFMout)/2 )
      {
         fprintf(ftest,"%04x",*p);       
         k++;
         p++;
         //fflush(ftest);
      }
      fprintf(ftest,"\n");                              
      //fflush(ftest); 
      fclose(ftest);
      //end of test
#endif
   } while (dataType  != C_P_END_FRAME);

   if (dataType == C_P_END_FRAME && m_outputs.IsInitial == false)
   {
      std::cout << "End of Initial Connection! " <<"\n";
      m_outputs.IsInitial = true;
   }

   
   delete p;
   //delete dataBuf;
   delete dataMsg;
   return dataType;
}

//unpack data according to Data formats defined in FMS2_ICD.pdf
int FM::unpackDatas(RecvDataMsg *dataBuf,CoreSim::Osal::Int32 num_msgs,FG* fg)
{
   CoreSim::Osal::Int32 msg_type   = 0;   
   CoreSim::Osal::Int32 num_words  = 0;   
   CoreSim::Osal::Int32 bus_number = 0;
   CoreSim::Osal::Int32 index      = 0;
   CommMemory m_comMemoryFMout = {0};
   if((fmlog = fopen("fmlog.txt","a+")) == NULL)
   {
      std::wcout << "Failed to open fmlog.txt! unpackDatas" <<std::endl;
      return -1;
   }
   
   char *p_dataBuf = (char *)(&dataBuf->m_RecMsgHeader);

   for (index = 0; index < num_msgs; index++)
   {
      //CoreSim::Osal::UInt16 temp   = 0;
      CoreSim::Osal::UInt16 Label  = 0;
      int k = 0;
	  char* ptr = NULL;

      msg_type  = ((RecvDataMsg *)p_dataBuf)->m_RecMsgHeader.MSG_TYPE ;   
      num_words = (((RecvDataMsg *)p_dataBuf)->m_RecMsgHeader.NUM_WORDS1&0xff)|((((RecvDataMsg *)p_dataBuf)->m_RecMsgHeader.NUM_WORDS2&0x0f)<<8);    
      bus_number = ((RecvDataMsg *)p_dataBuf)->m_RecMsgHeader.BusNum1 | ((RecvDataMsg *)p_dataBuf)->m_RecMsgHeader.BusNum2<<8;
      //std::cout << "Receive MSG TYPE:" << msg_type << "  MSG:"<< ((RecvDataMsg *)p_dataBuf)->DATA << std::endl;  
      
      switch (msg_type)
      {
      case C_P_VERSION:         
         memcpy(&p_version,p_dataBuf,num_words*4);
         //printf P_VERSION message
         fprintf(fmlog,"RECV P_VER: %04X sbv=%04X immv=%04X\n",p_version.MSG_Header,bytes4ToInt(p_version.Version),bytes4ToInt(p_version.IMM_Version));
         fflush(fmlog);
         //printf("RECV P_VER sbv=%04X immv=%04X\n",bytes4ToInt(p_version.Version),bytes4ToInt(p_version.IMM_Version));
         break;
      
      case C_P_SIM_CLOCK:
         memcpy(&p_clock,p_dataBuf,num_words*4);
         m_outputs.RTC =  bytes4ToInt(p_clock.SIM_CLOCK);   //c_miliseconds;
         fprintf(fmlog,"RECV P_SIM: %d ms \n",static_cast <CoreSim::Osal::UInt32>(m_outputs.RTC));
         fflush(fmlog);
         //printf("RECV P_SIM: %d ms\n",bytes4ToInt(p_clock.SIM_CLOCK));
         break;

      case C_P_END_FRAME:
         memcpy(&p_end_frame,p_dataBuf,num_words*4);
         fprintf(fmlog,"RECV P_END_FRAME! \n");
         fflush(fmlog);
         //printf("RECV P_END\n");
         break;
      
      case C_P_TEXT:
         memcpy(&p_text,p_dataBuf,num_words*4);
         if((num_words < 2) || (num_words > 128))
         {
            std::cout << "P_TEST message  NUM_WORDS error!" << num_words << std::endl;   
         }
         //printf P_TEXT MESSAGE, log num_words bytes.
         fprintf(fmlog,"RECV P_TEXT:  ");         
         while(p_text.DATA[k] != '\0')
         {
            fprintf(fmlog,"%c",p_text.DATA[k]);
            fflush(fmlog);
            k++;
         }
         fprintf(fmlog,"\n");
         fflush(fmlog);
         //printf("RECV P_TEX: %s\n",p_text.DATA);
         break;
      
      case C_P_BUS_429:
         memcpy(&p_bus_429,p_dataBuf,num_words*4);
         //m_outputs.bus_Data  = p_bus_429.BUS_DATA;   //TODO: Get Label
         //temp = reverseBit8(bytes4ToInt(p_bus_429.BUS_DATA));
         //Label += ((temp&0xC0)>>6)*100;
         //Label += ((temp&0x38)>>3)*10;
         //Label += temp&0x07; 
         Label = getLabel(bytes4ToInt(p_bus_429.BUS_DATA));
		   		
         fprintf(fmlog,"RECV P_BUS 429 %04X %04X %04X\n",p_bus_429.MSG_Header,bus_number,bytes4ToInt(p_bus_429.BUS_DATA));
         fflush(fmlog);
         //printf("RECV P_BUS 429 %04X %04X %04X\n",p_bus_429.MSG_Header,bus_number,bytes4ToInt(p_bus_429.BUS_DATA));

		 /*pack the data into CommonMemory here*/
       if(bus_number == Bus_FMFG_Protoclol_O)  //Bus 114
       {   
          /*FM power up signal. Write the state into FM/FG common memory. Adress is 0xf4000002. 
          However, According to A.9 of ICD Appendix A320 on page 12, not transmitted. */
          /*if(m_locals.fm_puprdy_fm == TRUE)
          {
            ptr = (char *)&(fg->m_comMemoryIO);
            *(CoreSim::Osal::UInt16 *)ptr = 0x00ff;
          }*/

          if(Label == 344)/*According to FM/FG common memory, lable of fm_healthy is 0X27*/
          {
             fm_hlty = getValue(bytes4ToInt(p_bus_429.BUS_DATA));
             m_locals.fm_hlty_fm = (fm_hlty == 0xff) ? TRUE: FALSE;
             sdi     = getSdi(bytes4ToInt(p_bus_429.BUS_DATA));
          }
          offaddr = findAddr(bus_number, Label);	
			 if(offaddr >= 0)
			 {
				 //ptr = (char *)&m_comMemoryFMout + offaddr;
				 ptr = (char *)&(fg->m_comMemoryIO) + offaddr;   //TODO: this is done by supposing that FM outputs only occupies the output memory which is not used by FG. To Recheck it later.
				 if(Label == 344)   //fm healthy   0x0027
             {
               *(CoreSim::Osal::UInt16 *)ptr = (bytes4ToInt(p_bus_429.BUS_DATA)>>10);		
             }else
             {
               *(CoreSim::Osal::UInt16 *)ptr = (bytes4ToInt(p_bus_429.BUS_DATA)>>10);		
             }
			 }
       }

       /*pack the data into CommonMemory. FM OUTPUT: A-BUS, B-BUSS, C-BUSS, MCDU, DATA-LOADER, EFIS_1, PRINTER, ADS, FM/FG PROTOCOL*/
       if((bus_number == Bus_Fmgc_A_B_O) || (bus_number == Bus_Fmgc_C_O) || (bus_number == Bus_Mcdu_OWN_O) || (bus_number == Bus_Mcdu_OPP_O) || (bus_number == Bus_DataLoader_Sim_O) || (bus_number == Bus_Efis_1_O) || (bus_number ==Bus_Printer_Acars_O) || (bus_number == Bus_ADS_O))
		 {
			 offaddr = findAddr(bus_number, Label);	
			 if(offaddr >= 0)
			 {
				 //ptr = (char *)&m_comMemoryFMout + offaddr;
				 ptr = (char *)&(fg->m_comMemoryIO) + offaddr;   //TODO: this is done by supposing that FM outputs only occupies the output memory which is not used by FG. To Recheck it later.
				 *(CoreSim::Osal::UInt32 *)ptr = bytes4ToInt(p_bus_429.BUS_DATA);			
			 }
		 }

       //mcdu data received from FMS, send it to vdn and read by MCDU 
       if (bus_number == Bus_Mcdu_OWN_O )   
       {
          m_outputs.Mcdu_bus[0].push(reverseBit8(bytes4ToInt(p_bus_429.BUS_DATA)));
          //m_outputs.Mcdu_bus[0].push(reverseMalSal(bytes4ToInt(p_bus_429.BUS_DATA)));
          m_outputs.Mcdu.Label125.setWord(reverseMalSal(bytes4ToInt(p_bus_429.BUS_DATA)));
          m_outputs.Mcdu.Label044.setWord(reverseBit8(bytes4ToInt(p_bus_429.BUS_DATA)));
          fprintf(fmlog,"RECV P_BUS 429 Bus_Mcdu_OWN_O: %04X %04X %04X Time:%d ms\n",p_bus_429.MSG_Header,bus_number,reverseBit8(bytes4ToInt(p_bus_429.BUS_DATA)),static_cast <CoreSim::Osal::UInt32>(m_outputs.RTC));
          fflush(fmlog);
       }
       else if(bus_number == Bus_Mcdu_OPP_O)
       {
          //m_outputs.Mcdu_bus[1].push(reverseBit8(bytes4ToInt(p_bus_429.BUS_DATA)));
          m_outputs.Mcdu_bus[1].push(reverseBit8(bytes4ToInt(p_bus_429.BUS_DATA)));
          fprintf(fmlog,"RECV P_BUS 429 Bus_Mcdu_OPP_O: %04X %04X %04X Time:%d ms\n",p_bus_429.MSG_Header,bus_number,reverseBit8(bytes4ToInt(p_bus_429.BUS_DATA)),static_cast <CoreSim::Osal::UInt32>(m_outputs.RTC));
          fflush(fmlog);
       }
       else if (bus_number == Bus_DataLoader_Sim_O)   //data loader/sim
       {         
          m_outputs.Dataload.push(p_bus_429.BUS_DATA);
       }
       else if (bus_number == Bus_Efis_1_O)   //efis_1
       {
          m_outputs.Efis.push(p_bus_429.BUS_DATA);
       } 
		 else if (bus_number == Bus_Printer_Acars_O)   //printer/acars
		 {
			m_outputs.Printer.push(p_bus_429.BUS_DATA);
       } 
		 else if (bus_number == Bus_ADS_O)  //ads
		 {
			 m_outputs.Ads.push(p_bus_429.BUS_DATA);
		 }
		 else if(bus_number == Bus_Fmgc_A_B_O)	//FMGC_A/B
		 {	 
			 m_outputs.Fmgc_A[0].push(p_bus_429.BUS_DATA);
		 }
		 else if(bus_number == Bus_Fmgc_C_O)	//FMGC_C
		 {	 
			 m_outputs.Fmgc1_C.push(p_bus_429.BUS_DATA);
		 }
         break;
      
      case C_P_BUS_DIS:
         memcpy(&p_bus_dis,p_dataBuf,num_words*4); 
		   //temp = reverseBit8(bytes4ToInt(p_bus_dis.BUS_DATA));
         //Label += ((temp&0xC0)>>6)*100;
         //Label += ((temp&0x38)>>3)*10;
         //Label += temp&0x07;
         Label = getLabel(bytes4ToInt(p_bus_dis.BUS_DATA));

		   if(bus_number == 114)
		   {
			   offaddr = findAddr(bus_number, Label);	
			   if(offaddr >= 0) 
			   {
				   ptr = (char *)&m_comMemoryFMout + offaddr;
				   //ptr = (char *)&m_comMemoryIO + offaddr;   //TODO: this is done by supposing that FM outputs only occupies the output memory which is not used by FG. To Recheck it later.
				   *(CoreSim::Osal::UInt32 *)ptr = p_bus_dis.BUS_DATA;
			   }
		   }
         if(bus_number == 123)  //Crossload_Enable
         {
            CrosLoad_Flag = true;
            //((InputsDIS *)DisInFG)->RM09D_AIRLINE_9 = (p_bus_dis.BUS_DATA == 0)?true:false;
         }
         fprintf(fmlog,"RECV P_BUS DIS: %04X %04X %04X\n",p_bus_dis.MSG_Header,bus_number,bytes4ToInt(p_bus_dis.BUS_DATA));
         fflush(fmlog);
         printf("RECV P_BUS DIS: %04X %04X %04X\n",p_bus_dis.MSG_Header,bus_number,bytes4ToInt(p_bus_dis.BUS_DATA));
         break;
      
      case C_P_BUS_ANA:
         memcpy(&p_bus_ana,p_dataBuf,num_words*4);   
         fprintf(fmlog,"RECV P_BUS ANA: %04X %04X %04X\n",p_bus_ana.MSG_Header,bus_number,bytes4ToInt(p_bus_ana.BUS_DATA));
         fflush(fmlog);
         printf("RECV P_BUS ANA: %04X %04X %04X\n",p_bus_ana.MSG_Header,bus_number,bytes4ToInt(p_bus_ana.BUS_DATA));
         break;

      case C_P_FP_LOAD:
         memcpy(&p_scp_respond,p_dataBuf,num_words*4);
         //fprintf P_FD_LOAD
         fprintf(fmlog,"RECV P_FP_LOAD:  NUM_WORDS(1): %d  Status(0:Fails,1:Success!):%d\n",num_words,p_scp_respond.MSG_Header.BusNum1);
         fflush(fmlog);
         if(p_scp_respond.MSG_Header.BusNum1 == 0)
         {
            std::cout << "Flight-Plan Load Fails!" << std::endl;
         }
         else
         {
            std::cout << "Flight-Plan Load Success!" << std::endl;
         }
         break;

      case C_P_FP_SAVE:
         memcpy(&p_scp_respond,p_dataBuf,num_words*4);
         //fprintf P_FD_SAVE
         fprintf(fmlog,"RECV P_FP_SAVE:  NUM_WORDS(1): %d  Status(0:Fails,1:Success!):%d\n",num_words,p_scp_respond.MSG_Header.BusNum1);
         fflush(fmlog);
         if(p_scp_respond.MSG_Header.BusNum1 == 0)
         {
            std::cout << "Flight-Plan Save Fails!" << std::endl;
         }
         else
         {
            std::cout << "Flight-Plan Save Success!" << std::endl;
         }
         break;

      default:

         break;
      }//end of switch
  
      p_dataBuf += num_words*4;

      m_outputs.bus_Number = bus_number;
      m_outputs.Label_Number = Label;
      //std::cout << "Receive_MSG:" << msg_type << "  Bus:" << bus_number << "  Label:"<< Label <<"  MSG:"<< ((RecvDataMsg *)p_dataBuf)->DATA << std::endl; 
       

      
      //fmgc data received from FMS2,then send to VDN 
      if(m_outputs.bus_Number == Bus_Fmgc_A_B_O && p_bus_429.MSG_Header.CAB == C_FMS_1_2)
      {
         switch (m_outputs.Label_Number)
         {
         case 146:
            m_outputs.A429_Bus_Aout.Fmgc_Label146.setWord(p_bus_429.BUS_DATA);
            m_outputs.A429_Bus_Bout.Fmgc_Label146.setWord(p_bus_429.BUS_DATA);
            break;
         case 147:
            m_outputs.A429_Bus_Aout.Fmgc_Label147.setWord(p_bus_429.BUS_DATA);
            m_outputs.A429_Bus_Bout.Fmgc_Label147.setWord(p_bus_429.BUS_DATA);
            break;
         case 274:
            m_outputs.A429_Bus_Aout.Fmgc_Label274.setWord(p_bus_429.BUS_DATA);
            m_outputs.A429_Bus_Bout.Fmgc_Label274.setWord(p_bus_429.BUS_DATA);
            break;
         case 275:
            m_outputs.A429_Bus_Aout.Fmgc_Label275.setWord(p_bus_429.BUS_DATA);
            m_outputs.A429_Bus_Bout.Fmgc_Label275.setWord(p_bus_429.BUS_DATA);
            break;
         default:
            break;
          }      
      }//end of if

      if(m_outputs.bus_Number == Bus_Fmgc_A_B_O && p_bus_429.MSG_Header.CAB == C_FMS_1)
      {
         switch (m_outputs.Label_Number)
         {
         case 146:
            m_outputs.A429_Bus_Aout.Fmgc_Label146.setWord(p_bus_429.BUS_DATA);
            //m_outputs.A429_Bus_Bout.Fmgc_Label146.setWord(p_bus_429.BUS_DATA);
            break;
         case 147:
            m_outputs.A429_Bus_Aout.Fmgc_Label147.setWord(p_bus_429.BUS_DATA);
            //m_outputs.A429_Bus_Bout.Fmgc_Label147.setWord(p_bus_429.BUS_DATA);
            break;
         case 274:
            m_outputs.A429_Bus_Aout.Fmgc_Label274.setWord(p_bus_429.BUS_DATA);
            //m_outputs.A429_Bus_Bout.Fmgc_Label274.setWord(p_bus_429.BUS_DATA);
            break;
         case 275:
            m_outputs.A429_Bus_Aout.Fmgc_Label275.setWord(p_bus_429.BUS_DATA);
            //m_outputs.A429_Bus_Bout.Fmgc_Label275.setWord(p_bus_429.BUS_DATA);
            break;
         default:
            break;
          }
      }
      else if(m_outputs.bus_Number == Bus_Fmgc_A_B_O && p_bus_429.MSG_Header.CAB == C_FMS_2)
      {
         switch (m_outputs.Label_Number)
         {
         case 146:
            //m_outputs.A429_Bus_Aout.Fmgc_Label146.setWord(p_bus_429.BUS_DATA);
            m_outputs.A429_Bus_Bout.Fmgc_Label146.setWord(p_bus_429.BUS_DATA);
            break;
         case 147:
            //m_outputs.A429_Bus_Aout.Fmgc_Label147.setWord(p_bus_429.BUS_DATA);
            m_outputs.A429_Bus_Bout.Fmgc_Label147.setWord(p_bus_429.BUS_DATA);
            break;
         case 274:
            //m_outputs.A429_Bus_Aout.Fmgc_Label274.setWord(p_bus_429.BUS_DATA);
            m_outputs.A429_Bus_Bout.Fmgc_Label274.setWord(p_bus_429.BUS_DATA);
            break;
         case 275:
            //m_outputs.A429_Bus_Aout.Fmgc_Label275.setWord(p_bus_429.BUS_DATA);
            m_outputs.A429_Bus_Bout.Fmgc_Label275.setWord(p_bus_429.BUS_DATA);
            break;
         default:
            break;
          }   
      }
      if (m_outputs.bus_Number == Bus_Fmgc_A_B_O)
      {
         switch (m_outputs.Label_Number)
         {
         case 40:
            m_outputs.A429_Bus_Aout.Fmgc_Label040.setWord(p_bus_429.BUS_DATA);
            break;
         case 41:
            m_outputs.A429_Bus_Aout.Fmgc_Label041.setWord(p_bus_429.BUS_DATA);
            break;
         case 42:
            m_outputs.A429_Bus_Aout.Fmgc_Label042.setWord(p_bus_429.BUS_DATA);
            break;
         /*case 146:
            m_outputs.A429_Bus_Aout.Fmgc_Label146.setWord(p_bus_429.BUS_DATA);
            m_outputs.A429_Bus_Bout.Fmgc_Label146.setWord(p_bus_429.BUS_DATA);
            break;
         case 147:
            m_outputs.A429_Bus_Aout.Fmgc_Label147.setWord(p_bus_429.BUS_DATA);
            m_outputs.A429_Bus_Bout.Fmgc_Label147.setWord(p_bus_429.BUS_DATA);
            break;*/
         case 233:
            m_outputs.A429_Bus_Aout.Fmgc_Label233.setWord(p_bus_429.BUS_DATA);
            break;
         case 234:
            m_outputs.A429_Bus_Aout.Fmgc_Label234.setWord(p_bus_429.BUS_DATA);
            break;
         case 235:
            m_outputs.A429_Bus_Aout.Fmgc_Label235.setWord(p_bus_429.BUS_DATA);
            break;
         case 236:
            m_outputs.A429_Bus_Aout.Fmgc_Label236.setWord(p_bus_429.BUS_DATA);
            break;
         case 237:
            m_outputs.A429_Bus_Aout.Fmgc_Label237.setWord(p_bus_429.BUS_DATA);
            break;
         case 273:
            m_outputs.A429_Bus_Aout.Fmgc_Label273.setWord(p_bus_429.BUS_DATA);
            break;
         /*case 274:
            m_outputs.A429_Bus_Aout.Fmgc_Label274.setWord(p_bus_429.BUS_DATA);
            m_outputs.A429_Bus_Bout.Fmgc_Label274.setWord(p_bus_429.BUS_DATA);
            break;
         case 275:
            m_outputs.A429_Bus_Aout.Fmgc_Label275.setWord(p_bus_429.BUS_DATA);
            m_outputs.A429_Bus_Bout.Fmgc_Label275.setWord(p_bus_429.BUS_DATA);
            break;*/
         case 276:
            m_outputs.A429_Bus_Aout.Fmgc_Label276.setWord(p_bus_429.BUS_DATA);
            break;
         case 277:
            m_outputs.A429_Bus_Aout.Fmgc_Label277.setWord(p_bus_429.BUS_DATA);
            break;
         case 327:
            m_outputs.A429_Bus_Aout.Fmgc_Label327.setWord(p_bus_429.BUS_DATA);
            break;

         case 75:
            m_outputs.A429_Bus_Bout.Fmgc_Label075.setWord(p_bus_429.BUS_DATA);
            break;
         case 77:
            m_outputs.A429_Bus_Bout.Fmgc_Label077.setWord(p_bus_429.BUS_DATA);
            break;
         case 164:
            m_outputs.A429_Bus_Bout.Fmgc_Label164.setWord(p_bus_429.BUS_DATA);
            break;
         case 210:
            m_outputs.A429_Bus_Bout.Fmgc_Label210.setWord(p_bus_429.BUS_DATA);
            break;
         case 270:
            m_outputs.A429_Bus_Bout.Fmgc_Label270.setWord(p_bus_429.BUS_DATA);
            break;
         case 300:
            m_outputs.A429_Bus_Bout.Fmgc_Label300.setWord(p_bus_429.BUS_DATA);
            break;
         case 304:
            m_outputs.A429_Bus_Bout.Fmgc_Label304.setWord(p_bus_429.BUS_DATA);
            break;
         case 310:
            m_outputs.A429_Bus_Bout.Fmgc_Label310.setWord(p_bus_429.BUS_DATA);
            break;
         case 312:
            m_outputs.A429_Bus_Bout.Fmgc_Label312.setWord(p_bus_429.BUS_DATA);
            break;
         case 345:
            m_outputs.A429_Bus_Bout.Fmgc_Label345.setWord(p_bus_429.BUS_DATA);
            break;
         case 346:
            m_outputs.A429_Bus_Bout.Fmgc_Label346.setWord(p_bus_429.BUS_DATA);
            break;
         default:
            break;
         }
      }//end of if 



      //fmgc data received from FMS2,then send to VDN 
      if(m_outputs.bus_Number == Bus_Fmgc_C_O && p_bus_429.MSG_Header.CAB == C_FMS_1_2)
      {
         switch (m_outputs.Label_Number)
         {
         case 150:
            m_outputs.A429_Bus_C1out.Fmgc_Label150.setWord(p_bus_429.BUS_DATA);
            m_outputs.A429_Bus_C2out.Fmgc_Label150.setWord(p_bus_429.BUS_DATA);
            break;
         case 260:
            m_outputs.A429_Bus_C1out.Fmgc_Label260.setWord(p_bus_429.BUS_DATA);
            m_outputs.A429_Bus_C2out.Fmgc_Label260.setWord(p_bus_429.BUS_DATA);
            break;
         case 310:
            m_outputs.A429_Bus_C1out.Fmgc_Label310.setWord(p_bus_429.BUS_DATA);
            m_outputs.A429_Bus_C2out.Fmgc_Label310.setWord(p_bus_429.BUS_DATA);
            break;
         case 311:
            m_outputs.A429_Bus_C1out.Fmgc_Label311.setWord(p_bus_429.BUS_DATA);
            m_outputs.A429_Bus_C2out.Fmgc_Label311.setWord(p_bus_429.BUS_DATA);
            break;
         default:
            break;
          }      
      }//end of if
      if(m_outputs.bus_Number == Bus_Fmgc_C_O && p_bus_429.MSG_Header.CAB == C_FMS_1)
      {
         switch (m_outputs.Label_Number)
         {
         case 150:
            m_outputs.A429_Bus_C1out.Fmgc_Label150.setWord(p_bus_429.BUS_DATA);
            //m_outputs.A429_Bus_C2out.Fmgc_Label150.setWord(p_bus_429.BUS_DATA);
            break;
         case 260:
            m_outputs.A429_Bus_C1out.Fmgc_Label260.setWord(p_bus_429.BUS_DATA);
            //m_outputs.A429_Bus_C2out.Fmgc_Label260.setWord(p_bus_429.BUS_DATA);
            break;
         case 310:
            m_outputs.A429_Bus_C1out.Fmgc_Label310.setWord(p_bus_429.BUS_DATA);
            //m_outputs.A429_Bus_C2out.Fmgc_Label310.setWord(p_bus_429.BUS_DATA);
            break;
         case 311:
            m_outputs.A429_Bus_C1out.Fmgc_Label311.setWord(p_bus_429.BUS_DATA);
            //m_outputs.A429_Bus_C2out.Fmgc_Label311.setWord(p_bus_429.BUS_DATA);
            break;
         default:
            break;
          }      
      }else if(m_outputs.bus_Number == Bus_Fmgc_C_O && p_bus_429.MSG_Header.CAB == C_FMS_2)
      {
         switch (m_outputs.Label_Number)
         {
         case 150:
            //m_outputs.A429_Bus_C1out.Fmgc_Label150.setWord(p_bus_429.BUS_DATA);
            m_outputs.A429_Bus_C2out.Fmgc_Label150.setWord(p_bus_429.BUS_DATA);
            break;
         case 260:
            //m_outputs.A429_Bus_C1out.Fmgc_Label260.setWord(p_bus_429.BUS_DATA);
            m_outputs.A429_Bus_C2out.Fmgc_Label260.setWord(p_bus_429.BUS_DATA);
            break;
         case 310:
            //m_outputs.A429_Bus_C1out.Fmgc_Label310.setWord(p_bus_429.BUS_DATA);
            m_outputs.A429_Bus_C2out.Fmgc_Label310.setWord(p_bus_429.BUS_DATA);
            break;
         case 311:
            //m_outputs.A429_Bus_C1out.Fmgc_Label311.setWord(p_bus_429.BUS_DATA);
            m_outputs.A429_Bus_C2out.Fmgc_Label311.setWord(p_bus_429.BUS_DATA);
            break;
         default:
            break;
          }  
      }
      if (m_outputs.bus_Number == Bus_Fmgc_C_O)
      {
         switch (m_outputs.Label_Number)
         {
         case 41:
            m_outputs.A429_Bus_C1out.Fmgc_Label041.setWord(p_bus_429.BUS_DATA);
            break;
         case 42:
            m_outputs.A429_Bus_C1out.Fmgc_Label042.setWord(p_bus_429.BUS_DATA);
            break;
         case 43:
            m_outputs.A429_Bus_C1out.Fmgc_Label043.setWord(p_bus_429.BUS_DATA);
            break;
         case 140:
            m_outputs.A429_Bus_C1out.Fmgc_Label140.setWord(p_bus_429.BUS_DATA);
            break;
         /*case 150:
            m_outputs.A429_Bus_C1out.Fmgc_Label150.setWord(p_bus_429.BUS_DATA);
            m_outputs.A429_Bus_C2out.Fmgc_Label150.setWord(p_bus_429.BUS_DATA);
            break;*/
         case 252:
            m_outputs.A429_Bus_C1out.Fmgc_Label252.setWord(p_bus_429.BUS_DATA);
            break;
         case 253:
            m_outputs.A429_Bus_C1out.Fmgc_Label253.setWord(p_bus_429.BUS_DATA);
            break;
         case 255:
            m_outputs.A429_Bus_C1out.Fmgc_Label255.setWord(p_bus_429.BUS_DATA);
            break;
         case 256:
            m_outputs.A429_Bus_C1out.Fmgc_Label256.setWord(p_bus_429.BUS_DATA);
            break;
         /*case 260:
            m_outputs.A429_Bus_C1out.Fmgc_Label260.setWord(p_bus_429.BUS_DATA);
            m_outputs.A429_Bus_C2out.Fmgc_Label260.setWord(p_bus_429.BUS_DATA);
            break;*/
         case 270:
            m_outputs.A429_Bus_C1out.Fmgc_Label270.setWord(p_bus_429.BUS_DATA);
            break;
         /*case 310:
            m_outputs.A429_Bus_C1out.Fmgc_Label310.setWord(p_bus_429.BUS_DATA);
            m_outputs.A429_Bus_C2out.Fmgc_Label310.setWord(p_bus_429.BUS_DATA);
            break;
         case 311:
            m_outputs.A429_Bus_C1out.Fmgc_Label311.setWord(p_bus_429.BUS_DATA);
            m_outputs.A429_Bus_C2out.Fmgc_Label311.setWord(p_bus_429.BUS_DATA);
            break;*/

         case 17:
            m_outputs.A429_Bus_C2out.Fmgc_Label017.setWord(p_bus_429.BUS_DATA);
            break;
         case 33:
            m_outputs.A429_Bus_C2out.Fmgc_Label033.setWord(p_bus_429.BUS_DATA);
            break;
         case 34:
            m_outputs.A429_Bus_C2out.Fmgc_Label034.setWord(p_bus_429.BUS_DATA);
            break;
         case 100:
            m_outputs.A429_Bus_C2out.Fmgc_Label100.setWord(p_bus_429.BUS_DATA);
            break;
         case 105:
            m_outputs.A429_Bus_C2out.Fmgc_Label105.setWord(p_bus_429.BUS_DATA);
            break;
         case 320:
            m_outputs.A429_Bus_C2out.Fmgc_Label320.setWord(p_bus_429.BUS_DATA);
            break;
         case 323:
            m_outputs.A429_Bus_C2out.Fmgc_Label323.setWord(p_bus_429.BUS_DATA);
            break;
         case 324:
            m_outputs.A429_Bus_C2out.Fmgc_Label324.setWord(p_bus_429.BUS_DATA);
            break;
         case 325:
            m_outputs.A429_Bus_C2out.Fmgc_Label325.setWord(p_bus_429.BUS_DATA);
            break;
         case 326:
            m_outputs.A429_Bus_C2out.Fmgc_Label326.setWord(p_bus_429.BUS_DATA);
            break;
         case 327:
            m_outputs.A429_Bus_C2out.Fmgc_Label327.setWord(p_bus_429.BUS_DATA);
            break;
         case 330:
            m_outputs.A429_Bus_C2out.Fmgc_Label330.setWord(p_bus_429.BUS_DATA);
            break;
         case 331:
            m_outputs.A429_Bus_C2out.Fmgc_Label331.setWord(p_bus_429.BUS_DATA);
            break;
         case 332:
            m_outputs.A429_Bus_C2out.Fmgc_Label334.setWord(p_bus_429.BUS_DATA);
            break;
         case 333:
            m_outputs.A429_Bus_C2out.Fmgc_Label333.setWord(p_bus_429.BUS_DATA);
            break;
         case 334:
            m_outputs.A429_Bus_C2out.Fmgc_Label334.setWord(p_bus_429.BUS_DATA);
            break;
         case 335:
            m_outputs.A429_Bus_C2out.Fmgc_Label335.setWord(p_bus_429.BUS_DATA);
            break;
         case 336:
            m_outputs.A429_Bus_C2out.Fmgc_Label336.setWord(p_bus_429.BUS_DATA);
            break;
         case 351:
            m_outputs.A429_Bus_C2out.Fmgc_Label351.setWord(p_bus_429.BUS_DATA);
            break;
         default:
            break;
        }
      }//end of if


   }//end of for

   fclose(fmlog);
   return msg_type;
}

CoreSim::Osal::Int16 FM::tcpCommunication(CoreSim::Osal::Int32 recMsgType,Common_Data& m_comdata,Common_Data2& m_comdata2,Dis_Data& m_disdat,FILE *fmlog)
{
   CoreSim::Osal::Bool isOdd          = false;
   CoreSim::Osal::Int16 sendnum = 0;
   //if(m_outputs.RTC % m_sendFrameDivisor == 0)
   if(C_P_END_FRAME == recMsgType)
   {
      if(fm_hlty == 0xff)
      {				  
		   if (!isOdd)
		   {
            packInitialDatas(m_comdata);
			   sendnum = m_txTcpSocket.send(&m_comdata,sizeof(m_comdata));
			   fprintf(fmlog,"Send Msg Legth: %d	Size_of_commons: %d\n",sendnum,sizeof(m_comdata));				
			   isOdd = true;
			
         }else
         {
            packInitialDatas2(m_comdata2);
            sendnum = m_txTcpSocket.send(&m_comdata2,sizeof(m_comdata2));						
            fprintf(fmlog,"Send Msg Legth: %d	Size_of_common2: %d\n",sendnum,sizeof(m_comdata2));
            isOdd = false;
         }
      }else
      {
      				        	         
		   packDisDatas(m_disdat);
		   sendnum = m_txTcpSocket.send(&m_disdat,sizeof(m_disdat));				  
		   fprintf(fmlog,"Send Msg Legth: %d	Size_of_disdata: %d\n",sendnum,sizeof(m_disdat));
						   					         			  
       }//end of fm_healthy

      if(sendnum < 0 )
      {
         std::cout << "Send Error! Normal TcpSend. " << std::endl;
		   fprintf(fmlog,"Send Error! Normal TcpSend. \n");
      }

      recMsgType = 0; 
   }
 
   return 0;
}

template <typename T>
CoreSim::Osal::Int16 FM::packCMDatas(T& input,P_BUS_429 &bus,CoreSim::Osal::UInt32 labelnum,FILE *fm,CoreSim::Osal::UInt32 m_busNum)
{
   CoreSim::Osal::UInt32 *p_data = NULL; 
   P_BUS_429 *p_429				 = NULL;
   CoreSim::Osal::Int16 m_Byte   = 0;
   p_data = (CoreSim::Osal::UInt32 *)(&input);
   p_429  = &bus;
   
   for(CoreSim::Osal::UInt32 i = 0; i < labelnum; i++ )
   {
      setMsg(*p_429,C_P_BUS_429);
      setCab(*p_429,C_FMS_1_2);
      setBusNumber(*p_429,m_busNum);
      setBusData(*p_429,bytes4ToInt(*p_data));
      m_Byte += 8;  
#if DEBUG_PRT
      fprintf(fmlog,"SEND P_BUS 429 %04X  %04X  %04X\n",p_429->MSG_Header,((p_429->MSG_Header.BusNum1)|(p_429->MSG_Header.BusNum2<<8)),bytes4ToInt(p_429->BUS_DATA));
      fflush(fmlog);
#endif

      p_429++;
      p_data++;
   }
   return m_Byte;
}

CoreSim::Osal::Int64 FM::findAddr(CoreSim::Osal::Int32 bus,CoreSim::Osal::UInt16 Label)
{
	CoreSim::Osal::Int64 addr = 0;
	int i = 0;
	int tmp   = 0;
	while(i < cm_busList.size())
	{
		//CoreSim::Osal::String tmstr = cm_labelList[i].substr(2,5);
		//tmp = strtol(&tmstr[0],NULL,16);
		tmp = strtol(&((cm_labelList[i].substr(4,5))[0]),NULL,16);
      tmp = getLabel(tmp);
		if ((tmp == Label) && (cm_busList[i] == bus))
		{
		  break;
		}
		i++;
		if(i >= cm_busList.size())
		{
			return -1;
		}
	}
	
	addr = (strtol(&(cm_sourAddrList[i].substr(6,4))[0],NULL,16) - 2)/2 ;
	
	return addr;
}

CoreSim::Osal::UInt32 FM::getLabel(CoreSim::Osal::UInt32 Data)
{
   CoreSim::Osal::UInt32 label  = 0;
   CoreSim::Osal::UInt32 temp   = 0;
   temp = reverseBit8(Data);
   label += ((temp&0xC0)>>6)*100;
   label += ((temp&0x38)>>3)*10;
   label += temp&0x07;
   return label;
}

void FM::reverseLabel(void)
{
   //m_inputs.GPS1_Bus.Label057.setLabel(reverseBit8(m_inputs.GPS1_Bus.Label057.getLabel()));
   //m_inputs.GPS1_Bus.Label076.setLabel(reverseBit8(m_inputs.GPS1_Bus.Label076.getLabel()));
   //m_inputs.GPS1_Bus.Label101.setLabel(reverseBit8(m_inputs.GPS1_Bus.Label101.getLabel()));
   //m_inputs.GPS1_Bus.Label102.setLabel(reverseBit8(m_inputs.GPS1_Bus.Label102.getLabel()));
   //m_inputs.GPS1_Bus.Label103.setLabel(reverseBit8(m_inputs.GPS1_Bus.Label103.getLabel()));
   //m_inputs.GPS1_Bus.Label110.setLabel(reverseBit8(m_inputs.GPS1_Bus.Label110.getLabel()));
   //m_inputs.GPS1_Bus.Label111.setLabel(reverseBit8(m_inputs.GPS1_Bus.Label111.getLabel()));
   //m_inputs.GPS1_Bus.Label112.setLabel(reverseBit8(m_inputs.GPS1_Bus.Label112.getLabel()));
   //m_inputs.GPS1_Bus.Label120.setLabel(reverseBit8(m_inputs.GPS1_Bus.Label120.getLabel()));
   //m_inputs.GPS1_Bus.Label121.setLabel(reverseBit8(m_inputs.GPS1_Bus.Label121.getLabel()));
   //m_inputs.GPS1_Bus.Label125.setLabel(reverseBit8(m_inputs.GPS1_Bus.Label125.getLabel()));
   //m_inputs.GPS1_Bus.Label130.setLabel(reverseBit8(m_inputs.GPS1_Bus.Label130.getLabel()));
   //m_inputs.GPS1_Bus.Label133.setLabel(reverseBit8(m_inputs.GPS1_Bus.Label133.getLabel()));
   //m_inputs.GPS1_Bus.Label136.setLabel(reverseBit8(m_inputs.GPS1_Bus.Label136.getLabel()));
   //m_inputs.GPS1_Bus.Label140.setLabel(reverseBit8(m_inputs.GPS1_Bus.Label140.getLabel()));
   //m_inputs.GPS1_Bus.Label141.setLabel(reverseBit8(m_inputs.GPS1_Bus.Label141.getLabel()));
   //m_inputs.GPS1_Bus.Label150.setLabel(reverseBit8(m_inputs.GPS1_Bus.Label150.getLabel()));
   //m_inputs.GPS1_Bus.Label165.setLabel(reverseBit8(m_inputs.GPS1_Bus.Label165.getLabel()));
   //m_inputs.GPS1_Bus.Label166.setLabel(reverseBit8(m_inputs.GPS1_Bus.Label166.getLabel()));
   //m_inputs.GPS1_Bus.Label174.setLabel(reverseBit8(m_inputs.GPS1_Bus.Label174.getLabel()));
   //m_inputs.GPS1_Bus.Label247.setLabel(reverseBit8(m_inputs.GPS1_Bus.Label247.getLabel()));
   //m_inputs.GPS1_Bus.Label260.setLabel(reverseBit8(m_inputs.GPS1_Bus.Label260.getLabel()));
   //m_inputs.GPS1_Bus.Label273.setLabel(reverseBit8(m_inputs.GPS1_Bus.Label273.getLabel()));
   //m_inputs.GPS1_Bus.Label377.setLabel(reverseBit8(m_inputs.GPS1_Bus.Label377.getLabel()));
   //
   //m_inputs.GPS2_Bus.Label057.setLabel(reverseBit8(m_inputs.GPS2_Bus.Label057.getLabel()));
   //m_inputs.GPS2_Bus.Label076.setLabel(reverseBit8(m_inputs.GPS2_Bus.Label076.getLabel()));
   //m_inputs.GPS2_Bus.Label101.setLabel(reverseBit8(m_inputs.GPS2_Bus.Label101.getLabel()));
   //m_inputs.GPS2_Bus.Label102.setLabel(reverseBit8(m_inputs.GPS2_Bus.Label102.getLabel()));
   //m_inputs.GPS2_Bus.Label103.setLabel(reverseBit8(m_inputs.GPS2_Bus.Label103.getLabel()));
   //m_inputs.GPS2_Bus.Label110.setLabel(reverseBit8(m_inputs.GPS2_Bus.Label110.getLabel()));
   //m_inputs.GPS2_Bus.Label111.setLabel(reverseBit8(m_inputs.GPS2_Bus.Label111.getLabel()));
   //m_inputs.GPS2_Bus.Label112.setLabel(reverseBit8(m_inputs.GPS2_Bus.Label112.getLabel()));
   //m_inputs.GPS2_Bus.Label120.setLabel(reverseBit8(m_inputs.GPS2_Bus.Label120.getLabel()));
   //m_inputs.GPS2_Bus.Label121.setLabel(reverseBit8(m_inputs.GPS2_Bus.Label121.getLabel()));
   //m_inputs.GPS2_Bus.Label125.setLabel(reverseBit8(m_inputs.GPS2_Bus.Label125.getLabel()));
   //m_inputs.GPS2_Bus.Label130.setLabel(reverseBit8(m_inputs.GPS2_Bus.Label130.getLabel()));
   //m_inputs.GPS2_Bus.Label133.setLabel(reverseBit8(m_inputs.GPS2_Bus.Label133.getLabel()));
   //m_inputs.GPS2_Bus.Label136.setLabel(reverseBit8(m_inputs.GPS2_Bus.Label136.getLabel()));
   //m_inputs.GPS2_Bus.Label140.setLabel(reverseBit8(m_inputs.GPS2_Bus.Label140.getLabel()));
   //m_inputs.GPS2_Bus.Label141.setLabel(reverseBit8(m_inputs.GPS2_Bus.Label141.getLabel()));
   //m_inputs.GPS2_Bus.Label150.setLabel(reverseBit8(m_inputs.GPS2_Bus.Label150.getLabel()));
   //m_inputs.GPS2_Bus.Label165.setLabel(reverseBit8(m_inputs.GPS2_Bus.Label165.getLabel()));
   //m_inputs.GPS2_Bus.Label166.setLabel(reverseBit8(m_inputs.GPS2_Bus.Label166.getLabel()));
   //m_inputs.GPS2_Bus.Label174.setLabel(reverseBit8(m_inputs.GPS2_Bus.Label174.getLabel()));
   //m_inputs.GPS2_Bus.Label247.setLabel(reverseBit8(m_inputs.GPS2_Bus.Label247.getLabel()));
   //m_inputs.GPS2_Bus.Label260.setLabel(reverseBit8(m_inputs.GPS2_Bus.Label260.getLabel()));
   //m_inputs.GPS2_Bus.Label273.setLabel(reverseBit8(m_inputs.GPS2_Bus.Label273.getLabel()));
   //m_inputs.GPS2_Bus.Label377.setLabel(reverseBit8(m_inputs.GPS2_Bus.Label377.getLabel()));
   //
   //m_inputs.MCDU1_Bus.Label270.setLabel(reverseBit8(m_inputs.MCDU1_Bus.Label270.getLabel()));
   //m_inputs.MCDU1_Bus.Label271.setLabel(reverseBit8(m_inputs.MCDU1_Bus.Label271.getLabel()));
   //m_inputs.MCDU1_Bus.Label272.setLabel(reverseBit8(m_inputs.MCDU1_Bus.Label272.getLabel()));
   //m_inputs.MCDU1_Bus.Label350.setLabel(reverseBit8(m_inputs.MCDU1_Bus.Label350.getLabel()));
   //m_inputs.MCDU1_Bus.Label354.setLabel(reverseBit8(m_inputs.MCDU1_Bus.Label354.getLabel()));
   //m_inputs.MCDU1_Bus.Label377.setLabel(reverseBit8(m_inputs.MCDU1_Bus.Label377.getLabel()));
   //                           
   //m_inputs.MCDU2_Bus.Label270.setLabel(reverseBit8(m_inputs.MCDU2_Bus.Label270.getLabel()));
   //m_inputs.MCDU2_Bus.Label271.setLabel(reverseBit8(m_inputs.MCDU2_Bus.Label271.getLabel()));
   //m_inputs.MCDU2_Bus.Label272.setLabel(reverseBit8(m_inputs.MCDU2_Bus.Label272.getLabel()));
   //m_inputs.MCDU2_Bus.Label350.setLabel(reverseBit8(m_inputs.MCDU2_Bus.Label350.getLabel()));
   //m_inputs.MCDU2_Bus.Label354.setLabel(reverseBit8(m_inputs.MCDU2_Bus.Label354.getLabel()));
   //m_inputs.MCDU2_Bus.Label377.setLabel(reverseBit8(m_inputs.MCDU2_Bus.Label377.getLabel()));
   //
   //m_inputs.Clock_bus.Label125.setLabel(reverseBit8(m_inputs.Clock_bus.Label125.getLabel()));
   //m_inputs.Clock_bus.Label150.setLabel(reverseBit8(m_inputs.Clock_bus.Label150.getLabel()));
   //m_inputs.Clock_bus.Label260.setLabel(reverseBit8(m_inputs.Clock_bus.Label260.getLabel()));
   //m_inputs.Clock_bus.Label377.setLabel(reverseBit8(m_inputs.Clock_bus.Label377.getLabel()));
   //
   //m_inputsA429.in_VOROWN.Label_222.setLabel(reverseBit8(m_inputsA429.in_VOROWN.Label_222.getLabel()));
   //m_inputsA429.in_VOROWN.Label_034.setLabel(reverseBit8(m_inputsA429.in_VOROWN.Label_034.getLabel()));
   //m_inputsA429.in_VOROWN.Label_242.setLabel(reverseBit8(m_inputsA429.in_VOROWN.Label_242.getLabel()));
   //m_inputsA429.in_VOROWN.Label_244.setLabel(reverseBit8(m_inputsA429.in_VOROWN.Label_244.getLabel()));
   //m_inputsA429.in_VOROWN.Label_100.setLabel(reverseBit8(m_inputsA429.in_VOROWN.Label_100.getLabel()));
   //
   //m_inputsA429.in_VOROPP.Label_222.setLabel(reverseBit8(m_inputsA429.in_VOROPP.Label_222.getLabel()));
   //m_inputsA429.in_VOROPP.Label_034.setLabel(reverseBit8(m_inputsA429.in_VOROPP.Label_034.getLabel()));
   //m_inputsA429.in_VOROPP.Label_242.setLabel(reverseBit8(m_inputsA429.in_VOROPP.Label_242.getLabel()));
   //m_inputsA429.in_VOROPP.Label_244.setLabel(reverseBit8(m_inputsA429.in_VOROPP.Label_244.getLabel()));
   //m_inputsA429.in_VOROPP.Label_100.setLabel(reverseBit8(m_inputsA429.in_VOROPP.Label_100.getLabel()));
   //
   //m_inputsA429.in_ADCOWN.ADR_Label234.setLabel(reverseBit8(m_inputsA429.in_ADCOWN.ADR_Label234.getLabel()));
   //m_inputsA429.in_ADCOWN.ADR_Label235.setLabel(reverseBit8(m_inputsA429.in_ADCOWN.ADR_Label235.getLabel()));
   //m_inputsA429.in_ADCOWN.ADR_Label236.setLabel(reverseBit8(m_inputsA429.in_ADCOWN.ADR_Label236.getLabel()));
   //m_inputsA429.in_ADCOWN.ADR_Label237.setLabel(reverseBit8(m_inputsA429.in_ADCOWN.ADR_Label237.getLabel()));

   return;
}

CoreSim::Osal::UInt32 FM::reverseBit8(CoreSim::Osal::UInt32 n)
{
   CoreSim::Osal::UInt32 value = n & 0xff;
   value = ((value&0x55)<<1 ) | ((value&0xAA)>>1);
   value = ((value&0x33)<<2 ) | ((value&0xCC)>>2);
   value = ((value&0x0F)<<4 ) | ((value&0xF0)>>4);
   value = (n & 0xffffff00) | value;
   return value;
}

//Revert SAL and MAL: revert bit 16-9 8-1 to bit 9-16 1-8 internally
CoreSim::Osal::UInt32 FM::reverseMalSal(CoreSim::Osal::UInt32 n)
{
   CoreSim::Osal::UInt32 value = n & 0xff;
   value = ((value&0x55)<<1 ) | ((value&0xAA)>>1);
   value = ((value&0x33)<<2 ) | ((value&0xCC)>>2);
   value = ((value&0x0F)<<4 ) | ((value&0xF0)>>4);
   //reverse bit 9-16 internally
   CoreSim::Osal::UInt32 value2 = (n >> 8) & 0xff;
   value2 = ((value2&0x55)<<1 ) | ((value2&0xAA)>>1);
   value2 = ((value2&0x33)<<2 ) | ((value2&0xCC)>>2);
   value2 = ((value2&0x0F)<<4 ) | ((value2&0xF0)>>4);

   value =  (n & 0xffff0000) | (value2<<8) | value;
   //value = (n & 0xffffff00) | value;
   return value;
}


CoreSim::Osal::UInt32 FM::bytes4ToInt(CoreSim::Osal::UInt32 RavData)
{
   CoreSim::Osal::UInt32 value = (RavData&0xFF) << 24;
   value |= (RavData&0xFF00) << 8;
   value |= (RavData&0xFF0000) >> 8;
   value |= (RavData&0xFF000000) >> 24;

   return value;
}

CoreSim::Osal::UInt16 FM::bytes2ToInt(CoreSim::Osal::UInt16 RavData)
{
   CoreSim::Osal::UInt16 value = (RavData&0xFF) << 8;
   value |= (RavData&0xFF00) >> 8;

   return value;
}

//FM-FG Common Mememory,bus number, label, SDI, variable name are linked with Source Address AND COMMENTS
void FM::getComMemory(CoreSim::NetVdn::NetVdnClient& vdn)
{
   vector<CoreSim::Osal::String> filesname; 
   vector<CoreSim::Osal::String> files; 

   GetFiles("bin\\Common\\CM\\","A320_CMSIM_906.csv",filesname,files);
   for (int k = 0; k < files.size(); k++)
   {
      CoreSim::Utils::CsvFile data_file(files[k],true);
      int i = 0;
      while(data_file.readLine())
      {
         CoreSim::Osal::String name  = data_file.getField("Name");
         CoreSim::Osal::Int32  bus   = Osal::Convert::toInt32(data_file.getField("BusNumber"));
         CoreSim::Osal::String label = data_file.getField("Label");
         CoreSim::Osal::Int32 sdi    = Osal::Convert::toInt32(data_file.getField("SDI_Care"));
         CoreSim::Osal::String sour_Addr = data_file.getField("Source_Address");
         CoreSim::Osal::String format    = data_file.getField("Format");
         CoreSim::Osal::String comments  = data_file.getField("Comments");
         CoreSim::Osal::String io        = data_file.getField("IO");

         //if(Comments!="NA"&&Comments!="")
         {
            cm_nameList.push_back(name);
            cm_busList.push_back(bus);
            cm_labelList.push_back(label);
            cm_sdiList.push_back(sdi);
            cm_sourAddrList.push_back(sour_Addr);
            cm_commList.push_back(comments);
            //use io to identify input and output
            cm_ioList.push_back(io);
            //vdn.subscribe(fmcdi.diList[i],topic,name,"");				
         }
         i++;
      }
   }

   return;
}

//get files base function
void FM::GetFiles(CoreSim::Osal::String path,CoreSim::Osal::String filter,vector<CoreSim::Osal::String>& filesname,vector<CoreSim::Osal::String>& files)
{
   intptr_t   hFile   =   0;  //handler
   struct _finddata_t fileinfo;  //include io.h
   string p;  
   if ((hFile = _findfirst(p.assign(path).append(filter).c_str(),&fileinfo)) !=  -1)  
   {  //get all .csv files
      do  
      {  
         files.push_back(p.assign(path).append(fileinfo.name) );  
         filesname.push_back(fileinfo.name);
      }while(_findnext(hFile, &fileinfo)  == 0);  
      _findclose(hFile);  
   } 
   
   return;
}

void FM::Initialization(FG* fg)
{
   /*according to A.7 of FMS2 ICD Appendix-A320, Aero/Engine Configuration values must be inverted 
   to obtain the values needed in the discrete words.*/
   //set di_word0 0x88762200
   setBit(di_word0_bus66,21,!((InputsDIS *)fg->DisInFG)->LM05A_AC1);        //Ac_Type_1
   setBit(di_word0_bus66,24,!((InputsDIS *)fg->DisInFG)->LM06A_AC2);        //Ac_Type_2
   setBit(di_word0_bus66,19,!((InputsDIS *)fg->DisInFG)->RM06E_AC_TYPE_3);	//Ac_Type_3
   setBit(di_word0_bus66,22,!((InputsDIS *)fg->DisInFG)->LM06B_AC3);        //Eng_Type_1
   setBit(di_word0_bus66,32,!((InputsDIS *)fg->DisInFG)->LM06C_AC4);        //Eng_Type_2
   setBit(di_word0_bus66,23,!((InputsDIS *)fg->DisInFG)->LM07A_AC5);        //Eng_Type_3
   setBit(di_word0_bus66,7,!((InputsDIS *)fg->DisInFG)->LM04A_NEO_COM);     //Eng_Type_4
   setBit(di_word0_bus66, 8,!((InputsDIS *)fg->DisInFG)->RM06C_ENG_TYPE_5);	//Engine_Type_5
   setBit(di_word0_bus66,13,!((InputsDIS *)fg->DisInFG)->RM09D_AIRLINE_9);	//Cross_Load_Mode_Internal
   setBit(di_word0_bus66,12,!((InputsDIS *)fg->DisInFG)->LM07B_AC6);        //Vmo_mmo_1
   setBit(di_word0_bus66,9,!((InputsDIS *)fg->DisInFG)->LM07C_AC7);         //Vmo_mmo_2
   setBit(di_word0_bus66,28,1);
   setBit(di_word0_bus66,18,1);
   setBit(di_word0_bus66,14,1);
   setBit(di_word0_bus66,10,1);
   //set di_word1 0x000004c5
   setBit(di_word1_bus68,11,!((InputsDIS *)fg->DisInFG)->RM10F_AIRLINE_19);   //Fg_Ac_Ident_1
   setBit(di_word1_bus68,3,!((InputsDIS *)fg->DisInFG)->RM10J_AIRLINE_22);    //Fg_Ac_Ident_2
   setBit(di_word1_bus68,8,1);
   setBit(di_word1_bus68,7,1);
   setBit(di_word1_bus68,1,1);


   /*used for FM runs seperately*/
   //set al_Pins_Dis1
   setBit(al_Pins_Dis1,1,((InputsDIS *)fg->DisInFG)->RM07G_AIRLINE_1);     //Fuel_Poli_1
   setBit(al_Pins_Dis1,2,((InputsDIS *)fg->DisInFG)->RM07H_AIRLINE_2);     //Fuel_Poli_2
   setBit(al_Pins_Dis1,3,((InputsDIS *)fg->DisInFG)->RM07J_AIRLINE_3);     //Fuel_Poli_3
   setBit(al_Pins_Dis1,4,((InputsDIS *)fg->DisInFG)->RM07K_AIRLINE_4);     //Adf
   setBit(al_Pins_Dis1,5,((InputsDIS *)fg->DisInFG)->RM08G_AIRLINE_5);     //Fms_Printer
   setBit(al_Pins_Dis1,6,((InputsDIS *)fg->DisInFG)->RM08H_AIRLINE_6);     //Fms_Acars
   setBit(al_Pins_Dis1,7,((InputsDIS *)fg->DisInFG)->RM08J_AIRLINE_7);     //Mini_Acars
   setBit(al_Pins_Dis1,8,((InputsDIS *)fg->DisInFG)->RM09C_AIRLINE_8);     //Ft_m
   setBit(al_Pins_Dis1,9,((InputsDIS *)fg->DisInFG)->RM09D_AIRLINE_9);     //Cross_Load
   setBit(al_Pins_Dis1,10,((InputsDIS *)fg->DisInFG)->RM09E_AIRLINE_10);   //Batch1
   setBit(al_Pins_Dis1,11,((InputsDIS *)fg->DisInFG)->RM09F_AIRLINE_11);   //Lb_Kg
   setBit(al_Pins_Dis1,12,((InputsDIS *)fg->DisInFG)->RM09G_AIRLINE_12);   //Deg_F_C
   setBit(al_Pins_Dis1,13,((InputsDIS *)fg->DisInFG)->RM09H_AIRLINE_13);   //Vappr
   setBit(al_Pins_Dis1,14,((InputsDIS *)fg->DisInFG)->RM09J_AIRLINE_14);   //Center_Tank
   setBit(al_Pins_Dis1,15,((InputsDIS *)fg->DisInFG)->RM09K_AIRLINE_15);   //Grid_Mora_Msa
   setBit(al_Pins_Dis1,16,((InputsDIS *)fg->DisInFG)->RM10C_AIRLINE_16);   //Qnh_Qfe
   //set al_Pins_Dis2
   setBit(al_Pins_Dis2,1,((InputsDIS *)fg->DisInFG)->RM10D_AIRLINE_17);    //Rte_Retention
   setBit(al_Pins_Dis2,2,((InputsDIS *)fg->DisInFG)->RM10E_AIRLINE_18);    //Gs_Before_Loc
   setBit(al_Pins_Dis2,3,((InputsDIS *)fg->DisInFG)->RM10F_AIRLINE_19);    //Fg_Ac_Ident_1
   setBit(al_Pins_Dis2,4,((InputsDIS *)fg->DisInFG)->RM10G_AIRLINE_20);    //Kilowords_400
   setBit(al_Pins_Dis2,5,((InputsDIS *)fg->DisInFG)->RM10J_AIRLINE_22);    //Fg_Ac_Ident_2
   setBit(al_Pins_Dis2,6,((InputsDIS *)fg->DisInFG)->RM10K_AIRLINE_23);    //Lip
   setBit(al_Pins_Dis2,7,((InputsDIS *)fg->DisInFG)->RM11F_AIRLINE_25);    //Fm_Perfom
   setBit(al_Pins_Dis2,8,((InputsDIS *)fg->DisInFG)->RM11H_AIRLINE_27);    //Derated_TO
   //set ac_Pins_Dis
   setBit(ac_Pins_Dis,1,((InputsDIS *)fg->DisInFG)->LM05A_AC1);            //Ac_Type_1
   setBit(ac_Pins_Dis,2,((InputsDIS *)fg->DisInFG)->LM06A_AC2);            //Ac_Type_2
   setBit(ac_Pins_Dis,3,((InputsDIS *)fg->DisInFG)->LM06B_AC3);            //Eng_Type_1
   setBit(ac_Pins_Dis,4,((InputsDIS *)fg->DisInFG)->LM06C_AC4);            //Eng_Type_2
   setBit(ac_Pins_Dis,5,((InputsDIS *)fg->DisInFG)->LM07A_AC5);            //Eng_Type_3
   setBit(ac_Pins_Dis,6,((InputsDIS *)fg->DisInFG)->LM07B_AC6);            //Vmo_mmo_1
   setBit(ac_Pins_Dis,7,((InputsDIS *)fg->DisInFG)->LM07C_AC7);            //Vmo_mmo_2
   setBit(ac_Pins_Dis,8,((InputsDIS *)fg->DisInFG)->LM07D_AC8);            //Strakes
   setBit(ac_Pins_Dis,9,((InputsDIS *)fg->DisInFG)->LM08A_SPI8C);          //Energy_Management
   setBit(ac_Pins_Dis,10,((InputsDIS *)fg->DisInFG)->LM08B_AC10);          //Gps_1
   setBit(ac_Pins_Dis,11,((InputsDIS *)fg->DisInFG)->LM08C_AC11);          //Gps_2
   setBit(ac_Pins_Dis,12,((InputsDIS *)fg->DisInFG)->LM04A_NEO_COM);       //Eng_Type_4

    m_locals.al_Pins_Dis1 = al_Pins_Dis1;
    m_locals.al_Pins_Dis2 = al_Pins_Dis2;
    m_locals.ac_Pins_Dis = ac_Pins_Dis;
    m_locals.cfm_engine = cfm_engine;
    m_locals.iae_engine = iae_engine;
    m_locals.pw_engine = pw_engine;
    m_locals.di_word0_bus66 = di_word0_bus66;
    m_locals.di_word1_bus68 = di_word1_bus68;

   return;
}

CoreSim::Osal::Bool FM::publishoutputs(CoreSim::NetVdn::NetVdnClient& vdn)
{

   //Publish variales to VDN
   vdn.publish(m_outputs.RTC,"SimFmgc","RTC","miliseconds");
   vdn.publish(m_outputs.bus_Number,"SimFmgc","Bus_Number","");
   vdn.publish(m_outputs.Label_Number,"SimFmgc","Label_Number","");

   //vdn.publish(m_outputs.FMGC_Fault_Malf[0],"SimFmgc","FMGC_Fault_Malf","Malfunction");
    
   //not included in FMGC SE. for debug monitor
   //vdn.publish(m_outputs.LM14D_ATH_OWN_ENGD_COM_OPP[0],"SoftFmgc","ATH_OWN_ENGD_COM_OPP");
   
   //vdn.publish(m_outputs.LM13H_FMGC_CROSS_LOAD_ENABLE[0],"SoftFmgc","LM13H_FMGC_CROSS_LOAD_ENABLE");
   //mcdu bus
   vdn.publish(m_outputs.Mcdu_bus[0],"A429_ON_SIDE_FMGC1","");
   vdn.publish(m_outputs.Mcdu_bus[0],"A429_ON_SIDE_FMGC2","");
   vdn.publish(m_outputs.Mcdu_bus[1],"A429_OFF_SIDE_FMGC2","");
   vdn.publish(m_outputs.Mcdu_bus[1],"A429_OFF_SIDE_FMGC1","");

   //mcdu for debug use
   vdn.publish(m_outputs.Mcdu.Label044,"SimFM->Outputs->A429_ON_SIDE_FMGC1_Debug","Label_Debug01","");
   vdn.publish(m_outputs.Mcdu.Label125,"SimFM->Outputs->A429_ON_SIDE_FMGC1_Debug","Label_Debug02","");
   vdn.publish(m_outputs.Mcdu.Label172,"SimFM->Outputs->A429_ON_SIDE_FMGC1_Debug","Label_Debug03","");
   //datal loader received from FMS2
   vdn.publish(m_outputs.Dataload,"A429_Sim_Dataload","");
   //efis received form FMS2
   vdn.publish(m_outputs.Efis,"A429_Sim_EFIS","");
   //printer/acars
   vdn.publish(m_outputs.Printer,"A429_PRINTER_ACARS","");
   //ADS
   vdn.publish(m_outputs.Ads,"A429_FMGC_M_ADS","");
#if 1 //Modifid at 2020 0406
   for (int side = 0; side < 2; side++)
   {
      //modified on 20200415 do not need to publish by label, publish as one shot
      /*
      m_outputs.Fmgc_A[side].registerLabel(m_outputs.A429_Bus_Aout.Fmgc_Label040,"Label_0040");
      m_outputs.Fmgc_A[side].registerLabel(m_outputs.A429_Bus_Aout.Fmgc_Label041,"Label_0041");
      m_outputs.Fmgc_A[side].registerLabel(m_outputs.A429_Bus_Aout.Fmgc_Label042,"Label_0042");
      m_outputs.Fmgc_A[side].registerLabel(m_outputs.A429_Bus_Aout.Fmgc_Label146,"Label_0146");
      m_outputs.Fmgc_A[side].registerLabel(m_outputs.A429_Bus_Aout.Fmgc_Label147,"Label_0147");
      m_outputs.Fmgc_A[side].registerLabel(m_outputs.A429_Bus_Aout.Fmgc_Label233,"Label_0233");
      m_outputs.Fmgc_A[side].registerLabel(m_outputs.A429_Bus_Aout.Fmgc_Label234,"Label_0234");
      m_outputs.Fmgc_A[side].registerLabel(m_outputs.A429_Bus_Aout.Fmgc_Label235,"Label_0235");
      m_outputs.Fmgc_A[side].registerLabel(m_outputs.A429_Bus_Aout.Fmgc_Label236,"Label_0236");
      m_outputs.Fmgc_A[side].registerLabel(m_outputs.A429_Bus_Aout.Fmgc_Label237,"Label_0237");
      m_outputs.Fmgc_A[side].registerLabel(m_outputs.A429_Bus_Aout.Fmgc_Label273,"Label_0273");
      m_outputs.Fmgc_A[side].registerLabel(m_outputs.A429_Bus_Aout.Fmgc_Label274,"Label_0274");
      m_outputs.Fmgc_A[side].registerLabel(m_outputs.A429_Bus_Aout.Fmgc_Label275,"Label_0275");
      m_outputs.Fmgc_A[side].registerLabel(m_outputs.A429_Bus_Aout.Fmgc_Label276,"Label_0276");
      m_outputs.Fmgc_A[side].registerLabel(m_outputs.A429_Bus_Aout.Fmgc_Label277,"Label_0277");
      m_outputs.Fmgc_A[side].registerLabel(m_outputs.A429_Bus_Aout.Fmgc_Label327,"Label_0327");
      */
      m_outputs.Fmgc_B[side].registerLabel(m_outputs.A429_Bus_Bout.Fmgc_Label075,"Label_0075");
      m_outputs.Fmgc_B[side].registerLabel(m_outputs.A429_Bus_Bout.Fmgc_Label077,"Label_0077");
      m_outputs.Fmgc_B[side].registerLabel(m_outputs.A429_Bus_Bout.Fmgc_Label146,"Label_0146");
      m_outputs.Fmgc_B[side].registerLabel(m_outputs.A429_Bus_Bout.Fmgc_Label147,"Label_0147");
      m_outputs.Fmgc_B[side].registerLabel(m_outputs.A429_Bus_Bout.Fmgc_Label164,"Label_0164");
      m_outputs.Fmgc_B[side].registerLabel(m_outputs.A429_Bus_Bout.Fmgc_Label210,"Label_0210");
      m_outputs.Fmgc_B[side].registerLabel(m_outputs.A429_Bus_Bout.Fmgc_Label270,"Label_0270");
      m_outputs.Fmgc_B[side].registerLabel(m_outputs.A429_Bus_Bout.Fmgc_Label274,"Label_0274");
      m_outputs.Fmgc_B[side].registerLabel(m_outputs.A429_Bus_Bout.Fmgc_Label275,"Label_0275");
      m_outputs.Fmgc_B[side].registerLabel(m_outputs.A429_Bus_Bout.Fmgc_Label300,"Label_0300");
      m_outputs.Fmgc_B[side].registerLabel(m_outputs.A429_Bus_Bout.Fmgc_Label304,"Label_0304");
      m_outputs.Fmgc_B[side].registerLabel(m_outputs.A429_Bus_Bout.Fmgc_Label310,"Label_0310");
      m_outputs.Fmgc_B[side].registerLabel(m_outputs.A429_Bus_Bout.Fmgc_Label312,"Label_0312");
      m_outputs.Fmgc_B[side].registerLabel(m_outputs.A429_Bus_Bout.Fmgc_Label345,"Label_0345");
      m_outputs.Fmgc_B[side].registerLabel(m_outputs.A429_Bus_Bout.Fmgc_Label346,"Label_0346");

      m_outputs.Fmgc1_C.registerLabel(m_outputs.A429_Bus_C1out.Fmgc_Label041,"Label_0041");
      m_outputs.Fmgc1_C.registerLabel(m_outputs.A429_Bus_C1out.Fmgc_Label042,"Label_0042");
      m_outputs.Fmgc1_C.registerLabel(m_outputs.A429_Bus_C1out.Fmgc_Label043,"Label_0043");
      m_outputs.Fmgc1_C.registerLabel(m_outputs.A429_Bus_C1out.Fmgc_Label140,"Label_0140");
      m_outputs.Fmgc1_C.registerLabel(m_outputs.A429_Bus_C1out.Fmgc_Label150,"Label_0150");
      m_outputs.Fmgc1_C.registerLabel(m_outputs.A429_Bus_C1out.Fmgc_Label252,"Label_0252");
      m_outputs.Fmgc1_C.registerLabel(m_outputs.A429_Bus_C1out.Fmgc_Label253,"Label_0253");
      m_outputs.Fmgc1_C.registerLabel(m_outputs.A429_Bus_C1out.Fmgc_Label255,"Label_0255");
      m_outputs.Fmgc1_C.registerLabel(m_outputs.A429_Bus_C1out.Fmgc_Label256,"Label_0256");
      m_outputs.Fmgc1_C.registerLabel(m_outputs.A429_Bus_C1out.Fmgc_Label260,"Label_0260");
      m_outputs.Fmgc1_C.registerLabel(m_outputs.A429_Bus_C1out.Fmgc_Label270,"Label_0270");
      m_outputs.Fmgc1_C.registerLabel(m_outputs.A429_Bus_C1out.Fmgc_Label310,"Label_0310");
      m_outputs.Fmgc1_C.registerLabel(m_outputs.A429_Bus_C1out.Fmgc_Label311,"Label_0311");
   
      m_outputs.Fmgc2_C.registerLabel(m_outputs.A429_Bus_C2out.Fmgc_Label017,"Label_0017");
      m_outputs.Fmgc2_C.registerLabel(m_outputs.A429_Bus_C2out.Fmgc_Label017,"Label_0033");
      m_outputs.Fmgc2_C.registerLabel(m_outputs.A429_Bus_C2out.Fmgc_Label017,"Label_0034");
      m_outputs.Fmgc2_C.registerLabel(m_outputs.A429_Bus_C2out.Fmgc_Label017,"Label_0100");
      m_outputs.Fmgc2_C.registerLabel(m_outputs.A429_Bus_C2out.Fmgc_Label017,"Label_0105");
      m_outputs.Fmgc2_C.registerLabel(m_outputs.A429_Bus_C2out.Fmgc_Label017,"Label_0150");
      m_outputs.Fmgc2_C.registerLabel(m_outputs.A429_Bus_C2out.Fmgc_Label017,"Label_0260");
      m_outputs.Fmgc2_C.registerLabel(m_outputs.A429_Bus_C2out.Fmgc_Label017,"Label_0310");
      m_outputs.Fmgc2_C.registerLabel(m_outputs.A429_Bus_C2out.Fmgc_Label017,"Label_0311");
      m_outputs.Fmgc2_C.registerLabel(m_outputs.A429_Bus_C2out.Fmgc_Label017,"Label_0320");
      m_outputs.Fmgc2_C.registerLabel(m_outputs.A429_Bus_C2out.Fmgc_Label017,"Label_0323");
      m_outputs.Fmgc2_C.registerLabel(m_outputs.A429_Bus_C2out.Fmgc_Label017,"Label_0324");
      m_outputs.Fmgc2_C.registerLabel(m_outputs.A429_Bus_C2out.Fmgc_Label017,"Label_0325");
      m_outputs.Fmgc2_C.registerLabel(m_outputs.A429_Bus_C2out.Fmgc_Label017,"Label_0326");
      m_outputs.Fmgc2_C.registerLabel(m_outputs.A429_Bus_C2out.Fmgc_Label017,"Label_0327");
      m_outputs.Fmgc2_C.registerLabel(m_outputs.A429_Bus_C2out.Fmgc_Label017,"Label_0330");
      m_outputs.Fmgc2_C.registerLabel(m_outputs.A429_Bus_C2out.Fmgc_Label017,"Label_0331");
      m_outputs.Fmgc2_C.registerLabel(m_outputs.A429_Bus_C2out.Fmgc_Label017,"Label_0332");
      m_outputs.Fmgc2_C.registerLabel(m_outputs.A429_Bus_C2out.Fmgc_Label017,"Label_0333");
      m_outputs.Fmgc2_C.registerLabel(m_outputs.A429_Bus_C2out.Fmgc_Label017,"Label_0334");
      m_outputs.Fmgc2_C.registerLabel(m_outputs.A429_Bus_C2out.Fmgc_Label017,"Label_0335");
      m_outputs.Fmgc2_C.registerLabel(m_outputs.A429_Bus_C2out.Fmgc_Label017,"Label_0336");
      m_outputs.Fmgc2_C.registerLabel(m_outputs.A429_Bus_C2out.Fmgc_Label017,"Label_0351");

      vdn.publish(m_outputs.Fmgc_A[side],m_busname[side][0],"");
      vdn.publish(m_outputs.Fmgc_B[side],m_busname[side][1],"");
      vdn.publish(m_outputs.Fmgc1_C,m_busname[side][2],"");
      vdn.publish(m_outputs.Fmgc2_C,m_busname[side][3],"");
   }
#endif

   vdn.publish(m_locals.fm_puprdy_fm,"SimFM","FM_PUPRY","");
   vdn.publish(m_locals.fm_hlty_fm,"SimFM","FM_HEALTHY","");
   vdn.publish(m_inputs.LB02_Power_28vdc[0],"SimFM","LB02_Power_28vdc[0]","");
   vdn.publish(m_inputs.LB02_Power_28vdc[0],"SimFM","LB02_Power_28vdc[1]","");
   vdn.publish(m_inputs.Fmgs_Reset,"SimFM","Fmgs_Reset","Reset");
   vdn.publish(m_locals.al_Pins_Dis1,"SimFM","AL_PINS_DIS1","");
   vdn.publish(m_locals.al_Pins_Dis2,"SimFM","AL_PINS_DIS2","");
   vdn.publish(m_locals.ac_Pins_Dis,"SimFM","AL_PINS_DIS","");
   vdn.publish(m_locals.cfm_engine,"SimFM","CFM_Engine","");
   vdn.publish(m_locals.iae_engine,"SimFM","IAE_Engine","");
   vdn.publish(m_locals.pw_engine,"SimFM","PW_Engine","");
   vdn.publish(m_locals.di_word0_bus66,"SimFM","DIS_WORD0","");
   vdn.publish(m_locals.di_word1_bus68,"SimFM","DIS_WORD1","");
   vdn.publish(m_locals.time_usd,"SimFM","Time_Used","frame-number");

   //for debug
   vdn.publish(m_inputs.MCDU1_Bus.Label270,"SimFM->Inputs->A429_MCDU1","Label_0270","");
   vdn.publish(m_inputs.MCDU1_Bus.Label271,"SimFM->Inputs->A429_MCDU1","Label_0271","");
   vdn.publish(m_inputs.MCDU1_Bus.Label272,"SimFM->Inputs->A429_MCDU1","Label_0272","");
   vdn.publish(m_inputs.MCDU1_Bus.Label300,"SimFM->Inputs->A429_MCDU1","Label_0300","");
   vdn.publish(m_inputs.MCDU1_Bus.Label301,"SimFM->Inputs->A429_MCDU1","Label_0301","");
   vdn.publish(m_inputs.MCDU1_Bus.Label302,"SimFM->Inputs->A429_MCDU1","Label_0302","");
   vdn.publish(m_inputs.MCDU1_Bus.Label303,"SimFM->Inputs->A429_MCDU1","Label_0303","");
   vdn.publish(m_inputs.MCDU1_Bus.Label304,"SimFM->Inputs->A429_MCDU1","Label_0304","");
   vdn.publish(m_inputs.MCDU1_Bus.Label307,"SimFM->Inputs->A429_MCDU1","Label_0307","");
   vdn.publish(m_inputs.MCDU1_Bus.Label350,"SimFM->Inputs->A429_MCDU1","Label_0350","");
   vdn.publish(m_inputs.MCDU1_Bus.Label352,"SimFM->Inputs->A429_MCDU1","Label_0352","");
   vdn.publish(m_inputs.MCDU1_Bus.Label354,"SimFM->Inputs->A429_MCDU1","Label_0354","");
   vdn.publish(m_inputs.MCDU1_Bus.Label377,"SimFM->Inputs->A429_MCDU1","Label_0377","");

   return true;
}

void FM::setBit(CoreSim::Osal::UInt32 &des, int bit, int value)
{
   if(bit >= 1 && bit <= 32)
   {
      des = des | (value << (bit-1));
   }
   
   return;
}

UINT32 FM::getSdi(CoreSim::Osal::UInt32 A429Data)
{
   unsigned int sdi = (A429Data&0x30)>>8;
   return sdi;
}

UINT32 FM::getValue(CoreSim::Osal::UInt32 A429Data)
{
   unsigned int value = (A429Data&0x01ffffc00)>>10;
   return value;
}

CoreSim::Osal::String FM::ConfigDataFromCsv(CoreSim::Osal::String path, CoreSim::Osal::String name)
{
   vector<CoreSim::Osal::String> filesname;
   vector<CoreSim::Osal::String> files;
   GetFiles(path, "*.csv", filesname, files);
   for (int k = 0; k < files.size(); k++)
   {
      Utils::CsvFile data_file(files[k], true);

      while (data_file.readLine())
      {
         if (name == data_file.getField("name"))
         {
            return data_file.getField("value");
         }
      }
      data_file.close();
   }
   return "-99999";
}