﻿/*****************************************************************************
*                                                                            *
*  COPYRIGHT 2018 ACCEL FLIGHT SIMULATION. ALL RIGHTS RESERVED               *
*                                                                            *
*  This file is part of A320 Neo Sim FMGC simulation                         *
*                                                                            *
*  All information and content in this document is the Property              *
*  to ACCEL (Tianjin) Flight Simulation Co., Ltd. and shall not              *
*  be disclosed, disseminated, copied, or used except for purposes           *
*  expressly authorized in writing by ACCEL. Any unauthorized use            *
*  of the content will be considered an infringement of                      *
*  ACCEL's intellectual property rights.                                     *
*                                                                            *
*                                                                            *
*  @file     main.cpp                                                        *
*  @brief    Entry point for the NetVdn Application.                         *
*                                                                            *
*            This is a Sim FM example of how to use the NetVdnClient to      *
*            publish and subscribe to data on the Virtual Data Network.      *
*                                                                            *
*            This example includes publishing and subscribing to variables   *
*            as well as sending and receiving requests.                      *
*                                                                            *
*  @author   Shi, Junjie                                                     *
*  @email    junjie.shi@accelflightsimulation.com                            *
*  @version  1.0.0.0(version)                                                *
*  @date     2018/07/21                                                      *
*                                                                            *
*----------------------------------------------------------------------------*
*  Remark         : Description                                              *
*----------------------------------------------------------------------------*
*  Change History :                                                          *
*  <Date>     | <Version> | <Author>       | <Description>                   *
*----------------------------------------------------------------------------*
*  1/15/2019  | 1.0.0.0   | Shi, Junjie   | Create file                      *
*----------------------------------------------------------------------------*
*                                                                            *
*****************************************************************************/
#pragma once
#include "CoreSimSdkVersion.h"
#include "tclap/CmdLine.h"
#include "fg.h"
#include "fm.h"
//#include "Header.h"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <Winsock.h>
#include <windows.h>
#include <cstdlib>
#pragma comment(lib, "Wsock32.lib")
using namespace TCLAP;

//AL PINS DIS1
CoreSim::Osal::UInt32 al_Pins_Dis1 = 0;
//AL PINS DIS2
CoreSim::Osal::UInt32 al_Pins_Dis2 = 0;
//AC PINS DIS
CoreSim::Osal::UInt32 ac_Pins_Dis  = 0;
//CFM ENGINE
CoreSim::Osal::UInt32 cfm_engine   = 0;
//IAE ENGINE
CoreSim::Osal::UInt32 iae_engine   = 0;
//PW ENGINE
CoreSim::Osal::UInt32 pw_engine    = 0;
//Discrete bus 0,Bus number 66
CoreSim::Osal::UInt32 di_word0_bus66 = 0;
//Discrete bus 1,Bus number 68
CoreSim::Osal::UInt32 di_word1_bus68 = 0;

int main(int argc, char *argv[])
{
   CoreSim::Osal::String vdn_server_ip   = "";
   CoreSim::Osal::UInt16 vdn_server_port = 0;
   CoreSim::Osal::UInt32 vdn_server_rate = 0;
   CoreSim::Osal::String fm_server_ip    = "";
   CoreSim::Osal::UInt16 fm_server_port  = 0;
   while(1)
   {
      FM *fm = new FM;
      FG *fg = new FG;

      string common_path = "bin\\conf\\";
      vdn_server_ip   = fm->ConfigDataFromCsv(common_path,"ip");
      vdn_server_port = Osal::Convert::toUInt16(fm->ConfigDataFromCsv(common_path,"port"));
      vdn_server_rate = Osal::Convert::toUInt32(fm->ConfigDataFromCsv(common_path,"rate"));
      fm_server_ip    = fm->ConfigDataFromCsv(common_path,"fmip"); 
      fm_server_port  = Osal::Convert::toUInt16(fm->ConfigDataFromCsv(common_path,"fmport"));
      try                                     
      {
         // Define the command line object. You can use the SoftwareVersion class' string or any string
         CmdLine cmd("NetVdnClient. This is a sample C++ Net VDN client application.",
            ' ', "1.0.0");

         // Indicate that we want TCLAP to throw exceptions instead of handling it itself, so we can
         // catch them and clean up.
         cmd.setExceptionHandling(false);

         // VDN server ip address override. Optional, default to localhost.
         ValueArg<std::string> vdn_server_ip_arg 
            ("i", "ip", "VDN server host IP address or hostname.",
            false, vdn_server_ip, "vdn-server-ip", cmd);  //192.168.1.10  

         // VDN server port override. Optional, defaults to 20000.
         ValueArg<CoreSim::Osal::UInt16> vdn_server_port_arg 
            ("p", "port", "VDN server port.",
            false, vdn_server_port, "vdn-server-port", cmd);

         // Net VDN rate. Optional, defaults to 1Hz. This is the rate at which the VDN server will send upates to this client.
         ValueArg<CoreSim::Osal::UInt32> vdn_server_rate_arg 
            ("r", "rate", "VDN server rate.",
            false,vdn_server_rate, "vdn-server-rate", cmd);

         // Print the SDK version and exit.
         SwitchArg sdk_version_arg("","sdk-version","Prints the SSA SDK version number.", cmd, false);

         try
         {
            cmd.parse(argc, argv);

            // If the user has asked us for the SDK version, print it and exit.       
            if (sdk_version_arg.isSet())  
            {
               CoreSim::Utils::SoftwareVersion v(CORESIM_SOFTWARE_VERSION);
               CoreSim::Osal::String version_string = v.getAsString();
               std::cout << "CORESIM SSA SDK version: " << version_string << std::endl;
               return 0;
            }
         
         }
         // If there were any arg parsing exceptions, catch them here.
         catch (TCLAP::ArgException& e)
         {
            StdOutput s;

            try
            {
               // Report the std TCLAP error format for the parse error. 
               // This throws an ExitException so we want to catch that
               // and do nothing, to allow us to return 1.
               s.failure (cmd, e);
            }
            catch (TCLAP::ExitException&)
            {
               // Do nothing. Even if ExitException is not thrown we will
               // return error (1) because there was some kind of arg
               // error.
            }
            return 1;
         }

         //vdn_server_ip = vdn_server_ip_arg.getValue();
         //vdn_server_port = vdn_server_port_arg.getValue();
         //vdn_server_rate = vdn_server_rate_arg.getValue();
      }
      catch (...)
      {
         cerr << "Error parsing arguments." << endl;
         return 1;
      }

      Vdn::Float64 sub_float             = 0.0;
      CoreSim::Osal::Int32 recMsgType_FM = 0; 
      //CoreSim::Osal::UInt32 label        = 0;
      //CoreSim::Osal::Int16 OffLoop       = 0;
      //CoreSim::Osal::Bool isOdd          = false;
      //CoreSim::Osal::Bool isRestart      = true;
      //CoreSim::Osal::Bool isInitial      = false;
      NetVdn::NetVdnClient vdn;
      FG::FGDI *fgdi = new FG::FGDI;
      FG::FGDO *fgdo = new FG::FGDO;
      FILE *fd   = NULL;

      fg->fgdi = fgdi;
      fg->fgdo = fgdo;


   #if 1
      fd = fopen("log.txt","a+");  //a+ to append; w+ to build,if exist will rebuild
      if(fd == NULL)
      {
         printf("failed to open file!"); 
         return 0;
      }

      fg->fd_10184 = fopen("log10184.txt","a+");
      if(fg->fd_10184 == NULL)
      {
         printf("failed to open file!"); 
         return 0;
      }

      fg->fd_10183 = fopen("log10183.txt","a+");
      if(fg->fd_10183 == NULL)
      {
         printf("failed to open file!"); 
         return 0;
      }

      fm->fmlog = fopen("fmlog.txt","a+");
      if(fm->fmlog == NULL)
      {
         printf("failed to open Fm Log file!"); 
         return 0;
      }
   #endif
      // Setup a request queue. A request queue is a mechanism to send/receive
      // one time requests/commands from another client. Requests can have
      // none or more parameters which contain more information about the
      // request to handle.
      Utils::RequestQueue request_queue("Default");
      // Register the request names you intend to handle.
      // This step is optional and just provides information to RTMM
      // to show the available requests.  
      request_queue.addRegisteredRequest("Flight_Plan_Save");
      request_queue.addRegisteredRequest("Flight_Plan_Load");

      Utils::RequestQueue request_malf("Malfuncions");
      request_malf.addRegisteredRequest("FMGC_Fault_Malf");
      request_malf.addRegisteredRequest("Ap_Disengaged_Malf");

      Utils::RequestQueue request_malf2("Malfuncions");
      request_malf2.addRegisteredRequest("FMGC_Fault_Malf");
      request_malf2.addRegisteredRequest("Ap_Disengaged_Malf");

      Utils::RequestQueue request_reset("Resets");
      request_reset.addRegisteredRequest("Fmgcs_Reset");
      // The synchronous client will throw an exception if the connection cannot
      // be made or is lost at any point. Once an exception occurs, you must
      // call connect and republish/subscribe to your varaibles again.
      try
      {
         if(!vdn.isConnected())
         {
            vdn.connect(vdn_server_ip, vdn_server_port, vdn_server_rate);
            std::cout << "end of vdn connect " + vdn_server_ip  + ". " << endl;
         }
         
         if(fm->m_outputs.is_Fmgc_Resets)
         {
            /*Get Common Memory varibales from A320_CMSIM_906.csv*/
	         fm->getComMemory(vdn);  
            /*Get data for FM: FMGC power_on,rest. A429 Data:MCDU,GPS,Clock,Dme,Dldr,atsu data.*/
            fm->subscribeInputsFM(vdn);  
            /*Get data for FG from DI.CSV: Program Pins. Get A429 Data: ACD,FAC,Fadec,Fcu,Fqi,Mmr,IR,RA,VOR,TACS,DME,CLOCK etc. Get variables from DO.CSV.*/
            fg->subscribeInputsFG(fg,vdn);
      
            vdn.read();
            /*Power on SimFMGC only if FMGC SE is power on!*/
            if((!fm->m_inputs.LB02_Power_28vdc[0]) && (!fm->m_inputs.LB02_Power_28vdc[1]))
            {
               std::cout << "Check FMGC Power! " << endl;
               return 0;
            }
      	        	  	
	        //iae_engine = 0x00FF; 
           cfm_engine = 0x00FF;   //TODO: add logic. for Leap engine test.
	        fm->code_cmp_1 = 0x5448; //0x4141 TH
	        fm->code_cmp_2 = 0x4156; //0x4141  AV
	        fm->code_cmp_3 = 0x3332; //0x4141  32
	        fm->code_cmp_4 = 0x582d; //0x4141  X_
	        fm->code_cmp_5 = 0x4141; //0x4141  AA
	        fm->code_cmp_6 = 0x4100; //0x4141  A  

           /*Inputs Hard: FG-FM. According to J35944ae on Page 29.*/
           /*Set DIS Inputs for FM: DIS 0--BUS 66,DIS 1-BUS 68.According to FM ICD Appendix A320, Page 6.*/
           /*Set Program Pins For FM: AL PINS0-2. According to FM ICD Appendix A320, Page 9.*/
           fm->Initialization(fg);
           //FM runs
           //Establish initiatial connect using TCP Socket,receive data until P_END_FRAME message received.
           //send analog/descretes(such as AC_TYPE)
           //h = CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)FMConnAndSendInitial,NULL,1,0 );
           //ResumeThread(h);
           fm->FMConnAndSendInitial(fg,fm_server_ip,fm_server_port);
           std::cout << "end of FMS connect " + fm_server_ip  + ". " << endl;
           
           
            //Connect to FG using UDP socket
           fg->UdpConnect(fg->fgdo);
           std::cout << "end of FG connect. " << endl;
           /*Test CM addr allocate. If AddrTest is true, at power up the address will be written to m_inputsA429.in_CMIO in file.*/
           if(fg->AddrTest == true)
           {
              fg->CMAddrTest();
           }
      
           fm->publishoutputs(vdn);
           fg->publishoutputs(vdn);
           // Publish a request queue. Publishing a request queue tells the VDN that we want
           // to receive requests for the given topic and queue name. 
           //vdn.publishRequestQueue("TestTopic", "Default", request_queue);
           vdn.publishRequestQueue("Simsoft", "Default", request_queue);     
           vdn.publishRequestQueue("Fmgc1", "Malfunctions", request_malf);
           vdn.publishRequestQueue("Fmgc2", "Malfunctions", request_malf2);
           vdn.publishRequestQueue("Fmgc","Resets",request_reset);
           // Send a request to the CoreSim executive to transition to the Run state.
           // Note that the request will not be delivered until we call commit.
           Utils::Request request ("Run");
           vdn.request("Kernel", "Default", request);
           
           fm->m_outputs.is_Fmgc_Resets = false;
         }//endof if(isrestart)
         

         while(1/*fg->fg_safeTest != 0xff00*/) //modified by 20200408: safetest pass later than AP/ATH safety test.
                                            //output AP/ATH safety test
         //while(fg->m_outputs.FG_hlty_fg != 0x00ff)
         {
             vdn.read();
             fm->m_locals.time_usd = vdn.getUniversalSimFrame();
            //fm->subscribeInputsFM(vdn); 
            //fg->subscribeInputsFG(fg,vdn);

#if 0 
#endif           
            fg->UdpCommunication(fg,fd,vdn);
		      //fg->bufferInputs(fg);

            //Add on to refresh inputs for FG
		      //fm->Initialization(fg);           
            //FM runs: receive Data from FMS2
            recMsgType_FM = fm->RecvDataFromFM(fg);
            fm->tcpCommunication(recMsgType_FM,fm->m_commoms,fm->m_commoms2,fm->m_disData,fm->fmlog);

            if(fm->CrosLoad_Flag)
            {
               ((InputsDIS *)fg->DisInFG)->RM09D_AIRLINE_9 = (fm->p_bus_dis.BUS_DATA == 0)?true:false;
               fm->CrosLoad_Flag = false;
            }
            
#if 1
            //FMGC Resets-Power off FMS
            while(!request_reset.empty())
            {
               Utils::Request request = request_reset.popFront(); 
               if(request.getName() == "Fmgs_Reset")
               {
                  fm->m_outputs.Fmgc_Resets = request.getBool(0);
                  if( fm->m_outputs.Fmgc_Resets && ((!fm->m_inputs.LB02_Power_28vdc[0]) || (!fm->m_inputs.LB02_Power_28vdc[1])) )
                  {
                     fm->m_outputs.is_Fmgc_Resets = true;
                     if(C_P_END_FRAME == recMsgType_FM)
                     { 
                        char buffs[]   = "Time to disconnect...";
                        //char buffs[]   = "FMS2 -brief_trace -pid 1616 -aircraft a320 -dual -show_dbg -issdll -mapping_addr 80790000";
                        static_cast<int>(fm->m_txTcpSocket.send(buffs,sizeof(buffs)));
                        
                     }         
                  }
               }
            }  
            if(fm->m_outputs.is_Fmgc_Resets)
            {
                std::cout << "FMGC_Resets Starting ..." << endl;
                fprintf(fm->fmlog, "FMGC_Resets!\n");
                break;             
            }
#endif      
            //fm->p_bus_429.BUS_DATA = 0x5E030000;
            //fm->m_outputs.Mcdu_bus[0].push(fm->reverseBit8(fm->bytes4ToInt(fm->p_bus_429.BUS_DATA)));
            //fm->m_outputs.Mcdu.Label172.setWord(fm->reverseBit8(fm->bytes4ToInt(fm->p_bus_429.BUS_DATA)));

            fm->m_locals.time_usd = vdn.getUniversalSimFrame() - fm->m_locals.time_usd;
            _sleep(1000 / 100);
            vdn.commit();
         }     
         //printf("Pass FG-SATETY-TEST! \n");                 
         //vdn.commit();
   

        // Loop forever (or until the user presses CTRL+C).
        //for (;;)
        {                      
            //int sendnum = 0;
          
#if 0
            //FMGC1 malfunction
		      while(!request_malf.empty())
            {
               Utils::Request request = request_malf.popFront();          
               if(request.getName() == "FMGC_Fault_Malf")
               {
                  fg->m_outputs.FMGC_Fault_Malf[0] = request.getBool(0);
                  if( fg->m_outputs.FMGC_Fault_Malf[0] && (!fm->m_inputs.LB02_Power_28vdc[0]) && (C_P_END_FRAME == recMsgType_FM) )
                  {
                     std::cout << "FMGC_Fault_Malf!" << endl;
                     fprintf(fm->fmlog, "FMGC_Fault_Malf!\n");
                     fm->packPwrOffData(fm->m_pwrData,C_FMS_1);
					      sendnum = fm->m_txTcpSocket.send(&fm->m_pwrData,sizeof(fm->m_pwrData));
					      fprintf(fm->fmlog,"Send Msg Legth: %d	Size_of_PowerOff: %d\n",sendnum,sizeof(fm->m_pwrData));
                  }else
                  {
                     std::cout << "FMGC Normally!" << endl;
                     fprintf(fm->fmlog,"FMGC Normally!\n");
                     fm->packPwrOnData(fm->m_pwrData,C_FMS_1);
					      sendnum = fm->m_txTcpSocket.send(&fm->m_pwrData,sizeof(fm->m_pwrData));
					      fprintf(fm->fmlog,"Send Msg Legth: %d	Size_of_PowerOff: %d\n",sendnum,sizeof(fm->m_pwrData));
                  }
               }
               else if(request.getName() == "Ap_Disengaged_Malf")
               {
                  fg->m_outputs.LM10D_AP_OWN_ENGD_COM[0] = !request.getBool(0);
               }

            }
            //FMGC2 malfunction
            while(!request_malf2.empty())
            {
               Utils::Request request = request_malf2.popFront();
               if(request.getName() == "FMGC_Fault_Malf")
               {
                  fg->m_outputs.FMGC_Fault_Malf[1] = request.getBool(0);
                  if( fg->m_outputs.FMGC_Fault_Malf[1] && (!fm->m_inputs.LB02_Power_28vdc[1]) && (C_P_END_FRAME == recMsgType_FM) )
                  {
                     std::cout << "FMGC Fault!" << endl;
                     fprintf(fm->fmlog, "FMGC Fault!\n");
                     fm->packPwrOffData(fm->m_pwrData,C_FMS_2);
					      sendnum = fm->m_txTcpSocket.send(&fm->m_pwrData,sizeof(fm->m_pwrData));
					      fprintf(fm->fmlog,"Send Msg Legth: %d	Size_of_PowerOff: %d\n",sendnum,sizeof(fm->m_pwrData));
                  }else
                  {
                     std::cout << "FMGC Normally!" << endl;
                     fprintf(fm->fmlog,"FMGC Normally!\n");
                     fm->packPwrOnData(fm->m_pwrData,C_FMS_2);
					      sendnum = fm->m_txTcpSocket.send(&fm->m_pwrData,sizeof(fm->m_pwrData));
					      fprintf(fm->fmlog,"Send Msg Legth: %d	Size_of_PowerOff: %d\n",sendnum,sizeof(fm->m_pwrData));
                  }
               }
               else if(request.getName() == "Ap_Disengaged_Malf")
               {
                  //fg->m_outputs.DO_AP_OWN_ENGD_COM_FB[1] = !request.getBool(0);
                  fg->m_outputs.LM10D_AP_OWN_ENGD_COM[1] = !request.getBool(0);
               }
            }

            // Handle any requests that came in.
            while (!request_queue.empty())
            {
               Utils::Request request = request_queue.popFront();     
               if(request.getName() == "Flight_Plan_Save")
               {        
                  fm->m_outputs.fpsave_name = request.getString(0);
                  int cunt = request.parameterCount();
                  CoreSim::Utils::Parameter para = request.getParameter(1);           
                  fm->m_outputs.fpsave_name = (char *)para.data;        
                  if( (fm->m_outputs.fpsave_name != "") && (C_P_END_FRAME == recMsgType_FM))
                  {
                     std::cout << "Flight_Plan_Save!" << endl;
                     fprintf(fm->fmlog, "Flight_Plan_Save!\n");
                
                     char *p = NULL;
                     char file_name[32] = {};  
                     memset(file_name,0,32);
                     p = file_name;

                     CoreSim::Osal::Int16 len = fm->m_outputs.fpsave_name.length();
                     len = ceil(len/4.000);
                                 
                     CoreSim::Osal::Int32 data = fm->bytes4ToInt((len + 4)*4);  //0x20000000;
                     memcpy(p,&data,4);
                     p = p+4;
                     data = fm->bytes4ToInt(2 | ((len + 3)<<16));   //0x02000700;
                     memcpy(p,&data,4);
                     p = p+4;
                     data = fm->bytes4ToInt(C_P_FP_SAVE | ((len+1)<<16)); //0x1f000500;
                     memcpy(p,&data,4);
                     p = p+4;
                     memcpy(p,fm->m_outputs.fpsave_name.c_str(),fm->m_outputs.fpsave_name.length());   
                     p = p + len*4;   //16;
                     data = 0x03000100;
                     memcpy(p,&data,4);

                     sendnum = fm->m_txTcpSocket.send(file_name,(len + 4)*4);             
                     //fm->m_outputs.fpsave_name = "C:\\fms2\\a320\\PS4087600-906\\flight_plans\\flighplantest.o";
                     //fm->packFPSaveData(fm->m_fpsave,fm->m_outputs.fpsave_name);         
					      //sendnum = fm->m_txTcpSocket.send(&fm->m_fpsave,sizeof(fm->m_fpsave));
					      fprintf(fm->fmlog,"Send Msg Legth: %d	Size_of_PowerOff: %d\n",sendnum,sizeof(fm->m_fpsave));
                  }
               }
               else if(request.getName() == "Flight_Plan_Load")
               {
                  fm->m_outputs.fpsave_name = request.getString(1);
                  if( fm->m_outputs.fpsave_name != "" )
                  {
                      std::cout << "Flight_Plan_Load!" << std::endl;
                  }
               }
               // Otherwise just print out the request information.
               else
               {
                  std::cout << "Received " + request.getName() + " request. ";
                  std::cout << "It has " << request.parameterCount() << " parameters." << std::endl;              
               }
               std::cout << std::endl;
               fprintf(fm->fmlog,"Received request: %s, RequestNUN: %d\n",request.getName(),request.parameterCount());
            }
#endif
           
   #if FM_DEBUG
           //FM: pack and send Data (one-shot type) to FMS2.        
            for (int side = 0; side < 2; side++)
            {    
               while(fm->m_inputs.Mcdu_Eis_bus[side].pop(label))
               {
                  fm->setMsg(fm->m_inputs.Mcdu_eis,C_P_BUS_429);
                  fm->setCab(fm->m_inputs.Mcdu_eis,C_FMS_1_2);
                  fm->setBusNumber(fm->m_inputs.Mcdu_eis,Bus_Ils);   //to do: check the bus num
                  fm->setBusData(fm->m_inputs.Mcdu_eis,label); 
                  fm->m_txTcpSocket.send(&fm->m_inputs.Mcdu_eis,sizeof(fm->m_inputs.Mcdu_eis));
               }


               //dme bus, get the label and packed then send to FM          
               while (fm->m_inputs.Dme_bus[side].pop(label))
               {
                  //if dme need to be send every frame,then do as follows. 
                  //dmeDataList[side].push_back(label);
                  fm->setMsg(fm->m_inputs.Dme,C_P_BUS_429);
                  fm->setCab(fm->m_inputs.Dme,side+1);
                  if(side == 0)
                  {
                     fm->setBusNumber(fm->m_inputs.Dme,Bus_DmeOwn);   //dme_1
                  }
                  else
                  {
                     fm->setBusNumber(fm->m_inputs.Dme,Bus_DmeOpp);   //dme_2
                  }
                  fm->setBusData(fm->m_inputs.Dme,label);
                  fm->m_txTcpSocket.send(&fm->m_inputs.Dme,sizeof(fm->m_inputs.Dme));
               }
            

               //atsu-acars bus, get the label and packed then send to FM 
               //m_inputs.Atsu_acars.pop(m_outputs.label_Atsu);
               while (fm->m_inputs.Atsu_acars[side].pop(label))
               {
                  fm->setMsg(fm->m_inputs.Atsu,C_P_BUS_429);
                  fm->setCab(fm->m_inputs.Atsu,C_FMS_1_2);
                  fm->setBusNumber(fm->m_inputs.Atsu,Bus_Atsu_acars);   //mu_1
                  fm->setBusData(fm->m_inputs.Atsu,label); 
                  fm->m_txTcpSocket.send(&fm->m_inputs.Atsu,sizeof(fm->m_inputs.Atsu));
               }
            
            }//end of for
   #endif    
            
         }//end of for 
      }//end of try
      catch(runtime_error e)
      {
         std::cerr << e.what() << std::endl;
         fprintf(fm->fmlog,"Runtiem error :%s\n",e.what(),sizeof(e.what()));
      }
      catch(exception e)
      {
         std::cerr << e.what() << std::endl;
         fprintf(fm->fmlog,"Exception error :%s\n",e.what(),sizeof(e.what()));
      }
      catch(bad_alloc & e)
      {
         std::cerr << e.what() << std::endl;
         fprintf(fm->fmlog,"Exception error :%s\n",e.what(),sizeof(e.what()));
      }
      catch (...)
      {
         std::cerr << "Could not connect to " << vdn_server_ip << ":" << vdn_server_port << " or connection was lost." << std::endl;  
      }
      //end of try

      //delete h;
      fclose(fd);
      delete fgdi;
      delete fgdo;
      delete fm;
      //fm->~FM();
      delete fg;
   }//end of while(1)
   return 0;
}





