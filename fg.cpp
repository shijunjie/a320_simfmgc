/*****************************************************************************
*                                                                            *
*  COPYRIGHT 2018 ACCEL FLIGHT SIMULATION. ALL RIGHTS RESERVED               *
*                                                                            *
*  This file is part of A320 Neo Sim FMGC simulation                         *
*                                                                            *
*  All information and content in this document is the Property              *
*  to ACCEL (Tianjin) Flight Simulation Co., Ltd. and shall not              *
*  be disclosed, disseminated, copied, or used except for purposes           *
*  expressly authorized in writing by ACCEL. Any unauthorized use            *
*  of the content will be considered an infringement of                      *
*  ACCEL's intellectual property rights.                                     *
*                                                                            *
*                                                                            *
*  @file     fg.cpp                                                          *
*  @brief    Entry point for the NetVdn Application.                         *
*                                                                            *
*            This is a Sim FM example of how to use the NetVdnClient to      *
*            publish and subscribe to data on the Virtual Data Network.      *
*                                                                            *
*            This example includes publishing and subscribing to variables   *
*            as well as sending and receiving requests.                      *
*                                                                            *
*  @author   Shi, Junjie                                                     *
*  @email    junjie.shi@accelflightsimulation.com                            *
*  @version  1.0.0.0(version)                                                *
*  @date     2018/07/21                                                      *
*                                                                            *
*----------------------------------------------------------------------------*
*  Remark         : Description                                              *
*----------------------------------------------------------------------------*
*  Change History :                                                          *
*  <Date>     | <Version> | <Author>       | <Description>                   *
*----------------------------------------------------------------------------*
*  1/15/2019  | 1.0.0.0   | Shi, Junjie   | Create file                      *
*----------------------------------------------------------------------------*
*                                                                            *
*****************************************************************************/
#pragma once
#include "fg.h"
InputsA429 m_inputsA429 = {0}; 
CoreSim::Osal::Bool FG::subscribeInputsFG(FG* fg,CoreSim::NetVdn::NetVdnClient& vdn)
{
   vector<CoreSim::Osal::String> filesname; 
   vector<CoreSim::Osal::String> files; 

   GetFiles("bin\\Common\\DIDO\\","DI.csv",filesname,files);
   for (int k = 0; k < files.size(); k++)
   {
      CoreSim::Utils::CsvFile data_file(files[k],true);
      int i = 0;
      while(data_file.readLine())
      {
         CoreSim::Osal::String name      = data_file.getField("Name");
         CoreSim::Osal::String name2     = data_file.getField("Name2");
         CoreSim::Osal::String ipAddr    = data_file.getField("IpAddress");
         CoreSim::Osal::UInt16 localPort = Osal::Convert::toUInt16(data_file.getField("LocalPort"));
         CoreSim::Osal::UInt16 destPort  = Osal::Convert::toUInt16(data_file.getField("DestPort"));
         CoreSim::Osal::UInt16 size      = Osal::Convert::toUInt16(data_file.getField("Size"));
         CoreSim::Osal::Int64  value     = Osal::Convert::toInt64(data_file.getField("Value"));
         CoreSim::Osal::String topic     = data_file.getField("Topic");
         CoreSim::Osal::String comments  = data_file.getField("Comments");
         CoreSim::Osal::UInt16 port      = (localPort != 0)? localPort : destPort;
         CoreSim::Osal::String ipAddr2    = data_file.getField("IpAddress2");

         fg->fgdi->addrList[i]  = CoreSim::Osal::Net::SocketAddress(ipAddr,port);
         fg->fgdi->diList[i]    = value;
         fg->fgdi->nameList[i]  = name;
         fg->fgdi->sizeList[i]  = size;
         fg->fgdi->addrList2[i] = CoreSim::Osal::Net::SocketAddress(ipAddr2,port);
         fg->fgdi->nameList2[i] = name2;
         //std::ostringstream buf;
         if((name2 != "/")&&(i < (UDP_SOCKET_TOTAL_DIS_IN-1)))
         {           
            //buf << name.c_str() << "[" << 0 << "]";
            vdn.subscribe(fg->DisInFG2[i], "Fmgc",fg->fgdi->nameList2[i]);	   	
         }
         else
         {
            vdn.subscribe(fg->DisInFG2[i], "Fmgc",fg->fgdi->nameList[i]);	  
         }
         i++;
      }
   }
   //subscirbe DIS data.All from FMGC SE for future maintainance.
   for(int i = 0; i <= FGDI::RM03D_FCU_OWN_ATH_SW_MON; i++)
   {
      vdn.subscribe(fg->DisInFG[i],"Fmgc",fg->fgdi->nameList[i]);
   }
   vdn.subscribe(fg->DisInFG[130],"Fmgc","RM06E_AC_TYPE_3");         //LM06E AC_TYPE_3
   vdn.subscribe(fg->DisInFG[131],"Fmgc","RM06C_ENG_TYPE_5");        //RM06C_ENG_TYPE_5
   vdn.subscribe(fg->DisInFG[132],"Fmgc","RM07C_PP_PARITY_2G");      //RM07C_PP_PARITY_2G
   vdn.subscribe(fg->DisInFG[133],"Fmgc","LM05D_NAV_BKUP_SEL_OWN");  //LM05D_NAV_BACKUP_SELECTED  

   //COM ADC OWN 
   vdn.subscribe(m_inputsA429.in_ADCOWN.ADR_Label203,"A429_ADR1_3","Label_0203");
   vdn.subscribe(m_inputsA429.in_ADCOWN.ADR_Label204,"A429_ADR1_3","Label_0204");  //.Note - FOR BARO ALTITUDE FMGC 1 considers Label 204 FMGC 2 considers Label 220
   vdn.subscribe(m_inputsA429.in_ADCOWN.ADR_Label205,"A429_ADR1_3","Label_0205");
   vdn.subscribe(m_inputsA429.in_ADCOWN.ADR_Label206,"A429_ADR1_3","Label_0206");
   vdn.subscribe(m_inputsA429.in_ADCOWN.ADR_Label210,"A429_ADR1_3","Label_0210");
   vdn.subscribe(m_inputsA429.in_ADCOWN.ADR_Label211,"A429_ADR1_3","Label_0211");
   vdn.subscribe(m_inputsA429.in_ADCOWN.ADR_Label213,"A429_ADR1_3","Label_0213");
   vdn.subscribe(m_inputsA429.in_ADCOWN.ADR_Label234,"A429_ADR1_3","Label_0234");
   vdn.subscribe(m_inputsA429.in_ADCOWN.ADR_Label235,"A429_ADR1_3","Label_0235");
   vdn.subscribe(m_inputsA429.in_ADCOWN.ADR_Label236,"A429_ADR1_3","Label_0236");
   vdn.subscribe(m_inputsA429.in_ADCOWN.ADR_Label237,"A429_ADR1_3","Label_0237");
   vdn.subscribe(m_inputsA429.in_ADCOWN.ADR_Label220,"A429_ADR1_3","Label_0220");  //.Note - FOR BARO ALTITUDE FMGC 1 considers Label 204 FMGC 2 considers Label 220
   vdn.subscribe(m_inputsA429.in_ADCOWN.ADR_Label242,"A429_ADR1_3","Label_0242");
   vdn.subscribe(m_inputsA429.in_ADCOWN.ADR_Label246,"A429_ADR1_3","Label_0246");
   //MON ADC OPP
   vdn.subscribe(m_inputsA429.in_ADCOPP.ADR_Label203,"A429_ADR2_3","Label_0203");
   vdn.subscribe(m_inputsA429.in_ADCOPP.ADR_Label206,"A429_ADR2_3","Label_0206");
   vdn.subscribe(m_inputsA429.in_ADCOPP.ADR_Label210,"A429_ADR2_3","Label_0210");
   vdn.subscribe(m_inputsA429.in_ADCOPP.ADR_Label211,"A429_ADR2_3","Label_0211");
   vdn.subscribe(m_inputsA429.in_ADCOPP.ADR_Label241,"A429_ADR2_3","Label_0241");
   //COM ADC 3
   vdn.subscribe(m_inputsA429.in_ADC3.ADR_Label203,"A429_ADR3_3","Label_0203");
   vdn.subscribe(m_inputsA429.in_ADC3.ADR_Label204,"A429_ADR3_3","Label_0204");
   vdn.subscribe(m_inputsA429.in_ADC3.ADR_Label205,"A429_ADR3_3","Label_0205");
   vdn.subscribe(m_inputsA429.in_ADC3.ADR_Label206,"A429_ADR3_3","Label_0206");
   vdn.subscribe(m_inputsA429.in_ADC3.ADR_Label210,"A429_ADR3_3","Label_0210");
   vdn.subscribe(m_inputsA429.in_ADC3.ADR_Label211,"A429_ADR3_3","Label_0211");
   vdn.subscribe(m_inputsA429.in_ADC3.ADR_Label213,"A429_ADR3_3","Label_0213");
   vdn.subscribe(m_inputsA429.in_ADC3.ADR_Label234,"A429_ADR3_3","Label_0234");
   vdn.subscribe(m_inputsA429.in_ADC3.ADR_Label235,"A429_ADR3_3","Label_0235");
   vdn.subscribe(m_inputsA429.in_ADC3.ADR_Label236,"A429_ADR3_3","Label_0236");
   vdn.subscribe(m_inputsA429.in_ADC3.ADR_Label237,"A429_ADR3_3","Label_0237");                                               
   vdn.subscribe(m_inputsA429.in_ADC3.ADR_Label220,"A429_ADR3_3","Label_0220");  
   vdn.subscribe(m_inputsA429.in_ADC3.ADR_Label242,"A429_ADR3_3","Label_0242");
   vdn.subscribe(m_inputsA429.in_ADC3.ADR_Label246,"A429_ADR3_3","Label_0246");
   //subscribe A429-FAC Data.
   vdn.subscribe(m_inputsA429.in_FACOWN.FAC_Label070,"A429_FAC1_OWN","Label_0070");
   vdn.subscribe(m_inputsA429.in_FACOWN.FAC_Label071,"A429_FAC1_OWN","Label_0071");
   vdn.subscribe(m_inputsA429.in_FACOWN.FAC_Label074,"A429_FAC1_OWN","Label_0074");
   vdn.subscribe(m_inputsA429.in_FACOWN.FAC_Label076,"A429_FAC1_OWN","Label_0076");
   vdn.subscribe(m_inputsA429.in_FACOWN.FAC_Label146,"A429_FAC1_OWN","Label_0146");
   vdn.subscribe(m_inputsA429.in_FACOWN.FAC_Label207,"A429_FAC1_OWN","Label_0207");
   vdn.subscribe(m_inputsA429.in_FACOWN.FAC_Label245,"A429_FAC1_OWN","Label_0245");
   vdn.subscribe(m_inputsA429.in_FACOWN.FAC_Label263,"A429_FAC1_OWN","Label_0263");
   vdn.subscribe(m_inputsA429.in_FACOWN.FAC_Label264,"A429_FAC1_OWN","Label_0264");
   vdn.subscribe(m_inputsA429.in_FACOWN.FAC_Label265,"A429_FAC1_OWN","Label_0265");
   vdn.subscribe(m_inputsA429.in_FACOWN.FAC_Label266,"A429_FAC1_OWN","Label_0266");
   vdn.subscribe(m_inputsA429.in_FACOWN.FAC_Label267,"A429_FAC1_OWN","Label_0267");
   vdn.subscribe(m_inputsA429.in_FACOWN.FAC_Label274,"A429_FAC1_OWN","Label_0274");
   vdn.subscribe(m_inputsA429.in_FACOWN.FAC_Label072,"A429_FAC1_OWN","Label_0072");   
   vdn.subscribe(m_inputsA429.in_FACOWN.FAC_Label073,"A429_FAC1_OWN","Label_0073");   
   vdn.subscribe(m_inputsA429.in_FACOWN.FAC_Label125,"A429_FAC1_OWN","Label_0125");  
   vdn.subscribe(m_inputsA429.in_FACOWN.FAC_Label260,"A429_FAC1_OWN","Label_0260");   
   vdn.subscribe(m_inputsA429.in_FACOWN.FAC_Label271,"A429_FAC1_OWN","Label_0271");   
   vdn.subscribe(m_inputsA429.in_FACOWN.FAC_Label272,"A429_FAC1_OWN","Label_0272");  
   vdn.subscribe(m_inputsA429.in_FACOWN.FAC_Label313,"A429_FAC1_OWN","Label_0313");  
   vdn.subscribe(m_inputsA429.in_FACOWN.FAC_Label352,"A429_FAC1_OWN","Label_0352");
   //vdn.subscribe(m_inputsA429.in_FACOWN.FAC_FG_VAL, "Fac1", "FAC_Hlty_Com_Own_Lm_13d");

   vdn.subscribe(m_inputsA429.in_FACOPP.FAC_Label070,"A429_FAC2_OWN","Label_0070");
   vdn.subscribe(m_inputsA429.in_FACOPP.FAC_Label071,"A429_FAC2_OWN","Label_0071");
   vdn.subscribe(m_inputsA429.in_FACOPP.FAC_Label074,"A429_FAC2_OWN","Label_0074");
   vdn.subscribe(m_inputsA429.in_FACOPP.FAC_Label076,"A429_FAC2_OWN","Label_0076");
   vdn.subscribe(m_inputsA429.in_FACOPP.FAC_Label146,"A429_FAC2_OWN","Label_0146");
   vdn.subscribe(m_inputsA429.in_FACOPP.FAC_Label207,"A429_FAC2_OWN","Label_0207");
   vdn.subscribe(m_inputsA429.in_FACOPP.FAC_Label245,"A429_FAC2_OWN","Label_0245");
   vdn.subscribe(m_inputsA429.in_FACOPP.FAC_Label263,"A429_FAC2_OWN","Label_0263");
   vdn.subscribe(m_inputsA429.in_FACOPP.FAC_Label264,"A429_FAC2_OWN","Label_0264");
   vdn.subscribe(m_inputsA429.in_FACOPP.FAC_Label265,"A429_FAC2_OWN","Label_0265");
   vdn.subscribe(m_inputsA429.in_FACOPP.FAC_Label266,"A429_FAC2_OWN","Label_0266");
   vdn.subscribe(m_inputsA429.in_FACOPP.FAC_Label267,"A429_FAC2_OWN","Label_0267");
   vdn.subscribe(m_inputsA429.in_FACOPP.FAC_Label274,"A429_FAC2_OWN","Label_0274");
   vdn.subscribe(m_inputsA429.in_FACOPP.FAC_Label072,"A429_FAC2_OWN","Label_0072");   
   vdn.subscribe(m_inputsA429.in_FACOPP.FAC_Label073,"A429_FAC2_OWN","Label_0073");   
   vdn.subscribe(m_inputsA429.in_FACOPP.FAC_Label125,"A429_FAC2_OWN","Label_0125");  
   vdn.subscribe(m_inputsA429.in_FACOPP.FAC_Label260,"A429_FAC2_OWN","Label_0260");   
   vdn.subscribe(m_inputsA429.in_FACOPP.FAC_Label271,"A429_FAC2_OWN","Label_0271");   
   vdn.subscribe(m_inputsA429.in_FACOPP.FAC_Label272,"A429_FAC2_OWN","Label_0272");  
   vdn.subscribe(m_inputsA429.in_FACOPP.FAC_Label313,"A429_FAC2_OWN","Label_0313");  
   vdn.subscribe(m_inputsA429.in_FACOPP.FAC_Label352,"A429_FAC2_OWN","Label_0352");
   //vdn.subscribe(m_inputsA429.in_FACOPP.FAC_FG_VAL, "Fac2", "FAC_Hlty_Com_Own_Lm_13d");
   //subscribe A429-FADEC OWN A data.
   vdn.subscribe(m_inputsA429.in_FADOWNA.Label_046,"A429_EEC1_1A_MALF","Label_0046");
   vdn.subscribe(m_inputsA429.in_FADOWNA.Label_047,"A429_EEC1_1A_MALF","Label_0047");
   vdn.subscribe(m_inputsA429.in_FADOWNA.Label_133,"A429_EEC1_1A_MALF","Label_0133");
   vdn.subscribe(m_inputsA429.in_FADOWNA.Label_166,"A429_EEC1_1A_MALF","Label_0166");
   vdn.subscribe(m_inputsA429.in_FADOWNA.Label_244,"A429_EEC1_1A_MALF","Label_0244");
   vdn.subscribe(m_inputsA429.in_FADOWNA.Label_270,"A429_EEC1_1A_MALF","Label_0270");
   vdn.subscribe(m_inputsA429.in_FADOWNA.Label_271,"A429_EEC1_1A_MALF","Label_0271");
   vdn.subscribe(m_inputsA429.in_FADOWNA.Label_272,"A429_EEC1_1A_MALF","Label_0272"); 
   vdn.subscribe(m_inputsA429.in_FADOWNA.Label_337,"A429_EEC1_1A_MALF","Label_0337");
   vdn.subscribe(m_inputsA429.in_FADOWNA.Label_341,"A429_EEC1_1A_MALF","Label_0341");
   vdn.subscribe(m_inputsA429.in_FADOWNA.Label_342,"A429_EEC1_1A_MALF","Label_0342");
   vdn.subscribe(m_inputsA429.in_FADOWNA.Label_343,"A429_EEC1_1A_MALF","Label_0343");    
   //FADEC OWN B 
   vdn.subscribe(m_inputsA429.in_FADOWNB.Label_046,"A429_EEC1_1B","Label_0046");
   vdn.subscribe(m_inputsA429.in_FADOWNB.Label_047,"A429_EEC1_1B","Label_0047");
   vdn.subscribe(m_inputsA429.in_FADOWNB.Label_133,"A429_EEC1_1B","Label_0133");
   vdn.subscribe(m_inputsA429.in_FADOWNB.Label_166,"A429_EEC1_1B","Label_0166");
   vdn.subscribe(m_inputsA429.in_FADOWNB.Label_244,"A429_EEC1_1B","Label_0244");
   vdn.subscribe(m_inputsA429.in_FADOWNB.Label_270,"A429_EEC1_1B","Label_0270");
   vdn.subscribe(m_inputsA429.in_FADOWNB.Label_271,"A429_EEC1_1B","Label_0271");
   vdn.subscribe(m_inputsA429.in_FADOWNB.Label_272,"A429_EEC1_1B","Label_0272");
   vdn.subscribe(m_inputsA429.in_FADOWNB.Label_337,"A429_EEC1_1B","Label_0337");
   vdn.subscribe(m_inputsA429.in_FADOWNB.Label_341,"A429_EEC1_1B","Label_0341");
   vdn.subscribe(m_inputsA429.in_FADOWNB.Label_342,"A429_EEC1_1B","Label_0342");
   vdn.subscribe(m_inputsA429.in_FADOWNB.Label_343,"A429_EEC1_1B","Label_0343");   
   //m_inputsA429.in_FADOWN.FADEC_bus.registerLabel(m_inputsA429.in_FADOWN.FADOWN_FG_VAL,""); 
   //FADEC OPP A/B

   vdn.subscribe(m_inputsA429.in_FADOPPA.Label_133,"A429_EEC2_1A_MALF","Label_0133");
   vdn.subscribe(m_inputsA429.in_FADOPPA.Label_166,"A429_EEC2_1A_MALF","Label_0166");
   vdn.subscribe(m_inputsA429.in_FADOPPA.Label_244,"A429_EEC2_1A_MALF","Label_0244");
   vdn.subscribe(m_inputsA429.in_FADOPPA.Label_270,"A429_EEC2_1A_MALF","Label_0270");
   vdn.subscribe(m_inputsA429.in_FADOPPA.Label_271,"A429_EEC2_1A_MALF","Label_0271");
   vdn.subscribe(m_inputsA429.in_FADOPPA.Label_272,"A429_EEC2_1A_MALF","Label_0272");
   vdn.subscribe(m_inputsA429.in_FADOPPA.Label_337,"A429_EEC2_1A_MALF","Label_0337");
   vdn.subscribe(m_inputsA429.in_FADOPPA.Label_341,"A429_EEC2_1A_MALF","Label_0341");
   vdn.subscribe(m_inputsA429.in_FADOPPA.Label_342,"A429_EEC2_1A_MALF","Label_0342");
   vdn.subscribe(m_inputsA429.in_FADOPPA.Label_343,"A429_EEC2_1A_MALF","Label_0343");
   
   vdn.subscribe(m_inputsA429.in_FADOPPB.Label_133,"A429_EEC2_1B","Label_0133");
   vdn.subscribe(m_inputsA429.in_FADOPPB.Label_166,"A429_EEC2_1B","Label_0166");
   vdn.subscribe(m_inputsA429.in_FADOPPB.Label_244,"A429_EEC2_1B","Label_0244");
   vdn.subscribe(m_inputsA429.in_FADOPPB.Label_270,"A429_EEC2_1B","Label_0270");
   vdn.subscribe(m_inputsA429.in_FADOPPB.Label_271,"A429_EEC2_1B","Label_0271");
   vdn.subscribe(m_inputsA429.in_FADOPPB.Label_272,"A429_EEC2_1B","Label_0272");
   vdn.subscribe(m_inputsA429.in_FADOPPB.Label_337,"A429_EEC2_1B","Label_0337");
   vdn.subscribe(m_inputsA429.in_FADOPPB.Label_341,"A429_EEC2_1B","Label_0341");
   vdn.subscribe(m_inputsA429.in_FADOPPB.Label_342,"A429_EEC2_1B","Label_0342");
   vdn.subscribe(m_inputsA429.in_FADOPPB.Label_343,"A429_EEC2_1B","Label_0343");
   //FADEC CFM ONLY
   if(cfm_engine)
   {
      vdn.subscribe(m_inputsA429.in_FADOWNA.Label_066,"A429_EEC1_1A_MALF","Label_0066"); //FADEC DISCRETE WORD 8  DW   (CFM only)
      vdn.subscribe(m_inputsA429.in_FADOWNA.Label_120,"A429_EEC1_1A_MALF","Label_0120"); //FADEC DISCRETE WORD 9  DW   (CFM only)
      vdn.subscribe(m_inputsA429.in_FADOWNA.Label_273,"A429_EEC1_1A_MALF","Label_0273"); //FADEC DISCRETE WORD 1
      vdn.subscribe(m_inputsA429.in_FADOWNA.Label_275,"A429_EEC1_1A_MALF","Label_0275"); //FADEC DISCRETE WORD 5       (CFM only)
      vdn.subscribe(m_inputsA429.in_FADOWNA.Label_276,"A429_EEC1_1A_MALF","Label_0276"); //FADEC DISCRETE WORD 6       (CFM only)
      vdn.subscribe(m_inputsA429.in_FADOWNA.Label_277,"A429_EEC1_1A_MALF","Label_0277"); //FADEC DISCRETE WORD 10      (CFM only)
      vdn.subscribe(m_inputsA429.in_FADOWNA.Label_333,"A429_EEC1_1A_MALF","Label_0333"); //FADEC DISCRETE WORD 8       (CFM only) 
      vdn.subscribe(m_inputsA429.in_FADOWNA.Label_346,"A429_EEC1_1A_MALF","Label_0346");
      vdn.subscribe(m_inputsA429.in_FADOWNA.Label_355,"A429_EEC1_1A_MALF","Label_0355"); //FADEC DISCRETE WORD 7       (CFM only)

      vdn.subscribe(m_inputsA429.in_FADOWNB.Label_066,"A429_EEC1_1B","Label_0066");       //FADEC DISCRETE WORD 8  DW   (CFM only)
      vdn.subscribe(m_inputsA429.in_FADOWNB.Label_120,"A429_EEC1_1B","Label_0120");       //FADEC DISCRETE WORD 9  DW   (CFM only)
      vdn.subscribe(m_inputsA429.in_FADOWNB.Label_273,"A429_EEC1_1B","Label_0273");       //FADEC DISCRETE WORD 1
      vdn.subscribe(m_inputsA429.in_FADOWNB.Label_275,"A429_EEC1_1B","Label_0275");       //FADEC DISCRETE WORD 5       (CFM only)
      vdn.subscribe(m_inputsA429.in_FADOWNB.Label_276,"A429_EEC1_1B","Label_0276");       //FADEC DISCRETE WORD 6       (CFM only)
      vdn.subscribe(m_inputsA429.in_FADOWNB.Label_277,"A429_EEC1_1B","Label_0277");       //FADEC DISCRETE WORD 10      (CFM only)
      vdn.subscribe(m_inputsA429.in_FADOWNB.Label_333,"A429_EEC1_1B","Label_0333");       //FADEC DISCRETE WORD 8       (CFM only) 
      vdn.subscribe(m_inputsA429.in_FADOWNB.Label_346,"A429_EEC1_1B","Label_0346");
      vdn.subscribe(m_inputsA429.in_FADOWNB.Label_355,"A429_EEC1_1B","Label_0355");       //FADEC DISCRETE WORD 7       (CFM only)

      vdn.subscribe(m_inputsA429.in_FADOPPA.Label_066,"A429_EEC2_1A_MALF","Label_0066");
      vdn.subscribe(m_inputsA429.in_FADOPPA.Label_120,"A429_EEC2_1A_MALF","Label_0120");
      vdn.subscribe(m_inputsA429.in_FADOPPA.Label_273,"A429_EEC2_1A_MALF","Label_0273");
      vdn.subscribe(m_inputsA429.in_FADOPPA.Label_275,"A429_EEC2_1A_MALF","Label_0275");
      vdn.subscribe(m_inputsA429.in_FADOPPA.Label_276,"A429_EEC2_1A_MALF","Label_0276");
      vdn.subscribe(m_inputsA429.in_FADOPPA.Label_277,"A429_EEC2_1A_MALF","Label_0277");
      vdn.subscribe(m_inputsA429.in_FADOPPA.Label_333,"A429_EEC2_1A_MALF","Label_0333");
      vdn.subscribe(m_inputsA429.in_FADOPPA.Label_346,"A429_EEC2_1A_MALF","Label_0346");
      vdn.subscribe(m_inputsA429.in_FADOPPA.Label_355,"A429_EEC2_1A_MALF","Label_0355");

      vdn.subscribe(m_inputsA429.in_FADOPPB.Label_066,"A429_EEC2_1B","Label_0066");
      vdn.subscribe(m_inputsA429.in_FADOPPB.Label_120,"A429_EEC2_1B","Label_0120");
      vdn.subscribe(m_inputsA429.in_FADOPPB.Label_273,"A429_EEC2_1B","Label_0273");
      vdn.subscribe(m_inputsA429.in_FADOPPB.Label_275,"A429_EEC2_1B","Label_0275");
      vdn.subscribe(m_inputsA429.in_FADOPPB.Label_276,"A429_EEC2_1B","Label_0276");
      vdn.subscribe(m_inputsA429.in_FADOPPB.Label_277,"A429_EEC2_1B","Label_0277");
      vdn.subscribe(m_inputsA429.in_FADOPPB.Label_333,"A429_EEC2_1B","Label_0333");
      vdn.subscribe(m_inputsA429.in_FADOPPB.Label_346,"A429_EEC2_1B","Label_0346");
      vdn.subscribe(m_inputsA429.in_FADOPPB.Label_355,"A429_EEC2_1B","Label_0355");
   }
   //FADEC IAE ONLY
   if(iae_engine)
   {
      vdn.subscribe(m_inputsA429.in_FADOWNA.Label_146,"A429_EEC1_1A_MALF","Label_0146");   //(IAE only)
      vdn.subscribe(m_inputsA429.in_FADOWNA.Label_340,"A429_EEC1_1A_MALF","Label_0340");   //(IAE only)

      vdn.subscribe(m_inputsA429.in_FADOWNB.Label_146,"A429_EEC1_1B","Label_0146");        //(IAE only)
      vdn.subscribe(m_inputsA429.in_FADOWNB.Label_340,"A429_EEC1_1B","Label_0340");        //(IAE only)  

      vdn.subscribe(m_inputsA429.in_FADOPPA.Label_146,"A429_EEC2_1A_MALF","Label_0146");
      vdn.subscribe(m_inputsA429.in_FADOPPA.Label_340,"A429_EEC2_1A_MALF","Label_0340");

      vdn.subscribe(m_inputsA429.in_FADOPPB.Label_146,"A429_EEC2_1B","Label_0146");
      vdn.subscribe(m_inputsA429.in_FADOPPB.Label_340,"A429_EEC2_1B","Label_0340");
   }
   //FADEC PW ONLY 
   if(pw_engine)
   {
      vdn.subscribe(m_inputsA429.in_FADOWNA.Label_124,"A429_EEC1_1A_MALF","Label_0124"); 
      vdn.subscribe(m_inputsA429.in_FADOWNA.Label_145,"A429_EEC1_1A_MALF","Label_0145"); 
      vdn.subscribe(m_inputsA429.in_FADOWNA.Label_146,"A429_EEC1_1A_MALF","Label_0146"); 
      vdn.subscribe(m_inputsA429.in_FADOWNA.Label_346,"A429_EEC1_1A_MALF","Label_0346");

      vdn.subscribe(m_inputsA429.in_FADOWNB.Label_124,"A429_EEC1_1B","Label_0124"); 
      vdn.subscribe(m_inputsA429.in_FADOWNB.Label_145,"A429_EEC1_1B","Label_0145"); 
      vdn.subscribe(m_inputsA429.in_FADOWNB.Label_146,"A429_EEC1_1B","Label_0146"); 
      vdn.subscribe(m_inputsA429.in_FADOWNB.Label_346,"A429_EEC1_1B","Label_0346");

      vdn.subscribe(m_inputsA429.in_FADOPPA.Label_124,"A429_EEC2_1A_MALF","Label_0124");
      vdn.subscribe(m_inputsA429.in_FADOPPA.Label_145,"A429_EEC2_1A_MALF","Label_0145");
      vdn.subscribe(m_inputsA429.in_FADOPPA.Label_146,"A429_EEC2_1A_MALF","Label_0146");
      vdn.subscribe(m_inputsA429.in_FADOPPA.Label_346,"A429_EEC2_1A_MALF","Label_0346");

      vdn.subscribe(m_inputsA429.in_FADOPPB.Label_124,"A429_EEC2_1B","Label_0124");
      vdn.subscribe(m_inputsA429.in_FADOPPB.Label_145,"A429_EEC2_1B","Label_0145");
      vdn.subscribe(m_inputsA429.in_FADOPPB.Label_146,"A429_EEC2_1B","Label_0146");
      vdn.subscribe(m_inputsA429.in_FADOPPB.Label_346,"A429_EEC2_1B","Label_0346");
   }
   
   //FCU  
   vdn.subscribe(m_inputsA429.in_FCUOWNA.Label_104,"A429_FCU_1A_MOD","Label_0104");  
   vdn.subscribe(m_inputsA429.in_FCUOWNA.Label_103,"A429_FCU_1A_MOD","Label_0103");  
   vdn.subscribe(m_inputsA429.in_FCUOWNA.Label_106,"A429_FCU_1A_MOD","Label_0106");  
   vdn.subscribe(m_inputsA429.in_FCUOWNA.Label_271,"A429_FCU_1A_MOD","Label_0271");  
   vdn.subscribe(m_inputsA429.in_FCUOWNA.Label_272,"A429_FCU_1A_MOD","Label_0272");  
   vdn.subscribe(m_inputsA429.in_FCUOWNA.Label_274,"A429_FCU_1A_MOD","Label_0274");  
   vdn.subscribe(m_inputsA429.in_FCUOWNA.Label_273,"A429_FCU_1A_MOD","Label_0273");
   vdn.subscribe(m_inputsA429.in_FCUOWNA.Label_214,"A429_FCU_1A_MOD","Label_0214"); 
   vdn.subscribe(m_inputsA429.in_FCUOWNA.Label_270,"A429_FCU_1A_MOD","Label_0270");  
   vdn.subscribe(m_inputsA429.in_FCUOWNA.Label_343,"A429_FCU_1A_MOD","Label_0343");
   vdn.subscribe(m_inputsA429.in_FCUOWNA.Label_350,"A429_FCU_1A_MOD","Label_0350");
   vdn.subscribe(m_inputsA429.in_FCUOWNA.Label_351,"A429_FCU_1A_MOD","Label_0351");
   vdn.subscribe(m_inputsA429.in_FCUOWNA.Label_352,"A429_FCU_1A_MOD","Label_0352");
   vdn.subscribe(m_inputsA429.in_FCUOWNA.Label_353,"A429_FCU_1A_MOD","Label_0353");
   vdn.subscribe(m_inputsA429.in_FCUOWNA.Label_354,"A429_FCU_1A_MOD","Label_0354"); 
   vdn.subscribe(m_inputsA429.in_FCUOWNA.Label_010,"A429_FCU_1A_MOD","Label_0010"); 
   //vdn.subscribe(m_inputsA429.in_FCUOWNA.Label_002,"A429_FCU_1A_MOD","Label_0002");	//	check if FCU outputs these label TODO
   //vdn.subscribe(m_inputsA429.in_FCUOWNA.Label_003,"A429_FCU_1A_MOD","Label_0003");	//	check if FCU outputs these label TODO
   //vdn.subscribe(m_inputsA429.in_FCUOWNA.Label_005,"A429_FCU_1A_MOD","Label_0005");	//	check if FCU outputs these label TODO
   //vdn.subscribe(m_inputsA429.in_FCUOWNA.Label_007,"A429_FCU_1A_MOD","Label_0007");	//	check if FCU outputs these label TODO

   vdn.subscribe(m_inputsA429.in_FCUOWNB.Label_101,"A429_FCU_1B_MOD","Label_0101");
   vdn.subscribe(m_inputsA429.in_FCUOWNB.Label_102,"A429_FCU_1B_MOD","Label_0102");
   vdn.subscribe(m_inputsA429.in_FCUOWNB.Label_114,"A429_FCU_1B_MOD","Label_0114");
   vdn.subscribe(m_inputsA429.in_FCUOWNB.Label_115,"A429_FCU_1B_MOD","Label_0115");
   vdn.subscribe(m_inputsA429.in_FCUOWNB.Label_271,"A429_FCU_1B_MOD","Label_0271");
   vdn.subscribe(m_inputsA429.in_FCUOWNB.Label_272,"A429_FCU_1B_MOD","Label_0272");
 
   //FQI  FQI1: A429_FQIC_1A  A429_FQIC_1B       FQI2: A429_FQIC_2A  A429_FQIC_2B
   vdn.subscribe(m_inputsA429.in_FQI.Label_247,"A429_FQIC_1B","Label_0247");         
   vdn.subscribe(m_inputsA429.in_FQI.Label_256,"A429_FQIC_1B","Label_0256");
   vdn.subscribe(m_inputsA429.in_FQI.Label_257,"A429_FQIC_1B","Label_0257");
   vdn.subscribe(m_inputsA429.in_FQI.Label_260,"A429_FQIC_1B","Label_0260");
   vdn.subscribe(m_inputsA429.in_FQI.Label_261,"A429_FQIC_1B","Label_0261");
   vdn.subscribe(m_inputsA429.in_FQI.Label_262,"A429_FQIC_1B","Label_0262");
   vdn.subscribe(m_inputsA429.in_FQI.Label_263,"A429_FQIC_1B","Label_0263");   //not published by X320
   vdn.subscribe(m_inputsA429.in_FQI.Label_264,"A429_FQIC_1B","Label_0264");   //not published by X320
   vdn.subscribe(m_inputsA429.in_FQI.Label_265,"A429_FQIC_1B","Label_0265");   //not published by X320
   vdn.subscribe(m_inputsA429.in_FQI.Label_310,"A429_FQIC_1B","Label_0310");   //not published by X320
   vdn.subscribe(m_inputsA429.in_FQI.Label_311,"A429_FQIC_1B","Label_0311");   //not published by X320
   vdn.subscribe(m_inputsA429.in_FQI.Label_312,"A429_FQIC_1B","Label_0312");   
   vdn.subscribe(m_inputsA429.in_FQI.Label_313,"A429_FQIC_1B","Label_0313");   

   //LS  LS1:A429_MMR1_ILS_1 A429_MMR1_ILS_2 LS2:A429_MMR2_ILS_1 A429_MMR2_ILS_2
   vdn.subscribe(m_inputsA429.in_LSOWN.Label_173,"A429_MMR1_ILS_1","Label_0173");
   vdn.subscribe(m_inputsA429.in_LSOWN.Label_033,"A429_MMR1_ILS_1","Label_0033");
   vdn.subscribe(m_inputsA429.in_LSOWN.Label_263,"A429_MMR1_ILS_1","Label_0263");
   vdn.subscribe(m_inputsA429.in_LSOWN.Label_264,"A429_MMR1_ILS_1","Label_0264");
   vdn.subscribe(m_inputsA429.in_LSOWN.Label_105,"A429_MMR1_ILS_1","Label_0105");
   vdn.subscribe(m_inputsA429.in_LSOWN.Label_174,"A429_MMR1_ILS_1","Label_0174");
   vdn.subscribe(m_inputsA429.in_LSOWN.Label_271,"A429_MMR1_ILS_1","Label_0271");
   
   vdn.subscribe(m_inputsA429.in_LSOPP.Label_173,"A429_MMR2_ILS_1","Label_0173");
   vdn.subscribe(m_inputsA429.in_LSOPP.Label_033,"A429_MMR2_ILS_1","Label_0033");
   vdn.subscribe(m_inputsA429.in_LSOPP.Label_263,"A429_MMR2_ILS_1","Label_0263");
   vdn.subscribe(m_inputsA429.in_LSOPP.Label_264,"A429_MMR2_ILS_1","Label_0264");
   vdn.subscribe(m_inputsA429.in_LSOPP.Label_105,"A429_MMR2_ILS_1","Label_0105");
   vdn.subscribe(m_inputsA429.in_LSOPP.Label_174,"A429_MMR2_ILS_1","Label_0174");
   vdn.subscribe(m_inputsA429.in_LSOPP.Label_271,"A429_MMR2_ILS_1","Label_0271");

   //MLS    Parameter not acquired by HI.But FM seems to need that input

   //IRS
   //IRS OWN all FM
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_175,"A429_IR1_3_MALF","Label_0175");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_247,"A429_IR1_3_MALF","Label_0247");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_254,"A429_IR1_3_MALF","Label_0254");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_255,"A429_IR1_3_MALF","Label_0255");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_264,"A429_IR1_3_MALF","Label_0264");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_266,"A429_IR1_3_MALF","Label_0266");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_267,"A429_IR1_3_MALF","Label_0267");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_273,"A429_IR1_3_MALF","Label_0273");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_274,"A429_IR1_3_MALF","Label_0274");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_310,"A429_IR1_3_MALF","Label_0310");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_311,"A429_IR1_3_MALF","Label_0311");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_366,"A429_IR1_3_MALF","Label_0366");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_367,"A429_IR1_3_MALF","Label_0367");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_270,"A429_IR1_3_MALF","Label_0270");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_041,"A429_IR1_3_MALF","Label_0041");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_042,"A429_IR1_3_MALF","Label_0042");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_103,"A429_IR1_3_MALF","Label_0103");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_110,"A429_IR1_3_MALF","Label_0110");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_111,"A429_IR1_3_MALF","Label_0111");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_112,"A429_IR1_3_MALF","Label_0112");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_137,"A429_IR1_3_MALF","Label_0137");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_076,"A429_IR1_3_MALF","Label_0076");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_130,"A429_IR1_3_MALF","Label_0130");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_131,"A429_IR1_3_MALF","Label_0131");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_150,"A429_IR1_3_MALF","Label_0150");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_162,"A429_IR1_3_MALF","Label_0162");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_163,"A429_IR1_3_MALF","Label_0163");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_256,"A429_IR1_3_MALF","Label_0256");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_257,"A429_IR1_3_MALF","Label_0257");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_260,"A429_IR1_3_MALF","Label_0260");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_261,"A429_IR1_3_MALF","Label_0261");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_343,"A429_IR1_3_MALF","Label_0343");    //not published by IRS
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_347,"A429_IR1_3_MALF","Label_0347");    //not published by IRS

   //par FG
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_314,"A429_IR1_3_MALF","Label_0314");    //TRUE HDG DEG       
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_323,"A429_IR1_3_MALF","Label_0323");    //FP ACCEL g         
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_322,"A429_IR1_3_MALF","Label_0322");    //FP ANGLE DEG       
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_324,"A429_IR1_3_MALF","Label_0324");    //PITCH ANGLE DEG    
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_325,"A429_IR1_3_MALF","Label_0325");    //ROLL ANGLE DEG     
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_364,"A429_IR1_3_MALF","Label_0364");    //VERT ACCEL g       
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_330,"A429_IR1_3_MALF","Label_0330");    //BODY YAWRATE DEG/S 
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_361,"A429_IR1_3_MALF","Label_0361");    //INERTIAL ALT FT    
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_365,"A429_IR1_3_MALF","Label_0365");    //INERTIAL V/S FT/MN 
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_320,"A429_IR1_3_MALF","Label_0320");    //MAG HDG DEG
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_301,"A429_IR1_3_MALF","Label_0301");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_313,"A429_IR1_3_MALF","Label_0313");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_315,"A429_IR1_3_MALF","Label_0315");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_316,"A429_IR1_3_MALF","Label_0316");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_317,"A429_IR1_3_MALF","Label_0317");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_332,"A429_IR1_3_MALF","Label_0332");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_336,"A429_IR1_3_MALF","Label_0336");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_337,"A429_IR1_3_MALF","Label_0337");

   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_010,"A429_IR1_3_MALF","Label_0010");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_011,"A429_IR1_3_MALF","Label_0011");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_012,"A429_IR1_3_MALF","Label_0012");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_013,"A429_IR1_3_MALF","Label_0013");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_014,"A429_IR1_3_MALF","Label_0014");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_015,"A429_IR1_3_MALF","Label_0015");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_016,"A429_IR1_3_MALF","Label_0016");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_043,"A429_IR1_3_MALF","Label_0043");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_044,"A429_IR1_3_MALF","Label_0044");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_312,"A429_IR1_3_MALF","Label_0312");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_334,"A429_IR1_3_MALF","Label_0334");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_350,"A429_IR1_3_MALF","Label_0350");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_275,"A429_IR1_3_MALF","Label_0275");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_301,"A429_IR1_3_MALF","Label_0301");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_313,"A429_IR1_3_MALF","Label_0313");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_315,"A429_IR1_3_MALF","Label_0315");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_316,"A429_IR1_3_MALF","Label_0316");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_317,"A429_IR1_3_MALF","Label_0317");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_332,"A429_IR1_3_MALF","Label_0332");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_336,"A429_IR1_3_MALF","Label_0336");
   vdn.subscribe(m_inputsA429.in_IRSOWN.Label_337,"A429_IR1_3_MALF","Label_0337");

   //IRS OPP
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_310,"A429_IR2_3_MALF","Label_0310");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_311,"A429_IR2_3_MALF","Label_0311");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_366,"A429_IR2_3_MALF","Label_0366");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_367,"A429_IR2_3_MALF","Label_0367");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_270,"A429_IR2_3_MALF","Label_0270");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_041,"A429_IR2_3_MALF","Label_0041");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_042,"A429_IR2_3_MALF","Label_0042");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_103,"A429_IR2_3_MALF","Label_0103");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_110,"A429_IR2_3_MALF","Label_0110");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_111,"A429_IR2_3_MALF","Label_0111");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_112,"A429_IR2_3_MALF","Label_0112");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_137,"A429_IR2_3_MALF","Label_0137");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_175,"A429_IR2_3_MALF","Label_0175");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_247,"A429_IR2_3_MALF","Label_0247");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_254,"A429_IR2_3_MALF","Label_0254");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_255,"A429_IR2_3_MALF","Label_0255");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_264,"A429_IR2_3_MALF","Label_0264");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_266,"A429_IR2_3_MALF","Label_0266");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_267,"A429_IR2_3_MALF","Label_0267");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_273,"A429_IR2_3_MALF","Label_0273");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_274,"A429_IR2_3_MALF","Label_0274");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_076,"A429_IR2_3_MALF","Label_0076");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_130,"A429_IR2_3_MALF","Label_0130");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_131,"A429_IR2_3_MALF","Label_0131");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_150,"A429_IR2_3_MALF","Label_0150");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_162,"A429_IR2_3_MALF","Label_0162");  //not published by IRS
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_163,"A429_IR2_3_MALF","Label_0163");  //not published by IRS
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_256,"A429_IR2_3_MALF","Label_0256");  
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_257,"A429_IR2_3_MALF","Label_0257");  
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_260,"A429_IR2_3_MALF","Label_0260");  
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_261,"A429_IR2_3_MALF","Label_0261");  
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_343,"A429_IR2_3_MALF","Label_0343");  //not published by IRS
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_347,"A429_IR2_3_MALF","Label_0347");  //not published by IRS
   //par FG
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_314,"A429_IR2_3_MALF","Label_0314");  //TRUE HDG DEG       
   //vdn.subscribe(m_inputsA429.in_IRSOPP.Label_323,"A429_IR2_3","Label_0323");     //FP ACCEL g         
   //vdn.subscribe(m_inputsA429.in_IRSOPP.Label_322,"A429_IR2_3","Label_0322");     //FP ANGLE DEG       
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_324,"A429_IR2_3_MALF","Label_0324");  //PITCH ANGLE DEG    
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_325,"A429_IR2_3_MALF","Label_0325");  //ROLL ANGLE DEG     
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_364,"A429_IR2_3_MALF","Label_0364");  //VERT ACCEL g       
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_330,"A429_IR2_3_MALF","Label_0330");  //BODY YAWRATE DEG/S 
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_361,"A429_IR2_3_MALF","Label_0361");  //INERTIAL ALT FT    
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_365,"A429_IR2_3_MALF","Label_0365");  //INERTIAL V/S FT/MN 
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_320,"A429_IR2_3_MALF","Label_0320");  //MAG HDG DEG
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_301,"A429_IR2_3_MALF","Label_0301");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_313,"A429_IR2_3_MALF","Label_0313");
   //vdn.subscribe(m_inputsA429.in_IRSOPP.Label_315,"A429_IR2_3","Label_0315");
   //vdn.subscribe(m_inputsA429.in_IRSOPP.Label_316,"A429_IR2_3","Label_0316");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_317,"A429_IR2_3_MLAF","Label_0317");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_332,"A429_IR2_3_MLAF","Label_0332");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_336,"A429_IR2_3_MLAF","Label_0336");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_337,"A429_IR2_3_MLAF","Label_0337");

   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_010,"A429_IR2_3_MALF","Label_0010");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_011,"A429_IR2_3_MALF","Label_0011");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_012,"A429_IR2_3_MALF","Label_0012");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_013,"A429_IR2_3_MALF","Label_0013");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_014,"A429_IR2_3_MALF","Label_0014");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_015,"A429_IR2_3_MALF","Label_0015");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_016,"A429_IR2_3_MALF","Label_0016");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_043,"A429_IR2_3_MALF","Label_0043");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_044,"A429_IR2_3_MALF","Label_0044");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_312,"A429_IR2_3_MALF","Label_0312");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_334,"A429_IR2_3_MALF","Label_0334");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_350,"A429_IR2_3_MALF","Label_0350");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_275,"A429_IR2_3_MALF","Label_0275");
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_301,"A429_IR2_3_MALF","Label_0301");  /*added according to ICD. not include in CM.*/
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_313,"A429_IR2_3_MALF","Label_0313");  /*added according to ICD. not include in CM.*/
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_317,"A429_IR2_3_MALF","Label_0317");  /*added according to ICD. not include in CM.*/
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_332,"A429_IR2_3_MALF","Label_0332");  /*added according to ICD. not include in CM.*/
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_336,"A429_IR2_3_MALF","Label_0336");  /*added according to ICD. not include in CM.*/
   vdn.subscribe(m_inputsA429.in_IRSOPP.Label_337,"A429_IR2_3_MALF","Label_0337");  /*added according to ICD. not include in CM.*/

   //IRS 3
   vdn.subscribe(m_inputsA429.in_IRS3.Label_310,"A429_IR3_3_MALF","Label_0310");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_311,"A429_IR3_3_MALF","Label_0311");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_366,"A429_IR3_3_MALF","Label_0366");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_367,"A429_IR3_3_MALF","Label_0367");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_270,"A429_IR3_3_MALF","Label_0270");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_041,"A429_IR3_3_MALF","Label_0041");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_042,"A429_IR3_3_MALF","Label_0042");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_103,"A429_IR3_3_MALF","Label_0103");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_110,"A429_IR3_3_MALF","Label_0110");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_111,"A429_IR3_3_MALF","Label_0111");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_112,"A429_IR3_3_MALF","Label_0112");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_137,"A429_IR3_3_MALF","Label_0137");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_175,"A429_IR3_3_MALF","Label_0175");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_247,"A429_IR3_3_MALF","Label_0247");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_254,"A429_IR3_3_MALF","Label_0254");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_255,"A429_IR3_3_MALF","Label_0255");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_264,"A429_IR3_3_MALF","Label_0264");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_266,"A429_IR3_3_MALF","Label_0266");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_267,"A429_IR3_3_MALF","Label_0267");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_273,"A429_IR3_3_MALF","Label_0273");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_274,"A429_IR3_3_MALF","Label_0274");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_076,"A429_IR3_3_MALF","Label_0076");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_130,"A429_IR3_3_MALF","Label_0130");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_131,"A429_IR3_3_MALF","Label_0131");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_150,"A429_IR3_3_MALF","Label_0150");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_162,"A429_IR3_3_MALF","Label_0162");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_163,"A429_IR3_3_MALF","Label_0163");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_256,"A429_IR3_3_MALF","Label_0256");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_257,"A429_IR3_3_MALF","Label_0257");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_260,"A429_IR3_3_MALF","Label_0260");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_261,"A429_IR3_3_MALF","Label_0261");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_343,"A429_IR3_3_MALF","Label_0343");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_347,"A429_IR3_3_MALF","Label_0347");
   //par FG
   vdn.subscribe(m_inputsA429.in_IRS3.Label_314,"A429_IR3_3_MALF","Label_0314");   //TRUE HDG DEG       
   vdn.subscribe(m_inputsA429.in_IRS3.Label_323,"A429_IR3_3_MALF","Label_0323");   //FP ACCEL g         
   vdn.subscribe(m_inputsA429.in_IRS3.Label_322,"A429_IR3_3_MALF","Label_0322");   //FP ANGLE DEG       
   vdn.subscribe(m_inputsA429.in_IRS3.Label_324,"A429_IR3_3_MALF","Label_0324");   //PITCH ANGLE DEG    
   vdn.subscribe(m_inputsA429.in_IRS3.Label_325,"A429_IR3_3_MALF","Label_0325");   //ROLL ANGLE DEG     
   vdn.subscribe(m_inputsA429.in_IRS3.Label_364,"A429_IR3_3_MALF","Label_0364");   //VERT ACCEL g       
   vdn.subscribe(m_inputsA429.in_IRS3.Label_330,"A429_IR3_3_MALF","Label_0330");   //BODY YAWRATE DEG/S 
   vdn.subscribe(m_inputsA429.in_IRS3.Label_361,"A429_IR3_3_MALF","Label_0361");   //INERTIAL ALT FT    
   vdn.subscribe(m_inputsA429.in_IRS3.Label_365,"A429_IR3_3_MALF","Label_0365");   //INERTIAL V/S FT/MN 
   vdn.subscribe(m_inputsA429.in_IRS3.Label_320,"A429_IR3_3_MALF","Label_0320");   //MAG HDG DEG 

   vdn.subscribe(m_inputsA429.in_IRS3.Label_010,"A429_IR3_3_MALF","Label_0010");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_011,"A429_IR3_3_MALF","Label_0011");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_012,"A429_IR3_3_MALF","Label_0012");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_013,"A429_IR3_3_MALF","Label_0013");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_014,"A429_IR3_3_MALF","Label_0014");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_015,"A429_IR3_3_MALF","Label_0015");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_016,"A429_IR3_3_MALF","Label_0016");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_043,"A429_IR3_3_MALF","Label_0043");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_044,"A429_IR3_3_MALF","Label_0044");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_312,"A429_IR3_3_MALF","Label_0312");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_334,"A429_IR3_3_MALF","Label_0334");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_350,"A429_IR3_3_MALF","Label_0350");
   vdn.subscribe(m_inputsA429.in_IRS3.Label_275,"A429_IR3_3_MALF","Label_0275");

   vdn.subscribe(m_inputsA429.in_IRS3.Label_301,"A429_IR3_3_MALF","Label_0301");   //added according to ICD. not include in CM.
   vdn.subscribe(m_inputsA429.in_IRS3.Label_313,"A429_IR3_3_MALF","Label_0313");   //added according to ICD. not include in CM.
   vdn.subscribe(m_inputsA429.in_IRS3.Label_315,"A429_IR3_3_MALF","Label_0315");   //added according to ICD. not include in CM.
   vdn.subscribe(m_inputsA429.in_IRS3.Label_316,"A429_IR3_3_MALF","Label_0316");   //added according to ICD. not include in CM.
   vdn.subscribe(m_inputsA429.in_IRS3.Label_317,"A429_IR3_3_MALF","Label_0317");   //added according to ICD. not include in CM.
   vdn.subscribe(m_inputsA429.in_IRS3.Label_332,"A429_IR3_3_MALF","Label_0332");   //added according to ICD. not include in CM.
   vdn.subscribe(m_inputsA429.in_IRS3.Label_336,"A429_IR3_3_MALF","Label_0336");   //added according to ICD. not include in CM.
   vdn.subscribe(m_inputsA429.in_IRS3.Label_337,"A429_IR3_3_MALF","Label_0337");   //added according to ICD. not include in CM.


   //RA OWN
   vdn.subscribe(m_inputsA429.in_RAOWN.Label_164,"A429_RA1_1_MALF","Label_0164");   
   //RA OPP
   vdn.subscribe(m_inputsA429.in_RAOPP.Label_164,"A429_RA2_1_MALF","Label_0164");  
   //VOR OWN
   vdn.subscribe(m_inputsA429.in_VOROWN.Label_222,"A429_VORMKR1_1","Label_0222");
   vdn.subscribe(m_inputsA429.in_VOROWN.Label_034,"A429_VORMKR1_1","Label_0034");
   vdn.subscribe(m_inputsA429.in_VOROWN.Label_242,"A429_VORMKR1_1","Label_0242");
   vdn.subscribe(m_inputsA429.in_VOROWN.Label_244,"A429_VORMKR1_1","Label_0244");
   vdn.subscribe(m_inputsA429.in_VOROWN.Label_100,"A429_VORMKR1_1","Label_0100");
   //VOR OPP
   vdn.subscribe(m_inputsA429.in_VOROPP.Label_222,"A429_VORMKR2_1","Label_0222");
   vdn.subscribe(m_inputsA429.in_VOROPP.Label_034,"A429_VORMKR2_1","Label_0034");
   vdn.subscribe(m_inputsA429.in_VOROPP.Label_242,"A429_VORMKR2_1","Label_0242");
   vdn.subscribe(m_inputsA429.in_VOROPP.Label_244,"A429_VORMKR2_1","Label_0244");
   vdn.subscribe(m_inputsA429.in_VOROPP.Label_100,"A429_VORMKR2_1","Label_0100");
   //TCAS IN
   vdn.subscribe(m_inputsA429.in_TCASOWN.Label_270,"A429_TCAS_FMGC_1","Label_0270");
   //TCAP OPP
   vdn.subscribe(m_inputsA429.in_TCASOPP.Label_270,"A429_TCAS_FMGC_2","Label_0270");
   //DME OWN
   vdn.subscribe(m_inputsA429.in_DME[0],"A429_DME1_1","");
   //DME OPP
   vdn.subscribe(m_inputsA429.in_DME[1],"A429_DME2_1","");
   //CLOCK
   vdn.subscribe(m_inputsA429.in_Clock,"A429_Clock","");

   vector<CoreSim::Osal::String> filesname2;       
   vector<CoreSim::Osal::String> files2;
   GetFiles("bin\\Common\\DIDO\\","DO.csv",filesname2,files2);
   for (int k = 0; k < files2.size(); k++)
   {
      CoreSim::Utils::CsvFile data_file(files2[k],true);
      int i = 0;
      while(data_file.readLine()) 
      {
         string name      = data_file.getField("Name");
         string ipAddr    = data_file.getField("IpAddress");
         UINT16 localPort = Osal::Convert::toUInt16(data_file.getField("LocalPort"));
         UINT16 destPort  = Osal::Convert::toUInt16(data_file.getField("DestPort"));
         UINT16 size      = Osal::Convert::toUInt16(data_file.getField("Size"));
         //INT64  value     = Osal::Convert::toInt64(data_file.getField("Value"));
         string source    = data_file.getField("Source");
         string comments  = data_file.getField("IsListen");
         UINT16 port     = (destPort != 0)? destPort : localPort;
         //no need to listen to these port: not sent by current FG (IsSend: No)
         //if(comments == "Y")
         {
            fg->fgdo->nameList[i]   = name;
            fg->fgdo->addrList[i]   = CoreSim::Osal::Net::SocketAddress(ipAddr,port);
            fg->fgdo->listenList[i] = comments;
            fg->fgdo->sizeList[i]   = size;
         }        
         i++;
      }
   }

   return false;
}

//It is defined to initialize parameters for future use.
void FG::CMAddrTest()
{
   m_inputsA429.in_CMIO.Prot.FM_PUPRDY = 0x0000;
   m_inputsA429.in_CMIO.Prot.FG_PUPRDY = 0x0004;
   m_inputsA429.in_CMIO.Prot.FG_SAFETEST = 0x0008;
   m_inputsA429.in_CMIO.Prot.PLCONTROL   = 0x000c;
   m_inputsA429.in_CMIO.Prot.RTC_PERIOD  = 0x0010;
   m_inputsA429.in_CMIO.Prot.FM_HLY      = 0x0018;
   m_inputsA429.in_CMIO.Prot.FG_HLY      = 0x001C;
   m_inputsA429.in_CMIO.Prot_NewFM.CODE_COMPFG_1 = 0x0020;
   m_inputsA429.in_CMIO.Prot_NewFM.CODE_COMPFG_2 = 0x0024;
   m_inputsA429.in_CMIO.Prot_NewFM.CODE_COMPFG_3 = 0x0028;
   m_inputsA429.in_CMIO.Prot_NewFM.CODE_COMPFG_6 = 0x0034;
   m_inputsA429.in_CMIO.Prot_NewFM.SEMAPHORE_DATALOAD = 0x0038;
   m_inputsA429.in_CMIO.Hard_FGFM_T.NG_PRES  = 0x0040;
   m_inputsA429.in_CMIO.Hard_FGFM_T.AL_PINS1 = 0x004c;
   m_inputsA429.in_CMIO.Hard_FGFM_T.SIDE_1   = 0x0058;
   m_inputsA429.in_CMIO.Hard_FGFM_E.SPATIAAL_CNT = 0x8c;
   m_inputsA429.in_CMIO.Fac.VMAXOP           = 0x160;
   m_inputsA429.in_CMIO.FadecOwn.ENG_SN_1       = 0x1e0;
   m_inputsA429.in_CMIO.FadecOwn.N1_EPR_LIMIT_OWN   = 0x1f0;
   m_inputsA429.in_CMIO.FadecOwn.N1_EPR_CMD_OWN     = 0x1f8;
   m_inputsA429.in_CMIO.FadecOwn.FAD_OWN_FGVAL  = 0x0230;
   m_inputsA429.in_CMIO.FadecOwn.SPARE_4[0]     = 0x0234;
   m_inputsA429.in_CMIO.FadecOwn.SPARE_4[1]     = 0x0238;
   m_inputsA429.in_CMIO.FadecOwn.SPARE_4[2]     = 0x023c;
   m_inputsA429.in_CMIO.FadecOpp.N1_EPR_LIMIT   = 0x0240;
   m_inputsA429.in_CMIO.FadecOpp.N1_EPR_CMD     = 0x0248;
   m_inputsA429.in_CMIO.FadecOpp.FAD_OPP_FGVAL  = 0x0270;
   m_inputsA429.in_CMIO.FadecOpp.SPARE_5[8]     = 0x0294;
   m_inputsA429.in_CMIO.Fcu.SEL_HDG          = 0x0298;
   m_inputsA429.in_CMIO.Fcu.SEL_TRK          = 0x0300;
   m_inputsA429.in_CMIO.Fqi_T.FUEL_QUANT     = 0x0320;
   m_inputsA429.in_CMIO.Fqi_T.SPARE_8[0]     = 0x0328;
   m_inputsA429.in_CMIO.LS.LOC_DEV           = 0x0340;
   m_inputsA429.in_CMIO.LS.GLIDE_SLOPE_DEV   = 0x0390;
   m_inputsA429.in_CMIO.Irs.TRUE_HDG         = 0x03c8;
   m_inputsA429.in_CMIO.Irs.IRSSOURCE        = 0x0420;
   m_inputsA429.in_CMIO.Irs_Opp_2.PPOS_LAT   = 0x04a0;
   m_inputsA429.in_CMIO.Vor.VOR_OWN_BRG      = 0x05c0;
   m_inputsA429.in_CMIO.Vor.VOR_OWN_FREQ     = 0x05c8;
   m_inputsA429.in_CMIO.Vor.VOR_OWN_ID1      = 0x05d0;
   m_inputsA429.in_CMIO.Vor.VOR_OWN_ID2      = 0x05d8;
   m_inputsA429.in_CMIO.Vor.VOR_OWN_FGVAL    = 0x05e0;
   m_inputsA429.in_CMIO.Vor.VOR_OWN_SELCRS   = 0x05e4;
   m_inputsA429.in_CMIO.Vor.VOR_OWN_BRG_NCD  = 0x05ec;
   m_inputsA429.in_CMIO.Vor.VOR_OPP_BRG      = 0x05f0;
   m_inputsA429.in_CMIO.Vor.VOR_OPP_FREQ     = 0x05f8;
   m_inputsA429.in_CMIO.Vor.VOR_OPP_ID1      = 0x0600;
   m_inputsA429.in_CMIO.Vor.VOR_OPP_ID2      = 0x0608;
   m_inputsA429.in_CMIO.Vor.VOROPPSELCRS     = 0x0610;
   m_inputsA429.in_CMIO.Inter_FGFM.FPA_TGT   = 0x06c0;
   m_inputsA429.in_CMIO.Inter_FGFM.OUTPUT_RLIM_1   = 0x0730;
   m_inputsA429.in_CMIO.Inter_FGFM.SPARE_17[4] = 0x0758;
   m_inputsA429.in_CMIO.SPARE_18[0]           =  0x0760;
   m_inputsA429.in_CMIO.SPARE_18[1]           =  0x0768;
   m_inputsA429.in_CMIO.SPARE_18[2]           =  0x0770;
   m_inputsA429.in_CMIO.SPARE_18[3]           =  0x0778;
   m_inputsA429.in_CMIO.SPARE_18[4]           =  0x0780;
   m_inputsA429.in_CMIO.SPARE_18[5]           =  0x0788;
   m_inputsA429.in_CMIO.SPARE_18[6]           =  0x0790;
   m_inputsA429.in_CMIO.SPARE_18[7]           =  0x0798;
   m_inputsA429.in_CMIO.SPARE_18[8]           =  0x07a0;
   m_inputsA429.in_CMIO.SPARE_18[9]           =  0x07a8;
   m_inputsA429.in_CMIO.SPARE_18[10]          =  0x07b0;
   m_inputsA429.in_CMIO.SPARE_18[11]          =  0x07b8;
   m_inputsA429.in_CMIO.SPARE_18[12]          =  0x07c0;
   m_inputsA429.in_CMIO.SPARE_18[13]          =  0x07c8;
   m_inputsA429.in_CMIO.SPARE_18[14]          =  0x07d0;
   m_inputsA429.in_CMIO.SPARE_18[15]          =  0x07d8;
   m_inputsA429.in_CMIO.SPARE_18[16]          =  0x07e0;
   m_inputsA429.in_CMIO.SPARE_18[17]          =  0x07e8;
   m_inputsA429.in_CMIO.SPARE_19[0]           =  0x07f0;
   m_inputsA429.in_CMIO.SPARE_19[1]           =  0x07f8;
   m_inputsA429.in_CMIO.Bite_FG.FG2FM_STATUS  =  0x0800;
   m_inputsA429.in_CMIO.Bite_FG.SPARE_20[4]   =  0x0820;
   m_inputsA429.in_CMIO.Ami.AL_MDF_INFO_W1    =  0x0824;
   m_inputsA429.in_CMIO.Ami.AL_MDF_INFO_W7    =  0x083c;
   m_inputsA429.in_CMIO.Bite_FM.BITEDATA_TYPE   = 0x0840;
   m_inputsA429.in_CMIO.Bite_FM.BITE_DATA_SEM   = 0x0888;
   m_inputsA429.in_CMIO.CODE_COMP_FM_WORD[0]    = 0x088c;
   m_inputsA429.in_CMIO.CODE_COMP_FM_WORD[5]    = 0x08a0;
   m_inputsA429.in_CMIO.OPERATIONAL_SOFT_PN[0]  = 0x08a4;
   m_inputsA429.in_CMIO.OPERATIONAL_SOFT_PN[6]  = 0x08bc;
   m_inputsA429.in_CMIO.PERFO_DATA_BASE_W[0]    = 0x08c0;
   m_inputsA429.in_CMIO.CONFIGURATION_FILE_W[0] = 0x08dc;
   m_inputsA429.in_CMIO.MAGN_DATA_BASE_W[0]     = 0x08F8;
   m_inputsA429.in_CMIO.NAV_DATA_BASE_W[0]      = 0x0914;
   m_inputsA429.in_CMIO.CITYPAIR_WD[0]          = 0x0940;
   m_inputsA429.in_CMIO.FLIGHTNO_WD[0]          = 0x0958;
   m_inputsA429.in_CMIO.WEIGHT                  = 0x0a00;
   m_inputsA429.in_CMIO.C_BUSS_DATA[0]          = 0x0a58;
   m_inputsA429.in_CMIO.NUM_C_BUSS              = 0x0ad8;
   m_inputsA429.in_CMIO.Inter_FMFG.VAPP         = 0x0ae0;
   m_inputsA429.in_CMIO.Inter_FMFG.Z_ACC_ALT    = 0x0b00;
   m_inputsA429.in_CMIO.Inter_FMFG.VCDU         = 0x0b90;
   m_inputsA429.in_CMIO.Inter_FMFG.WIND_MAGNITUDE     = 0x0c18;
   m_inputsA429.in_CMIO.Inter_FMFG.HIGH_TGT_SPD_MG    = 0x0C58;
   m_inputsA429.in_CMIO.Inter_FMFG.DSPL_SPD_AUTO_TGT_VA = 0x0c78;
   m_inputsA429.in_CMIO.Inter_FMFG.FM_MAG_TRACK       = 0x0c7c;
   m_inputsA429.in_CMIO.Inter_FMFG.AUTO_XALT          = 0x0c88;
   m_inputsA429.in_CMIO.Inter_FMFG.AUTO_XALT_VAL      = 0x0c90;
   m_inputsA429.in_CMIO.Inter_FMFG.FM_PITCH_AT_DIS_3  = 0x0c94;
   m_inputsA429.in_CMIO.Inter_FMFG.SPAT_ACKNOWLEDGE   = 0x0c98;
   m_inputsA429.in_CMIO.Inter_FMFG.SPARE_43[0]        = 0X0c9c;
   m_inputsA429.in_CMIO.Inter_FMFG.SPARE_43[2]        = 0X0ca4;
   m_inputsA429.in_CMIO.Inter_FMFG.FM_EST_POS_ERROR   = 0x0ca8;
   m_inputsA429.in_CMIO.Inter_FMFG.DEST_TEMP          = 0x0d00;
   m_inputsA429.in_CMIO.Inter_FMFG.SN_MCDU_OWN_W[0]   = 0x0e1c;
   m_inputsA429.in_CMIO.Irs_Own_End.GPS_ALTITUDE_AMSL = 0x0ec0;
   m_inputsA429.in_CMIO.Irs_Own_End.PRED_WPT_HIL      = 0x0f18;
      
   m_inputsA429.in_CMIO.Irs_Opp_End.GPS_ALTITUDE_AMSL = 0x0f80;      
   m_inputsA429.in_CMIO.Irs_3_End.GPS_ALTITUDE_AMSL   = 0x1040;
   m_inputsA429.in_CMIO.Irs_Si_Own.P_POS_LAT_D     = 0x1100;
   m_inputsA429.in_CMIO.Irs_Si_Opp.P_POS_LAT_D     = 0x1200;
   m_inputsA429.in_CMIO.Irs_Si_3.P_POS_LAT_D       = 0x1300;
   m_inputsA429.in_CMIO.Irs_Si_3.TRK_ANGLE_TRUE_D  = 0x1318;
   m_inputsA429.in_CMIO.Irs_Si_3.MAG_HEADING_D     = 0x1338;
   m_inputsA429.in_CMIO.Irs_Si_3.PLATFORM_HEADING  = 0x1350;
   m_inputsA429.in_CMIO.Irs_Si_3.IRS_DISCRETE_2    = 0x1360;
   m_inputsA429.in_CMIO.Mls.FG_RMPOWN_MLS_SEL      = 0x1400;
   m_inputsA429.in_CMIO.Mls.MLS_LOC_DEV_NCD        = 0x1438;
   m_inputsA429.in_CMIO.FM_STATE_AT_PWR_UP_W[0]    = 0x1c00;
   m_inputsA429.in_CMIO.SPARE_59                   = 0x1c50;
   m_inputsA429.in_CMIO.Fqi_E.FUEL_QTY_left_outer_tank = 0x1c60;
   m_inputsA429.in_CMIO.SPARE_60[0]                = 0x1cc0;
   m_inputsA429.in_CMIO.SPARE_60[51]               = 0x1ff0;
   
   printf("Common Memory allocate Test Finishe! Please check log.txt!.................");
   return;
}


//Connect to FG using UDP socket broadcast.
void FG::UdpConnect(FGDO *fgdo)
{
   //Set broadcast and Join Multicast 
   m_txUdpSocket.setBroadcast(1); 
   m_txUdpSocket.setReuseAddress(TRUE);  
   m_txUdpSocket2.setBroadcast(1); 
   m_txUdpSocket2.setReuseAddress(TRUE);

   //CoreSim::Osal::Net::IpAddress m_ipAddr("224.230.0.1"); 
   CoreSim::Osal::Net::IpAddress m_myAddr("192.168.9.154"); //10.9.18.25
   //m_txUdpSocket.joinMulticastGroup(m_ipAddr,m_myAddr);

   int j=0;
   for (int i = FGDO::ATH_OWN_ENGD_COM_OPP_I; i <= FGDO::RUDDER_LOCK_COM_I/*UDP_SOCKET_NUMBER_DIS*/; i++)
   {
      if(fgdo->listenList[i] == "Y")
      {       
         fgdo->addrList[i].setIpAddress(m_myAddr.getInAddrAny());
         if(j >= UDP_SOCKET_NUMBER_DIS)
         {
            std::cout << "Error! Socket Number Exceeds! Please Recheck the Recevie DIS socket number!!!";
            return;
         }
         m_socketList[j].bind(fgdo->addrList[i]);
         m_socketList[j].setReuseAddress(TRUE);
         //m_socketList[i].joinMulticastGroup(m_ipAddr,m_myAddr);
         m_socketList[j].setRecvTimeout(0);
         j++;
      }     
   }

   for (int i = 0; i < UDP_SOCKET_NUMBER_A429; i++)
   {
      fgdo->addrList[i + UDP_SOCKET_TOTAL_DIS].setIpAddress(m_myAddr.getInAddrAny());
      m_socketA429[i].bind(fgdo->addrList[i + UDP_SOCKET_TOTAL_DIS]);
      m_socketA429[i].setReuseAddress(TRUE);
      //m_socketList[i].joinMulticastGroup(m_ipAddr,m_myAddr);
      m_socketA429[i].setRecvTimeout(0);     
   }
  
   return;
}

#if 1
template <typename T>
void FG::packA429Datas(T& input,int socketNo,FGDI* fgdi,FILE* fd)
{
   CoreSim::Vdn::A429Label *p_Label = NULL;  
   UINT32 sendData[sizeof(input)/64];
   p_Label = (CoreSim::Vdn::A429Label *)(&input);
   int k   = 0;

   for(int i = 0; i< sizeof(input)/64; i++,p_Label++)
   {
      //sendData[i] = 0;
      sendData[i] = (*p_Label).getVal();
   }

   int sendnum = m_txSocketA429[socketNo].sendTo(&sendData,sizeof(sendData),fgdi->addrList[socketNo+UDP_SOCKET_TOTAL_DIS_IN],5U); 

   INT16 port = fgdi->addrList[socketNo+UDP_SOCKET_TOTAL_DIS_IN].getPort();
   if((port == 10181) || (port == 10175) || (port == 10176) || (port == 10183) || (port == 10184))
   {
      CoreSim::Osal::Net::IpAddress addr = fgdi->addrList[socketNo+UDP_SOCKET_TOTAL_DIS_IN].getIpAddress();
      if(addr.toString() == "192.168.80.154")
      {
         fgdi->addrList[socketNo+UDP_SOCKET_TOTAL_DIS_IN] = CoreSim::Osal::Net::SocketAddress("192.168.80.155",port);
         sendnum = m_txSocketA429[socketNo].sendTo(&sendData,sizeof(sendData),fgdi->addrList[socketNo+UDP_SOCKET_TOTAL_DIS_IN],5U);
      }
      else if(addr.toString() == "192.168.80.155")
      {
         fgdi->addrList[socketNo+UDP_SOCKET_TOTAL_DIS_IN] = CoreSim::Osal::Net::SocketAddress("192.168.80.154",port);
         sendnum = m_txSocketA429[socketNo].sendTo(&sendData,sizeof(sendData),fgdi->addrList[socketNo+UDP_SOCKET_TOTAL_DIS_IN],5U);
      }      
   }

   if(sendnum < 0 )
   {
      //std::cout<<"send error..." <<"\n";
      fprintf(fd,"Send ERROR! %d: %d Port: %d \n", socketNo,sendnum,(fgdi->addrList[socketNo+UDP_SOCKET_TOTAL_DIS_IN]).getPort());
   }
   else
   {
      fprintf(fd,"Send: %d: %d Port: %d MSG:  ", socketNo,sendnum,fgdi->addrList[socketNo+UDP_SOCKET_TOTAL_DIS_IN].getPort());
      p_Label = (CoreSim::Vdn::A429Label *)(&input);
      while ( k!= sendnum/4 )
      {          
         fprintf(fd,"%08X",(*p_Label).getVal());

         fflush(fd); 
         p_Label++;
         k++;
      }
      fprintf(fd,"\n");                              
      fflush(fd); 
   }

   return;
}
#endif

void FG::packA429DatasMonOut(UCHAR* input,int socketNo,FGDI* fgdi)
{
   CoreSim::Vdn::A429Label *p_Label = NULL; 
   p_Label = (CoreSim::Vdn::A429Label *)(&input);
//   int sendnum = m_txSocketA429[socketNo].sendTo(&input,sizeof(input),fgdi.addrList[socketNo+UDP_SOCKET_TOTAL_DIS_IN],5U); 
   
   INT16 port = fgdi->addrList[socketNo+UDP_SOCKET_TOTAL_DIS_IN].getPort();
   if(port == 10181) 
   {
      CoreSim::Osal::Net::IpAddress addr = fgdi->addrList[socketNo+UDP_SOCKET_TOTAL_DIS_IN].getIpAddress();
      if(addr.toString() == "192.168.80.154")
      {
         fgdi->addrList[socketNo+UDP_SOCKET_TOTAL_DIS_IN] = CoreSim::Osal::Net::SocketAddress("192.168.80.155",port);
//         sendnum = m_txSocketA429[socketNo].sendTo(&input,sizeof(input),fgdi.addrList[socketNo+UDP_SOCKET_TOTAL_DIS_IN],5U);
      }
      else if(addr.toString() == "192.168.80.155")
      {
         fgdi->addrList[socketNo+UDP_SOCKET_TOTAL_DIS_IN] = CoreSim::Osal::Net::SocketAddress("192.168.80.154",port);
//         sendnum = m_txSocketA429[socketNo].sendTo(&input,sizeof(input),fgdi.addrList[socketNo+UDP_SOCKET_TOTAL_DIS_IN],5U);
      }      
   }

 /*  if(sendnum < 0 )
   {
      //std::cout<<"send error..." <<"\n";
      fprintf(fd,"Send ERROR! %d: %d Port: %d \n", socketNo,sendnum,(fgdi.addrList[socketNo+UDP_SOCKET_TOTAL_DIS_IN]).getPort());
   }
   else
   {
      fprintf(fd,"Send: %d: %d Port: %d MSG:  ", socketNo,sendnum,fgdi.addrList[socketNo+UDP_SOCKET_TOTAL_DIS_IN].getPort());
      p_Label = (CoreSim::Vdn::A429Label *)(&input);
      while ( k!= sendnum/4 )
      {          
         fprintf(fd,"%08X",(*p_Label).getVal());

         fflush(fd); 
         p_Label++;
         k++;
      }
      fprintf(fd,"\n");                              
      fflush(fd); 
   }
*/
   return;
}

/*Receive DIS data from FG here*/
void FG::RecvDisDataFromFG(FGDO* fgdo,FILE* fd,CoreSim::NetVdn::NetVdnClient& vdn)
{
	int j    = 0;
	int size = 0;
	UCHAR *recvBuff = NULL;  
	CoreSim::Osal::Int32 Recvnum = 0;
	if((fd = fopen("log.txt","a+")) == NULL)
	{
		std::wcout << "Failed to open log.txt! RecvDisDataFromFG" <<std::endl;
		return;
	}

	for (int i = FGDO::ATH_OWN_ENGD_COM_OPP_I; i <= FGDO::RUDDER_LOCK_COM_I; i++)
	{
		CoreSim::Osal::Net::SocketAddress clientAddr;  
      DisFromFG[i] = 0;
      fgdo->valueList[i] = false;
		if(fgdo->listenList[i] == "Y")
		{
			size = fgdo->sizeList[i];
			//dynamic allocate the receive buffer according to the size
			recvBuff = (UCHAR*)malloc(size*sizeof(UCHAR)); 
			memset(recvBuff,0,size);
			if(j >= UDP_SOCKET_NUMBER_DIS)
			{
				printf("Error! Socket Number Exceeds 19! Please Recheck the Recevie DIS socket number!!!");
				fprintf(fd,"Error! Socket Number Exceeds 19! Please Recheck the Recevie DIS socket number!!!");
				return;
			}
			Recvnum =  m_socketList[j].recvFrom(recvBuff,size,clientAddr);
			if ( Recvnum == SOCKET_ERROR)  
			{  
				int err = WSAGetLastError();  
				printf("recv error:%d\n", err);    
			}
			else if( Recvnum > 0)
			{
				fgdo->valueList[i] = static_cast <CoreSim::Osal::Bool>(*recvBuff);
            DisFromFG[i] = static_cast <CoreSim::Osal::Bool>(*recvBuff);
				//fgdo.valueList.push_back(recvBuff);
				//fgdo.clientaddrList.push_back(clientAddr);   //maybe need to changed to fgdo.addrList
				fprintf(fd,"RECV %d: %d Port: %d MSG: %d \n", i,Recvnum,fgdo->addrList[i].getPort(),recvBuff[0]);                              
				fflush(fd);           

				printf("RECV %d: %d Port: %d MSG: %d \n", i,Recvnum,fgdo->addrList[i].getPort(),recvBuff[0]); 
			}
			else
			{           
				printf("No MSG! Port: %d\n",fgdo->addrList[i].getPort());
			}
			j++; 
		}     
	}//end of for

	fclose(fd);
	return;
}

void FG::DecodeSharedMemIoOut(CommMemory& m_comMemoryIO, Outputs& m_outputs)
{
   m_outputs.FG_Pup_Rdy         = m_comMemoryIO.Prot.FG_PUPRDY;
   m_outputs.FG_hlty_fg         = m_comMemoryIO.Prot.FG_HLY;
   m_outputs.FG_SafeTest        = m_comMemoryIO.Prot.FG_SAFETEST;
   m_outputs.SIDE1              = m_comMemoryIO.Hard_FGFM_T.SIDE_1;         //PUP (1st sync.) + App.
   m_outputs.FAC_FG_VAL         = m_comMemoryIO.Fac.FAC_FG_VAL;             //FAC FG VAL
   m_outputs.FAC_OPP_SELECT     = m_comMemoryIO.Fac.FAC_OPP_SELECT;         //FAC OPP SELECT
   m_outputs.ADC_FG_VAL         = m_comMemoryIO.Adc_E.ADCFGVAL;             //ADC FG VAL
   m_outputs.ADC_SOURCE         = m_comMemoryIO.Adc_E.ADCSOURCE;            //ADC SOURCE
   m_outputs.FAD_OWN_FG_VAL     = m_comMemoryIO.FadecOwn.FAD_OWN_FGVAL;     //FADEC OWN FG VAL
   m_outputs.FAD_OPP_FG_VAL     = m_comMemoryIO.FadecOpp.FAD_OPP_FGVAL;     //FADEC OPP FG VAL
   m_outputs.FCU_FG_VAL         = m_comMemoryIO.Fcu.FCU_FGVAL;              //FCU FG VAL
   m_outputs.LS_MMR_FG_VAL      = m_comMemoryIO.LS.MMRFGVAL;                //LS
   m_outputs.LS_MMR_OPP_SELECT  = m_comMemoryIO.LS.MMROPPSELECT;            //LS
   m_outputs.LS_LOC_DEV_NOT_NCD = m_comMemoryIO.LS.LOC_DEV_NOT_NCD;
   m_outputs.LS_FREQ_NOT_NCD    = m_comMemoryIO.LS.LS_FREQ_NOT_NCD;
   m_outputs.LS_RWY_HDG_NOT_NCD = m_comMemoryIO.LS.LS_RWY_HDG_NOT_NCD;
   m_outputs.LS_BILS_INST       = m_comMemoryIO.LS.BILSINST;
   m_outputs.LS_BMLS_INST       = m_comMemoryIO.LS.BMLSINST;
   m_outputs.LS_BGLS_INST       = m_comMemoryIO.LS.BGLSINST;
   m_outputs.MLS_FG_VAL         = m_comMemoryIO.Mls.MLS_MLSFGVAL;           //MLS
   m_outputs.MLS_OPP_SEL        = m_comMemoryIO.Mls.MLS_OPPSEL;             
   m_outputs.IRS_FG_VAL         = m_comMemoryIO.Irs.IRSFGVAL;               //LS
   m_outputs.IRS_SOURCE         = m_comMemoryIO.Irs.IRSSOURCE;
   m_outputs.VOR_OWN_FG_VAL     = m_comMemoryIO.Vor.VOR_OWN_FGVAL;          //VOR
   m_outputs.FM_HLY             = m_comMemoryIO.Prot.FM_HLY;
   m_outputs.FG_HLY             = m_comMemoryIO.Prot.FG_HLY;
   m_outputs.FM_PUPRDY          = m_comMemoryIO.Prot.FM_PUPRDY;
   m_outputs.CFM_ENG            = m_comMemoryIO.Hard_FGFM_E.CFM_ENG;
   m_outputs.IAE_ENG            = m_comMemoryIO.Hard_FGFM_E.IAE_ENG;
   m_outputs.PW_ENGINE          = m_comMemoryIO.PW_ENGINE;

   return;
}

void FG::DecodeComFtGuidOut(FGDO* fgdo,int i,CoreSim::Osal::Int32 Recvnum,UCHAR *recvBuff,FILE* fd,FILE* fd_10180)
{
   if((fd = fopen("log.txt","a+")) == NULL)
	{
		std::wcout << "Failed to open log.txt! RecvA429DataFromFG" <<std::endl;
		return;
	}
   if((fd_10180 = fopen("log10183.txt","a+")) == NULL)
	{
		std::wcout << "Failed to open log10183.txt! RecvA429DataFromFG" <<std::endl;
		return;
	}
   int size = fgdo->sizeList[i];
   if(fgdo->addrList[i].getPort() == P_COM_FT_GUID_OUT)
   {
      fprintf(fd_10180,"RECV %d: %d Port: %d MSG: ", i,Recvnum,fgdo->addrList[i].getPort());
      int k=0;
      while ( k!= size )
      {        
         fprintf(fd_10180,"%02x",recvBuff[k]);
         if(k%4 == 0 && recvBuff[k] == 0xbe)  //L175
         {  
            fprintf(fd,"RECV %d: %d Port: %d L175:%02x%02x%02x%02x\n",i,Recvnum,fgdo->addrList[i].getPort(),recvBuff[k],recvBuff[k+1],recvBuff[k+2],recvBuff[k+3]);
            fflush(fd);  
         }
         if(k%4 == 0 && recvBuff[k] == 0xab)  //L325
         {  
            fprintf(fd,"RECV %d: %d Port: %d L325:%02x%02x%02x%02x\n",i,Recvnum,fgdo->addrList[i].getPort(),recvBuff[k],recvBuff[k+1],recvBuff[k+2],recvBuff[k+3]);
            fflush(fd);  
         }
         if(k%4 == 0 && recvBuff[k] == 0x6b)  //L326 B17-ENGINES STOPPED   B18-MAIN LANDING GEAR   B19-BSCU FAIL  B20-BOTH AP ENGAGE  B21-AP ENGD OPP  B22-FD ENGD OPP  B23-A/THR ENGD OPP 
         {  
            fprintf(fd,"RECV %d: %d Port: %d L326:%02x%02x%02x%02x\n",i,Recvnum,fgdo->addrList[i].getPort(),recvBuff[k],recvBuff[k+1],recvBuff[k+2],recvBuff[k+3]);
            fflush(fd);  
            if(((recvBuff[k+2]>>2)&0x01) == 1)  //FD ENGAGE OPP
            {
               printf("RECV %d: %d Port: %d  FD ENGAGE OPP!!! \n", i,Recvnum,fgdo->addrList[i].getPort());                  
               fprintf(fd,"FD ENGAGE OPP!\n");
               fflush(fd); 
            }
            if(((recvBuff[k+2]>>3)&0x01) == 1) //AP ENGAGE OPP
            {
               printf("RECV %d: %d Port: %d  AP ENGAGE OPP!!!\n", i,Recvnum,fgdo->addrList[i].getPort());
               fprintf(fd,"AP ENGAGE OPP!\n");
               fflush(fd); 
            }
         }
         if(k%4 == 0 && recvBuff[k] == 0xeb)  //L327                
         {  
            fprintf(fd,"RECV %d: %d Port: %d L327:%02x%02x%02x%02x\n",i,Recvnum,fgdo->addrList[i].getPort(),recvBuff[k],recvBuff[k+1],recvBuff[k+2],recvBuff[k+3]);
            fflush(fd); 
				BPRTYFG = (recvBuff[k+1]>>1)&0x01;
         }
			if(k%4 == 0 && recvBuff[k] == 0x5b)    //L332
			{
			   fprintf(fd,"RECV %d: %d Port: %d L332:%02x%02x%02x%02x\n",i,Recvnum,fgdo->addrList[i].getPort(),recvBuff[k],recvBuff[k+1],recvBuff[k+2],recvBuff[k+3]);
            fflush(fd); 
				BVAGMMR = (recvBuff[k+1]>>4)&0x01;   //MMRFGVAL
			}
			if(k%4 == 0 && recvBuff[k] == 0xdb)    //L333
			{
			   BOFACSEL = !((recvBuff[k+1]>>5)&0x1);   //FAC OPP SELECT ! BFACSEL(FAC OWN SELECT L333 bit11)
			}
			if(k%4 == 0 && recvBuff[k] == 0xbb)    //L335
			{
			   BVAGFCU = (recvBuff[k+2]>>4)&0x01;   //FCUFGVAL
			}
         k++;
      }//end of while ( k!= size )
      fprintf(fd_10180,"\n");                              
      fflush(fd_10180);  
      fclose(fd_10180);
   } //end of if(fgdo->addrList[i].getPort() == P_COM_FT_GUID_OUT)
   return;
}


/*Receive A429 Data from FG here*/
void FG::RecvA429DataFromFG(FGDO* fgdo,FILE* fd)
{
   int j    = 0;
   int size = 0;	
   UCHAR *recvBuff = NULL; 
   UCHAR m_simsoftBf[4224] = "";
   UCHAR m_dataBuff[2048]  = "";
   CoreSim::Osal::Int32 Recvnum = 0;
   fd_10184 = fopen("log10184.txt","a+");
   fd_10183 = fopen("log10183.txt","a+");
   fd_10175 = fopen("log10175.txt","a+");
   fd_10176 = fopen("log10176.txt","a+");
   fd_10178 = fopen("log10178.txt","a+");
   fd_10177 = fopen("log10177.txt","a+");
   fd_10180 = fopen("log10180.txt","a+");
   fd_10181 = fopen("log10181.txt","a+");
   fd_10182 = fopen("log10182.txt","a+");
	if((fd = fopen("log.txt","a+")) == NULL)
	{
		std::wcout << "Failed to open log.txt! RecvA429DataFromFG" <<std::endl;
		return;
	}

   memset(&m_comMemoryFM,0,sizeof(m_comMemoryFM));
   memset(&m_comMemoryIO,0,sizeof(m_comMemoryIO));
   memset(&m_dataMonOut,0,sizeof(m_dataMonOut));
   memset(&m_dataFmgcOwnA,0,sizeof(m_dataFmgcOwnA));
   memset(&m_dataFmgcOwnB,0,sizeof(m_dataFmgcOwnB));


   for (int i = FGDO::COM_FMGC_OWN_A_OUT; i <= FGDO::COMMAND_SIMSOFT_OUT_IO; i++)
   {
      CoreSim::Osal::Net::SocketAddress clientAddr;  
      size = fgdo->sizeList[i];
      //dynamic allocate the receive buffer according to the size
      recvBuff = (UCHAR*)malloc(size*sizeof(UCHAR)); 
      memset(recvBuff,0,size);
      
      Recvnum =  m_socketA429[j].recvFrom(recvBuff,size,clientAddr);
      
      if ( Recvnum == SOCKET_ERROR )  
      {  
         int err = WSAGetLastError();  
         printf("recv error:%d\n", err);    
      }
      else if( Recvnum > 0 )
      {
         switch(fgdo->addrList[i].getPort())
         {
            case P_FM_FG_SHARED_MEM_FM_OUT:   
               memcpy(&m_comMemoryFM,recvBuff,fgdo->sizeList[i]); 
               break;
            
            case P_FM_FG_SHARED_MEM_IO_OUT:   
               memcpy(&m_comMemoryIO,recvBuff,fgdo->sizeList[i]);
               			   
			      //updates variables in funcion: bufferInputs(*fgdi)			                                                                   
               //get CM data and copy to m_commons
               DecodeSharedMemIoOut(m_comMemoryIO, m_outputs);                    
               //for debug use only
               if(m_comMemoryIO.Prot.FG_SAFETEST == 0xff00)
               {
                  printf("RECV %d: %d Port: %d  FG-SATETY-TEST! \n", i,Recvnum,fgdo->addrList[i].getPort());
                  fprintf(fd,"RECV %d: %d Port: %d  FG-SATETY-TEST! \n", i,Recvnum,fgdo->addrList[i].getPort());
               } 
               break;
            case P_COMMAND_SIMSOFT_OUT_COM: 
            case P_COMMAND_SIMSOFT_OUT_MON:
            case P_COMMAND_SIMSOFT_OUT_IO:
               memcpy(m_simsoftBf,(char *)recvBuff,fgdo->sizeList[i]);
               //fprintf(fd,"RECV %d: %d Port: %d MSG: ", i,Recvnum,fgdo.addrList[j].getPort());
               break;
            //case P_COM_FMGC_OWN_A_OUT:
            //case P_COM_FMGC_OWN_B_OUT:
            case P_COM_FMGC_OWN_C_OUT:
            case P_COM_FT_GUID_OUT:
            case P_COM_FT_INNER_OUT:
            //case P_MON_FMGC_MON_OUT:
            case P_MON_FT_MONITOR_OUT:
               memcpy(m_dataBuff,(char *)recvBuff,fgdo->sizeList[i]);		  
               //fprintf(fd,"RECV %d: %d Port: %d MSG: ", i,Recvnum,fgdo.addrList[j].getPort());
               break;
            case P_MON_FMGC_MON_OUT:
               memcpy(m_dataMonOut,(char *)recvBuff,fgdo->sizeList[i]);		             
               break;
            case P_COM_FMGC_OWN_A_OUT:
               memcpy(m_dataFmgcOwnA,(char *)recvBuff,fgdo->sizeList[i]);	
               //publish FMGC-1 OWN A
               /*UINT32 *p = NULL;
               p = (UINT32 *)recvBuff;
               for(int i=0; i< Recvnum/4; i++)
               {
                 CoreSim::Osal::UInt32 Data = 0; 
                 Data = *p;
                 p++;
                 m_outputs.Fmgc_OwnA[0].push(Data);
               }
               */
               break;
            default:
               break;
         }//end of switch(fgdo->addrList[i].getPort())


         DecodeComFtGuidOut(fgdo,i,Recvnum,recvBuff,fd,fd_10180);

         if(fgdo->addrList[i].getPort() == P_COM_FMGC_OWN_A_OUT)
         {
            fprintf(fd_10175,"RECV %d: %d Port: %d MSG: ", i,Recvnum,fgdo->addrList[i].getPort());
            int k=0;
            while ( k!= size )
            {        
               fprintf(fd_10175,"%02x",recvBuff[k]);
			      if(k%4 == 0 && recvBuff[k] == 0x7d)   //L276
			      {
					   fprintf(fd,"RECV %d: %d Port: %d L276:%02x%02x%02x%02x\n",i,Recvnum,fgdo->addrList[i].getPort(),recvBuff[k],recvBuff[k+1],recvBuff[k+2],recvBuff[k+3]);
					   BVAIRS  = (recvBuff[k+2]>>5)&0X1;   //IRS OWN VALID
					   BVAOIRS = (recvBuff[k+2]>>4)&0X1;   //IRS OPP VALID
					   BVAIRS3 = (recvBuff[k+3]>>4)&0X1;   //IRS 3 VALID
					   BVFGFAD = (recvBuff[k+1]>>4)&0x1;   //FADOWNFGVAL
			      }
			      if(k%4 == 0 && recvBuff[k] == 0xe6)   //L147
			      {
			           fprintf(fd,"RECV %d: %d Port: %d L147:%02x%02x%02x%02x\n",i,Recvnum,fgdo->addrList[i].getPort(),recvBuff[k],recvBuff[k+1],recvBuff[k+2],recvBuff[k+3]);
					   BMLSINST = (recvBuff[k+2]>>5)&0x1;   //BMLSINST
					   BGLSINST = (recvBuff[k+2]>>4)&0x1;   //BGLSINST
					   BFMLSINH = (recvBuff[k+2]>>3)&0x1;   //BFMLSINH
			      }
               k++;
            }
            fprintf(fd_10175,"\n");                              
            fflush(fd_10175);    
         }//end of if(fgdo->addrList[i].getPort() == P_COM_FMGC_OWN_A_OUT)
         if(fgdo->addrList[i].getPort() == P_COM_FMGC_OWN_B_OUT)
         {
            fprintf(fd_10176,"RECV %d: %d Port: %d MSG: ", i,Recvnum,fgdo->addrList[i].getPort());
            int k=0;
            while ( k!= size )
            {        
               fprintf(fd_10176,"%02x",recvBuff[k]);
			      if(k%4 == 0 && recvBuff[k] == 0xe6)   //L147
			      {
			           fprintf(fd,"RECV %d: %d Port: %d L147:%02x%02x%02x%02x\n",i,Recvnum,fgdo->addrList[i].getPort(),recvBuff[k],recvBuff[k+1],recvBuff[k+2],recvBuff[k+3]);
					   BMLSINST = (recvBuff[k+2]>>5)&0x1;   //BMLSINST
					   BGLSINST = (recvBuff[k+2]>>4)&0x1;   //BGLSINST
					   BFMLSINH = (recvBuff[k+2]>>3)&0x1;   //BFMLSINH
			      }
               k++;
            }
            fprintf(fd_10176,"\n");                              
            fflush(fd_10176);    
         }
         if(fgdo->addrList[i].getPort() == P_COM_FMGC_OWN_C_OUT)
         {
            fprintf(fd_10178,"RECV %d: %d Port: %d MSG: ", i,Recvnum,fgdo->addrList[i].getPort());
            int k=0;
            while ( k!= size )
            {        
               fprintf(fd_10178,"%02x",recvBuff[k]);
               k++;
            }
            fprintf(fd_10178,"\n");                              
            fflush(fd_10178);    
         }
         if(fgdo->addrList[i].getPort() == P_COM_FT_INNER_OUT)
         {
            fprintf(fd_10177,"RECV %d: %d Port: %d MSG: ", i,Recvnum,fgdo->addrList[i].getPort());
            int k=0;
            while ( k!= size )
            {        
               fprintf(fd_10177,"%02x",recvBuff[k]);
			      if(k%4 == 0 && recvBuff[k] == 0x8a)      //L121
			      {
			         BVAIRS3 = recvBuff[k+1]&0x01;        //IRS 3 VALID
				      BVAOIRS = (recvBuff[k+2]>>7)&0x01;   //IRS OPP VALID
				      BVAIRS  = (recvBuff[k+1]>>1)&0x01;   //IRS OWN VALID
				      BADC3SEL = !((recvBuff[k+2]>>6)&0x01);   //ADC_SOURCE
			      }
			      if(k%4 == 0 && recvBuff[k] == 0x96)     //L151
			      {
				      fprintf(fd,"RECV %d: %d Port: %d L151:%02x%02x%02x%02x\n",i,Recvnum,fgdo->addrList[i].getPort(),recvBuff[k],recvBuff[k+1],recvBuff[k+2],recvBuff[k+3]);
				      fflush(fd);			  
				      BIRS3SEL = !((recvBuff[k+1]>>5)&0X01);   //IRS_SOURCE				               					
			      }

               k++;
            }
            fprintf(fd_10177,"\n");                              
            fflush(fd_10177);    
         }//end of if(fgdo->addrList[i].getPort() == P_COM_FT_INNER_OUT)
         if(fgdo->addrList[i].getPort() == P_MON_FMGC_MON_OUT)
         {
            fprintf(fd_10181,"RECV %d: %d Port: %d MSG: ", i,Recvnum,fgdo->addrList[i].getPort());
            int k=0;
            while ( k!= size )
            {        
               fprintf(fd_10181,"%02x",recvBuff[k]);
			      if(k%4 == 0 && recvBuff[k] == 0x66)  //L146               
               {  
                  fprintf(fd,"RECV %d: %d Port: %d L146:%02x%02x%02x%02x\n",i,Recvnum,fgdo->addrList[i].getPort(),recvBuff[k],recvBuff[k+1],recvBuff[k+2],recvBuff[k+3]);
                  fflush(fd);  
				      BVAADC  = recvBuff[k+2]&0x01;
				      BVAOADC = (recvBuff[k+3]>>7)&0x01;
				      BVAADC3 = (recvBuff[k+3]>>6)&0x01;
               }
			      if(k%4 == 0 && recvBuff[k] == 0x1d)   //270
			      {
				     fprintf(fd,"RECV %d: %d Port: %d L270:%02x%02x%02x%02x\n",i,Recvnum,fgdo->addrList[i].getPort(),recvBuff[k],recvBuff[k+1],recvBuff[k+2],recvBuff[k+3]);
                     fflush(fd);
				     BVFGOFAD = (recvBuff[k+3]>>6)&0x01;
			      }
               k++;
            }
            fprintf(fd_10181,"\n");                              
            fflush(fd_10181);    
         }//end of if(fgdo->addrList[i].getPort() == P_MON_FMGC_MON_OUT)
         if(fgdo->addrList[i].getPort() == P_MON_FT_MONITOR_OUT)
         {
            fprintf(fd_10182,"RECV %d: %d Port: %d MSG: ", i,Recvnum,fgdo->addrList[i].getPort());
            int k=0;
            while ( k!= size )
            {        
               fprintf(fd_10182,"%02x",recvBuff[k]);
               if(k%4 == 0 && recvBuff[k] == 0xbe)  //L175
               {  
                  fprintf(fd,"RECV %d: %d Port: %d L175:%02x%02x%02x%02x\n",i,Recvnum,fgdo->addrList[i].getPort(),recvBuff[k],recvBuff[k+1],recvBuff[k+2],recvBuff[k+3]);
                  fflush(fd);  
               }
               if(k%4 == 0 && recvBuff[k] == 0xab)  //L325
               {  
                  fprintf(fd,"RECV %d: %d Port: %d L325:%02x%02x%02x%02x\n",i,Recvnum,fgdo->addrList[i].getPort(),recvBuff[k],recvBuff[k+1],recvBuff[k+2],recvBuff[k+3]);
                  fflush(fd);  
               }
               if(k%4 == 0 && recvBuff[k] == 0xcb)  //L326 B17-ENGINES STOPPED   B18-MAIN LANDING GEAR   B19-BSCU FAIL  B20-BOTH AP ENGAGE  B21-AP ENGD OPP  B22-FD ENGD OPP  B23-A/THR ENGD OPP 
               {  
                  fprintf(fd,"RECV %d: %d Port: %d L326:%02x%02x%02x%02x\n",i,Recvnum,fgdo->addrList[i].getPort(),recvBuff[k],recvBuff[k+1],recvBuff[k+2],recvBuff[k+3]);
                  fflush(fd);  
               }
               if(k%4 == 0 && recvBuff[k] == 0xeb)  //L327                
               {  
                  fprintf(fd,"RECV %d: %d Port: %d L327:%02x%02x%02x%02x\n",i,Recvnum,fgdo->addrList[i].getPort(),recvBuff[k],recvBuff[k+1],recvBuff[k+2],recvBuff[k+3]);
                  fflush(fd);  
               }
			      if(k%4 == 0 && recvBuff[k] == 0x96)   //L151
			      {
				      fprintf(fd,"RECV %d: %d Port: %d L151:%02x%02x%02x%02x\n",i,Recvnum,fgdo->addrList[i].getPort(),recvBuff[k],recvBuff[k+1],recvBuff[k+2],recvBuff[k+3]);
				      fflush(fd);
				      BADC3SEL = !((recvBuff[k+2]>>6)&0X01);   //ADC_SOURCE
				      BVAIRS   =  (recvBuff[k+1]>>1)&0X01;   //IRS OWN VALID
				      BVAIRS3  =  recvBuff[k+1]&0X01;        //IRS 3 VALID
				      BIRS3SEL = !((recvBuff[k+1]>>5)&0X01);   //IRS_SOURCE
				      BVAGFCU  =  (recvBuff[k+3]>>7)&0x01;   //FCUFGVAL                  					
			      }
			      if(k%4 == 0 && recvBuff[k] == 0x9b)   //L331
			      {
				      fprintf(fd,"RECV %d: %d Port: %d L331:%02x%02x%02x%02x\n",i,Recvnum,fgdo->addrList[i].getPort(),recvBuff[k],recvBuff[k+1],recvBuff[k+2],recvBuff[k+3]);
				      fflush(fd);
			          BVAADC  = (recvBuff[k+2]>>4)&0x01;
				      BVAOADC = (recvBuff[k+3]>>2)&0x01;
				      BVAADC3 = (recvBuff[k+3]>>3)&0x01;				   	
			      }
			      if(k%4 == 0 && recvBuff[k] == 0x5b)   //332
			      {	
				     fprintf(fd,"RECV %d: %d Port: %d L332:%02x%02x%02x%02x\n",i,Recvnum,fgdo->addrList[i].getPort(),recvBuff[k],recvBuff[k+1],recvBuff[k+2],recvBuff[k+3]);
				     fflush(fd);
			         BVAGMMR = (recvBuff[k+1]>>4)&0X01;       //MMR FG VAL
				     BOMMRSEL = !((recvBuff[k+1]>>5)&0x01);   //MMROPPSELECT  !(BMMRSEL(MMR OWN SELECT L332-bit11)
				     BINNLOC  = (recvBuff[k+2]>>4)&0X01;      //LOC DEV NOT NCD  L332 Bit20 BNNLOC, CONF T 1.08s
			      }

               k++;
            }//end of while
            fprintf(fd_10182,"\n");                              
            fflush(fd_10182);    
         }//end of if(fgdo->addrList[i].getPort() == P_MON_FT_MONITOR_OUT)
         
         printf("RECV %d: %d Port: %d \n", i,Recvnum,fgdo->addrList[i].getPort());  

      }//end of else if( Recvnum > 0 )
      else
      {
         printf("Not RECV %d: Port: %d \n", i,fgdo->addrList[i].getPort());
      }
      j++;
   }//enf of for

   fclose(fd);
   fclose(fd_10184);
   fclose(fd_10183);
   fclose(fd_10180);
   fclose(fd_10175);
   fclose(fd_10176);
   fclose(fd_10178);
   fclose(fd_10177);
   fclose(fd_10181);
   fclose(fd_10182);
   return;
}

//Send and Receive data to/from FG using UDP socket Broadcast
void FG::UdpCommunication(FG* fg,FILE* fd,CoreSim::NetVdn::NetVdnClient& vdn)
{
   CoreSim::Osal::Int32 sendnum = 0;
   //CoreSim::Osal::Int32 Recvnum = 0;
   //UCHAR *recvBuff = NULL;  
   CoreSim::Osal::Net::SocketAddress clientAddr;
   //UCHAR m_simsoftBf[4224] = "";
   //UCHAR m_dataBuff[2048]  = "";
   //int size = 0;
   FGDI *fgdi = NULL;
   FGDO *fgdo = NULL;
   fd = fopen("log.txt","a+");
   fgdi = fg->fgdi;
   fgdo = fg->fgdo;

   //send DIS data to FG
   for (int i = FGDI::RM08F_SIDE_1_MON; i <= FGDI::RM03D_FCU_OWN_ATH_SW_MON /*UDP_SOCKET_TOTAL_DIS_IN 130*/; i++)
   {   

      //CoreSim::Osal::UChar data = fgdi->diList[i].getVal()&0xff;   //Used in Sim FG stadge.
      CoreSim::Osal::UChar data = fg->DisInFG[i].getVal()&0xff;      //to be modified later in integration stadge.

      if(i == FGDI::RT01J_NEO_MON)   //for NEO
      {
        data = 1;
      }
      //get internaly link
      if((i == FGDI::LT03G_AP_OPP_ENGD_MON_COM) || (i == FGDI::RT03G_AP_OPP_ENGD_MON_MON)) //FMGC1 AD-14D to FMGC2 AA-3G AD-3G.
      {
         data = fgdo->valueList[FGDO::AP_OWN_ENGD_MON_OPP_I];  //AD-14D m_outputs.RT14D_AP_OWN_ENGD_MON_OPP[0]
      }
      if((i == FGDI::LT02E_AP_OPP_ENGD_COM_COM) || (i == FGDI::RT02E_AP_OPP_ENGD_COM_MON))   //FMGC1 AA-14B to FMGC2 AA-2E AD-2E
      {
         data = fgdo->valueList[FGDO::AP_OWN_ENGD_COM_OPP_I];  //AA-14B m_outputs.LT14B_AP_OWN_ENGD_COM_OPP[0]
      }
      if(i == FGDI::LT01F_ATH_OPP_ENGD_COM)  //FMGC1 AA-14D to FMGC2 AA-1F
      {
         data = fgdo->valueList[FGDO::ATH_OWN_ENGD_COM_OPP_I];  //AA-14D  m_outputs.LT14D_ATH_OWN_ENGD_COM_OPP
      }
      if(i == FGDI::RT01F_ATH_OPP_ENGD_MON)  //FMGC1 AD-14F to FMGC2 AD-1F
      {
         data = fgdo->valueList[FGDO::ATH_OWN_ENGD_MON_OPP_I];  //AD-14F m_outputs.RT14F_ATH_OWN_ENGD_MON_OPP
      }
      if(i == FGDI::LT04E_FD_OPP_ENGD_COM)   //FMGC1 AA-15C to FMGC2 AA-4E
      {
         data = fgdo->valueList[FGDO::FD_OWN_ENGD_COM_OPP_I];  //AA-15C m_outputs.LT15C_FD_OWN_ENGD_COM_OPP
      }
      if(i == FGDI::RT04E_FD_OPP_ENGD_MON)   //FMGC1 AD-15E to FMGC2 AD-4E  
      {
         data = fgdo->valueList[FGDO::FD_OWN_ENGD_MON_OPP_I];  //AD-15E    m_outputs.RT15E_FD_OWN_ENGD_MON_OPP_I           
      }
      if(i == FGDI::LT02B_FMGC_OPP_HLTY)  //FMGC1 AB-12H to FMGC2 AA-2B
      {
         data = fgdo->valueList[FGDO::FMGC_HLTY_I];  //AB-12H
      }

      //on ground 
      /*
      if((i == FGDI::RM06F_ENGINE_OWN_STOP) || i== (FGDI::RT06F_ENGINE_OPP_STOP))
      {
        data = 0;
      }
      */
      //send to fmgc1
      sendnum  = m_txUdpSocket.sendTo(&data,sizeof(data),fgdi->addrList[i],5U);
      if(sendnum < 0)
      {
         fprintf(fd,"Send ERROR! %d: %d IP:%d.%d.%d.%d Port: %d \n", i,sendnum,fgdi->addrList[i].getIpAddress().getOctet(1),fgdi->addrList[i].getIpAddress().getOctet(2),fgdi->addrList[i].getIpAddress().getOctet(3),fgdi->addrList[i].getIpAddress().getOctet(4),fgdi->addrList[i].getPort());
      }
      else
      {
         fprintf(fd,"Send: %d: %d IP:%d.%d.%d.%d Port: %d MSG: %d \n", i,sendnum,fgdi->addrList[i].getIpAddress().getOctet(1),fgdi->addrList[i].getIpAddress().getOctet(2),fgdi->addrList[i].getIpAddress().getOctet(3),fgdi->addrList[i].getIpAddress().getOctet(4),fgdi->addrList[i].getPort(),data);
      }

      //send to fmgc2
      INT16 port = fgdi->addrList[i].getPort();
      if((port != 10000) && (port != 10021) && (port != 10023) && (port != 10002) && (port != 10004) && (port != 10025) && (port != 10009))
      {
         data = DisInFG2[i].getVal()&0xff;
      } 

      CoreSim::Osal::Int32 sendnum2 = m_txUdpSocket2.sendTo(&data,sizeof(data),fgdi->addrList2[i],5U);
      if(sendnum2 < 0)
      {
         fprintf(fd,"Send ERROR! %d: %d IP:%d.%d.%d.%d Port: %d \n", i,sendnum2,fgdi->addrList2[i].getIpAddress().getOctet(1),fgdi->addrList2[i].getIpAddress().getOctet(2),fgdi->addrList2[i].getIpAddress().getOctet(3),fgdi->addrList2[i].getIpAddress().getOctet(4),fgdi->addrList2[i].getPort());
      }
      else
      {
         fprintf(fd,"Send: %d: %d IP:%d.%d.%d.%d Port: %d MSG: %d \n", i,sendnum2,fgdi->addrList2[i].getIpAddress().getOctet(1),fgdi->addrList2[i].getIpAddress().getOctet(2),fgdi->addrList2[i].getIpAddress().getOctet(3),fgdi->addrList2[i].getIpAddress().getOctet(4),fgdi->addrList2[i].getPort(),data);        
      }

   }//end of for 

#if 1
   //send A429 data to FG
   for(int i=0; i < 1; )
   {
      //COM_ADC_OWN_IN-size:2048-port:10198
      packA429Datas(m_inputsA429.in_ADCOWN,i,fgdi,fd);
      i++;   
      //COM_ADC_3_IN-size:2048-port:10199
      packA429Datas(m_inputsA429.in_ADC3,i,fgdi,fd);     
      i++;
      //COM_FAC_OWN_IN-size:2048-port:10216
      packA429Datas(m_inputsA429.in_FACOWN,i,fgdi,fd); 
      i++;      
      //COM_FAC_OPP_IN-size:2048-port:10217
      packA429Datas(m_inputsA429.in_FACOPP,i,fgdi,fd); 
      i++;
      //COM_FADEC_OWN_A_IN-size:2048-port:10212
      packA429Datas(m_inputsA429.in_FADOWNA,i,fgdi,fd);
      i++;     
      //COM_FADEC_OWN_B_IN-size:2048-port:10213
      packA429Datas(m_inputsA429.in_FADOWNB,i,fgdi,fd);
      i++;    
      //COM_FCU_OWN_A_IN-size:2048-port:10218
      packA429Datas(m_inputsA429.in_FCUOWNA,i,fgdi,fd);
      i++;
      //COM_FCU_OWN_B_IN-size:2048-port:10219
      packA429Datas(m_inputsA429.in_FCUOWNB,i,fgdi,fd);
      i++;  
      //COM_FMGC_MON_IN-size:2048-port:10181  FROM FMGC1/2 AE-14A/14B TO FMGC1/2 AB-9J/9K
      packA429DatasMonOut(m_dataMonOut,i,fgdi);    
      i++; 
      //COM_FMGC_OPP_IN-size:2048-port:10175 data from COM_FMGC_OWN_A_OUT of oppsite FG.
      packA429DatasMonOut(m_dataFmgcOwnA,i,fgdi);        
      i++; 
      //COM_FQI_IN-size:2048-port:10225
      packA429Datas(m_inputsA429.in_FQI,i,fgdi,fd);
      i++;       
      //COM_ILS_OWN_IN-size:2048-port:10201
      packA429Datas(m_inputsA429.in_LSOWN,i,fgdi,fd);
      i++;
      //COM_ILS_OPP_IN-size:2048-port:10203
      packA429Datas(m_inputsA429.in_LSOPP,i,fgdi,fd);
      i++;
      //COM_IRS_OWN_IN-size:2048-port:10195
      packA429Datas(m_inputsA429.in_IRSOWN,i,fgdi,fd);
      i++;
      //COM_IRS_OPP_IN-size:2048-port:10196
      packA429Datas(m_inputsA429.in_IRSOPP,i,fgdi,fd);
      i++;
      //COM_IRS_3_IN-size:2048-port:10197
      packA429Datas(m_inputsA429.in_IRS3,i,fgdi,fd);
      i++;
      //COM_RA_OWN_IN-size:2048-port:10205
      packA429Datas(m_inputsA429.in_RAOWN,i,fgdi,fd);
      i++;
      //COM_RA_OPP_IN-size:2048-port:10209
      packA429Datas(m_inputsA429.in_RAOPP,i,fgdi,fd);
      i++;
      //COM_TCAS_IN-size:2048-port:10207
      packA429Datas(m_inputsA429.in_TCASOWN,i,fgdi,fd);
      i++;
      //COM_VOR_OWN_IN-size:2048-port:10206
      packA429Datas(m_inputsA429.in_VOROWN,i,fgdi,fd);
      i++;
      //COM_VOR_OPP_IN-size:2048-port:10211
      packA429Datas(m_inputsA429.in_VOROPP,i,fgdi,fd);
      i++;
      //COM_SPATIAL_1_IN-size:2048-port:10226
      i++;
      //COM_SPATIAL_2_IN-size:2048-port:10227
      i++;
      //MON_TCAS_OPP_IN-size:2048-port:10210
      packA429Datas(m_inputsA429.in_TCASOPP,i,fgdi,fd);
      i++;
      //MON_ADC_OPP_IN-size:2048-port:10200
      packA429Datas(m_inputsA429.in_ADCOPP,i,fgdi,fd);
      i++;
      //MON_FADEC_OPP_A_IN-size:2048-port:10214
      packA429Datas(m_inputsA429.in_FADOPPA,i,fgdi,fd);
      i++;
      //MON_FADEC_OPP_B_IN-size:2048-port:10215
      packA429Datas(m_inputsA429.in_FADOPPB,i,fgdi,fd);
      i++;
      //MON_FMGC_OWN_A_IN-size:2048-port:10175
      packA429DatasMonOut(m_dataFmgcOwnA,i,fgdi); 
      i++;
      //MON_FMGC_OWN_B_IN-size:2048-port:10176
      packA429DatasMonOut(m_dataFmgcOwnB,i,fgdi); 
      i++;
      //FM_FG_SHARED_MEM_IO_IN-size:4096-port:10183
      //packA429Datas(m_inputsA429.in_CMIO,i,fgdi,fd);   //TODO: to check CM data later.     
      
      int sendnum1 = m_txSocketA429[i].sendTo(&m_inputsA429.in_CMIO,sizeof(m_inputsA429.in_CMIO),fgdi->addrList[i+UDP_SOCKET_TOTAL_DIS_IN],5U); 
      if(sendnum1 > 0)
      {
         fprintf(fd,"Send: %d: %d IP:%d.%d.%d.%d Port: %d MSG:  ", i,sendnum1,fgdi->addrList[i+UDP_SOCKET_TOTAL_DIS_IN].getIpAddress().getOctet(1),fgdi->addrList[i+UDP_SOCKET_TOTAL_DIS_IN].getIpAddress().getOctet(2),fgdi->addrList[i+UDP_SOCKET_TOTAL_DIS_IN].getIpAddress().getOctet(3),fgdi->addrList[i+UDP_SOCKET_TOTAL_DIS_IN].getIpAddress().getOctet(4),fgdi->addrList[i+UDP_SOCKET_TOTAL_DIS_IN].getPort());
         CoreSim::Osal::UInt16 *p_Label = NULL;
         int k = 0;
         p_Label = &(m_inputsA429.in_CMIO.Prot.FM_PUPRDY);
         while ( k!= sendnum1/2 )
         {          
            fprintf(fd,"%04X",(*p_Label));

            fflush(fd); 
            p_Label++;
            k++;
         }
         fprintf(fd,"\n");                              
         fflush(fd); 
      }

      if(fgdi->addrList[i+UDP_SOCKET_TOTAL_DIS_IN].getPort() == 10183 && fgdi->addrList[i+UDP_SOCKET_TOTAL_DIS_IN].getIpAddress().toString() == "192.168.9.154")
      {           
         int sendnum2 = m_txSocketA429[i].sendTo(&m_inputsA429.in_CMIO,sizeof(m_inputsA429.in_CMIO),fgdi->addrList2[i+UDP_SOCKET_TOTAL_DIS_IN],5U); 
         if(sendnum2 > 0)
         {
            fprintf(fd,"Send: %d: %d IP:%d.%d.%d.%d Port: %d MSG:  ", i,sendnum1>0?sendnum1:sendnum2,fgdi->addrList2[i+UDP_SOCKET_TOTAL_DIS_IN].getIpAddress().getOctet(1),fgdi->addrList2[i+UDP_SOCKET_TOTAL_DIS_IN].getIpAddress().getOctet(2),fgdi->addrList2[i+UDP_SOCKET_TOTAL_DIS_IN].getIpAddress().getOctet(3),fgdi->addrList2[i+UDP_SOCKET_TOTAL_DIS_IN].getIpAddress().getOctet(4),fgdi->addrList2[i+UDP_SOCKET_TOTAL_DIS_IN].getPort());
            CoreSim::Osal::UInt16 *p_Label = NULL;
            int k = 0;
            p_Label = &(m_inputsA429.in_CMIO.Prot.FM_PUPRDY);
            while ( k!= sendnum1/2 )
            {          
               fprintf(fd,"%04X",(*p_Label));

               fflush(fd); 
               p_Label++;
               k++;
            }
            fprintf(fd,"\n");                              
            fflush(fd); 
         }
      }

      i++;
      //FM_FG_SHARED_MEM_FM_IN-size:4096-port: 10184
      i++;
      //COMMAND_SIMSOFT-size:2048-port:10230
      i++;
      //SNAPSHOT_IN_COM-size:4224-port:10232
      i++;
      //SNAPSHOT_IN_MON-size:4224-port:10234
      i++;
      //SNAPSHOT_IN_IO-size:4224-port:10236
      //i++;    
   }//end of for
#endif
   RecvDisDataFromFG(fgdo,fd,vdn);

   RecvA429DataFromFG(fgdo,fd);

   //m_outputs.FG_SafeTest                    = fg_safeTest;
   m_outputs.RM14D_FD_OWN_ENGD_MON[0]       =  fgdo->valueList[FGDO::FD_OWN_ENGD_MON_OWN_I];
   m_outputs.LM14D_FD_OWN_ENGD_COM[0]       =  fgdo->valueList[FGDO::FD_OWN_ENGD_COM_OWN_I];
   m_outputs.RT14D_AP_OWN_ENGD_MON_OPP[0]   =  fgdo->valueList[FGDO::AP_OWN_ENGD_MON_OPP_I]; 
   m_outputs.LT14B_AP_OWN_ENGD_COM_OPP[0]   =  fgdo->valueList[FGDO::AP_OWN_ENGD_COM_OPP_I];
   m_outputs.RM12G_AP_OWN_ENGD_MON[0]       =  fgdo->valueList[FGDO::AP_OWN_ENGD_MON_OWN_I];
   m_outputs.LM10D_AP_OWN_ENGD_COM[0]       =  fgdo->valueList[FGDO::AP_OWN_ENGD_COM_OWN_I];
   m_outputs.RM12E_ATHR_OWN_ENGD_MON[0]     =  fgdo->valueList[FGDO::ATH_OWN_ENGD_MON_OWN_I];
   m_outputs.RM12E_ATHR_OWN_ENGD_MON[1]     =  fgdo->valueList[FGDO::ATH_OWN_ENGD_MON_OWN_I];
   m_outputs.LM12D_ATHR_OWN_ENGD_COM[0]     =  fgdo->valueList[FGDO::ATH_OWN_ENGD_COM_OWN_I];
   m_outputs.LM12D_ATHR_OWN_ENGD_COM[1]     =  fgdo->valueList[FGDO::ATH_OWN_ENGD_COM_OWN_I];
   m_outputs.RM13D_RUDDER_LOCK_MON[0]       =  fgdo->valueList[FGDO::RUDDER_LOCK_MON_I];
   m_outputs.LM13D_RUDDER_LOCK_COM[0]       =  fgdo->valueList[FGDO::RUDDER_LOCK_COM_I];
   m_outputs.RM15C_STICK_LOCK_MON[0]        =  fgdo->valueList[FGDO::STICK_LOCK_MON_I];
   m_outputs.LM15C_STICK_LOCK_COM[0]        =  fgdo->valueList[FGDO::STICK_LOCK_COM_I];
   m_outputs.LT15C_FD_OWN_ENGD_COM_OPP[0]   =  fgdo->valueList[FGDO::FD_OWN_ENGD_COM_OPP_I];
   m_outputs.LM09A_LS_INHIB[0]              =  fgdo->valueList[FGDO::LS_INH_I];
   m_outputs.LM12H_FMGC_HEALTHY[0]          =  fgdo->valueList[FGDO::FMGC_HLTY_I];
   m_outputs.LM12H_FMGC_HEALTHY[1]          =  fgdo->valueList[FGDO::FMGC_HLTY_I];
   m_outputs.LM09B_FCU_OWN_HLTY[0]          =  fgdo->valueList[FGDO::FCU_OWN_HLT_I];
   m_outputs.RT15E_FD_OWN_ENGD_MON_OPP_I[0] =  fgdo->valueList[FGDO::FD_OWN_ENGD_MON_OPP_I];
   m_outputs.LT14D_ATH_OWN_ENGD_COM_OPP[0]  =  fgdo->valueList[FGDO::ATH_OWN_ENGD_COM_OPP_I];
   m_outputs.RT14F_ATH_OWN_ENGD_MON_OPP[0]  =  fgdo->valueList[FGDO::ATH_OWN_ENGD_MON_OPP_I];


   fclose(fd);
   return;
}

//get files base function
void FG::GetFiles(CoreSim::Osal::String path,CoreSim::Osal::String filter,vector<CoreSim::Osal::String>& filesname,vector<CoreSim::Osal::String>& files)
{
   intptr_t   hFile   =   0;  //handler
   struct _finddata_t fileinfo;  //include io.h
   string p;  
   if ((hFile = _findfirst(p.assign(path).append(filter).c_str(),&fileinfo)) !=  -1)  
   {  //get all .csv files
      do  
      {  
         files.push_back(p.assign(path).append(fileinfo.name) );  
         filesname.push_back(fileinfo.name);
      }while(_findnext(hFile, &fileinfo)  == 0);  
      _findclose(hFile);  
   } 
   
   return;
}

void FG::bufferInputs(FG* fg)
{
   /*According to ICD Appendix-A320 of chapter A.13 on Page 11, the external simulation wil not be required to 
   send this value to Fms2. Likewise, the FMS2 Simulation System will not provide this value to the external simulation. */
	//m_inputsA429.in_CMIO.Prot.MEM_BUSY               = 0xff00;	//20200420, delete it. TODO:It is important to modify the state of the parameter.
	
#if BufInput
	//TODO: to update the variabels according to FG after ENGING Normal in FG output
	al_Pins_Dis1 = m_comMemoryIO.Hard_FGFM_T.AL_PINS1;
	al_Pins_Dis2 = m_comMemoryIO.Hard_FGFM_T.AL_PINS2;
	ac_Pins_Dis  = m_comMemoryIO.Hard_FGFM_T.AC_PINS;
	//fg_hlty      = m_comMemoryIO.Prot.FG_HLY;
	code_cmp_1   = m_comMemoryIO.Prot_NewFM.CODE_COMPFG_1;
	code_cmp_2   = m_comMemoryIO.Prot_NewFM.CODE_COMPFG_2;
	code_cmp_3   = m_comMemoryIO.Prot_NewFM.CODE_COMPFG_3;
	code_cmp_4   = m_comMemoryIO.Prot_NewFM.CODE_COMPFG_4;
	code_cmp_5   = m_comMemoryIO.Prot_NewFM.CODE_COMPFG_5;
	code_cmp_6   = m_comMemoryIO.Prot_NewFM.CODE_COMPFG_6;	
	cfm_engine   = m_comMemoryIO.Hard_FGFM_E.CFM_ENG;
	iae_engine   = m_comMemoryIO.Hard_FGFM_E.IAE_ENG;
	pw_engine    = m_comMemoryIO.PW_ENGINE;

	setBit(di_word0_bus66,21,!getBit(ac_Pins_Dis,1));				//Ac_Type_1
	setBit(di_word0_bus66,24,!getBit(ac_Pins_Dis,2));				//Ac_Type_2
	setBit(di_word0_bus66,19,!((InputsDIS *)DisInFG)->RM06E_AC_TYPE_3);			//Ac_Type_3
	setBit(di_word0_bus66,22,!getBit(ac_Pins_Dis,3));				//Eng_Type_1
	setBit(di_word0_bus66,32,!getBit(ac_Pins_Dis,4));				//Eng_Type_2
	setBit(di_word0_bus66,23,!getBit(ac_Pins_Dis,5));				//Eng_Type_3
	setBit(di_word0_bus66, 7,!getBit(ac_Pins_Dis,12));			    //Eng_Type_4
	setBit(di_word0_bus66, 8,!((InputsDIS *)DisInFG)->RM06C_ENG_TYPE_5);			//Engine_Type_5
	setBit(di_word0_bus66,26,!DisInFG[FGDI::LM04D_NOSE_GEAR_OWN]);		//Nose Gear Pressed
	setBit(di_word0_bus66,15,!DisInFG[FGDI::LT06B_NAV_CONTROL_OPP]);    //RMP_Nav_Control_Opp
	setBit(di_word0_bus66,13,!((InputsDIS *)DisInFG)->RM09D_AIRLINE_9);			//Cross_Load_Mode_Internal
	setBit(di_word0_bus66,12,!getBit(ac_Pins_Dis,6));				//Vmo_mmo_1
	setBit(di_word0_bus66, 9,!getBit(ac_Pins_Dis,7));				//Vmo_mmo_2 
	setBit(di_word0_bus66,28,1);													//Data_Loader_Connected
	setBit(di_word0_bus66, 5,!((InputsDIS *)DisInFG)->RM07C_PP_PARITY_2G);       //FM_Parity_Error_2G
	setBit(di_word0_bus66, 4,!DisInFG[FGDI::LT02A_CDU_OPP_FAIL]);       //MCDU_Opp_Healthy_Internal
	setBit(di_word0_bus66,18,1);													//
	setBit(di_word0_bus66,14,1);													//
	setBit(di_word0_bus66,10,1);													//
	
	//set di_word1 0x000004c5
	setBit(di_word1_bus68,13,!((InputsDIS *)DisInFG)->LM05D_NAV_BACKUP_SELECTED);//NAV_BACKUP_SELECTED
	setBit(di_word1_bus68, 9,!DisInFG[FGDI::LM02A_CDU_OWN_FAIL]);       //MCDU_Own_Healthy_internal
	setBit(di_word1_bus68, 5,!DisInFG[FGDI::LM07E_NAV_CONTROL_OWN]);    //RMP_Nav_Control_Own
	setBit(di_word1_bus68,11,!((InputsDIS *)DisInFG)->RM10F_AIRLINE_19);			//Fg_Ac_Ident_1
	setBit(di_word1_bus68, 3,!((InputsDIS *)DisInFG)->RM10J_AIRLINE_22);			//Fg_Ac_Ident_2
	setBit(di_word1_bus68, 8,1);                                                 //AC_5
	setBit(di_word1_bus68, 7,1);                                                 //Engine_7
	setBit(di_word1_bus68, 1,1);                                                 //AFMC_Installed
#endif
	
	/*updata data for FM package*/
#if 0
	fg.DisInFG[FG::FGDI::LM04D_NOSE_GEAR_OWN]       = (m_comMemoryIO.Hard_FGFM_T.NG_PRES == 0x00ff)?1:0;		   
	fg.DisInFG[FG::FGDI::LT02A_CDU_OPP_FAIL]        = (m_comMemoryIO.Hard_FGFM_T.CDU_FAIL&0x00ff == 0x00ff)?1:0;
	fg.DisInFG[FG::FGDI::LM02A_CDU_OWN_FAIL]        = (m_comMemoryIO.Hard_FGFM_T.CDU_FAIL&0xff00 == 0xff00)?1:0;
	fg.DisInFG[FG::FGDI::LT06B_NAV_CONTROL_OPP]     = (m_comMemoryIO.Hard_FGFM_T.NAV&0x00ff == 0x00ff)?1:0;
	fg.DisInFG[FG::FGDI::LM07E_NAV_CONTROL_OWN]     = (m_comMemoryIO.Hard_FGFM_T.NAV&0xff00 == 0xff00)?1:0;
	fg.DisInFG[FG::FGDI::LM06D_SIDE_1_COM]          = (m_comMemoryIO.Hard_FGFM_T.SIDE_1 == 0x00ff)?1:0;
	fg.DisInFG[FG::FGDI::LT02B_FMGC_OPP_HLTY]       = (m_comMemoryIO.Hard_FGFM_T.FMGC_OPP_HLY == 0x00ff)?1:0;
	fg.DisInFG[FG::FGDI::LT02E_AP_OPP_ENGD_COM_COM] = (m_comMemoryIO.Hard_FGFM_T.AP_ENG&0x00ff == 0x00ff)?1:0;
   /*updata in_FG2FM data for FM package, bus 100*/		   
	in_FG2FM.MAG_TRUE_SEL	   = m_comMemoryIO.Hard_FGFM_T.MAG_TRUE_SEL;  //ARINC INPUT DISCRETE ?FMGC OWN A: DISCRETE WORD 5 ?LABEL 145, bit 23: NORTH REF P/B
	in_FG2FM.FG_HLY			   = m_comMemoryIO.Prot.FG_HLY;
	in_FG2FM.CROSS_LOAD		   = m_comMemoryIO.Prot_NewFM.CROSS_LOAD;
	in_FG2FM.SIDE_1			   = m_comMemoryIO.Hard_FGFM_T.SIDE_1;
	in_FG2FM.FMGC_OPP_HLY	   = m_comMemoryIO.Hard_FGFM_T.FMGC_OPP_HLY;
	in_FG2FM.AP_ENG			   = m_comMemoryIO.Hard_FGFM_T.AP_ENG;
	in_FG2FM.DERATED_CLI	      = m_comMemoryIO.Hard_FGFM_T.DERATED_CLI;
	in_FG2FM.FPA_TGT		      = m_comMemoryIO.Inter_FGFM.FPA_TGT;
	in_FG2FM.VS_TGT		      = m_comMemoryIO.Inter_FGFM.VS_TGT;           
	in_FG2FM.ALT_TGT		      = m_comMemoryIO.Inter_FGFM.ALT_TGT;          
	in_FG2FM.SPD_TGT		      = m_comMemoryIO.Inter_FGFM.SPD_TGT;          
	in_FG2FM.MACH_TGT	         = m_comMemoryIO.Inter_FGFM.MACH_TGT;         
	in_FG2FM.FMGC_DIS_WD_1     = m_comMemoryIO.Inter_FGFM.FMGC_DIS_WD_1;    
	in_FG2FM.FMGC_DIS_WD_2     = m_comMemoryIO.Inter_FGFM.FMGC_DIS_WD_2;    
	in_FG2FM.FMGC_DIS_WD_3     = m_comMemoryIO.Inter_FGFM.FMGC_DIS_WD_3;    
	in_FG2FM.FMGC_DIS_WD_4     = m_comMemoryIO.Inter_FGFM.FMGC_DIS_WD_4;    
	in_FG2FM.FMGC_DIS_WD_5     = m_comMemoryIO.Inter_FGFM.FMGC_DIS_WD_5;    
	in_FG2FM.FMGC_ATS_DIS_WD   = m_comMemoryIO.Inter_FGFM.FMGC_ATS_DIS_WD;  
	in_FG2FM.FG_PITCH_ROLL_DIS = m_comMemoryIO.Inter_FGFM.FG_PITCH_ROLL_DIS;
	in_FG2FM.FMGC_PRIORITY     = m_comMemoryIO.Inter_FGFM.FMGC_PRIORITY;    
	in_FG2FM.OPP_FMGC_DIS_WD_4 = m_comMemoryIO.Inter_FGFM.OPP_FMGC_DIS_WD_4;
	in_FG2FM.OUTPUT_RLIM_1     = m_comMemoryIO.Inter_FGFM.OUTPUT_RLIM_1;  
	in_FG2FM.RMP_OWN_MLS_SEL   = 0;
	in_FG2FM.RMP_OPP_MLS_SEL   = 0;
#endif	
	/*update CM data*/	
	//inputs hard (fg-fm) 
	CoreSim::Osal::UInt16 tmp1 = 0;
   CoreSim::Osal::UInt16 tmp2 = 0;

   m_inputsA429.in_CMIO.Hard_FGFM_T.NG_PRES         = fg->DisInFG[FG::FGDI::LM04D_NOSE_GEAR_OWN]?0x00ff:0x0; 

   tmp1 = 0;
   tmp2 = 0;
   if(((InputsDIS *)fg->DisInFG)->LM02A_CDU_OWN_FAIL.getVal()     != false)   {tmp1 = 0xff;}
   if(((InputsDIS *)fg->DisInFG)->LT02A_CDU_OPP_FAIL.getVal()     != false)   {tmp2 = 0xff;}
   m_inputsA429.in_CMIO.Hard_FGFM_T.CDU_FAIL        = tmp1 | (tmp2<<8);  //SEE DIS TABLE 4.8.5

	tmp1 = 0;
	tmp2 = 0;
   if(fg->DisInFG[FG::FGDI::LM07E_NAV_CONTROL_OWN]){tmp1 = 0xff;}
   if(fg->DisInFG[FG::FGDI::LT06B_NAV_CONTROL_OPP]){tmp2 = 0xff;}
   m_inputsA429.in_CMIO.Hard_FGFM_T.NAV            = tmp1 | (tmp2<<8);  //SEE DIS TABLE 4.8.6

   m_inputsA429.in_CMIO.Hard_FGFM_T.AL_PINS1       = m_comMemoryIO.Hard_FGFM_T.AL_PINS1&0xffff;
   m_inputsA429.in_CMIO.Hard_FGFM_T.AL_PINS2       = m_comMemoryIO.Hard_FGFM_T.AL_PINS2&0xffff;
   m_inputsA429.in_CMIO.Hard_FGFM_T.AC_PINS        = m_comMemoryIO.Hard_FGFM_T.AC_PINS&0xffff;
   m_inputsA429.in_CMIO.Hard_FGFM_T.SIDE_1         = fg->DisInFG[FG::FGDI::LM06D_SIDE_1_COM]?0x00ff:0x0;        
   m_inputsA429.in_CMIO.Hard_FGFM_T.FMGC_OPP_HLY   = fg->DisInFG[FG::FGDI::LT02B_FMGC_OPP_HLTY]?0x00ff:0x0;
    
	tmp1 = 0;
	tmp2 = 0;
   if(m_outputs.LM10D_AP_OWN_ENGD_COM[0])   {tmp1 = 0xff;}				   //LM03E from LM10D, internal link.
	if(fg->DisInFG[FG::FGDI::LT02E_AP_OPP_ENGD_COM_COM]) {tmp2 = 0xff;}			   //LT02E
   m_inputsA429.in_CMIO.Hard_FGFM_T.AP_ENG         = tmp1 | (tmp2<<8);    //SEE DIS TABLE 4.8.4

    m_inputsA429.in_CMIO.Hard_FGFM_T.MAG_TRUE_SEL   = 0XFF00;			   //TODO:
    m_inputsA429.in_CMIO.Hard_FGFM_T.DERATED_CLI    = ((InputsDIS *)fg->DisInFG)->RM11H_AIRLINE_27;
    m_inputsA429.in_CMIO.Hard_FGFM_T.ENG_OWN_STOP   = fg->DisInFG[FG::FGDI::RM06F_ENGINE_OWN_STOP]?0x00ff:0x0;  
    m_inputsA429.in_CMIO.Hard_FGFM_T.ENG_OPP_STOP   = fg->DisInFG[FG::FGDI::RT06F_ENGINE_OPP_STOP]?0x00ff:0x0;  

	//ADC Data
	m_inputsA429.in_CMIO.Adc_T.BAROC_HPa_1		= m_inputsA429.in_ADCOWN.ADR_Label234.getVal();		  //M this should be moved to the specific place
	m_inputsA429.in_CMIO.Adc_T.BAROC_HB_1		= m_inputsA429.in_ADCOWN.ADR_Label235.getVal();		  //M
	m_inputsA429.in_CMIO.Adc_T.BAROC_HPa_2		= m_inputsA429.in_ADCOWN.ADR_Label236.getVal();		  //M
	m_inputsA429.in_CMIO.Adc_T.BAROC_HB_2		= m_inputsA429.in_ADCOWN.ADR_Label237.getVal();		  //M
  
	CoreSim::Osal::Int32 temp = 0;
	m_inputsA429.in_IRSOWN.Label_350.getBits2C(23,4,1,temp);   
	m_inputsA429.in_CMIO.Adc_E.ADCFGVAL			= temp;//BVAADC;//(temp != 0x06)?0xffff:0x0;//m_inputsA429.in_ADCOWN.ADR_Label;          //0598  BOOL C2  TODO
	temp = 0;
	m_inputsA429.in_IRSOWN.Label_350.getBits2C(11,2,1,temp);
	m_inputsA429.in_CMIO.Adc_E.ADCSOURCE		= temp;//BADC3SEL;//temp;//m_inputsA429.in_ADCOWN.ADR_Label;               //059C  ENUM C2 ADCFGVAL  0 = OWN, 1 = #3  //TODO
	//if(m_inputsA429.in_CMIO.Adc_E.ADCFGVAL == 0xffff)
	{
		m_inputsA429.in_CMIO.Adc_E.PRESS_ALT	= m_inputsA429.in_ADCOWN.ADR_Label203;                //0560  203 IEEE C2 C 
		m_inputsA429.in_CMIO.Adc_E.BARO_ALT		= m_inputsA429.in_ADCOWN.ADR_Label204;                //0568  ?  IEEE C2 C 
		m_inputsA429.in_CMIO.Adc_E.MACH			= m_inputsA429.in_ADCOWN.ADR_Label205;                //0570  205 IEEE C2 C 
		m_inputsA429.in_CMIO.Adc_E.CAS			= m_inputsA429.in_ADCOWN.ADR_Label206;                //0578  206 IEEE C2 C 
		m_inputsA429.in_CMIO.Adc_E.TAS			= m_inputsA429.in_ADCOWN.ADR_Label210;                //0580  210 IEEE C2 C 
		m_inputsA429.in_CMIO.Adc_E.TAT			= m_inputsA429.in_ADCOWN.ADR_Label211;                //0588  211 IEEE C2 C
		m_inputsA429.in_CMIO.Adc_E.SAT			= m_inputsA429.in_ADCOWN.ADR_Label213;                //0590  213 A429 C2 M 		
	}

   //IRS/GPS HYBRID OWN
	m_inputsA429.in_CMIO.Irs.TRUE_HDG                 = m_inputsA429.in_IRS.Label_314;
    m_inputsA429.in_CMIO.Irs.FP_ACCEL                 = m_inputsA429.in_IRS.Label_323;
    m_inputsA429.in_CMIO.Irs.FP_ANGLE                 = m_inputsA429.in_IRS.Label_322;
    m_inputsA429.in_CMIO.Irs.PITCH_ANGLE              = m_inputsA429.in_IRS.Label_324;
    m_inputsA429.in_CMIO.Irs.ROLL_ANGLE               = m_inputsA429.in_IRS.Label_325;
    m_inputsA429.in_CMIO.Irs.VERT_ACCEL               = m_inputsA429.in_IRS.Label_364;
    m_inputsA429.in_CMIO.Irs.BODY_YAWRATE             = m_inputsA429.in_IRS.Label_330;
    m_inputsA429.in_CMIO.Irs.INERTIAL_ALT             = m_inputsA429.in_IRS.Label_361;
    m_inputsA429.in_CMIO.Irs.INERTIAL_VS              = m_inputsA429.in_IRS.Label_365;
    m_inputsA429.in_CMIO.Irs.MAG_HDG                  = m_inputsA429.in_IRS.Label_320;
    m_inputsA429.in_CMIO.Irs.MAGHDGVAL                = 0xFF;//m_inputsA429.in_IRS.MAG_HDG_VAL;  
    m_inputsA429.in_CMIO.Irs.IRSFGVAL                 = 0;//BVAIRS;//m_inputsA429.in_IRS.IRS_FG_VAL;   
    m_inputsA429.in_CMIO.Irs.IRSSOURCE                = 0;//BIRS3SEL;//m_inputsA429.in_IRS.IRS_SOURCE;   
  
	m_inputsA429.in_CMIO.Irs_Own.HYB_GS				  = m_inputsA429.in_IRSOWN.Label_175;			   //0098   175 
	m_inputsA429.in_CMIO.Irs_Own.GPS_HORIZ_FOM		  = m_inputsA429.in_IRSOWN.Label_247;			   //00A0   247
	m_inputsA429.in_CMIO.Irs_Own.HYB_PRES_POS_LAT	  = m_inputsA429.in_IRSOWN.Label_254;			   //00A8   254
	m_inputsA429.in_CMIO.Irs_Own.HYB_PRES_POS_LON	  = m_inputsA429.in_IRSOWN.Label_255;			   //00B0   255
	m_inputsA429.in_CMIO.Irs_Own.HYB_HORIZ_FOM		  = m_inputsA429.in_IRSOWN.Label_264;			   //00B8   264
	m_inputsA429.in_CMIO.Irs_Own.HYB_NS_VEL			  = m_inputsA429.in_IRSOWN.Label_266;			   //00C0   266
	m_inputsA429.in_CMIO.Irs_Own.HYB_EW_VEL			  = m_inputsA429.in_IRSOWN.Label_267;			   //00C8   267
	m_inputsA429.in_CMIO.Irs_Own.GPS_SENSOR_STATUS	  = m_inputsA429.in_IRSOWN.Label_273;			   //00D0   273
	m_inputsA429.in_CMIO.Irs_Own.GPADIRS_STATUS		  = m_inputsA429.in_IRSOWN.Label_274;			   //00D8   274

	m_inputsA429.in_CMIO.Irs_Own_2.PPOS_LAT           = m_inputsA429.in_IRSOWN.Label_310;			   //0440   310
	m_inputsA429.in_CMIO.Irs_Own_2.PPOS_LON           = m_inputsA429.in_IRSOWN.Label_311;			   //0448   311
	m_inputsA429.in_CMIO.Irs_Own_2.NS_VEL             = m_inputsA429.in_IRSOWN.Label_366;			   //0450   366
   m_inputsA429.in_CMIO.Irs_Own_2.EW_VEL             = m_inputsA429.in_IRSOWN.Label_367;			   //0458   367
   m_inputsA429.in_CMIO.Irs_Own_2.IRS_OWN_DIS        = m_inputsA429.in_IRSOWN.Label_270;			   //0460   270
   m_inputsA429.in_CMIO.Irs_Own_2.IRS_OWN_SET_LAT    = m_inputsA429.in_IRSOWN.Label_041;			   //0468   041
   m_inputsA429.in_CMIO.Irs_Own_2.IRS_OWN_SET_LON    = m_inputsA429.in_IRSOWN.Label_042;			   //0470   042
   m_inputsA429.in_CMIO.Irs_Own_2.GPS_TRK_NGLE_TRUE  = m_inputsA429.in_IRSOWN.Label_103;			   //0478   103
   m_inputsA429.in_CMIO.Irs_Own_2.GPS_PRES_POS_LAT   = m_inputsA429.in_IRSOWN.Label_110;			   //0480   110
   m_inputsA429.in_CMIO.Irs_Own_2.GPS_PRES_POS_LON   = m_inputsA429.in_IRSOWN.Label_111;			   //0488   111
   m_inputsA429.in_CMIO.Irs_Own_2.GPS_GS             = m_inputsA429.in_IRSOWN.Label_112;			   //0490   112
   m_inputsA429.in_CMIO.Irs_Own_2.HYB_TRK_ANGLE_TRUE = m_inputsA429.in_IRSOWN.Label_137;			   //0498   137

	m_inputsA429.in_CMIO.Irs_Own_End.GPS_ALTITUDE_AMSL       = m_inputsA429.in_IRSOWN.Label_076;	   //0EC0 076
	m_inputsA429.in_CMIO.Irs_Own_End.GPS_HORIZ_INTEG_LIMIT   = m_inputsA429.in_IRSOWN.Label_130;	   //0EC8 130
	m_inputsA429.in_CMIO.Irs_Own_End.HYBRID_HORIZ_INTG_LIMIT = m_inputsA429.in_IRSOWN.Label_131;	   //0ED0 131
	m_inputsA429.in_CMIO.Irs_Own_End.GPS_UTC                 = m_inputsA429.in_IRSOWN.Label_150;	   //0ED8 150
	m_inputsA429.in_CMIO.Irs_Own_End.PRED_DEST_ETA_ECHO      = m_inputsA429.in_IRSOWN.Label_162;	   //0EE0 162
	m_inputsA429.in_CMIO.Irs_Own_End.PRED_WPT_ETA_ECHO       = m_inputsA429.in_IRSOWN.Label_163;	   //0EE8 163
	m_inputsA429.in_CMIO.Irs_Own_End.HYBRID_LAT_FRACTIONS    = m_inputsA429.in_IRSOWN.Label_256;	   //0EF0 256
	m_inputsA429.in_CMIO.Irs_Own_End.HYBRID_LONG_FRACTIONS   = m_inputsA429.in_IRSOWN.Label_257;	   //0EF8 257
	m_inputsA429.in_CMIO.Irs_Own_End.GPS_DATE                = m_inputsA429.in_IRSOWN.Label_260;	   //0F00 260
	m_inputsA429.in_CMIO.Irs_Own_End.HYBRID_ALTITUDE         = m_inputsA429.in_IRSOWN.Label_261;	   //0F08 261
	m_inputsA429.in_CMIO.Irs_Own_End.PRED_DET_HIL            = m_inputsA429.in_IRSOWN.Label_343;	   //0F10 343
	m_inputsA429.in_CMIO.Irs_Own_End.PRED_WPT_HIL            = m_inputsA429.in_IRSOWN.Label_347;	   //0F18 347

	//IRS/GPS HYBRID OPP
	m_inputsA429.in_CMIO.Irs_Opp.HYB_GS						    = m_inputsA429.in_IRSOPP.Label_175;       //0098   175
	m_inputsA429.in_CMIO.Irs_Opp.GPS_HORIZ_FOM				 = m_inputsA429.in_IRSOPP.Label_247;       //00A0   247
	m_inputsA429.in_CMIO.Irs_Opp.HYB_PRES_POS_LAT			 = m_inputsA429.in_IRSOPP.Label_254;       //00A8   254
	m_inputsA429.in_CMIO.Irs_Opp.HYB_PRES_POS_LON			 = m_inputsA429.in_IRSOPP.Label_255;       //00B0   255
	m_inputsA429.in_CMIO.Irs_Opp.HYB_HORIZ_FOM				 = m_inputsA429.in_IRSOPP.Label_264;       //00B8   264
	m_inputsA429.in_CMIO.Irs_Opp.HYB_NS_VEL					 = m_inputsA429.in_IRSOPP.Label_266;       //00C0   266
	m_inputsA429.in_CMIO.Irs_Opp.HYB_EW_VEL					 = m_inputsA429.in_IRSOPP.Label_267;       //00C8   267
	m_inputsA429.in_CMIO.Irs_Opp.GPS_SENSOR_STATUS			 = m_inputsA429.in_IRSOPP.Label_273;       //00D0   273
	m_inputsA429.in_CMIO.Irs_Opp.GPADIRS_STATUS				 = m_inputsA429.in_IRSOPP.Label_274;       //00D8   274

	m_inputsA429.in_CMIO.Irs_Opp_2.PPOS_LAT                   = m_inputsA429.in_IRSOPP.Label_310;	   //0440   310
	m_inputsA429.in_CMIO.Irs_Opp_2.PPOS_LON                   = m_inputsA429.in_IRSOPP.Label_311;	   //0448   311
	m_inputsA429.in_CMIO.Irs_Opp_2.NS_VEL                     = m_inputsA429.in_IRSOPP.Label_366;	   //0450   366
   m_inputsA429.in_CMIO.Irs_Opp_2.EW_VEL                    = m_inputsA429.in_IRSOPP.Label_367;	   //0458   367
   m_inputsA429.in_CMIO.Irs_Opp_2.IRS_OWN_DIS               = m_inputsA429.in_IRSOPP.Label_270;	   //0460   270
   m_inputsA429.in_CMIO.Irs_Opp_2.IRS_OWN_SET_LAT           = m_inputsA429.in_IRSOPP.Label_041;	   //0468   041
   m_inputsA429.in_CMIO.Irs_Opp_2.IRS_OWN_SET_LON           = m_inputsA429.in_IRSOPP.Label_042;	   //0470   042
   m_inputsA429.in_CMIO.Irs_Opp_2.GPS_TRK_NGLE_TRUE         = m_inputsA429.in_IRSOPP.Label_103;	   //0478   103
   m_inputsA429.in_CMIO.Irs_Opp_2.GPS_PRES_POS_LAT          = m_inputsA429.in_IRSOPP.Label_110;	   //0480   110
   m_inputsA429.in_CMIO.Irs_Opp_2.GPS_PRES_POS_LON          = m_inputsA429.in_IRSOPP.Label_111;	   //0488   111
   m_inputsA429.in_CMIO.Irs_Opp_2.GPS_GS                    = m_inputsA429.in_IRSOPP.Label_112;	   //0490   112
   m_inputsA429.in_CMIO.Irs_Opp_2.HYB_TRK_ANGLE_TRUE        = m_inputsA429.in_IRSOPP.Label_137;	   //0498   137
																									   
	m_inputsA429.in_CMIO.Irs_Opp_End.GPS_ALTITUDE_AMSL       = m_inputsA429.in_IRSOPP.Label_076;	   //0EC0 076
	m_inputsA429.in_CMIO.Irs_Opp_End.GPS_HORIZ_INTEG_LIMIT   = m_inputsA429.in_IRSOPP.Label_130;	   //0EC8 130
	m_inputsA429.in_CMIO.Irs_Opp_End.HYBRID_HORIZ_INTG_LIMIT = m_inputsA429.in_IRSOPP.Label_131;	   //0ED0 131
	m_inputsA429.in_CMIO.Irs_Opp_End.GPS_UTC                 = m_inputsA429.in_IRSOPP.Label_150;	   //0ED8 150
	m_inputsA429.in_CMIO.Irs_Opp_End.PRED_DEST_ETA_ECHO      = m_inputsA429.in_IRSOPP.Label_162;	   //0EE0 162
	m_inputsA429.in_CMIO.Irs_Opp_End.PRED_WPT_ETA_ECHO       = m_inputsA429.in_IRSOPP.Label_163;	   //0EE8 163
	m_inputsA429.in_CMIO.Irs_Opp_End.HYBRID_LAT_FRACTIONS    = m_inputsA429.in_IRSOPP.Label_256;	   //0EF0 256
	m_inputsA429.in_CMIO.Irs_Opp_End.HYBRID_LONG_FRACTIONS   = m_inputsA429.in_IRSOPP.Label_257;	   //0EF8 257
	m_inputsA429.in_CMIO.Irs_Opp_End.GPS_DATE                = m_inputsA429.in_IRSOPP.Label_260;	   //0F00 260
	m_inputsA429.in_CMIO.Irs_Opp_End.HYBRID_ALTITUDE         = m_inputsA429.in_IRSOPP.Label_261;	   //0F08 261
	m_inputsA429.in_CMIO.Irs_Opp_End.PRED_DET_HIL            = m_inputsA429.in_IRSOPP.Label_343;	   //0F10 343
	m_inputsA429.in_CMIO.Irs_Opp_End.PRED_WPT_HIL            = m_inputsA429.in_IRSOPP.Label_347;	   //0F18 347

	//IRS/GPS HYBRID 3
	m_inputsA429.in_CMIO.Irs_3.HYB_GS						 = m_inputsA429.in_IRS3.Label_175;         //0098   175
	m_inputsA429.in_CMIO.Irs_3.GPS_HORIZ_FOM				 = m_inputsA429.in_IRS3.Label_247;         //00A0   247
	m_inputsA429.in_CMIO.Irs_3.HYB_PRES_POS_LAT			 = m_inputsA429.in_IRS3.Label_254;         //00A8   254
	m_inputsA429.in_CMIO.Irs_3.HYB_PRES_POS_LON			 = m_inputsA429.in_IRS3.Label_255;         //00B0   255
	m_inputsA429.in_CMIO.Irs_3.HYB_HORIZ_FOM				 = m_inputsA429.in_IRS3.Label_264;         //00B8   264
	m_inputsA429.in_CMIO.Irs_3.HYB_NS_VEL					 = m_inputsA429.in_IRS3.Label_266;         //00C0   266
	m_inputsA429.in_CMIO.Irs_3.HYB_EW_VEL					 = m_inputsA429.in_IRS3.Label_267;         //00C8   267
	m_inputsA429.in_CMIO.Irs_3.GPS_SENSOR_STATUS			 = m_inputsA429.in_IRS3.Label_273;         //00D0   273
	m_inputsA429.in_CMIO.Irs_3.GPADIRS_STATUS				 = m_inputsA429.in_IRS3.Label_274;         //00D8   274
															 
	m_inputsA429.in_CMIO.Irs_3_2.PPOS_LAT					 = m_inputsA429.in_IRS3.Label_310;			//0440   310
	m_inputsA429.in_CMIO.Irs_3_2.PPOS_LON					 = m_inputsA429.in_IRS3.Label_311;			//0448   311
	m_inputsA429.in_CMIO.Irs_3_2.NS_VEL						 = m_inputsA429.in_IRS3.Label_366;			//0450   366
   m_inputsA429.in_CMIO.Irs_3_2.EW_VEL					 = m_inputsA429.in_IRS3.Label_367;			//0458   367
   m_inputsA429.in_CMIO.Irs_3_2.IRS_OWN_DIS				 = m_inputsA429.in_IRS3.Label_270;			//0460   270
   m_inputsA429.in_CMIO.Irs_3_2.IRS_OWN_SET_LAT		 = m_inputsA429.in_IRS3.Label_041;			//0468   041
   m_inputsA429.in_CMIO.Irs_3_2.IRS_OWN_SET_LON		 = m_inputsA429.in_IRS3.Label_042;			//0470   042
   m_inputsA429.in_CMIO.Irs_3_2.GPS_TRK_NGLE_TRUE		 = m_inputsA429.in_IRS3.Label_103;			//0478   103
   m_inputsA429.in_CMIO.Irs_3_2.GPS_PRES_POS_LAT		 = m_inputsA429.in_IRS3.Label_110;			//0480   110
   m_inputsA429.in_CMIO.Irs_3_2.GPS_PRES_POS_LON		 = m_inputsA429.in_IRS3.Label_111;			//0488   111
   m_inputsA429.in_CMIO.Irs_3_2.GPS_GS					 = m_inputsA429.in_IRS3.Label_112;			//0490   112
   m_inputsA429.in_CMIO.Irs_3_2.HYB_TRK_ANGLE_TRUE	 = m_inputsA429.in_IRS3.Label_137;			//0498   137

	m_inputsA429.in_CMIO.Irs_3_End.GPS_ALTITUDE_AMSL         = m_inputsA429.in_IRS3.Label_076;			//0EC0 076
	m_inputsA429.in_CMIO.Irs_3_End.GPS_HORIZ_INTEG_LIMIT     = m_inputsA429.in_IRS3.Label_130;			//0EC8 130
	m_inputsA429.in_CMIO.Irs_3_End.HYBRID_HORIZ_INTG_LIMIT   = m_inputsA429.in_IRS3.Label_131;			//0ED0 131
	m_inputsA429.in_CMIO.Irs_3_End.GPS_UTC                   = m_inputsA429.in_IRS3.Label_150;			//0ED8 150
	m_inputsA429.in_CMIO.Irs_3_End.PRED_DEST_ETA_ECHO        = m_inputsA429.in_IRS3.Label_162;			//0EE0 162
	m_inputsA429.in_CMIO.Irs_3_End.PRED_WPT_ETA_ECHO         = m_inputsA429.in_IRS3.Label_163;			//0EE8 163
	m_inputsA429.in_CMIO.Irs_3_End.HYBRID_LAT_FRACTIONS      = m_inputsA429.in_IRS3.Label_256;			//0EF0 256
	m_inputsA429.in_CMIO.Irs_3_End.HYBRID_LONG_FRACTIONS     = m_inputsA429.in_IRS3.Label_257;			//0EF8 257
	m_inputsA429.in_CMIO.Irs_3_End.GPS_DATE                  = m_inputsA429.in_IRS3.Label_260;			//0F00 260
	m_inputsA429.in_CMIO.Irs_3_End.HYBRID_ALTITUDE           = m_inputsA429.in_IRS3.Label_261;			//0F08 261
	m_inputsA429.in_CMIO.Irs_3_End.PRED_DET_HIL              = m_inputsA429.in_IRS3.Label_343;			//0F10 343
	m_inputsA429.in_CMIO.Irs_3_End.PRED_WPT_HIL              = m_inputsA429.in_IRS3.Label_347;			//0F18 347

	//FCU A-B
	m_inputsA429.in_CMIO.Fcu.FCUEIS_DIS_1		= m_inputsA429.in_FCUOWNA.Label_271;					 //02C0  271
	m_inputsA429.in_CMIO.Fcu.FCU_FGVAL			= 0;//BVAGFCU;//((InputsDIS *)DisInFG)->LM04F_FCU_OWN_HLTY;			 //m_inputsA429.in_FCUOWNA.Label_001;               //02EC  BOO
	if(m_inputsA429.in_CMIO.Fcu.FCU_FGVAL)
	{		
		CoreSim::Osal::Bool BHDGVSSEL(false);
		CoreSim::Osal::Bool BTRKFPASEL(false);
		CoreSim::Osal::Bool BXFCUMACHSEL(false);
		m_inputsA429.in_CMIO.Fcu.SEL_VS						= m_inputsA429.in_FCUOWNA.Label_104;        //02A8  104
		m_inputsA429.in_CMIO.Fcu.SEL_AIRSPEED				= m_inputsA429.in_FCUOWNA.Label_103;        //02B0  103
		m_inputsA429.in_CMIO.Fcu.SEL_MACH					= m_inputsA429.in_FCUOWNA.Label_106;        //02B8  106		
		m_inputsA429.in_CMIO.Fcu.FCUEIS_DIS_2				= m_inputsA429.in_FCUOWNA.Label_272;        //02C8  272
		m_inputsA429.in_CMIO.Fcu.FCU_DIS_1					= m_inputsA429.in_FCUOWNA.Label_274;        //02D0  274
		m_inputsA429.in_CMIO.Fcu.FCU_DIS_2					= m_inputsA429.in_FCUOWNA.Label_273;        //02D8  273
		m_inputsA429.in_FCUOWNA.Label_274.getBit(24,BHDGVSSEL);
		m_inputsA429.in_CMIO.Fcu.SEL_VS_VAL					= BHDGVSSEL;								//02E0 BOOL
		m_inputsA429.in_FCUOWNA.Label_010.getBit(14,BXFCUMACHSEL);
		m_inputsA429.in_CMIO.Fcu.SEL_AS_VAL					= !BXFCUMACHSEL;							//02E4  BOO
		m_inputsA429.in_CMIO.Fcu.SEL_MACH_VAL				= BXFCUMACHSEL;								//02E8  BOO
		m_inputsA429.in_FCUOWNA.Label_274.getBit(25,BTRKFPASEL); 
		m_inputsA429.in_CMIO.Fcu.SEL_FPA_VAL				= BTRKFPASEL;								//02F0  BOO	
		m_inputsA429.in_CMIO.Fcu.SEL_HDG					= m_inputsA429.in_FCUOWNB.Label_101;        //0298  101
		m_inputsA429.in_CMIO.Fcu.SEL_ALT					= m_inputsA429.in_FCUOWNB.Label_102;        //02A0  102
		m_inputsA429.in_CMIO.Fcu.SEL_FPA					= m_inputsA429.in_FCUOWNB.Label_115;        //02F8  115
		m_inputsA429.in_CMIO.Fcu.SEL_TRK					= m_inputsA429.in_FCUOWNB.Label_114;        //0300  114
	}

	//MLS  BusNum:18
	//FAC  BusNum:215  
	m_inputsA429.in_CMIO.Fac.FAC_FG_VAL					= fg->DisInFG[FG::FGDI::LM01C_FAC_OWN_HLTY_COM];     //01C0   BOOL 	//m_inputsA429.in_FACOWN.FAC_FG_VAL;         
	m_inputsA429.in_CMIO.Fac.FAC_OPP_SELECT		   = fg->DisInFG[FG::FGDI::LM01C_FAC_OWN_HLTY_COM]?0:1; //01C4,ENUM F  //FACOWN or FACOPP sends to CM, it is selected from FAC_FG_VAL.
	if(m_inputsA429.in_CMIO.Fac.FAC_FG_VAL)  //0xffff
	{
		m_inputsA429.in_CMIO.Fac.VMAX						= m_inputsA429.in_FACOWN.FAC_Label207;       //01C8   207 
		m_inputsA429.in_CMIO.Fac.VFEN						= m_inputsA429.in_FACOWN.FAC_Label267;       //01D0   267
		m_inputsA429.in_CMIO.Fac.VMANVR					= m_inputsA429.in_FACOWN.FAC_Label265;       //0170   265
		m_inputsA429.in_CMIO.Fac.V3						= m_inputsA429.in_FACOWN.FAC_Label263;       //0178   263
		m_inputsA429.in_CMIO.Fac.V4						= m_inputsA429.in_FACOWN.FAC_Label264;       //0180   264
		m_inputsA429.in_CMIO.Fac.VMAXOP					= m_inputsA429.in_FACOWN.FAC_Label266;       //0160   266 
		m_inputsA429.in_CMIO.Fac.VMIN						= m_inputsA429.in_FACOWN.FAC_Label245;       //0168   245  	                       
		m_inputsA429.in_CMIO.Fac.WEIGHT					= m_inputsA429.in_FACOWN.FAC_Label074;       //0190   074 
		m_inputsA429.in_CMIO.Fac.CENTRAGE				= m_inputsA429.in_FACOWN.FAC_Label076;       //0198   076 
		m_inputsA429.in_CMIO.Fac.GAMMA_A					= m_inputsA429.in_FACOWN.FAC_Label070;       //01A0   070 
		m_inputsA429.in_CMIO.Fac.GAMMA_T					= m_inputsA429.in_FACOWN.FAC_Label071;       //01A8   071 
		m_inputsA429.in_CMIO.Fac.FAC_DIS_1				= m_inputsA429.in_FACOWN.FAC_Label146;       //01B0   146 
		m_inputsA429.in_CMIO.Fac.FAC_DIS_2				= m_inputsA429.in_FACOWN.FAC_Label274;       //01B8   274 
	}
	else if(fg->DisInFG[FG::FGDI::RM01C_FAC_OWN_HLTY_MON]) //m_inputsA429.in_FACOPP.FAC_FG_VAL
	{
		m_inputsA429.in_CMIO.Fac.VMAX						= m_inputsA429.in_FACOPP.FAC_Label207;        //01C8   207 
		m_inputsA429.in_CMIO.Fac.VFEN						= m_inputsA429.in_FACOPP.FAC_Label267;        //01D0   267
		m_inputsA429.in_CMIO.Fac.VMANVR					= m_inputsA429.in_FACOPP.FAC_Label265;        //0170   265
		m_inputsA429.in_CMIO.Fac.V3						= m_inputsA429.in_FACOPP.FAC_Label263;        //0178   263
		m_inputsA429.in_CMIO.Fac.V4						= m_inputsA429.in_FACOPP.FAC_Label264;        //0180   264
		m_inputsA429.in_CMIO.Fac.VMAXOP					= m_inputsA429.in_FACOPP.FAC_Label266;        //0160   266 
		m_inputsA429.in_CMIO.Fac.VMIN						= m_inputsA429.in_FACOPP.FAC_Label245;        //0168   245  	                       
		m_inputsA429.in_CMIO.Fac.WEIGHT					= m_inputsA429.in_FACOPP.FAC_Label074;        //0190   074 
		m_inputsA429.in_CMIO.Fac.CENTRAGE				= m_inputsA429.in_FACOPP.FAC_Label076;        //0198   076 
		m_inputsA429.in_CMIO.Fac.GAMMA_A					= m_inputsA429.in_FACOPP.FAC_Label070;        //01A0   070 
		m_inputsA429.in_CMIO.Fac.GAMMA_T					= m_inputsA429.in_FACOPP.FAC_Label071;        //01A8   071 
		m_inputsA429.in_CMIO.Fac.FAC_DIS_1				= m_inputsA429.in_FACOPP.FAC_Label146;        //01B0   146 
		m_inputsA429.in_CMIO.Fac.FAC_DIS_2				= m_inputsA429.in_FACOPP.FAC_Label274;        //01B8   274 
	}     

	//VOR OWN
	m_inputsA429.in_CMIO.Vor.VOR_OWN_BRG				= m_inputsA429.in_VOROWN.Label_222;           //05C0  222 A429
	m_inputsA429.in_CMIO.Vor.VOR_OWN_FREQ				= m_inputsA429.in_VOROWN.Label_034;           //05C8  034 A429
	m_inputsA429.in_CMIO.Vor.VOR_OWN_ID1				= m_inputsA429.in_VOROWN.Label_242;           //05D0  242 A429
	m_inputsA429.in_CMIO.Vor.VOR_OWN_ID2				= m_inputsA429.in_VOROWN.Label_244;           //05D8  244 A429
	m_inputsA429.in_CMIO.Vor.VOR_OWN_FGVAL				= 0Xffff;//m_inputsA429.in_VOROWN.Label_;              //05E0      BOOL
	m_inputsA429.in_CMIO.Vor.VOR_OWN_SELCRS			= m_inputsA429.in_VOROWN.Label_100;           //05E4  100 A429
	m_inputsA429.in_CMIO.Vor.VOR_OWN_BRG_NCD			= 0x0;//m_inputsA429.in_VOROWN.Label_;              //05EC      BOOL
	//VOR OPP
	m_inputsA429.in_CMIO.Vor.VOR_OPP_BRG				= m_inputsA429.in_VOROPP.Label_222;           //05F0 222 A429 
	m_inputsA429.in_CMIO.Vor.VOR_OPP_FREQ				= m_inputsA429.in_VOROPP.Label_034;           //05F8 034 A429 
	m_inputsA429.in_CMIO.Vor.VOR_OPP_ID1				= m_inputsA429.in_VOROPP.Label_242;           //0600 242 A429 
	m_inputsA429.in_CMIO.Vor.VOR_OPP_ID2				= m_inputsA429.in_VOROPP.Label_244;           //0608 244 A429 
	m_inputsA429.in_CMIO.Vor.VOROPPSELCRS				= m_inputsA429.in_VOROPP.Label_100;           //0610 100 A429 

	//FQI        BusNum:22
	m_inputsA429.in_CMIO.Fqi_T.FUEL_QUANT                 = m_inputsA429.in_FQI.Label_247;      
	m_inputsA429.in_CMIO.Fqi_E.FUEL_QTY_left_outer_tank	= m_inputsA429.in_FQI.Label_256;				//1C60 256
	m_inputsA429.in_CMIO.Fqi_E.FUEL_QTY_left_inner_tank	= m_inputsA429.in_FQI.Label_257;				//1C68 257
	m_inputsA429.in_CMIO.Fqi_E.FUEL_QTY_center_tank			= m_inputsA429.in_FQI.Label_260;				//1C70 260
	m_inputsA429.in_CMIO.Fqi_E.FUEL_QTY_right_outer_tank	= m_inputsA429.in_FQI.Label_261;				//1C78 261
	m_inputsA429.in_CMIO.Fqi_E.FUEL_QTY_right_inner_tank	= m_inputsA429.in_FQI.Label_262;				//1C80 262
	m_inputsA429.in_CMIO.Fqi_E.FUEL_QTY_ACT_1				   = m_inputsA429.in_FQI.Label_263;				//1C88 263
	m_inputsA429.in_CMIO.Fqi_E.FUEL_QTY_ACT_2				   = m_inputsA429.in_FQI.Label_264;				//1C90 264
	m_inputsA429.in_CMIO.Fqi_E.FUEL_QTY_ACT_3				   = m_inputsA429.in_FQI.Label_265;				//1C98 265
	m_inputsA429.in_CMIO.Fqi_E.FUEL_QTY_ACT_4				   = m_inputsA429.in_FQI.Label_310;				//1CA0 310
	m_inputsA429.in_CMIO.Fqi_E.FUEL_QTY_ACT_5				   = m_inputsA429.in_FQI.Label_311;				//1CA8 311
	m_inputsA429.in_CMIO.Fqi_E.FUEL_QTY_TOTAL_ACT		   = m_inputsA429.in_FQI.Label_312;				//1CB0 312
	m_inputsA429.in_CMIO.Fqi_E.FUEL_QTY_ACT_6				   = m_inputsA429.in_FQI.Label_313;				//1CB8 313

	//FADEC OWN  BusNum:217	 
	CoreSim::Osal::Bool BFADVAL(false);
	if(iae_engine || pw_engine)
	{
		m_inputsA429.in_FADOWNA.Label_270.getBit(24,BFADVAL); //0=A active; 1=B active
		m_inputsA429.in_CMIO.FadecOwn.N1_EPR_ACT_OWN   = m_inputsA429.in_FADOWNA.Label_340;					//0200
		m_inputsA429.in_CMIO.FadecOwn.FAC_DIS_4_1      = m_inputsA429.in_FADOWNA.Label_270;					//0228
	}
	else if(cfm_engine)
	{
		m_inputsA429.in_FADOWNA.Label_273.getBit(24,BFADVAL);
		m_inputsA429.in_CMIO.FadecOwn.N1_EPR_ACT_OWN   = m_inputsA429.in_FADOWNA.Label_346;					//0200  346/340			
		m_inputsA429.in_CMIO.FadecOwn.FAC_DIS_2        = m_inputsA429.in_FADOWNA.Label_271;					//0218  271  M 		
		m_inputsA429.in_CMIO.FadecOwn.FAC_DIS_4_1      = m_inputsA429.in_FADOWNA.Label_273;					//0228  273/270
	}
	m_inputsA429.in_CMIO.FadecOwn.FAD_OWN_FGVAL			= 0;//BVFGFAD;//!BFADVAL;                                       //0230  001  B  //m_inputsA429.in_FADOWNA.Label_001;
	m_inputsA429.in_CMIO.FadecOwn.ENG_SN_1					= m_inputsA429.in_FADOWNA.Label_046;             //01E0  046  M 
	m_inputsA429.in_CMIO.FadecOwn.ENG_SN_2					= m_inputsA429.in_FADOWNA.Label_047;             //01E8  047  M 
	m_inputsA429.in_CMIO.FadecOwn.FAC_DIS_3					= m_inputsA429.in_FADOWNA.Label_272;             //0220  272  C
	//if(m_inputsA429.in_CMIO.FadecOwn.FAD_OWN_FGVAL == 0xffff)
	{
		m_inputsA429.in_CMIO.FadecOwn.SELECTED_TLA_OWN		= m_inputsA429.in_FADOWNA.Label_133;			 //0208  133  C
		m_inputsA429.in_CMIO.FadecOwn.FUEL_FLOW_OWN			= m_inputsA429.in_FADOWNA.Label_244;			 //0210  244  M		
		m_inputsA429.in_CMIO.FadecOwn.N1_EPR_LIMIT_OWN		= m_inputsA429.in_FADOWNA.Label_337;			 //01F0  337  C 
		m_inputsA429.in_CMIO.FadecOwn.N1_EPR_CMD_OWN		   = m_inputsA429.in_FADOWNA.Label_341;			 //01F8  341  C 
		/*if(cfm_engine)
		{				
		}
		if(iae_engine || pw_engine)
		{			
		}*/
	}

	//FADEC OPP  BusNum:218

	m_inputsA429.in_CMIO.FadecOpp.FAD_OPP_FGVAL				= 0;//BVFGOFAD?0xff:0x0;//BFADVAL;//m_inputsA429.in_FADOWNB.Label_001;             //0270     //TODO: should it be set according to the subscribe status?
	m_inputsA429.in_CMIO.FadecOpp.FUEL_FLOW					= m_inputsA429.in_FADOWNB.Label_244;             //0260  244  M
	//if(m_inputsA429.in_CMIO.FadecOpp.FAD_OPP_FGVAL == 0xffff)
	{
		m_inputsA429.in_CMIO.FadecOpp.N1_EPR_LIMIT			= m_inputsA429.in_FADOWNB.Label_337;             //0240  337  C 
		m_inputsA429.in_CMIO.FadecOpp.N1_EPR_CMD			   = m_inputsA429.in_FADOWNB.Label_341;             //0248  341  C
		m_inputsA429.in_CMIO.FadecOpp.FAD_DIS_X				= m_inputsA429.in_FADOWNB.Label_271 ;            //0268  XX   C 
		if(cfm_engine)
		{
			m_inputsA429.in_CMIO.FadecOpp.N1_EPR_ACT        = m_inputsA429.in_FADOWNB.Label_346;             //0250  346/340
		}
		if(iae_engine || pw_engine)
		{
			m_inputsA429.in_CMIO.FadecOpp.N1_EPR_ACT        = m_inputsA429.in_FADOWNB.Label_340;             //0250  346/340
		}
		m_inputsA429.in_CMIO.FadecOpp.SELECTED_TLA         = m_inputsA429.in_FADOWNB.Label_133;             //0258  133  C
	}

   //LS
   m_inputsA429.in_CMIO.LS.MMRFGVAL                = 0;//BVAGMMR?0xff:0;             //0360  BOOL  ILS_FG
   m_inputsA429.in_CMIO.LS.MMROPPSELECT            = 0;//BOMMRSEL?0xff:0x0;         //0364  ENUM  LS_OPP
   m_inputsA429.in_CMIO.LS.LOC_DEV                 = m_inputsA429.in_LSOWN.Label_173;               //0340  173 C
   m_inputsA429.in_CMIO.LS.LS_FREQ                 = m_inputsA429.in_LSOWN.Label_033;               //0348  033 C
   m_inputsA429.in_CMIO.LS.LS_ID1                  = m_inputsA429.in_LSOWN.Label_263;               //0350  263 M
   m_inputsA429.in_CMIO.LS.LS_ID2                  = m_inputsA429.in_LSOWN.Label_264;               //0358  264 M  
   m_inputsA429.in_CMIO.LS.LS_RWY_HDG              = m_inputsA429.in_LSOWN.Label_033;               //0368  105 C
   m_inputsA429.in_CMIO.LS.LOC_DEV_NOT_NCD         = 0;//BINNLOC;										//0370  BOOL
   //m_inputsA429.in_CMIO.LS.LS_FREQ_NOT_NCD         = ;      //0374  BOOL
   //m_inputsA429.in_CMIO.LS.LS_RWY_HDG_NOT_NCD      = ;   //0378  BOOL
   //m_inputsA429.in_CMIO.LS.BILSINST                = ;             //037C  BOOL
   //m_inputsA429.in_CMIO.LS.BMLSINST                = ;             //0380  BOOL
   //m_inputsA429.in_CMIO.LS.BGLSINST                = ;             //0384  BOOL
   m_inputsA429.in_CMIO.LS.GLIDE_SLOPE_DEV         = m_inputsA429.in_LSOWN.Label_174;      //0390  174 
   //m_inputsA429.in_CMIO.LS.GLIDE_SLOPE_DEV_NOT_NCD = ;      //0398  BOOL
   
   //MLS
   m_inputsA429.in_CMIO.Mls.MLS_MLSFGVAL           = 0xff00;             //1430 BOOL
   m_inputsA429.in_CMIO.Mls.MLS_OPPSEL             = 0;               //1434 BOOL
   //m_inputsA429.in_CMIO.Mls.FG_RMPOWN_MLS_SEL      = ;        //1400 BOOL
   //m_inputsA429.in_CMIO.Mls.FG_RMPOPP_MLS_SEL      = ;        //1404 BOOL
   m_inputsA429.in_CMIO.Mls.MLS_MLS_CHANNEL        = m_inputsA429.in_MLS.Label_036;         //1408 A429
   //m_inputsA429.in_CMIO.Mls.MLS_MLS_RUNWAY_HEADING = m_inputsA429.in_MLS.;   //1410 A429
   m_inputsA429.in_CMIO.Mls.MLS_MLS_LOC_DEV        = m_inputsA429.in_MLS.Label_173;          //1418 A429
   //m_inputsA429.in_CMIO.Mls.MLS_MLS_ID_1           = m_inputsA429.in_MLS.;             //1420 A429
   //m_inputsA429.in_CMIO.Mls.MLS_MLS_ID_2           = m_inputsA429.in_MLS.;             //1428 A429
   //m_inputsA429.in_CMIO.Mls.MLS_LOC_DEV_NCD        = ;          //1438 BOOL
#if 1
   //INTERN.FG-FM
   m_inputsA429.in_CMIO.Inter_FGFM.FPA_TGT           = m_comMemoryIO.Inter_FGFM.FPA_TGT;;         //06C0     IEEE
   m_inputsA429.in_CMIO.Inter_FGFM.VS_TGT            = m_comMemoryIO.Inter_FGFM.VS_TGT;           ;         //06C8     IEEE
   m_inputsA429.in_CMIO.Inter_FGFM.ALT_TGT           = m_comMemoryIO.Inter_FGFM.ALT_TGT;          ;         //06D0     IEEE
   m_inputsA429.in_CMIO.Inter_FGFM.SPD_TGT           = m_comMemoryIO.Inter_FGFM.SPD_TGT;          ;         //06D8     IEEE
   m_inputsA429.in_CMIO.Inter_FGFM.MACH_TGT          = m_comMemoryIO.Inter_FGFM.MACH_TGT;         ;         //06E0     IEEE
   m_inputsA429.in_CMIO.Inter_FGFM.FMGC_DIS_WD_1     = m_comMemoryIO.Inter_FGFM.FMGC_DIS_WD_1;    ;         //06E8 274 A429
   m_inputsA429.in_CMIO.Inter_FGFM.FMGC_DIS_WD_2     = m_comMemoryIO.Inter_FGFM.FMGC_DIS_WD_2;    ;         //06F0 275 A429
   m_inputsA429.in_CMIO.Inter_FGFM.FMGC_DIS_WD_3     = m_comMemoryIO.Inter_FGFM.FMGC_DIS_WD_3;    ;         //06F8 273 A429
   m_inputsA429.in_CMIO.Inter_FGFM.FMGC_DIS_WD_4     = m_comMemoryIO.Inter_FGFM.FMGC_DIS_WD_4;    ;         //0700 146 A429
   m_inputsA429.in_CMIO.Inter_FGFM.FMGC_DIS_WD_5     = m_comMemoryIO.Inter_FGFM.FMGC_DIS_WD_5;    ;         //0708 145 A429
   m_inputsA429.in_CMIO.Inter_FGFM.FMGC_ATS_DIS_WD   = m_comMemoryIO.Inter_FGFM.FMGC_ATS_DIS_WD;  ;         //0710 270 A429
   m_inputsA429.in_CMIO.Inter_FGFM.FG_PITCH_ROLL_DIS = m_comMemoryIO.Inter_FGFM.FG_PITCH_ROLL_DIS;;         //0718     DIS 
   m_inputsA429.in_CMIO.Inter_FGFM.FMGC_PRIORITY     = m_comMemoryIO.Inter_FGFM.FMGC_PRIORITY;    ;         //071C     BOOL
   m_inputsA429.in_CMIO.Inter_FGFM.OPP_FMGC_DIS_WD_4 = m_comMemoryIO.Inter_FGFM.OPP_FMGC_DIS_WD_4;;         //0724 146 A429
   m_inputsA429.in_CMIO.Inter_FGFM.OUTPUT_RLIM_1     = m_comMemoryIO.Inter_FGFM.OUTPUT_RLIM_1;  ;           //0730     IEEE
   m_inputsA429.in_CMIO.Inter_FGFM.FG_RTC_COUNTER    = 0 ;                                                  //0720     INT 
   //OUTPUT: FM-FG
   memcpy(&(m_inputsA429.in_CMIO.Inter_FMFG),&(m_comMemoryIO.Inter_FMFG),sizeof(m_comMemoryIO.Inter_FMFG));
#endif
   //FM-FG-PHRIPH A-BUSS
   m_outputs.Label_0040 = m_comMemoryIO.CITYPAIR_WD[0];
   m_outputs.Label_0041 = m_comMemoryIO.CITYPAIR_WD[1];
   m_outputs.Label_0042 = m_comMemoryIO.CITYPAIR_WD[2];
   m_outputs.Label_0233 = m_comMemoryIO.FLIGHTNO_WD[0];
   m_outputs.Label_0234 = m_comMemoryIO.FLIGHTNO_WD[1];
   m_outputs.Label_0235 = m_comMemoryIO.FLIGHTNO_WD[2];
   m_outputs.Label_0236 = m_comMemoryIO.FLIGHTNO_WD[3];
   m_outputs.Label_0237 = m_comMemoryIO.FLIGHTNO_WD[4];
   //FM-FG-PHRIPH B-BUSS
   m_outputs.Label_0075 = m_comMemoryIO.WEIGHT;
   m_outputs.Label_0077 = m_comMemoryIO.CG;
   //FM-FG-PHRIPH C-BUSS
   m_outputs.Label_0167 = m_comMemoryIO.C_BUSS_DATA[0];
   m_outputs.Label_0216 = m_comMemoryIO.C_BUSS_DATA[1];
   m_outputs.Label_0230 = m_comMemoryIO.C_BUSS_DATA[2];
   m_outputs.Label_0251 = m_comMemoryIO.C_BUSS_DATA[3];
   m_outputs.Label_0320 = m_comMemoryIO.C_BUSS_DATA[4];
   m_outputs.Label_0323 = m_comMemoryIO.C_BUSS_DATA[5];
   m_outputs.Label_0324 = m_comMemoryIO.C_BUSS_DATA[6];
   m_outputs.Label_0325 = m_comMemoryIO.C_BUSS_DATA[7];
   m_outputs.Label_0326 = m_comMemoryIO.C_BUSS_DATA[8];
   m_outputs.Label_0327 = m_comMemoryIO.C_BUSS_DATA[9];
   m_outputs.Label_0330 = m_comMemoryIO.C_BUSS_DATA[10];
   m_outputs.Label_0331 = m_comMemoryIO.C_BUSS_DATA[11];
   m_outputs.Label_0333 = m_comMemoryIO.C_BUSS_DATA[12];
   m_outputs.Label_0334 = m_comMemoryIO.C_BUSS_DATA[13];
   m_outputs.Label_0335 = m_comMemoryIO.C_BUSS_DATA[14];
   m_outputs.Label_0336 = m_comMemoryIO.C_BUSS_DATA[15];
   


   //m_inputsA429.in_CMIO.Prot.MEM_BUSY = 0x0000;
   return;
}


CoreSim::Osal::Bool FG::publishoutputs(CoreSim::NetVdn::NetVdnClient& vdn)
{
   //for (int side=0; side<2; side++)
   //{
      //CoreSim::Osal::String topic = topics[side];
      //FMGC SE Discrete inputs from s/w FMGC   

      vdn.publish(m_outputs.LT14D_ATH_OWN_ENGD_COM_OPP[0],"SoftFmgc", "LT14D_ATH_OWN_ENGD_COM_OPP[0]","");
      vdn.publish(m_outputs.LT14D_ATH_OWN_ENGD_COM_OPP[1],"SoftFmgc", "LT14D_ATH_OWN_ENGD_COM_OPP[1]","");
      vdn.publish(m_outputs.RT14F_ATH_OWN_ENGD_MON_OPP[0],"SoftFmgc", "RT14F_ATH_OWN_ENGD_MON_OPP[0]","");
      vdn.publish(m_outputs.RT14F_ATH_OWN_ENGD_MON_OPP[1],"SoftFmgc", "RT14F_ATH_OWN_ENGD_MON_OPP[1]","");
      vdn.publish(m_outputs.LM12D_ATHR_OWN_ENGD_COM[0],"SoftFmgc", "LM12D_ATHR_OWN_ENGD_COM[0]","");
      vdn.publish(m_outputs.LM12D_ATHR_OWN_ENGD_COM[1],"SoftFmgc", "LM12D_ATHR_OWN_ENGD_COM[1]","");
      vdn.publish(m_outputs.RM12E_ATHR_OWN_ENGD_MON[0],"SoftFmgc", "RM12E_ATHR_OWN_ENGD_MON[0]","");
      vdn.publish(m_outputs.RM12E_ATHR_OWN_ENGD_MON[1],"SoftFmgc", "RM12E_ATHR_OWN_ENGD_MON[1]","");

      vdn.publish(m_outputs.LM10D_AP_OWN_ENGD_COM[0],"SoftFmgc", "LM10D_AP_OWN_ENGD_COM[0]","");
      vdn.publish(m_outputs.LM10D_AP_OWN_ENGD_COM[1],"SoftFmgc", "LM10D_AP_OWN_ENGD_COM[1]","");
      vdn.publish(m_outputs.RM12G_AP_OWN_ENGD_MON[0],"SoftFmgc", "RM12G_AP_OWN_ENGD_MON[0]","");
      vdn.publish(m_outputs.RM12G_AP_OWN_ENGD_MON[1],"SoftFmgc", "RM12G_AP_OWN_ENGD_MON[1]","");
      vdn.publish(m_outputs.LT14B_AP_OWN_ENGD_COM_OPP[0],"SoftFmgc", "LT14B_AP_OWN_ENGD_COM_OPP[0]","");   //A2 J4-31 (DI-09); A3 J4-31 (DI-09) 
      vdn.publish(m_outputs.LT14B_AP_OWN_ENGD_COM_OPP[1],"SoftFmgc", "LT14B_AP_OWN_ENGD_COM_OPP[1]",""); 
      vdn.publish(m_outputs.RT14D_AP_OWN_ENGD_MON_OPP[0],"SoftFmgc", "RT14D_AP_OWN_ENGD_MON_OPP[0]","");   //A2 J4-30 (DI-10); A3 J4-30 (DI-10)
      vdn.publish(m_outputs.RT14D_AP_OWN_ENGD_MON_OPP[1],"SoftFmgc", "RT14D_AP_OWN_ENGD_MON_OPP[1]","");

      vdn.publish(m_outputs.LT15C_FD_OWN_ENGD_COM_OPP[0],"SoftFmgc","LT15C_FD_OWN_ENGD_COM_OPP[0]","");
      vdn.publish(m_outputs.LT15C_FD_OWN_ENGD_COM_OPP[1],"SoftFmgc","LT15C_FD_OWN_ENGD_COM_OPP[1]","");
      vdn.publish(m_outputs.RT15E_FD_OWN_ENGD_MON_OPP_I[0],"SoftFmgc","FD_OWN_ENGD_MON_OPP_I[0]","");
      vdn.publish(m_outputs.RT15E_FD_OWN_ENGD_MON_OPP_I[1],"SoftFmgc","FD_OWN_ENGD_MON_OPP_I[1]","");
      vdn.publish(m_outputs.LM14D_FD_OWN_ENGD_COM[0],"SoftFmgc", "LM14D_FD_OWN_ENGD_COM[0]","");
      vdn.publish(m_outputs.LM14D_FD_OWN_ENGD_COM[1],"SoftFmgc", "LM14D_FD_OWN_ENGD_COM[1]","");
      vdn.publish(m_outputs.RM14D_FD_OWN_ENGD_MON[0],"SoftFmgc", "RM14D_FD_OWN_ENGD_MON[0]",""); 
      vdn.publish(m_outputs.RM14D_FD_OWN_ENGD_MON[1],"SoftFmgc", "RM14D_FD_OWN_ENGD_MON[1]",""); 

      vdn.publish(m_outputs.LM09B_FCU_OWN_HLTY[0],"SoftFmgc","LM09B_FCU_OWN_HLTY[0]","");
      vdn.publish(m_outputs.LM09B_FCU_OWN_HLTY[1],"SoftFmgc","LM09B_FCU_OWN_HLTY[1]","");
      vdn.publish(m_outputs.LM09A_LS_INHIB[0],"SoftFmgc", "LM09A_LS_INHIB[0]","");
      vdn.publish(m_outputs.LM09A_LS_INHIB[1],"SoftFmgc", "LM09A_LS_INHIB[1]","");
      vdn.publish(m_outputs.LM12H_FMGC_HEALTHY[0],"SoftFmgc", "LM12H_FMGC_HEALTHY[0]","");
      vdn.publish(m_outputs.LM12H_FMGC_HEALTHY[1],"SoftFmgc", "LM12H_FMGC_HEALTHY[1]","");

      vdn.publish(m_outputs.LM13D_RUDDER_LOCK_COM[0],"SoftFmgc", "LM13D_RUDDER_LOCK_COM[0]","");   
      vdn.publish(m_outputs.LM13D_RUDDER_LOCK_COM[1],"SoftFmgc", "LM13D_RUDDER_LOCK_COM[1]","");
      vdn.publish(m_outputs.RM13D_RUDDER_LOCK_MON[0],"SoftFmgc", "RM13D_RUDDER_LOCK_MON[0]","");
      vdn.publish(m_outputs.RM13D_RUDDER_LOCK_MON[1],"SoftFmgc", "RM13D_RUDDER_LOCK_MON[1]","");
      vdn.publish(m_outputs.LM15C_STICK_LOCK_COM[0],"SoftFmgc", "LM15C_STICK_LOCK_COM[0]","");
      vdn.publish(m_outputs.LM15C_STICK_LOCK_COM[1],"SoftFmgc", "LM15C_STICK_LOCK_COM[1]","");
      vdn.publish(m_outputs.RM15C_STICK_LOCK_MON[0],"SoftFmgc", "RM15C_STICK_LOCK_MON[0]","");
      vdn.publish(m_outputs.RM15C_STICK_LOCK_MON[1],"SoftFmgc", "RM15C_STICK_LOCK_MON[1]","");

      //for test
      vdn.publish(DisInFG2[FGDI::LM01B_JNB_Autoland_COM],"SoftFmgc", "LM01B_JNB_Autoland_COM","");
      //DisInFG2[FGDI::LM01B_JNB_Autoland_COM] = m_outputs.LM12D_ATHR_OWN_ENGD_COM[0];
      //vdn.publish(DisInFG2[FGDI::LM01B_JNB_Autoland_COM],"SoftFmgc", "LM12D_ATHR_OWN_ENGD_COM[0]","");
   //}

   
   vdn.publish(m_outputs.FMGC_Fault_Malf[0],"SimFmgc","FMGC_Fault_Malf","Malfunction");
   vdn.publish(m_outputs.Land_Capability_Malf, "Fmgc", "Land_Capability_Malf", "Malfunction");
   vdn.publish(m_outputs.No_Rollout_Malf,      "Fmgc", "No_Rollout_Malf",      "Malfunction");

   
   //not included in FMGC SE. for debug monitor
   vdn.publish(m_outputs.FG_Pup_Rdy,"SimFG","FG_Power_Rdy","");
   vdn.publish(m_outputs.FG_hlty_fg,"SimFG","FG_Healthy","");
   vdn.publish(m_outputs.FG_SafeTest,"SimFG","FG_Safety_Test","");
   vdn.publish(/*m_outputs.FMGC_Healthy[0]*/m_outputs.LM12H_FMGC_HEALTHY[0],"SimFG","FMGC_Healthy[0]","");
   vdn.publish(/*m_outputs.FMGC_Healthy[1]*/m_outputs.LM12H_FMGC_HEALTHY[1],"SimFG","FMGC_Healthy[1]","");

   vdn.publish(m_outputs.LM14D_ATH_OWN_ENGD_COM_OPP[0],"SoftFmgc","ATH_OWN_ENGD_COM_OPP","");
   
   vdn.publish(m_outputs.LM13H_FMGC_CROSS_LOAD_ENABLE[0],"SoftFmgc","LM13H_FMGC_CROSS_LOAD_ENABLE","");
   
   /*According to ICD_FMFG_J35944 ON PAGE 53,A-BUSS, B-BUSS, C-BUSS DATA should receive from FM, send to FG, then to periperals.*/
   //FM-FG-PHRIPH A-BUSS
   vdn.publish(m_outputs.Label_0040,"FMGC","Label_0040");
   vdn.publish(m_outputs.Label_0041,"FMGC","Label_0041");
   vdn.publish(m_outputs.Label_0042,"FMGC","Label_0042");
   vdn.publish(m_outputs.Label_0233,"FMGC","Label_0233");
   vdn.publish(m_outputs.Label_0234,"FMGC","Label_0234");
   vdn.publish(m_outputs.Label_0235,"FMGC","Label_0235");
   vdn.publish(m_outputs.Label_0236,"FMGC","Label_0236");
   vdn.publish(m_outputs.Label_0237,"FMGC","Label_0237");
   //FM-FG-PHRIPH B-BUSS
   vdn.publish(m_outputs.Label_0075,"FMGC","Label_0075");
   vdn.publish(m_outputs.Label_0077,"FMGC","Label_0077");
   //FM-FG-PHRIPH C-BUSS
   vdn.publish(m_outputs.Label_0167,"FMGC","Label_0167");
   vdn.publish(m_outputs.Label_0216,"FMGC","Label_0216");
   vdn.publish(m_outputs.Label_0230,"FMGC","Label_0230");
   vdn.publish(m_outputs.Label_0251,"FMGC","Label_0251");
   vdn.publish(m_outputs.Label_0320,"FMGC","Label_0320");
   vdn.publish(m_outputs.Label_0323,"FMGC","Label_0323");
   vdn.publish(m_outputs.Label_0324,"FMGC","Label_0324");
   vdn.publish(m_outputs.Label_0325,"FMGC","Label_0325");
   vdn.publish(m_outputs.Label_0326,"FMGC","Label_0326");
   vdn.publish(m_outputs.Label_0327,"FMGC","Label_0327");
   vdn.publish(m_outputs.Label_0330,"FMGC","Label_0330");
   vdn.publish(m_outputs.Label_0331,"FMGC","Label_0331");
   vdn.publish(m_outputs.Label_0333,"FMGC","Label_0333");
   vdn.publish(m_outputs.Label_0334,"FMGC","Label_0334");
   vdn.publish(m_outputs.Label_0335,"FMGC","Label_0335");
   vdn.publish(m_outputs.Label_0336,"FMGC","Label_0336");

   /*publish input DIS data for FM and FG  for future maintainance. The data are subscribed from FMGC SE.*/
   for(int i = 0; i <= FGDI::RM03D_FCU_OWN_ATH_SW_MON; i++)
   {
      vdn.publish(DisInFG[i],"SimFG",fgdi->nameList[i],"");
      if(fgdi->nameList2[i] != "/")
      {
        vdn.publish(DisInFG2[i],"SimFG",fgdi->nameList2[i],"");
      }
   }
   /*publish DIS output data from Sim FG.*/
   for (int i = 0; i <= FGDO::RUDDER_LOCK_COM_I; i++)
	{
      vdn.publish(DisFromFG[i],"SimFG",fgdo->nameList[i],"");    
   }
   vdn.publish(DisInFG[130],"SimFG","RM06E_AC_TYPE_3","");         //LM06E AC_TYPE_3
   vdn.publish(DisInFG[131],"SimFG","RM06C_ENG_TYPE_5","");        //RM06C_ENG_TYPE_5
   vdn.publish(DisInFG[132],"SimFG","RM07C_PP_PARITY_2G","");      //RM07C_PP_PARITY_2G
   vdn.publish(DisInFG[133],"SimFG","LM05D_NAV_BKUP_SEL_OWN","");  //LM05D_NAV_BACKUP_SELECTED  

   return true;
}

void FG::setBit(CoreSim::Osal::UInt32 &des, int bit, int value)
{
   if(bit >= 1 && bit <= 32)
   {
      des = des | (value << (bit-1));
   }
   
   return;
}

CoreSim::Osal::Bool FG::getBit(UINT32 value,UINT32 bit)
{
	if(bit <1 || bit >32)
	{
		return 0;
	}
	CoreSim::Osal::Bool val = (value >>(bit-1))&0x01;
	return val;
}