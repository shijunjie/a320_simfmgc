﻿/*****************************************************************************
*                                                                            *
*  COPYRIGHT 2018 ACCEL FLIGHT SIMULATION. ALL RIGHTS RESERVED               *
*                                                                            *
*  This file is part of A320 Neo Sim FMGC simulation                         *
*                                                                            *
*  All information and content in this document is the Property              *
*  to ACCEL (Tianjin) Flight Simulation Co., Ltd. and shall not              *
*  be disclosed, disseminated, copied, or used except for purposes           *
*  expressly authorized in writing by ACCEL. Any unauthorized use            *
*  of the content will be considered an infringement of                      *
*  ACCEL's intellectual property rights.                                     *
*                                                                            *
*                                                                            *
*  @file     Header.h                                                        *
*  @brief    Entry point for the NetVdn Application.                         *
*                                                                            *
*            This is a Sim FM example of how to use the NetVdnClient to      *
*            publish and subscribe to data on the Virtual Data Network.      *
*                                                                            *
*            This example includes publishing and subscribing to variables   *
*            as well as sending and receiving requests.                      *
*                                                                            *
*  @author   Shi, Junjie                                                     *
*  @email    junjie.shi@accelflightsimulation.com                            *
*  @version  1.0.0.0(version)                                                *
*  @date     2018/07/21                                                      *
*                                                                            *
*----------------------------------------------------------------------------*
*  Remark         : Description                                              *
*----------------------------------------------------------------------------*
*  Change History :                                                          *
*  <Date>     | <Version> | <Author>       | <Description>                   *
*----------------------------------------------------------------------------*
*  1/15/2019  | 1.0.0.0   | Shi, Junjie   | Create file                      *
*----------------------------------------------------------------------------*
*                                                                            *
*****************************************************************************/
#pragma once
#include "SimElement/Element.h"
#include "Vdn/VdnClient.h"
#include "Osal/Threading/Mutex.h"
#include "Osal/Net/IpAddress.h"
#include "Osal/Net/SocketAddress.h"
#include "Osal/Net/TcpSocket.h"
#include "winsock2.h"
#include "Utils/A429Word.h"


//for debug purpose
#define BufInput 0
#define FM_DEBUG 0
#define FG_DEBUG 1
#define CFM_DEBUG 1
#define DEBUG_PRT 1
#define Power_DBG 0
#define CMPRT_DBG 0



//CM Data Format
#define Data_Dis            2
#define Data_Bl_Movs        2
#define Data_Bl_Movs_4T1    2
#define Data_Ieee_Movd      4
#define Data_Ieee_Movr      4
#define Data_Ieee_Movd_4Tv  2
#define Data_Int_Movs       2
#define Data_4R1            4
#define Data_4R2            18
#define Data_Ipt            14
#define Data_4T1            4
#define Data_4T2            4

const CoreSim::Osal::String topics[] =
{
   "Fmgc1",
   "Fmgc2"
};

//miliseconds
#define c_miliseconds 0.001

const CoreSim::Osal::String gpstopics[] =
{
   "A429_MMR1_GNSS_1",
   "A429_MMR1_GNSS_2"
};

const CoreSim::Osal::String m_busname[2][4] = 
{ 
  { "A429_FMGC1_OWN_A", //A429_Sim_FMGC1_OWN_A
   "A429_FMGC1_OWN_B",//A429_Sim_FMGC1_OWN_B
   "A429_FMGC1_OWN_CB1",//A429_Sim_FMGC1_OWN_CB1
   "A429_FMGC1_OWN_CB2"},//A429_Sim_FMGC1_OWN_CB2
   {"A429_FMGC2_OWN_A",//A429_Sim_FMGC2_OWN_A
   "A429_FMGC2_OWN_B",//A429_Sim_FMGC2_OWN_B
   "A429_FMGC2_OWN_CB1",//A429_Sim_FMGC2_OWN_CB1
   "A429_FMGC2_OWN_CB2"}//A429_Sim_FMGC2_OWN_CB2
};

//PROTOCOL 0000-001F
class Prot
{
public:
   CoreSim::Osal::UInt16 FM_PUPRDY;            //0000
   CoreSim::Osal::UInt16 FG_PUPRDY;            //0004
   CoreSim::Osal::UInt16 FG_SAFETEST;          //0008
   CoreSim::Osal::UInt16 PLCONTROL;            //000C
   CoreSim::Osal::UInt16 RTC_PERIOD;           //0010
   CoreSim::Osal::UInt16 MEM_BUSY;             //0014
   CoreSim::Osal::UInt16 FM_HLY;               //0018
   CoreSim::Osal::UInt16 FG_HLY;               //001c
};

//PROT. NEW FM  0020-003F
class Protocol_NewFM
{
public:
                               
   CoreSim::Osal::UInt16 CODE_COMPFG_1;        //0020
   CoreSim::Osal::UInt16 CODE_COMPFG_2;        //0024
   CoreSim::Osal::UInt16 CODE_COMPFG_3;        //0028
   CoreSim::Osal::UInt16 CODE_COMPFG_4;        //002c
   CoreSim::Osal::UInt16 CODE_COMPFG_5;        //0030
   CoreSim::Osal::UInt16 CODE_COMPFG_6;        //0034
   CoreSim::Osal::UInt16 SEMAPHORE_DATALOAD;   //0038  5AA5= init. val. set by FG
   CoreSim::Osal::UInt16 CROSS_LOAD;           //003c
};


//FG-FM:Hard     0040 0073
#pragma pack(push,1)
class Input_Hard_T
{
public:
   CoreSim::Osal::UInt16 NG_PRES;              //0040
   CoreSim::Osal::UInt16 CDU_FAIL;             //0044  TABLE 4.8.5
   CoreSim::Osal::UInt16 NAV;                  //0048  TABLE 4.8.6
   CoreSim::Osal::UInt16 AL_PINS1;             //004c  TABLE 4.8.2
   CoreSim::Osal::UInt16 AL_PINS2;             //0050  TABLE 4.8.3
   CoreSim::Osal::UInt16 AC_PINS;              //0054  TABLE 4.8.1
   //AC_PINS Ac_Pins;
   CoreSim::Osal::UInt16 SIDE_1;               //0058  TRUE=SIDE1(LM06D)
   CoreSim::Osal::UInt16 FMGC_OPP_HLY;         //005C   TRUE=HLTHY (LT02B)
   CoreSim::Osal::UInt16 AP_ENG;               //0060   TABLE 4.8.4
   CoreSim::Osal::UInt16 MAG_TRUE_SEL;         //0064   ‘True’ = TRUE SEL
   CoreSim::Osal::UInt16 DERATED_CLI;          //0068
   CoreSim::Osal::UInt16 ENG_OWN_STOP;         //006C   ‘True’ = STOPPED
   CoreSim::Osal::UInt16 ENG_OPP_STOP;         //0070   ‘True’ = STOPPED
};
#pragma pack(pop)

//FG-FM:Hard     008C 0097
class Input_Hard_E
{
public:                                    
   CoreSim::Osal::UInt16 SPATIAAL_CNT;         //008C   SPARE SPATIAAL CONNECTION
   CoreSim::Osal::UInt16 CFM_ENG;              //0090   ‘True’ = CFM
   CoreSim::Osal::UInt16 IAE_ENG;              //0094   ‘True’ = IAE
};

//FAC 0160-01D7 //SPARE  01D8-01DF
#pragma pack(push,1)
class Input_FAC
{
public:
   //FAC    C2  0160-0187
   CoreSim::Osal::UInt32 VMAXOP;               //0160   266  C  VMAXOP
   CoreSim::Osal::UInt32 VMIN;                 //0168   245  C
   CoreSim::Osal::UInt32 VMANVR;               //0170   265  M
   CoreSim::Osal::UInt32 V3;                   //0178   263  M
   CoreSim::Osal::UInt32 V4;                   //0180   264  M
   //SPARE  0188-018F                          
   CoreSim::Osal::UInt32 SPARE_2;               //0188-018F 
   //FAC   C2  0190-01D7                       
   CoreSim::Osal::UInt32 WEIGHT;               //0190   074  C
   CoreSim::Osal::UInt32 CENTRAGE;             //0198   076  C
   CoreSim::Osal::UInt32 GAMMA_A;              //01A0   070  C
   CoreSim::Osal::UInt32 GAMMA_T;              //01A8   071  C
   CoreSim::Osal::UInt32 FAC_DIS_1;            //01B0   146  C  FAC DIS WD 1
   CoreSim::Osal::UInt32 FAC_DIS_2;            //01B8   274  C
   CoreSim::Osal::UInt16 FAC_FG_VAL;           //01C0   BOOL FACFGVAL
   CoreSim::Osal::UInt16 FAC_OPP_SELECT;        //01C4,ENUM FACOPPSELECT  0 = OWN, 1 = OPP
   CoreSim::Osal::UInt32 VMAX;                 //01C8   207  M
   CoreSim::Osal::UInt32 VFEN;                 //01D0   267  M
   //SPARE  01D8-01DF                          
   CoreSim::Osal::UInt32 SPARE_3;              //01D8-01DF
};
#pragma pack(pop)

#pragma pack(push,1)
class Input_FADEC
{
public:
   //FADEC OWN  C2  01E0-0233
   CoreSim::Osal::UInt32 ENG_SN_1;                 //01E0  046  M  ENG S/N WD1
   CoreSim::Osal::UInt32 ENG_SN_2;                 //01E8  047  M  ENG S/N WD2
   CoreSim::Osal::UInt32 N1_EPR_LIMIT_OWN;         //01F0  337  C  CFM only/PW only/IAE only
   CoreSim::Osal::UInt32 N1_EPR_CMD_OWN;           //01F8  341  C  CFM only/PW only/IAE only
   CoreSim::Osal::UInt32 N1_EPR_ACT_OWN;           //0200  346/340  C  CFM only(N1_ACT,346)/IAE only(EPR_ACT,340)/PW only(N1_ACT,340)
   CoreSim::Osal::UInt32 SELECTED_TLA_OWN;         //0208  133  C
   CoreSim::Osal::UInt32 FUEL_FLOW_OWN;            //0210  244  M
   CoreSim::Osal::UInt32 FAC_DIS_2;                //0218  271  M  CFM only
   CoreSim::Osal::UInt32 FAC_DIS_3;                //0220  272  C
   CoreSim::Osal::UInt32 FAC_DIS_4_1;              //0228  273/270  C  CFM only(FAC_DIS_4,273)/IAE only(FAC_DIS_1,270)/PW only(FAC_DIS_1,270)
   CoreSim::Osal::UInt16 FAD_OWN_FGVAL;            //0230  001  BOOL
   //SPARE 0234-023F
   CoreSim::Osal::UInt16 SPARE_4[3];
};
#pragma pack(pop)

#pragma pack(push,1)
class Input_FADECOPP
{
public:
   //FADEC OPP  C2   0240-0273  
   CoreSim::Osal::UInt32 N1_EPR_LIMIT;             //0240  337  C  CFM only/PW only/IAE only
   CoreSim::Osal::UInt32 N1_EPR_CMD;               //0248  341  C  CFM only/PW only/IAE only
   CoreSim::Osal::UInt32 N1_EPR_ACT;               //0250  346/340  C  CFM only(N1_ACT,346)/IAE only(EPR_ACT,340)/PW only(N1_ACT,340)
   CoreSim::Osal::UInt32 SELECTED_TLA;             //0258  133  C
   CoreSim::Osal::UInt32 FUEL_FLOW;                //0260  244  M
   CoreSim::Osal::UInt32 FAD_DIS_X;                //0268  XX   C   CFM only/IAE only/PW only
   CoreSim::Osal::UInt16 FAD_OPP_FGVAL;            //0270
   //SPARE 0274-0297
   CoreSim::Osal::UInt16 SPARE_5[9];
};
#pragma pack(pop)

//FCU 0298-0307
#pragma pack(push,1)
class Input_FCU
{
public:
   //FCU  C2 0298-02F3
   CoreSim::Osal::UInt32 SEL_HDG;              //0298  101  C
   CoreSim::Osal::UInt32 SEL_ALT;              //02A0  102  C
   CoreSim::Osal::UInt32 SEL_VS;               //02A8  104  C  SEL V/S
   CoreSim::Osal::UInt32 SEL_AIRSPEED;         //02B0  103  C
   CoreSim::Osal::UInt32 SEL_MACH;             //02B8  106  C
   CoreSim::Osal::UInt32 FCUEIS_DIS_1;         //02C0  271  M   SEE FMGC ICD
   CoreSim::Osal::UInt32 FCUEIS_DIS_2;         //02C8  272  C  SEE FMGC ICD
   CoreSim::Osal::UInt32 FCU_DIS_1;            //02D0  274  C  SEE FMGC ICD
   CoreSim::Osal::UInt32 FCU_DIS_2;            //02D8  273  C  SEE FMGC ICD
   CoreSim::Osal::UInt16 SEL_VS_VAL;           //02E0 BOOL SEL V/S VAL
   CoreSim::Osal::UInt16 SEL_AS_VAL;           //02E4  BOOL SEL A/S VAL
   CoreSim::Osal::UInt16 SEL_MACH_VAL;         //02E8  BOOL
   CoreSim::Osal::UInt16 FCU_FGVAL;            //02EC  BOOL
   CoreSim::Osal::UInt16 SEL_FPA_VAL;          //02F0  BOOL
   //SPARE 02F4-02F7
   CoreSim::Osal::UInt16 SPARE_6;
   //FCU   02F8-0307
   CoreSim::Osal::UInt32 SEL_FPA;              //02F8  115 C
   CoreSim::Osal::UInt32 SEL_TRK;              //0300  114
   //SPARE 0308-031F                           
   CoreSim::Osal::UInt32 SPARE_7[3];           //0308-031F
};
#pragma pack(pop)

//ls 0340-039F
#pragma pack(push,1)
class Input_LS
{
public:
   //LS  C2  0340-0387
   CoreSim::Osal::UInt32 LOC_DEV;              //0340  173 C
   CoreSim::Osal::UInt32 LS_FREQ;              //0348  033 C
   CoreSim::Osal::UInt32 LS_ID1;               //0350  263 M
   CoreSim::Osal::UInt32 LS_ID2;               //0358  264 M
   CoreSim::Osal::UInt16 MMRFGVAL;             //0360  BOOL  ILS_FG_VAL
   CoreSim::Osal::UInt16 MMROPPSELECT;         //0364  ENUM  LS_OPP_SEL
   CoreSim::Osal::UInt32 LS_RWY_HDG;           //0368  105 C
   CoreSim::Osal::UInt16 LOC_DEV_NOT_NCD;      //0370  BOOL
   CoreSim::Osal::UInt16 LS_FREQ_NOT_NCD;      //0374  BOOL
   CoreSim::Osal::UInt16 LS_RWY_HDG_NOT_NCD;   //0378  BOOL
   CoreSim::Osal::UInt16 BILSINST;             //037C  BOOL
   CoreSim::Osal::UInt16 BMLSINST;             //0380  BOOL
   CoreSim::Osal::UInt16 BGLSINST;             //0384  BOOL
   //SPARE 0388-038F
   CoreSim::Osal::UInt32 SPARE_9;              //0388-038F
   //LS   C2  0390-039F
   CoreSim::Osal::UInt32 GLIDE_SLOPE_DEV;      //0390  174 
   CoreSim::Osal::UInt16 GLIDE_SLOPE_DEV_NOT_NCD;      //0398  BOOL
   CoreSim::Osal::UInt16 SPARE_10;                     //039C    
};
#pragma pack(pop)

class Input_ADC_T
{
public:
   //ADC  C4  03A0-03BF
   CoreSim::Osal::UInt32 BAROC_HPa_1;          //03A0 M 234  BARO CORR HPa # 1
   CoreSim::Osal::UInt32 BAROC_HB_1;           //03A8 M 235  BARO CORR HB # 1
   CoreSim::Osal::UInt32 BAROC_HPa_2;          //03B0 M 236
   CoreSim::Osal::UInt32 BAROC_HB_2;           //03B8 M 237
    //SPARE 03C0-03C7                           
   CoreSim::Osal::UInt32 SPARE_11;

};

#pragma pack(push,1)
class Input_ADC_E
{
public:
   //ADC   0560-059F
   CoreSim::Osal::UInt32 PRESS_ALT;             //0560  203 IEEE C2 C 
   CoreSim::Osal::UInt32 BARO_ALT;              //0568  ®   IEEE C2 C 
   CoreSim::Osal::UInt32 MACH;                  //0570  205 IEEE C2 C 
   CoreSim::Osal::UInt32 CAS;                   //0578  206 IEEE C2 C 
   CoreSim::Osal::UInt32 TAS;                   //0580  210 IEEE C2 C 
   CoreSim::Osal::UInt32 TAT;                   //0588  211 IEEE C2 C
   CoreSim::Osal::UInt32 SAT;                   //0590  213 A429 C2 M 
   CoreSim::Osal::UInt16 ADCFGVAL;              //0598  BOOL C2 
   CoreSim::Osal::UInt16 ADCSOURCE;             //059C  ENUM C2 ADCFGVAL  0 = OWN, 1 = #3
   //SPARE 05A0-05BF
   CoreSim::Osal::UInt32 SPARE_14[4];
};
#pragma pack(pop)

//IRS  C1/C2 03C8-0423
#pragma pack(push,1)
class Input_IRS
{
public:                        
   CoreSim::Osal::UInt32 TRUE_HDG;             //03C8  C1  314
   CoreSim::Osal::UInt32 FP_ACCEL;             //03D0  C1  323
   CoreSim::Osal::UInt32 FP_ANGLE;             //03D8  C1  322
   CoreSim::Osal::UInt32 PITCH_ANGLE;          //03E0  C1 324 
   CoreSim::Osal::UInt32 ROLL_ANGLE;           //03E8  C1  325
   CoreSim::Osal::UInt32 VERT_ACCEL;           //03F0  C1  364
   CoreSim::Osal::UInt32 BODY_YAWRATE;         //03F8  C1  330
   CoreSim::Osal::UInt32 INERTIAL_ALT;         //0400  C1  361
   CoreSim::Osal::UInt32 INERTIAL_VS;          //0408  C1  365
   CoreSim::Osal::UInt32 MAG_HDG;              //0410  C1  320
   CoreSim::Osal::UInt16 MAGHDGVAL;            //0418  C2  BOOL
   CoreSim::Osal::UInt16 IRSFGVAL;             //041C  C2  BOOL
   CoreSim::Osal::UInt16 IRSSOURCE;            //0420  C2  ENUM
   //SPARE 0424-043F                           
   CoreSim::Osal::UInt16 SPARE_12[7]; 
};
#pragma pack(pop)

//GPS IRS OWN 0440-049F  (//GPS IRS OPP 04A0-04F7 //SPARE 04F8-04FF)  //GP IRS 3 0500-055F 
class Input_GPIRS_E
{
public:
                                                //GPS IRS OWN 0440-049F                                                 //GP IRS 3 0500-055F                  
   CoreSim::Osal::UInt32 PPOS_LAT;				//0440   310,C2,PPOS LAT OWN         //04A0  310 C2 M                    //0500  310 C2  M 
   CoreSim::Osal::UInt32 PPOS_LON;				//0448   311,C2,PPOS LON OWN         //04A8  311 C2 M                    //0508  311 C2  M 
   CoreSim::Osal::UInt32 NS_VEL;				//0450   366,C2,N/S VEL OWN          //04B0  366 C2 M                    //0510  366 C2  M 
   CoreSim::Osal::UInt32 EW_VEL;				//0458   367,C2,E/W VEL OWN          //04B8  367 C2 M                    //0518  367 C2  M 
   CoreSim::Osal::UInt32 IRS_OWN_DIS;           //0460   270,C3,IRS OWN DIS          //04C0  270 C3 M                    //0520  270 C3  M 
   CoreSim::Osal::UInt32 IRS_OWN_SET_LAT;       //0468   041,C3,IRSOWNSETLAT         //04C8  041 C3 M                    //0528  041 C3  M 
   CoreSim::Osal::UInt32 IRS_OWN_SET_LON;       //0470   042,C3,IRSOWNSETLON         //04D0  042 C3 M                    //0530  042 C3  M 
   CoreSim::Osal::UInt32 GPS_TRK_NGLE_TRUE;     //0478   103,C4,GPS TRK ANGLE TRUE   //04D8  103 C4  M                   //0538  103 C4  M 
   CoreSim::Osal::UInt32 GPS_PRES_POS_LAT;      //0480   110,C4,GPS PRES POS-LAT     //04E0  110 C4  M                   //0540  110 C4  M 
   CoreSim::Osal::UInt32 GPS_PRES_POS_LON;      //0488   111,C4,GPR PRES POS-LON     //04E8  111 C4  M                   //0548  111 C4  M 
   CoreSim::Osal::UInt32 GPS_GS;                //0490   112,C4,GPS GROUND SPD       //04F0  112 C4  M  GPS GROUND SPD   //0550  112 C4  M  GPS_GROUND SPD
   CoreSim::Osal::UInt32 HYB_TRK_ANGLE_TRUE;    //0498   137,C2,HYB TRK ANGLE TRUE   //SPARE 04F8-04FF                   //0558  137 C2  M
};

#pragma pack(push,1)
class Input_VOR
{
public:
   //VOR OWN 05C0-05EF
   CoreSim::Osal::UInt32 VOR_OWN_BRG;           //05C0  222 A429 C2   C 
   CoreSim::Osal::UInt32 VOR_OWN_FREQ;          //05C8  034 A429 C2   M 
   CoreSim::Osal::UInt32 VOR_OWN_ID1;           //05D0  242 A429 C2   M 
   CoreSim::Osal::UInt32 VOR_OWN_ID2;           //05D8  244 A429 C2   M 
   CoreSim::Osal::UInt16 VOR_OWN_FGVAL;         //05E0      BOOL C2   
   CoreSim::Osal::UInt32 VOR_OWN_SELCRS;        //05E4  100 A429 C2   M 
   CoreSim::Osal::UInt16 VOR_OWN_BRG_NCD;       //05EC      BOOL C2
   //VOR OPP 05F0-0617
   CoreSim::Osal::UInt32 VOR_OPP_BRG;           //05F0 222 A429 C2 M 
   CoreSim::Osal::UInt32 VOR_OPP_FREQ;          //05F8 034 A429 C2 M 
   CoreSim::Osal::UInt32 VOR_OPP_ID1;           //0600 242 A429 C2 M 
   CoreSim::Osal::UInt32 VOR_OPP_ID2;           //0608 244 A429 C2 M 
   CoreSim::Osal::UInt32 VOROPPSELCRS;          //0610 100 A429 C2 M
   //SPARE   0618-061F
   CoreSim::Osal::UInt32 SPARE_15;
};
#pragma pack(pop)


class PARTNUM
{
public:
   //PART NUMBER    0074 008B                           
   CoreSim::Osal::UInt16 PART_NUM_1;           //0074   See Note 21
   CoreSim::Osal::UInt16 PART_NUM_2;           //0078
   CoreSim::Osal::UInt16 PART_NUM_3;           //007C
   CoreSim::Osal::UInt16 PART_NUM_4;           //0080
   CoreSim::Osal::UInt16 PART_NUM_5;           //0084
   CoreSim::Osal::UInt16 PART_NUM_6;           //0088
};

//GP IRS OWN   0098 00DF   //GP IRS OPP   0628 0668  //GP IRS 3   0670 06B0
class Input_GPIRS_T  
{
public:                          
   CoreSim::Osal::UInt32 HYB_GS;               //0098   175,c4,HYB GROUND SPD      //0628   //0670
   CoreSim::Osal::UInt32 GPS_HORIZ_FOM;        //00A0   247,C4,GPS HORIZ FOM       //0630   //0678
   CoreSim::Osal::UInt32 HYB_PRES_POS_LAT;     //00A8   254,C2,HYB PRES POS-LAT    //0638   //0680
   CoreSim::Osal::UInt32 HYB_PRES_POS_LON;     //00B0   255,C2,HYB PRES POS-LON    //0640   //0688
   CoreSim::Osal::UInt32 HYB_HORIZ_FOM;        //00B8   264,C2,HYB HORIZ FOM       //0648   //0690
   CoreSim::Osal::UInt32 HYB_NS_VEL;           //00C0   266,C2,HYB N-S VEL         //0650   //0698
   CoreSim::Osal::UInt32 HYB_EW_VEL;           //00C8   267,C2,HYB E-W VEL         //0658   //06A0
   CoreSim::Osal::UInt32 GPS_SENSOR_STATUS;    //00D0   273,C4,GPS SENSOR STATUS   //0660   //06A8
   CoreSim::Osal::UInt32 GPADIRS_STATUS;       //00D8   274,C4,GPADIRS STATUS      //0668   //06B0
};

class Input_IRS_E
{
public:
   CoreSim::Osal::UInt32 GPS_ALTITUDE_AMSL;       //0EC0 076 A429 C4 M 
   CoreSim::Osal::UInt32 GPS_HORIZ_INTEG_LIMIT;   //0EC8 130 A429 C2 M 
   CoreSim::Osal::UInt32 HYBRID_HORIZ_INTG_LIMIT; //0ED0 131 A429 C2 M 
   CoreSim::Osal::UInt32 GPS_UTC;                 //0ED8 150 A429 C4 M 
   CoreSim::Osal::UInt32 PRED_DEST_ETA_ECHO;      //0EE0 162 A429 C2 M 
   CoreSim::Osal::UInt32 PRED_WPT_ETA_ECHO;       //0EE8 163 A429 C2 M 
   CoreSim::Osal::UInt32 HYBRID_LAT_FRACTIONS;    //0EF0 256 A429 C2 M 
   CoreSim::Osal::UInt32 HYBRID_LONG_FRACTIONS;   //0EF8 257 A429 C2 M 
   CoreSim::Osal::UInt32 GPS_DATE;                //0F00 260 A429 C4 M 
   CoreSim::Osal::UInt32 HYBRID_ALTITUDE;         //0F08 261 A429 C4 M 
   CoreSim::Osal::UInt32 PRED_DET_HIL;            //0F10 343 A429 C2 M 
   CoreSim::Osal::UInt32 PRED_WPT_HIL;            //0F18 347 A429 C2 M
};

//IRS_SI_OWN/IRS_SI_3
class Input_IRS_SI
{
public:
   CoreSim::Osal::UInt32 P_POS_LAT_D;             //1100 1300 010 A429 C4 FOR MCDU DISPLAY   (BCD)
   CoreSim::Osal::UInt32 P_POS_LON_D;             //1108 1308 011 A429 C4 FOR MCDU DISPLAY   (BCD)
   CoreSim::Osal::UInt32 GROUND_SPEED_D;          //1110 1310 012 A429 C4 FOR MCDU DISPLAY   (BCD) 
   CoreSim::Osal::UInt32 TRK_ANGLE_TRUE_D;        //1118 1318 013 A429 C4 FOR MCDU DISPLAY   (BCD) 
   CoreSim::Osal::UInt32 MAGNETIC_HDG_D;          //1120 1320 014 A429 C4 FOR MCDU DISPLAY   (BCD) 
   CoreSim::Osal::UInt32 WIND_SPEED_D;            //1128 1328 015 A429 C4 FOR MCDU DISPLAY   (BCD) 
   CoreSim::Osal::UInt32 WIND_DIR_TRUE_D;         //1130 1330 016 A429 C4 FOR MCDU DISPLAY   (BCD) 
   CoreSim::Osal::UInt32 MAG_HEADING_D;           //1138 1338 043 A429 C3 Set Heading Echo   (BCD) 
   CoreSim::Osal::UInt32 TRUE_HEADING_D;          //1140 1340 044 A429 C4 FOR MCDU DISPLAY   (BCD) 
   CoreSim::Osal::UInt32 GROUND_SPEED;            //1148 1348 312 A429 C1                                       
   CoreSim::Osal::UInt32 PLATFORM_HEADING;        //1150 1350 334 A429 C1                                       
   CoreSim::Osal::UInt32 IRS_STATUS_WORD;         //1158 1358 350 A429 C4 Used by IRS MON page    
   CoreSim::Osal::UInt32 IRS_DISCRETE_2;          //1160 1360 275 A429 C3
};

//IRS_SI_OPP
class Input_IRS_SI_OPP
{
public:
   CoreSim::Osal::UInt32 P_POS_LAT_D;             //1200 010 A429 C4 FOR MCDU DISPLAY   (BCD)
   CoreSim::Osal::UInt32 P_POS_LON_D;             //1208 011 A429 C4 FOR MCDU DISPLAY   (BCD)
   CoreSim::Osal::UInt32 GROUND_SPEED_D;          //1210 012 A429 C4 FOR MCDU DISPLAY   (BCD) 
   CoreSim::Osal::UInt32 TRK_ANGLE_TRUE_D;        //1218 013 A429 C4 FOR MCDU DISPLAY   (BCD) 
   CoreSim::Osal::UInt32 MAGNETIC_HDG_D;          //1220 014 A429 C4 FOR MCDU DISPLAY   (BCD) 
   CoreSim::Osal::UInt32 WIND_SPEED_D;            //1228 015 A429 C4 FOR MCDU DISPLAY   (BCD) 
   CoreSim::Osal::UInt32 WIND_DIR_TRUE_D;         //1230 016 A429 C4 FOR MCDU DISPLAY   (BCD) 
   CoreSim::Osal::UInt32 MAG_HEADING_D;           //1238 043 A429 C3 Set Heading Echo   (BCD) 
   CoreSim::Osal::UInt32 TRUE_HEADING_D;          //1240 044 A429 C4 FOR MCDU DISPLAY   (BCD) 
   CoreSim::Osal::UInt32 GROUND_SPEED;            //1248 312 A429 C1                                       
   CoreSim::Osal::UInt32 PLATFORM_HEADING;        //1250 334 A429 C1                                       
   CoreSim::Osal::UInt32 IRS_STATUS_WORD;         //1258 350 A429 C4 Used by IRS MON page    
   CoreSim::Osal::UInt32 INERTIAL_ALT;            //1260 361 A429 C3 (Difference with GP ISR OWN)
   CoreSim::Osal::UInt32 IRS_DISCRETE_2;          //1268 275 A429 C3 (Difference with GP ISR OWN)
};

//MLS 1               1400 143B
#pragma pack(push,1)
class Input_MLS
{
public:
         
   CoreSim::Osal::UInt16 FG_RMPOWN_MLS_SEL;        //1400 BOOL Parameter not acquired by HI (TBD)
   CoreSim::Osal::UInt16 FG_RMPOPP_MLS_SEL;        //1404 BOOL Parameter not acquired by HI (TBD)
   CoreSim::Osal::UInt32 MLS_MLS_CHANNEL ;         //1408 A429 Parameter not acquired by HI (TBD)
   CoreSim::Osal::UInt32 MLS_MLS_RUNWAY_HEADING;   //1410 A429 Parameter not acquired by HI (TBD)
   CoreSim::Osal::UInt32 MLS_MLS_LOC_DEV;          //1418 A429 Parameter not acquired by HI (TBD)
   CoreSim::Osal::UInt32 MLS_MLS_ID_1;             //1420 A429 Parameter not acquired by HI (TBD)
   CoreSim::Osal::UInt32 MLS_MLS_ID_2;             //1428 A429 Parameter not acquired by HI (TBD)
   CoreSim::Osal::UInt16 MLS_MLSFGVAL;             //1430 BOOL Parameter not acquired by HI (TBD)
   CoreSim::Osal::UInt16 MLS_OPPSEL;               //1434 BOOL Parameter not acquired by HI (TBD)
   CoreSim::Osal::UInt16 MLS_LOC_DEV_NCD;          //1438 BOOL Parameter not acquired by HI (TBD)
};
#pragma pack(pop)

class Input_FQI_T
{
public:
   //FQI C5 0320-0327                          
   CoreSim::Osal::UInt32 FUEL_QUANT;           //0320  247
   //SPARE 0328-033F                           
   CoreSim::Osal::UInt32 SPARE_8[3];           //0328-033F
};

//FQI 1    C5         1C60 1CBF
class Input_FQI_E
{
public:      
   CoreSim::Osal::UInt32 FUEL_QTY_left_outer_tank;   //1C60 256 A429  M 
   CoreSim::Osal::UInt32 FUEL_QTY_left_inner_tank;   //1C68 257 A429  M 
   CoreSim::Osal::UInt32 FUEL_QTY_center_tank;       //1C70 260 A429  M 
   CoreSim::Osal::UInt32 FUEL_QTY_right_outer_tank;  //1C78 261 A429  M 
   CoreSim::Osal::UInt32 FUEL_QTY_right_inner_tank;  //1C80 262 A429  M 
   CoreSim::Osal::UInt32 FUEL_QTY_ACT_1;             //1C88 263 A429  M 
   CoreSim::Osal::UInt32 FUEL_QTY_ACT_2;             //1C90 264 A429  M 
   CoreSim::Osal::UInt32 FUEL_QTY_ACT_3;             //1C98 265 A429  M 
   CoreSim::Osal::UInt32 FUEL_QTY_ACT_4;             //1CA0 310 A429  M 
   CoreSim::Osal::UInt32 FUEL_QTY_ACT_5;             //1CA8 311 A429  M 
   CoreSim::Osal::UInt32 FUEL_QTY_TOTAL_ACT;         //1CB0 312 A429  M 
   CoreSim::Osal::UInt32 FUEL_QTY_ACT_6;             //1CB8 313 A429  M 
};

//FG -> FM : Bite      0800 080F
#pragma pack(push,1)
class BITE_FG2FM
{
public:
   
   CoreSim::Osal::UInt16 FG2FM_STATUS;          //0800 BIN STATUS MONITORING
   CoreSim::Osal::UInt16 FIDS_CMD;              //0804 BIN
   CoreSim::Osal::UInt16 COMMAND_DATA;          //0808 BIN
   CoreSim::Osal::UInt16 CMD_SEMAPHOR;          //080C BIN
   //SPARE      0810 0823
   CoreSim::Osal::UInt16 SPARE_20[5];
};
#pragma pack(pop)

//FM -> FG : Bite      0840 088B
#pragma pack(push,1)
class BITE_FM2FG
{
public:   
   CoreSim::Osal::UInt16 BITEDATA_TYPE;         //0840 BIN  
   CoreSim::Osal::UInt16 NUM_ATA_WDS;           //0844 BIN  
   CoreSim::Osal::UInt16 BITE_DATA_1;           //0848 BIN  
   CoreSim::Osal::UInt16 BITE_DATA_2;           //084C BIN  
   CoreSim::Osal::UInt16 BITE_DATA_3;           //0850 BIN  
   CoreSim::Osal::UInt16 BITE_DATA_4;           //0854 BIN  
   CoreSim::Osal::UInt16 BITE_DATA_5;           //0858 BIN  
   CoreSim::Osal::UInt16 BITE_DATA_6;           //085C BIN  
   CoreSim::Osal::UInt16 BITE_DATA_7;           //0860 BIN  
   CoreSim::Osal::UInt16 BITE_DATA_8;           //0864 BIN  
   CoreSim::Osal::UInt16 BITE_DATA_9;           //0868 BIN  
   CoreSim::Osal::UInt16 BITE_DATA_10;          //086C BIN  
   CoreSim::Osal::UInt16 BITE_DATA_11;          //0870 BIN  
   CoreSim::Osal::UInt16 BITE_DATA_12;          //0874 BIN  
   CoreSim::Osal::UInt16 BITE_DATA_13;          //0878 BIN  
   CoreSim::Osal::UInt16 BITE_DATA_14;          //087C BIN  
   CoreSim::Osal::UInt16 BITE_DATA_15;          //0880 BIN 
   CoreSim::Osal::UInt16 BITE_DATA_16;          //0884 BIN  
   CoreSim::Osal::UInt16 BITE_DATA_SEM;         //0888 BIN
};
#pragma pack(pop)


//FM -> FG: AMI        0824 083F
#pragma pack(push,1)
class AMI
{
public:
   CoreSim::Osal::UInt16 AL_MDF_INFO_W1;        //0824 BIN WORD 1 of 7  A/L_MODIFIABLE INFO W
   CoreSim::Osal::UInt16 AL_MDF_INFO_W2;        //0828 BIN WORD 2 of 7  A/L_MODIFIABLE INFO W
   CoreSim::Osal::UInt16 AL_MDF_INFO_W3;        //082C BIN WORD 3 of 7  A/L_MODIFIABLE INFO W
   CoreSim::Osal::UInt16 AL_MDF_INFO_W4;        //0830 BIN WORD 4 of 7  A/L_MODIFIABLE INFO W
   CoreSim::Osal::UInt16 AL_MDF_INFO_W5;        //0834 BIN WORD 5 of 7  A/L_MODIFIABLE INFO W
   CoreSim::Osal::UInt16 AL_MDF_INFO_W6;        //0838 BIN WORD 6 of 7  A/L_MODIFIABLE INFO W
   CoreSim::Osal::UInt16 AL_MDF_INFO_W7;        //083C BIN WORD 7 of 7  A/L_MODIFIABLE INFO W
};
#pragma pack(pop)

//FG -> FM : Internal  06C0 0737
class Inter_FG2FM
{
public:         
   CoreSim::Osal::UInt32 FPA_TGT;               //06C0     IEEE C2 
   CoreSim::Osal::UInt32 VS_TGT;                //06C8     IEEE C2 
   CoreSim::Osal::UInt32 ALT_TGT;               //06D0     IEEE C2 
   CoreSim::Osal::UInt32 SPD_TGT;               //06D8     IEEE C2 
   CoreSim::Osal::UInt32 MACH_TGT;              //06E0     IEEE C2 
   CoreSim::Osal::UInt32 FMGC_DIS_WD_1;         //06E8 274 A429 C2 SEE FMGC ICD-OWN A FMGC DIS WORD 1
   CoreSim::Osal::UInt32 FMGC_DIS_WD_2;         //06F0 275 A429 C2 SEE FMGC ICD-OWN A 
   CoreSim::Osal::UInt32 FMGC_DIS_WD_3;         //06F8 273 A429 C2 SEE FMGC ICD-OWN A 
   CoreSim::Osal::UInt32 FMGC_DIS_WD_4;         //0700 146 A429 C2 SEE FMGC ICD-OWN A 
   CoreSim::Osal::UInt32 FMGC_DIS_WD_5;         //0708 145 A429 C2 SEE FMGC ICD-OWN A 
   CoreSim::Osal::UInt32 FMGC_ATS_DIS_WD;       //0710 270 A429 C2 SEE FMGC ICD-OWN A 
   CoreSim::Osal::UInt16 FG_PITCH_ROLL_DIS;     //0718     DIS  C2 SEE DIS TABLE 4.8.7 
   CoreSim::Osal::UInt16 FMGC_PRIORITY;         //071C     BOOL C2 TRUE = Master : 
   CoreSim::Osal::UInt16 FG_RTC_COUNTER;        //0720     INT  C1 
   CoreSim::Osal::UInt32 OPP_FMGC_DIS_WD_4;     //0724 146 A429 C2 C SEE FMGC ICD-OWN A 
   CoreSim::Osal::UInt32 OUTPUT_RLIM_1;         //0730     IEEE C1 
   //SPARE      0738 075F
   CoreSim::Osal::UInt32 SPARE_17[5]; 
};

#pragma pack(push,1)
class Inter_FM2FG
{
public:
   //FM -> FG : Internal  C2  0AE0 0AEB
   CoreSim::Osal::UInt32 VAPP;                   //0AE0  IEEE
   CoreSim::Osal::UInt16 VAPP_VAL;               //0AE8  BOOL
   //SPARE                0AEC 0AEF
   CoreSim::Osal::UInt16 SPARE_25;
   //FM -> FG : Internal  0AF0 0AFB
   CoreSim::Osal::UInt32 V2;                     //0AF0 IEEE C2 
   CoreSim::Osal::UInt16 V2_VAL;                 //0AF8 BOOL C2
   //SPARE                0AFC 0AFF              
   CoreSim::Osal::UInt16 SPARE_26;               //0AFC
   //FM -> FG : Internal  0B00 0B0B              
   CoreSim::Osal::UInt32 Z_ACC_ALT;              //0B00 IEEE C2 
   CoreSim::Osal::UInt16 Z_ACC_ALT_VAL;          //0B08 BOOL C2
   //SPARE                0B0C 0B0F              
   CoreSim::Osal::UInt16 SPARE_27;               //0B0C
   //FM -> FG : Internal  0B10 0B1B              
   CoreSim::Osal::UInt32 GD_SPD_AUTOCTL_TGT;     //0B10 IEEE C2 
   CoreSim::Osal::UInt16 GD_SPD_AUTOCTL_TGT_V;   //0B18 BOOL C2
   //SPARE                0B1C 0B1F              
   CoreSim::Osal::UInt16 SPARE_28;               //0B1C
   //FM -> FG : Internal  0B20 0B2B              
   CoreSim::Osal::UInt32 MACH_AUTOCTL_TGT;       //0B20 IEEE C2 
   CoreSim::Osal::UInt16 MACH_AUTOCTL_TGT_VAL;   //0B28 BOOL C2 
   //SPARE                0B2C 0B2F              
   CoreSim::Osal::UInt16 SPARE_29;               //0B2C
   //FM -> FG : Internal  0B30 0B3B              
   CoreSim::Osal::UInt32 NXTPH_PRESEL_SPD;       //0B30 IEEE C4 
   CoreSim::Osal::UInt16 NXTPH_PRESEL_SPD_VAL;   //0B38 BOOL C4 
   //SPARE                0B3C 0B3F              
   CoreSim::Osal::UInt16 SPARE_30;               //0B3C
   //FM -> FG : Internal 0B40 0B4B               
   CoreSim::Osal::UInt32 NXTPH_PRESELMACH;       //0B40 IEEE C4 
   CoreSim::Osal::UInt16 NXTPH_PRESELMACH_VAL;   //0B48 BOOL C4 
   //SPARE                0B4C 0B4F              
   CoreSim::Osal::UInt16 SPARE_31;               //0B4C
   //FM -> FG : Internal  0B50 0B53              
   CoreSim::Osal::UInt16 NXTPH_SYNCH_REQ;        //0B50 BOOL C2
   //SPARE                0B54 0B57              
   CoreSim::Osal::UInt16 SPARE_32;               //0B54
   //FM -> FG : Internal  0B58 0B8B              
   CoreSim::Osal::UInt32 N1_EPR_THRUST_TGT;      //0B58 IEEE C2 CFM/PW(N1 THRUST TGT)|IAE(EPR THRUST TGT) 
   CoreSim::Osal::UInt16 M_DELTA_THETA_C_VAL;    //0B60 BOOL C2
   CoreSim::Osal::UInt16 N1_EPR_THRUST_TGT_VAL;  //0B64 BOOL C2 CFM/PW(N1 THRUST TGT VAL)|IAE(EPR THRUST TGT VAL)
   CoreSim::Osal::UInt32 VS_TGT;                 //0B68 IEEE C2 
   CoreSim::Osal::UInt32 FPA_TGT;                //0B70 IEEE C2 
   CoreSim::Osal::UInt32 M_DELTA_THETA_C;        //0B78 IEEE C2 
   CoreSim::Osal::UInt32 ALT_CONSTR;             //0B80 IEEE C2 
   CoreSim::Osal::UInt16 ALT_CONSTR_VAL;         //0B88 BOOL C2 
   //SPARE                0B8C 0B8F              
   CoreSim::Osal::UInt16 SPARE_33;               //0B8C
   //FM -> FG : Internal  0B90 0B9B              
   CoreSim::Osal::UInt32 VCDU;                   //0B90 IEEE C2 
   CoreSim::Osal::UInt16 VCDU_VAL;               //0B98 BOOL C2
   //SPARE                0B9C 0B9F              
   CoreSim::Osal::UInt16 SPARE_34;               //0B9C
   //FM -> FG : Internal  0BA0 0BAB              
   CoreSim::Osal::UInt32 FLEX_TO_TEMP;           //0BA0 IEEE C2 
   CoreSim::Osal::UInt16 FLEX_TO_TEMP_VAL;       //0BA8 BOOL C2
   //SPARE                0BAC 0BAF              
   CoreSim::Osal::UInt16 SPARE_35;               //0BAC
   //FM -> FG : Internal  0BB0 0BC3              
   CoreSim::Osal::UInt16 FM_PITCH_AT_DIS_1;      //0BB0 DIS  C2 SEE DIS TABLE 4.9.1 FMPITAT1
   CoreSim::Osal::UInt16 FM_PITCH_AT_DIS_2;      //0BB4 DIS  C2 SEE DIS TABLE 4.9.2 FMPITAT2
   CoreSim::Osal::UInt32 RWY_HDG;                //0BB8 IEEE
   CoreSim::Osal::UInt16 RWY_HDG_VAL;            //0BC0 BOOL
   //SPARE                0BC4 0BC7              
   CoreSim::Osal::UInt16 SPARE_36;               //0BC4
   //FM -> FG : Internal  0BC8 0BFB
   CoreSim::Osal::UInt32 TRACK_TGT;              //0BC8 IEEE C2 
   CoreSim::Osal::UInt32 HDG_TGT;                //0BD0 IEEE C2 
   CoreSim::Osal::UInt32 VOR_CRS_TGT;            //0BD8 IEEE C2 
   CoreSim::Osal::UInt32 M_PHI_C;                //0BE0 IEEE C2 
   CoreSim::Osal::UInt32 DIST_TO_VOR_STA;        //0BE8 IEEE C2 
   CoreSim::Osal::UInt32 FM_DIST_TO_LOC;         //0BF0 IEEE C2 
   CoreSim::Osal::UInt16 DIST_TO_LOC_VAL;        //0BF8 BOOL C2
   //SPARE                0BFC 0BFF              
   CoreSim::Osal::UInt16 SPARE_37;               //0BFC
   //FM -> FG : Internal  0C00 0C13              
   CoreSim::Osal::UInt16 FM_ROLL_BDIS_1;         //0C00 DIS  C2 SEE DIS TABLE 4.9.4 FMROLL1
   CoreSim::Osal::UInt16 FM_ROLL_BDIS_2;         //0C04 DIS  C2 SEE DIS TABLE 4.9.5 FMROLL2
   CoreSim::Osal::UInt32 ZCRUISE;                //0C08 IEEE C2 
   CoreSim::Osal::UInt16 ZCRUISE_VAL;            //0C10 BOOL C2 
   //SPARE                0C14 0C17              
   CoreSim::Osal::UInt16 SPARE_38;               //0C14
   //FM -> FG : Internal  0C18 0C23              
   CoreSim::Osal::UInt32 WIND_MAGNITUDE;         //0C18 IEEE C2 
   CoreSim::Osal::UInt16 WIND_VALID;             //0C20 BOOL C2
   //SPARE                0C24 0C27              
   CoreSim::Osal::UInt16 SPARE_39;               //0C24
   //FM -> FG : Internal  0C28 0C33              
   CoreSim::Osal::UInt32 SPD_MAN_TGT;            //0C28 IEEE C2 
   CoreSim::Osal::UInt16 SPD_MAN_TGT_VAL;        //0C30 BOOL C2
   //SPARE                0C34 0C37              
   CoreSim::Osal::UInt16 SPARE_40;               //0C34
   //FM -> FG : Internal  0C38 0C43              
   CoreSim::Osal::UInt32 MACH_MAN_TGT;           //0C38 IEEE C2 
   CoreSim::Osal::UInt16 MACH_MAN_TGT_VAL;       //0C40 BOOL C2
   //SPARE                0C44 0C47              
   CoreSim::Osal::UInt16 SPARE_41;               //0C44
   //FM -> FG : Internal  0C48 0C53              
   CoreSim::Osal::UInt32 LOW_TGT_SPD_MG;         //0C48 IEEE C2 
   CoreSim::Osal::UInt16 LOW_TGT_SPD_MG_VAL;     //0C50 BOOL C2 
   //SPARE                0C54 0C57              
   CoreSim::Osal::UInt16 SPARE_42;               //0C54
   //FM -> FG : Internal  0C58 0C9B
   CoreSim::Osal::UInt32 HIGH_TGT_SPD_MG;        //0C58 IEEE C2
   CoreSim::Osal::UInt16 HIGH_TGT_SPD_MG_VAL;    //0C60 BOOL C2 
   CoreSim::Osal::UInt16 LANDING_CONFIG_3;       //0C64 BOOL C2 
   CoreSim::Osal::UInt32 MAN_XALT;               //0C68 IEEE C2 
   CoreSim::Osal::UInt32 DSPL_SPD_AUTO_TGT;      //0C70 IEEE_C2
   CoreSim::Osal::UInt16 DSPL_SPD_AUTO_TGT_VA;   //0C78 BOOL C2 BFMDSAVA
   CoreSim::Osal::UInt32 FM_MAG_TRACK;           //0C7C IEEE C2 DCR 10031 ( NOT APP. )
   CoreSim::Osal::UInt16 FM_MAG_TRACK_VAL;       //0C84 BOOL C2 DCR 10031 ( NOT APP. )
   CoreSim::Osal::UInt32 AUTO_XALT;              //0C88 IEEE C2 FMAXALT
   CoreSim::Osal::UInt16 AUTO_XALT_VAL;          //0C90 BOOL C2 BFMAXAVA
   CoreSim::Osal::UInt16 FM_PITCH_AT_DIS_3;      //0C94 DIS  C2 SEE DIS TABLE 4.9.3  FMPITAT3
   CoreSim::Osal::UInt16 SPAT_ACKNOWLEDGE;       //0C98 BOOL C2 NOT ACQUIRED BY FG
   //SPARE                0C9C 0CA7              
   CoreSim::Osal::UInt16 SPARE_43[3];            //0C9C
   //FM -> FG : Internal  0CA8 0CD7              
   CoreSim::Osal::UInt32 FM_EST_POS_ERROR;       //0CA8 IEEE C2 
   CoreSim::Osal::UInt16 FM_EST_POS_ERROR_VAL;   //0CB0 BOOL C2 WARNING ! This data is not acquired on FG side for the moment !(TBD)
   CoreSim::Osal::UInt32 FM_DIST_TO_LOC_AXIS;    //0CB4 IEEE C2 
   CoreSim::Osal::UInt16 FM_DIST_TO_LOC_AXIS_VAL; //0CBC BOOL C2 This data is not acquired on FG side for the moment !(TBD)
   CoreSim::Osal::UInt32 FM_DIST_TO_RWY_THR;      //0CC0 IEEE C2 
   CoreSim::Osal::UInt16 FM_DIST_TO_RWY_THR_VAL;  //0CC8 BOOL C2 This data is not acquired on FG side for the moment !(TBD)
   CoreSim::Osal::UInt32 FM_RWY_THR_TO_LOC;       //0CCC IEEE C2 NEW FM-FG DEFINITION
   CoreSim::Osal::UInt16 FM_RWY_THR_TO_LOC_VAL;   //0CD4 BOOL C2
   //SPARE                0CD8 0CDF               
   CoreSim::Osal::UInt32 SPARE_44;                //0CD8
   //FM -> FG : Internal  0CE0 0CEB               
   CoreSim::Osal::UInt16 SPARE_45[3];             //0CE0  
   //SPARE                0CEC 0CEF               
   CoreSim::Osal::UInt16 SPARE_46;                //0CEC  
   //FM -> FG : Internal  0CF0 0CFB               
   CoreSim::Osal::UInt16 SPARE_47[3];             //0CF0
   //SPARE                0CFC 0CFF               
   CoreSim::Osal::UInt16 SPARE_48;                //0CFC
   //FM -> FG : Internal  0D00 0D0B               
   CoreSim::Osal::UInt32 DEST_TEMP;               //0D00 IEEE C2 (TBD)
   CoreSim::Osal::UInt16 DEST_TEMP_VAL;           //0D08 BOOL C2 (TBD)
   //SPARE                0D0C 0D0F               
   CoreSim::Osal::UInt16 SPARE_49;                //0D0C
   //FM -> FG : Internal  0D10 0D1B               
   CoreSim::Osal::UInt32 DEST_QNH;                //0D10 IEEE C2 (TBD)
   CoreSim::Osal::UInt16 DEST_QNH_VAL;            //0D18 BOOL C2 (TBD)
   //SPARE                0D1C 0D1F               
   CoreSim::Osal::UInt16 SPARE_50;                //0D1C
   //FM -> FG : Internal  0D20 0D2B               
   CoreSim::Osal::UInt32 RWY_ELEV_N;              //0D20 IEEE C2 (TBD)
   CoreSim::Osal::UInt16 RWY_ELEV_VAL;            //0D28 BOOL C2 (TBD)
   //SPARE                0D2C 0D2F               
   CoreSim::Osal::UInt16 SPARE_51;                //0D2C
   //FM -> FG : Internal  0D30 0D4B               
   CoreSim::Osal::UInt16 NAV_MODE;                //0D30 ENUM C2 0=NO NAV1=INERTIAL VORDME2=INERTIAL ONLY3=INERTIAL DMEDME4=INERTIAL GPS(TBD)
   CoreSim::Osal::UInt16 HIGH_ACCURACY_STATUS;    //0D34 BOOL C2 
   CoreSim::Osal::UInt32 FM_FREQUENCY;            //0D38 IEEE C2 
   CoreSim::Osal::UInt32 FM_APPROACH_MODE;        //0D40 IEEE C2 
   CoreSim::Osal::UInt16 FM_APPROACH_MODE_FREQUENCY_VAL;  //0D48 BOOL C2   
   //SPARE                0D4C 0DFF
   CoreSim::Osal::UInt16 SPARE_52[45];            //0D4C
   //FM -> FG : Internal  0E00 0E37               
   CoreSim::Osal::UInt16 PN_MCDU_OWN_W[7];        //0E00
   CoreSim::Osal::UInt16 SN_MCDU_OWN_W[7];        //0E1C 
};
#pragma pack(pop)

#pragma pack(push,1)
class CommMemory
{
public:
   //PROTOCOL       0000-001F
   Prot Prot;
   //PROT. NEW FM   0020 003F
   Protocol_NewFM Prot_NewFM;
   //FG-FM:Hard     0040 0073
   Input_Hard_T   Hard_FGFM_T;
   //PART NUMBER    0074 008B 
   PARTNUM        Prot_PartNum;
   //FG-FM:Hard     008C 0097  
   Input_Hard_E   Hard_FGFM_E;

   //GP IRS OWN     0098 00DF
   Input_GPIRS_T    Irs_Own;  
   //SPARE  00E0-015F                          
   CoreSim::Osal::UInt32 SPARE[3];
   CoreSim::Osal::UInt16 Ad_FG_SIDE;  //00F8 Side 1=0XAA55H; Side 2 =0X55AAH;
   CoreSim::Osal::UInt16 PW_ENGINE;   //00FC 
   CoreSim::Osal::UInt32 SPARE17[12];
   //FAC    C2  0160-0187; //SPARE  0188-018F      //FAC   C2  0190-01D7 //SPARE  01D8-01DF  
   Input_FAC Fac;

   //FADEC OWN  C2  01E0-0233 //SPARE 0234-023F    
   Input_FADEC FadecOwn;
   //FADEC OPP  C2   0240-0273 //SPARE 0274-0297
   Input_FADECOPP FadecOpp;

   //FCU  C2 0298-02F3 //SPARE 02F4-02F7 //FCU   02F8-0307
   Input_FCU Fcu;     

   //FQI C5 0320-0327  //SPARE 0328-033F 
   Input_FQI_T Fqi_T;

   //LS  C2  0340-0387 //SPARE 0388-038F //LS   C2  0390-039F
   Input_LS LS;
     
   //ADC  C4  03A0-03BF
   Input_ADC_T Adc_T; 

   //IRS  C1/C2 03C8-043F  
   Input_IRS    Irs;
   
   //GP IRS OWN 0440-049F  (//GPS IRS OPP 04A0-04F7 //SPARE 04F8-04FF)  //GP IRS 3 0500-055F 
   Input_GPIRS_E Irs_Own_2;
   Input_GPIRS_E Irs_Opp_2;
   Input_GPIRS_E Irs_3_2;
           
   //ADC   0560-059F   //SPARE 05A0-05BF
   Input_ADC_E Adc_E;

   //VOR OWN 05C0-05EF //VOR OPP 05F0-0617
   Input_VOR Vor; 

   //GP IRS OPP 0620-066F
   CoreSim::Osal::UInt32 HYB_TRK_ANGLE_TRUE;    //0620 137 A429 C2 M 
   Input_GPIRS_T    Irs_Opp;
   //GP IRS 3   0670 06B7
   Input_GPIRS_T    Irs_3;
   //SPARE      06B8 06BF
   CoreSim::Osal::UInt32 SPARE_16;

   //FG -> FM : Internal  06C0 075F
   Inter_FG2FM Inter_FGFM;
   
   //SPATIAL    0760 07EF
   CoreSim::Osal::UInt32 SPARE_18[18];
   //SPARE      07F0 07FF
   CoreSim::Osal::UInt32 SPARE_19[2];

   //FG -> FM : Bite      0800 0823
   BITE_FG2FM Bite_FG;
   
   //FM -> FG: AMI        0824 083F
   AMI Ami; 
   //FM -> FG : Bite      0840 088B
   BITE_FM2FG Bite_FM; 

   //CODE COMP FM         088C 08A3
   CoreSim::Osal::UInt16 CODE_COMP_FM_WORD[6];  //088C-08A3 INT POWER-UP
   //OPERAT. S/W P/N      08A4 08BF
   CoreSim::Osal::UInt16 OPERATIONAL_SOFT_PN[7];//08A4-08BF INT OPERATIONAL SOFT P/N 1
   //Perf. Database       08C0 08DB
   CoreSim::Osal::UInt16 PERFO_DATA_BASE_W[7];  //08C0-08DB INT PERFO DATA BASE W.1
   //Configuration file   08DC 08F7
   CoreSim::Osal::UInt16 CONFIGURATION_FILE_W[7];//08DC-08F7 INT CONFIGURATION FILE W.1
   //Magnetic Data Base   08F8 0913
   CoreSim::Osal::UInt16 MAGN_DATA_BASE_W[7];    //08F8-0913 INT MAGN. DATA BASE W.1
   //NAV Data Base        0914 0927
   CoreSim::Osal::UInt16 NAV_DATA_BASE_W[5];     //0914_0927 INT NAV. DATA BASE W.
   //SPARE                0928 093F
   CoreSim::Osal::UInt32 SPARE_21[3];

   //A BUS   C4           0940 097F                          
   CoreSim::Osal::UInt32 CITYPAIR_WD[3];         //0940 040-041-042 a429
   CoreSim::Osal::UInt32 FLIGHTNO_WD[5];         //0958 233-234-235-236-237  A429
   //SPARE                0980 09FF              
   CoreSim::Osal::UInt32 SPARE_22[16];           
   //B BUS  C3              0A00 0A0F            
   CoreSim::Osal::UInt32 WEIGHT;                 //0A00  075
   CoreSim::Osal::UInt32 CG;                     //0A08  077
   //SPARE                0A10 0A57              
   CoreSim::Osal::UInt32 SPARE_23[9];            
   //C BUS C1             0A58 0ADB              
   CoreSim::Osal::UInt32 C_BUSS_DATA[16];        //0A58  A429
   CoreSim::Osal::UInt16 NUM_C_BUSS;             //0AD8  INT  NUM A429 WDS IN BUFF
   //SPARE                0ADC 0ADF
   CoreSim::Osal::UInt16 SPARE_24;

   //FM -> FG : Internal  0AE0 0E37
   Inter_FM2FG      Inter_FMFG;
   //SPARE                0E38 0EBF               
   CoreSim::Osal::UInt16 PN_MCDU_OPP_W[7];        //0E38-0E53 (TBD)
   CoreSim::Osal::UInt16 SN_MCDU_OPP_W[7];        //0E54-0E6F (TBD)
   CoreSim::Osal::UInt32 SPARE_51[10];            //0E70-0EBF

   //GP IRS OWN          0EC0 0F1F
   Input_IRS_E Irs_Own_End;
   //SPARE               0F20 0F7F
   CoreSim::Osal::UInt64 SPARE_52[6];
   //GP IRS OPP          0F80 0FDF
   Input_IRS_E Irs_Opp_End;
   //SPARE               0FE0 103F
   CoreSim::Osal::UInt64 SPARE_53[6];
   //GP IRS 3            1040 109F
   Input_IRS_E Irs_3_End;
   //SPARE               10A0 10FF
   CoreSim::Osal::UInt64 SPARE_54[6];

   //GP IRS OWN SI 1     1100 1167
   Input_IRS_SI  Irs_Si_Own; 
   //SPARE               1168 11FF
   CoreSim::Osal::UInt32 SPARE_55[19];
   //GP IRS OPP SI 1     1200 126F  (Thales/Smiths FM Only)
   Input_IRS_SI_OPP  Irs_Si_Opp;
   //SPARE               1270 12FF
   CoreSim::Osal::UInt64 SPARE_56[9];
   //GP IRS 3 SI 1       1300 1367 (Same with IRS OWN)
   Input_IRS_SI  Irs_Si_3;
   //SPARE               1368 13FF
   CoreSim::Osal::UInt32 SPARE_57[19];

   //MLS 1               1400 143B
   Input_MLS Mls;
   //SPARE               143C 1BFF
   CoreSim::Osal::UInt16 SPARE_58[497];

   //FM ST. AT PU ** 1   1C00 1C4F
   CoreSim::Osal::UInt16 FM_STATE_AT_PWR_UP_W[20];  //1C00 INT Parameter not used by HI FM runtime applications, used by FM Platform SW HI FMduring power-up
   //SPARE               1C50 1C5F
   CoreSim::Osal::UInt64 SPARE_59;

   //FQI 1    C5         1C60 1CBF
   Input_FQI_E Fqi_E;
   //SPARE               1CC0 1FFF
   CoreSim::Osal::UInt64 SPARE_60[52];

};
#pragma pack(pop)

class InputsDIS
{
   public:
   InputsDIS():
	   RM08F_SIDE_1_MON(false),
	   RM07G_AIRLINE_1(false),
	   RM07H_AIRLINE_2(false),
	   RM07J_AIRLINE_3(false),
	   RM07K_AIRLINE_4(false),
	   RM08G_AIRLINE_5(false),
	   RM08H_AIRLINE_6(false),
	   RM08J_AIRLINE_7(false),
	   RM09C_AIRLINE_8(false),
	   RM09D_AIRLINE_9(false),
	   RM09E_AIRLINE_10(false),
	   RM09F_AIRLINE_11(false),
	   RM09G_AIRLINE_12(false),
	   RM09H_AIRLINE_13(false),
	   RM09J_AIRLINE_14(false),
	   RM09K_AIRLINE_15(false),
	   RM10C_AIRLINE_16(false),
	   RM10D_AIRLINE_17(false),
	   RM10E_AIRLINE_18(false),
	   RM10F_AIRLINE_19(false),
	   RM10G_AIRLINE_20(false),
	   RM10H_AIRLINE_21(false),
	   RM10J_AIRLINE_22(false),
	   RM10K_AIRLINE_23(false),
	   RM11E_AIRLINE_24(false),
	   RM11F_AIRLINE_25(false),
	   RM11G_AIRLINE_26(false),
	   RM11H_AIRLINE_27(false),
	   RM11J_NO_AP_DISC_IN_FDES(false),
	   RM11K_AP_TCAS_FUNCTION_COM(false),
	   RM06G_SOFT_GO_AROUND(false),
	   RM06H_AIRLINE_31(false),
	   RM06J_AIRLINE_32(false),
	   RM06K_AIRLINE_33(false),
	   RM08C_AIRLINE_34(false),
	   RM08D_AIRLINE_35(false),
	   RM08E_AIRLINE_36(false),
	   LM01B_JNB_Autoland_COM(false),
	   LM02B_NAV_IN_GA_COM(false),
	   LM03A_SPI3C(false),
	   LM04A_NEO_COM(false),
	   LM04B_TRAFFIC_ADVISORY(false),
	   LM05B_SPI6C(false),
	   LM08A_SPI8C(false),
	   LM08E_AUTO_BRAKE_ARMED(false),
	   LM09C_SPI12C(false),
	   LM09D_SPI13C(false),
	   LM09E_SPI14C(false),
	   LT04B_SPI16C(false),
	   LT05B_SPI17C(false),
	   LT06C_SPI18C(false),
	   LT06D_SPI19C(false),
	   LT06A_SPI20C(false),
	   LM05A_AC1(false),
	   LM06A_AC2(false),
	   LM06B_AC3(false),
	   LM06C_AC4(false),
	   LM07A_AC5(false),
	   LM07B_AC6(false),
	   LM07C_AC7(false),
	   LM07D_AC8(false),
	   LM08B_AC10(false),
	   LM08C_AC11(false),
	   LM08D_PARITY_COM(false),
	   RM08K_PARITY_MON(false),
	   RM01J_JNB_Autoland_MON(false),
	   RM02J_NAV_IN_GA_MON(false),
	   RM02K_AP_TCAS_FUNCTION_MON(false),
	   RM03J_AIRLINE37(false),
	   RM03K_AIRLINE38(false),
	   RM04K_AIRLINE39(false),
	   RM05K_AIRLINE40(false),
	   RT01J_NEO_MON(false),
	   RT02J_SPI9M(false),
	   RT02K_SPI10M(false),
	   RT03J_SPI11M(false),
	   RT03K_SPI12M(false),
	   RT04J_SPI13M(false),
	   RT04K_SPI14M(false),
	   RT05J_SPI15M(false),
	   LT01C_FAC_OPP_HLTY_COM(false),
	   RT01C_FAC_OPP_HLTY_MON(false),
	   LT01F_ATH_OPP_ENGD_COM(false),
	   RT01F_ATH_OPP_ENGD_MON(false),
	   LT02C_ELAC_OPP_AP_DISC_COM(false),
	   RT02C_ELAC_OPP_AP_DISC_MON(false),
	   LT03G_AP_OPP_ENGD_MON_COM(false),
	   LT02E_AP_OPP_ENGD_COM_COM(false),
	   LT04E_FD_OPP_ENGD_COM(false),
	   RT04E_FD_OPP_ENGD_MON(false),
	   LT04G_BSCU_OPP_VALID_COM(false),
	   RT04G_BSCU_OPP_VALID_MON(false),
	   LM01C_FAC_OWN_HLTY_COM(false),
	   RM01C_FAC_OWN_HLTY_MON(false),
	   LM02C_ELAC_OWN_AP_DISC_COM(false),
	   RM02C_ELAC_OWN_AP_DISC_MON(false),
	   LM04G_BSCU_OWN_VALID_COM(false),
	   RM04G_BSCU_OWN_VALID_MON(false),
	   LM05G_FCU_OWN_AP_SW_COM(false),
	   LM01H_AP_INST_DISC_NC(false), 
	   LM05C_AP_INST_DISC_NO(false),
	   RT02F_RIGHT_WHEEL_SPEED(false),
	   RM02F_LEFT_WHEEL_SPEED(false),
	   LM02G_FWC_OWN_VALID(false),
	   LT02G_FWC_OPP_VALID(false),
	   LM02H_PFD_OWN_VALID(false),
	   LT02H_PFD_OPP_VALID(false),
	   LM03B_PS_SPLIT(false),
	   LM03D_FCU_OWN_ATH_SW_COM(false),
	   RM05C_ATH_INST_DISC_NC(false),
	   RM03H_ATH_INST_DISC_NO(false),
	   RM04J_ATT_3_SW(false),
	   LM04D_NOSE_GEAR_OWN(false),
	   RM05J_ADC_3_SW(false),
	   LM06D_SIDE_1_COM(false),
	   LT01B_NORTH_REF_PB(false),
	   LM04F_FCU_OWN_HLTY(false),
	   RT04F_FCU_OPP_HLTY(false),
	   RM06F_ENGINE_OWN_STOP(false),
	   RT06F_ENGINE_OPP_STOP(false),
	   LT02A_CDU_OPP_FAIL(false),
	   LM07E_NAV_CONTROL_OWN(false),
	   LT06B_NAV_CONTROL_OPP(false),
	   LM02A_CDU_OWN_FAIL(false),
	   LT02B_FMGC_OPP_HLTY(false),
	   BSP_CONNEX(false),
	   RT03G_AP_OPP_ENGD_MON_MON(false),
	   RT02E_AP_OPP_ENGD_COM_MON(false),
	   RM05G_FCU_OWN_AP_SW_MON(false),
	   RM03D_FCU_OWN_ATH_SW_MON(false),
	   RM06E_AC_TYPE_3(false),
	   RM06C_ENG_TYPE_5(false),
	   RM07C_PP_PARITY_2G(false),
	   LM05D_NAV_BACKUP_SELECTED(false)
      {}
	   CoreSim::Vdn::Bool RM08F_SIDE_1_MON;
		CoreSim::Vdn::Bool RM07G_AIRLINE_1;
		CoreSim::Vdn::Bool RM07H_AIRLINE_2;
		CoreSim::Vdn::Bool RM07J_AIRLINE_3;
		CoreSim::Vdn::Bool RM07K_AIRLINE_4;
		CoreSim::Vdn::Bool RM08G_AIRLINE_5;
		CoreSim::Vdn::Bool RM08H_AIRLINE_6;
		CoreSim::Vdn::Bool RM08J_AIRLINE_7;
		CoreSim::Vdn::Bool RM09C_AIRLINE_8;
		CoreSim::Vdn::Bool RM09D_AIRLINE_9;
		CoreSim::Vdn::Bool RM09E_AIRLINE_10;
		CoreSim::Vdn::Bool RM09F_AIRLINE_11;
		CoreSim::Vdn::Bool RM09G_AIRLINE_12;
		CoreSim::Vdn::Bool RM09H_AIRLINE_13;
		CoreSim::Vdn::Bool RM09J_AIRLINE_14;
		CoreSim::Vdn::Bool RM09K_AIRLINE_15;
		CoreSim::Vdn::Bool RM10C_AIRLINE_16;
		CoreSim::Vdn::Bool RM10D_AIRLINE_17;
		CoreSim::Vdn::Bool RM10E_AIRLINE_18;
		CoreSim::Vdn::Bool RM10F_AIRLINE_19;
		CoreSim::Vdn::Bool RM10G_AIRLINE_20;
		CoreSim::Vdn::Bool RM10H_AIRLINE_21;
		CoreSim::Vdn::Bool RM10J_AIRLINE_22;
		CoreSim::Vdn::Bool RM10K_AIRLINE_23;
		CoreSim::Vdn::Bool RM11E_AIRLINE_24;
		CoreSim::Vdn::Bool RM11F_AIRLINE_25;
		CoreSim::Vdn::Bool RM11G_AIRLINE_26;
		CoreSim::Vdn::Bool RM11H_AIRLINE_27;
		CoreSim::Vdn::Bool RM11J_NO_AP_DISC_IN_FDES;
		CoreSim::Vdn::Bool RM11K_AP_TCAS_FUNCTION_COM;
		CoreSim::Vdn::Bool RM06G_SOFT_GO_AROUND;
		CoreSim::Vdn::Bool RM06H_AIRLINE_31;
		CoreSim::Vdn::Bool RM06J_AIRLINE_32;
		CoreSim::Vdn::Bool RM06K_AIRLINE_33;
		CoreSim::Vdn::Bool RM08C_AIRLINE_34;
		CoreSim::Vdn::Bool RM08D_AIRLINE_35;
		CoreSim::Vdn::Bool RM08E_AIRLINE_36;
		CoreSim::Vdn::Bool LM01B_JNB_Autoland_COM;
		CoreSim::Vdn::Bool LM02B_NAV_IN_GA_COM;
		CoreSim::Vdn::Bool LM03A_SPI3C;
		CoreSim::Vdn::Bool LM04A_NEO_COM;
		CoreSim::Vdn::Bool LM04B_TRAFFIC_ADVISORY;
		CoreSim::Vdn::Bool LM05B_SPI6C;
		CoreSim::Vdn::Bool LM08A_SPI8C;
		CoreSim::Vdn::Bool LM08E_AUTO_BRAKE_ARMED;
		CoreSim::Vdn::Bool LM09C_SPI12C;
		CoreSim::Vdn::Bool LM09D_SPI13C;
		CoreSim::Vdn::Bool LM09E_SPI14C;
		CoreSim::Vdn::Bool LT04B_SPI16C;
		CoreSim::Vdn::Bool LT05B_SPI17C;
		CoreSim::Vdn::Bool LT06C_SPI18C;
		CoreSim::Vdn::Bool LT06D_SPI19C;
		CoreSim::Vdn::Bool LT06A_SPI20C;
		CoreSim::Vdn::Bool LM05A_AC1;
		CoreSim::Vdn::Bool LM06A_AC2;
		CoreSim::Vdn::Bool LM06B_AC3;
		CoreSim::Vdn::Bool LM06C_AC4;
		CoreSim::Vdn::Bool LM07A_AC5;
		CoreSim::Vdn::Bool LM07B_AC6;
		CoreSim::Vdn::Bool LM07C_AC7;
		CoreSim::Vdn::Bool LM07D_AC8;
		CoreSim::Vdn::Bool LM08B_AC10;
		CoreSim::Vdn::Bool LM08C_AC11;
		CoreSim::Vdn::Bool LM08D_PARITY_COM;
		CoreSim::Vdn::Bool RM08K_PARITY_MON;
		CoreSim::Vdn::Bool RM01J_JNB_Autoland_MON;
		CoreSim::Vdn::Bool RM02J_NAV_IN_GA_MON;
		CoreSim::Vdn::Bool RM02K_AP_TCAS_FUNCTION_MON;
		CoreSim::Vdn::Bool RM03J_AIRLINE37;
		CoreSim::Vdn::Bool RM03K_AIRLINE38;
		CoreSim::Vdn::Bool RM04K_AIRLINE39;
		CoreSim::Vdn::Bool RM05K_AIRLINE40;
		CoreSim::Vdn::Bool RT01J_NEO_MON;
		CoreSim::Vdn::Bool RT02J_SPI9M;
		CoreSim::Vdn::Bool RT02K_SPI10M;
		CoreSim::Vdn::Bool RT03J_SPI11M;
		CoreSim::Vdn::Bool RT03K_SPI12M;
		CoreSim::Vdn::Bool RT04J_SPI13M;
		CoreSim::Vdn::Bool RT04K_SPI14M;
		CoreSim::Vdn::Bool RT05J_SPI15M;
		CoreSim::Vdn::Bool LT01C_FAC_OPP_HLTY_COM;
		CoreSim::Vdn::Bool RT01C_FAC_OPP_HLTY_MON;
		CoreSim::Vdn::Bool LT01F_ATH_OPP_ENGD_COM;
		CoreSim::Vdn::Bool RT01F_ATH_OPP_ENGD_MON;
		CoreSim::Vdn::Bool LT02C_ELAC_OPP_AP_DISC_COM;
		CoreSim::Vdn::Bool RT02C_ELAC_OPP_AP_DISC_MON;
		CoreSim::Vdn::Bool LT03G_AP_OPP_ENGD_MON_COM;
		CoreSim::Vdn::Bool LT02E_AP_OPP_ENGD_COM_COM;
		CoreSim::Vdn::Bool LT04E_FD_OPP_ENGD_COM;
		CoreSim::Vdn::Bool RT04E_FD_OPP_ENGD_MON;
		CoreSim::Vdn::Bool LT04G_BSCU_OPP_VALID_COM;
		CoreSim::Vdn::Bool RT04G_BSCU_OPP_VALID_MON;
		CoreSim::Vdn::Bool LM01C_FAC_OWN_HLTY_COM;
		CoreSim::Vdn::Bool RM01C_FAC_OWN_HLTY_MON;
		CoreSim::Vdn::Bool LM02C_ELAC_OWN_AP_DISC_COM;
		CoreSim::Vdn::Bool RM02C_ELAC_OWN_AP_DISC_MON;
		CoreSim::Vdn::Bool LM04G_BSCU_OWN_VALID_COM;
		CoreSim::Vdn::Bool RM04G_BSCU_OWN_VALID_MON;
		CoreSim::Vdn::Bool LM05G_FCU_OWN_AP_SW_COM;
		CoreSim::Vdn::Bool LM01H_AP_INST_DISC_NC;
		CoreSim::Vdn::Bool LM05C_AP_INST_DISC_NO;
		CoreSim::Vdn::Bool RT02F_RIGHT_WHEEL_SPEED;
		CoreSim::Vdn::Bool RM02F_LEFT_WHEEL_SPEED;
		CoreSim::Vdn::Bool LM02G_FWC_OWN_VALID;
		CoreSim::Vdn::Bool LT02G_FWC_OPP_VALID;
		CoreSim::Vdn::Bool LM02H_PFD_OWN_VALID;
		CoreSim::Vdn::Bool LT02H_PFD_OPP_VALID;
		CoreSim::Vdn::Bool LM03B_PS_SPLIT;
		CoreSim::Vdn::Bool LM03D_FCU_OWN_ATH_SW_COM;
		CoreSim::Vdn::Bool RM05C_ATH_INST_DISC_NC;
		CoreSim::Vdn::Bool RM03H_ATH_INST_DISC_NO;
		CoreSim::Vdn::Bool RM04J_ATT_3_SW;
		CoreSim::Vdn::Bool LM04D_NOSE_GEAR_OWN;
		CoreSim::Vdn::Bool RM05J_ADC_3_SW;
		CoreSim::Vdn::Bool LM06D_SIDE_1_COM;
		CoreSim::Vdn::Bool LT01B_NORTH_REF_PB;
		CoreSim::Vdn::Bool LM04F_FCU_OWN_HLTY;
		CoreSim::Vdn::Bool RT04F_FCU_OPP_HLTY;
		CoreSim::Vdn::Bool RM06F_ENGINE_OWN_STOP;
		CoreSim::Vdn::Bool RT06F_ENGINE_OPP_STOP;
		CoreSim::Vdn::Bool LT02A_CDU_OPP_FAIL;
		CoreSim::Vdn::Bool LM07E_NAV_CONTROL_OWN;
		CoreSim::Vdn::Bool LT06B_NAV_CONTROL_OPP;
		CoreSim::Vdn::Bool LM02A_CDU_OWN_FAIL;
		CoreSim::Vdn::Bool LT02B_FMGC_OPP_HLTY;
		CoreSim::Vdn::Bool BSP_CONNEX;
		CoreSim::Vdn::Bool RT03G_AP_OPP_ENGD_MON_MON;
		CoreSim::Vdn::Bool RT02E_AP_OPP_ENGD_COM_MON;
		CoreSim::Vdn::Bool RM05G_FCU_OWN_AP_SW_MON;
		CoreSim::Vdn::Bool RM03D_FCU_OWN_ATH_SW_MON;

		CoreSim::Vdn::Bool RM06E_AC_TYPE_3;
		CoreSim::Vdn::Bool RM06C_ENG_TYPE_5;
		CoreSim::Vdn::Bool RM07C_PP_PARITY_2G;
		CoreSim::Vdn::Bool LM05D_NAV_BACKUP_SELECTED;
};

class InputsA429_ADCOWN
{
   public:
      //CoreSim::Vdn::A429Receiver       ADC_bus;

      //  Baro Correction Hpa #1 (HPa) A429
      CoreSim::Vdn::A429Label           ADR_Label234;
      //  Baro Correction HB #1 (in.Hg) A429
      CoreSim::Vdn::A429Label           ADR_Label235;
      //  Baro Correction Hpa #2 (HPa) A429
      CoreSim::Vdn::A429Label           ADR_Label236;
      //  Baro Correction HB #2 (in.Hg) A429
      CoreSim::Vdn::A429Label           ADR_Label237;
      //  PRESS ALT - Standard Altitude (feet) IEEE
      CoreSim::Vdn::A429Label           ADR_Label203;
      //  BARO ALT - Baro Correction Altitude #1 (feet) IEEE
      CoreSim::Vdn::A429Label           ADR_Label204;   //.Note - FOR BARO ALTITUDE FMGC 1 considers Label 204 FMGC 2 considers Label 220
      //  MACH - Mach IEEE
      CoreSim::Vdn::A429Label           ADR_Label205;
      //  Computed Airspeed (CAS) (knots) IEEE
      CoreSim::Vdn::A429Label           ADR_Label206;
      //  True Aispeed (TAS) (knots) IEEE
      CoreSim::Vdn::A429Label           ADR_Label210;
      //  Total Air Temperature (TAT) (deg C) IEEE
      CoreSim::Vdn::A429Label           ADR_Label211;
      //  Static Air Temperature (deg C) IEEE
      CoreSim::Vdn::A429Label           ADR_Label213;
      //ADC FG VAL - BOOL
      //CoreSim::Vdn::Bool                ADC_FG_VAL;
      //ADC SOURCE - ENUM 0=own,1=#3
      //CoreSim::Vdn::Int64               ADC_Source;

      /*added according to ICD,not included in CM*/
      //BARO ALT - Baro Correction Altitude #2 (feet) IEEE
      CoreSim::Vdn::A429Label           ADR_Label220;  //.Note - FOR BARO ALTITUDE FMGC 1 considers Label 204 FMGC 2 considers Label 220
      //TOTAL PRESSURE
      CoreSim::Vdn::A429Label           ADR_Label242;
      //AVERAGE STATIC PRESSURE CORRECTED
      CoreSim::Vdn::A429Label           ADR_Label246;
};

class InputsA429_ADCOPP
{
   public:
      //CoreSim::Vdn::A429Receiver       ADC_bus;
      //  PRESS ALT - Standard Altitude (feet) IEEE
      CoreSim::Vdn::A429Label           ADR_Label203;
      //  Computed Airspeed (CAS) (knots) IEEE
      CoreSim::Vdn::A429Label           ADR_Label206;
      //  True Aispeed (TAS) (knots) IEEE
      CoreSim::Vdn::A429Label           ADR_Label210;
      //  Total Air Temperature (TAT) (deg C) IEEE
      CoreSim::Vdn::A429Label           ADR_Label211;
      //ADC FG VAL - BOOL
      //CoreSim::Vdn::Bool                ADC_FG_VAL;
      //ADC SOURCE - ENUM 0=own,1=#3
      //CoreSim::Vdn::Int64               ADC_Source;
      /*added according to ICD,not included in CM*/
      //CORRECTED ANGLE OF ATTACK
      CoreSim::Vdn::A429Label           ADR_Label241;
};

class InputsA429_ADC3
{
   public:
      //  Baro Correction Hpa #1 (HPa) A429
      CoreSim::Vdn::A429Label           ADR_Label234;
      //  Baro Correction HB #1 (in.Hg) A429
      CoreSim::Vdn::A429Label           ADR_Label235;
      //  Baro Correction Hpa #2 (HPa) A429
      CoreSim::Vdn::A429Label           ADR_Label236;
      //  Baro Correction HB #2 (in.Hg) A429
      CoreSim::Vdn::A429Label           ADR_Label237;
      //  PRESS ALT - Standard Altitude (feet) IEEE
      CoreSim::Vdn::A429Label           ADR_Label203;
      //  BARO ALT - Baro Correction Altitude #1 (feet) IEEE
      CoreSim::Vdn::A429Label           ADR_Label204;
      //  MACH - Mach IEEE
      CoreSim::Vdn::A429Label           ADR_Label205;
      //  Computed Airspeed (CAS) (knots) IEEE
      CoreSim::Vdn::A429Label           ADR_Label206;
      //  True Aispeed (TAS) (knots) IEEE
      CoreSim::Vdn::A429Label           ADR_Label210;
      //  Total Air Temperature (TAT) (deg C) IEEE
      CoreSim::Vdn::A429Label           ADR_Label211;
      //  Static Air Temperature (deg C) IEEE
      CoreSim::Vdn::A429Label           ADR_Label213;
      //ADC FG VAL - BOOL
      //CoreSim::Vdn::Bool                ADC_FG_VAL;
      //ADC SOURCE - ENUM 0=own,1=#3
      //CoreSim::Vdn::Int64               ADC_Source;
      /*added according to ICD,not included in CM*/
      //BARO ALT - Baro Correction Altitude #2 (feet) IEEE
      CoreSim::Vdn::A429Label           ADR_Label220;  
      //TOTAL PRESSURE
      CoreSim::Vdn::A429Label           ADR_Label242;
      //AVERAGE STATIC PRESSURE CORRECTED
      CoreSim::Vdn::A429Label           ADR_Label246;
};

class InputsA429_FacOwn
{
public:
   //CoreSim::Vdn::A429Receiver        FAC_bus;
   //MAX ALLOWABLE AIRSPEED (VMAXOP) (KTS) A429
   CoreSim::Vdn::A429Label           FAC_Label266;
   //VLS MINIMUM SPEED (VMIN)  (KTS) A429
   CoreSim::Vdn::A429Label           FAC_Label245;
   // MANEUVERING SPD VMAN (VMANVR) 
   CoreSim::Vdn::A429Label           FAC_Label265;
   //MIN SPD FOR FLAPS RETRACTION V3 (KTS) A429
   CoreSim::Vdn::A429Label           FAC_Label263;
   //MIN SPD FOR SLATS RETRACTION V4 (KTS) A429
   CoreSim::Vdn::A429Label           FAC_Label264;
   //WEIGHT (LB) A429
   CoreSim::Vdn::A429Label           FAC_Label074;
   //CENTERANGE (%MAC) A429
   CoreSim::Vdn::A429Label           FAC_Label076;
   //FPA AERODINAMIC (GAMMA A)  A429
   CoreSim::Vdn::A429Label           FAC_Label070;
   //FPA TOTALE (GAMMA T)
   CoreSim::Vdn::A429Label           FAC_Label071;
   //FAC DIS WD 1
   CoreSim::Vdn::A429Label           FAC_Label146;
   //FAC DIS WD 2
   CoreSim::Vdn::A429Label           FAC_Label274;
   //BOOL  FAC_FG_VAL
   //CoreSim::Vdn::Bool           FAC_FG_VAL;  
   //ENUM 0=OWN,1=OPP  FAC_OPP_Select
   //CoreSim::Vdn::Int64               FAC_Label206;  //TODO: check with fenglin.and other SEs.
   //VMAX (KTS) A429
   CoreSim::Vdn::A429Label           FAC_Label207;
   //PREDICTIVE MANEUVER SPEED VFEN (KTS) A429
   CoreSim::Vdn::A429Label           FAC_Label267;
   /*added according to ICD. not included in CM*/
   //D (gt-ga) Vc
   CoreSim::Vdn::A429Label           FAC_Label072;
   //D(gt-ga) M
   CoreSim::Vdn::A429Label           FAC_Label073;
   //GMT
   CoreSim::Vdn::A429Label           FAC_Label125;
   //DATE
   CoreSim::Vdn::A429Label           FAC_Label260;
   //DISCRETE WORD 3
   CoreSim::Vdn::A429Label           FAC_Label271;
   //DISCRETE WORD 4
   CoreSim::Vdn::A429Label           FAC_Label272;
   //RUDDER TRIM POSITION
   CoreSim::Vdn::A429Label           FAC_Label313;
   //FIDS COMMAND
   CoreSim::Vdn::A429Label           FAC_Label352;
};

class InputsA429_FacOpp
{
public:
   //CoreSim::Vdn::A429Receiver        FAC_bus;
   //MAX ALLOWABLE AIRSPEED (VMAXOP) (KTS) A429
   CoreSim::Vdn::A429Label           FAC_Label266;
   //VLS MINIMUM SPEED (VMIN)  (KTS) A429
   CoreSim::Vdn::A429Label           FAC_Label245;
   // MANEUVERING SPD VMAN (VMANVR) 
   CoreSim::Vdn::A429Label           FAC_Label265;
   //MIN SPD FOR FLAPS RETRACTION V3 (KTS) A429
   CoreSim::Vdn::A429Label           FAC_Label263;
   //MIN SPD FOR SLATS RETRACTION V4 (KTS) A429
   CoreSim::Vdn::A429Label           FAC_Label264;
   //WEIGHT (LB) A429
   CoreSim::Vdn::A429Label           FAC_Label074;
   //CENTERANGE (%MAC) A429
   CoreSim::Vdn::A429Label           FAC_Label076;
   //FPA AERODINAMIC (GAMMA A)  A429
   CoreSim::Vdn::A429Label           FAC_Label070;
   //FPA TOTALE (GAMMA T)
   CoreSim::Vdn::A429Label           FAC_Label071;
   //FAC DIS WD 1
   CoreSim::Vdn::A429Label           FAC_Label146;
   //FAC DIS WD 2
   CoreSim::Vdn::A429Label           FAC_Label274;
   //BOOL
   //CoreSim::Vdn::Bool                FAC_FG_VAL;
   //ENUM 0=OWN,1=OPP
   //CoreSim::Vdn::Int64               FAC_OPP_Select;
   //VMAX (KTS) A429
   CoreSim::Vdn::A429Label           FAC_Label207;
   //PREDICTIVE MANEUVER SPEED VFEN (KTS) A429
   CoreSim::Vdn::A429Label           FAC_Label267;
      /*added according to ICD. not included in CM*/
   //D (gt-ga) Vc
   CoreSim::Vdn::A429Label           FAC_Label072;
   //D(gt-ga) M
   CoreSim::Vdn::A429Label           FAC_Label073;
   //GMT
   CoreSim::Vdn::A429Label           FAC_Label125;
   //DATE
   CoreSim::Vdn::A429Label           FAC_Label260;
   //DISCRETE WORD 3
   CoreSim::Vdn::A429Label           FAC_Label271;
   //DISCRETE WORD 4
   CoreSim::Vdn::A429Label           FAC_Label272;
   //RUDDER TRIM POSITION
   CoreSim::Vdn::A429Label           FAC_Label313;
   //FIDS COMMAND
   CoreSim::Vdn::A429Label           FAC_Label352;
};

class InputsA429_FADOPP
{
public:
   CoreSim::Vdn::A429Receiver       FADOPP_bus;
   //SELECTED TLA
   CoreSim::Vdn::A429Label          Label_133;
   //FUEL FLOW
   CoreSim::Vdn::A429Label          Label_244;
   //FADOPPFGVAL
   //CoreSim::Vdn::Bool               FADOPP_FG_VAL;
   //CoreSim::Vdn::A429Label          Label_001;
   
   //N1 LIMIT
   CoreSim::Vdn::A429Label          Label_337;
   //N1 CMD
   CoreSim::Vdn::A429Label          Label_341;
   //FAD DIS WD X
   CoreSim::Vdn::A429Label          Label_271;//FAD_DIS_WD_X
   //CFM Only
   //N1 ACT
   CoreSim::Vdn::A429Label          Label_346;
   
   //PW ONLY  //IAE ONLY
   //N1 ACT
   CoreSim::Vdn::A429Label          Label_340;
};

class InputsA429_FCUOWNA
{
public:
  //CoreSim::Vdn::A429Receiver       FCU_bus;     
  //SEL V/S (FT/MN_
  CoreSim::Vdn::A429Label           Label_104;  
  //SEL AIRSPEED (KTS) 
  CoreSim::Vdn::A429Label           Label_103; 
  //SEL MACH (MACH) 
  CoreSim::Vdn::A429Label           Label_106; 
  //FCUEIS DIS 1 
  CoreSim::Vdn::A429Label           Label_271; 
  //FCUEIS DIS 2 
  CoreSim::Vdn::A429Label           Label_272;
  //FCU DIS 1 
  CoreSim::Vdn::A429Label           Label_274;
  //FCU DIS 2 
  CoreSim::Vdn::A429Label           Label_273; 
  /*added according to ICD. not included in CM*/
  //FCU FLEX TO TEMP 
  CoreSim::Vdn::A429Label           Label_214;
  //FCU ATS DW 
  CoreSim::Vdn::A429Label           Label_270;
  //FCU N1 TARGET COM
  CoreSim::Vdn::A429Label           Label_343;
  //MAINTENANCE WORD 1
  CoreSim::Vdn::A429Label           Label_350;
  //MAINTENANCE WORD 2
  CoreSim::Vdn::A429Label           Label_351;
  //MAINTENANCE WORD 3
  CoreSim::Vdn::A429Label           Label_352;
  //MAINTENANCE WORD 4
  CoreSim::Vdn::A429Label           Label_353;
  //MAINTENANCE WORD 
  CoreSim::Vdn::A429Label           Label_354;

  //SEL MACH VAL  //SEL A/S V
  CoreSim::Vdn::A429Label           Label_010;
  //SEL V/S VA 
  //CoreSim::Vdn::A429Label           Label_007;
  //SEL A/S V
  //CoreSim::Vdn::A429Label           Label_003;
  //SEL MACH VAL
  //CoreSim::Vdn::A429Label           Label_005;  
  //FCUFGVAL
  //CoreSim::Vdn::A429Label           Label_001;   
  // SEL FPA VAL
  //CoreSim::Vdn::A429Label           Label_002;       
};

class InputsA429_FCUOWNB
{
public:
   //SEL HDG (DEG)
  CoreSim::Vdn::A429Label           Label_101; 
  //SEL ALT (FT)
  CoreSim::Vdn::A429Label           Label_102;     
  //FCUEIS DIS 1 
  CoreSim::Vdn::A429Label           Label_271; 
  //FCUEIS DIS 2 
  CoreSim::Vdn::A429Label           Label_272;  
  //SEL FPA (DEG)
  CoreSim::Vdn::A429Label           Label_115;    
  //SEL TRK (DEG)
  CoreSim::Vdn::A429Label           Label_114;    
};

class InputsA429_FQI
{
public:
   //CoreSim::Vdn::A429Receiver       FQI_bus;
   //FUEL QUANT (LBS) 
   CoreSim::Vdn::A429Label          Label_247;               
   //FUEL QTY left outer tank  (LBS)    M  SEE NOTE 22 
   CoreSim::Vdn::A429Label          Label_256;
   //FUEL QTY left inner tank  (LBS)    M  SEE NOTE 22 
   CoreSim::Vdn::A429Label          Label_257;
   //FUEL QTY center tank      (LBS)    M  SEE NOTE 22 
   CoreSim::Vdn::A429Label          Label_260;
   //FUEL QTY right outer tank (LBS)    M  SEE NOTE 22 
   CoreSim::Vdn::A429Label          Label_261;
   //FUEL QTY right inner tank (LBS)    M  SEE NOTE 22 
   CoreSim::Vdn::A429Label          Label_262;
   //FUEL QTY ACT 1            (LBS)    M  SEE NOTE 22 
   CoreSim::Vdn::A429Label          Label_263;
   //FUEL QTY ACT 2            (LBS)    M  SEE NOTE 22 
   CoreSim::Vdn::A429Label          Label_264; 
   //FUEL QTY ACT 3            (LBS)    M  SEE NOTE 22 
   CoreSim::Vdn::A429Label          Label_265;
   //FUEL QTY ACT 4            (LBS)    M  SEE NOTE 22 
   CoreSim::Vdn::A429Label          Label_310; 
   //FUEL QTY ACT 5            (LBS)    M  SEE NOTE 22 
   CoreSim::Vdn::A429Label          Label_311;
   //FUEL QTY TOTAL ACT        (LBS)    M  SEE NOTE 22 
   CoreSim::Vdn::A429Label          Label_312;
   //FUEL QTY ACT 6            (LBS)    M  SEE NOTE 22                    
   CoreSim::Vdn::A429Label          Label_313;
};

class InputsA429_LSOWN
{
public:
   //CoreSim::Vdn::A429Receiver     LS_bus;
   //LOC DEV DDM       A429 
   CoreSim::Vdn::A429Label        Label_173;    
   //LS FREQ MHZ       A429
   CoreSim::Vdn::A429Label        Label_033;    
   //LS ID1            A429 
   CoreSim::Vdn::A429Label        Label_263;    
   //LS ID2            A429
   CoreSim::Vdn::A429Label        Label_264;     
   //MMRFGVAL(previously ILSFGVAL)      
   //CoreSim::Vdn::A429Label        Label_200;        
   //MMROPPSELECT(previously ILSOPPSELECT)   ENUM 
   //CoreSim::Vdn::A429Label        Label_100;    
   //LS RWY HDG         A429 
   CoreSim::Vdn::A429Label        Label_105;    
   //LOC DEV NOT NCD   BOOL 
   //CoreSim::Vdn::A429Label        Label_300;
   //LS FREQ NOT NCD   BOOL 
   //CoreSim::Vdn::A429Label        Label_040;
   //LS RWY HDG NOT NCD    BOOL 
   //CoreSim::Vdn::A429Label        Label_240;
   //BILSINST          BOOL 
   //CoreSim::Vdn::A429Label        Label_   ;
    //BMLSINST         BOOL 
   //CoreSim::Vdn::A429Label        Label_340;
   //BGLSINST          BOOL 
   //CoreSim::Vdn::A429Label        Label_050 ;
   //BFMLSINH          BOOL 
   //CoreSim::Vdn::A429Label        Label_   ;
   //GLIDE SLOPE DEV (DDM)   A429 .
   CoreSim::Vdn::A429Label        Label_174;    //Parameter not acquired by HI.This data is not acquired on FG side for the moment.
   //GLIDE SLOPE DEV NOT NCD   BOOL             //Parameter not acquired by HI.This data is not acquired on FG side for the moment. 
   /*added according to ICD. not included in CM.*/
   //MMR DISCRETE WORD
   CoreSim::Vdn::A429Label        Label_271;
};

class InputsA429_LSOPP
{
public:
   //CoreSim::Vdn::A429Receiver     LS_bus;
   //LOC DEV DDM       A429 
   CoreSim::Vdn::A429Label        Label_173;    
   //LS FREQ MHZ       A429
   CoreSim::Vdn::A429Label        Label_033;    
   //LS ID1            A429 
   CoreSim::Vdn::A429Label        Label_263;    
   //LS ID2            A429
   CoreSim::Vdn::A429Label        Label_264;     
   //MMRFGVAL(previously ILSFGVAL)      
   //CoreSim::Vdn::A429Label        Label_200;        
   //MMROPPSELECT(previously ILSOPPSELECT)   ENUM 
   //CoreSim::Vdn::A429Label        Label_100;    
   //LS RWY HDG         A429 
   CoreSim::Vdn::A429Label        Label_105;    
   //LOC DEV NOT NCD   BOOL 
   //CoreSim::Vdn::A429Label        Label_300;
   //LS FREQ NOT NCD   BOOL 
   //CoreSim::Vdn::A429Label        Label_040;
   //LS RWY HDG NOT NCD    BOOL 
   CoreSim::Vdn::A429Label        Label_240;
   //BILSINST          BOOL 
   //CoreSim::Vdn::A429Label        Label_   ;
    //BMLSINST         BOOL 
   //CoreSim::Vdn::A429Label        Label_340;
   //BGLSINST          BOOL 
   //CoreSim::Vdn::A429Label        Label_050 ;
   //BFMLSINH          BOOL 
   //CoreSim::Vdn::A429Label        Label_   ;
   //GLIDE SLOPE DEV (DDM)   A429 .
   CoreSim::Vdn::A429Label        Label_174;    //Parameter not acquired by HI.This data is not acquired on FG side for the moment.
   //GLIDE SLOPE DEV NOT NCD   BOOL             //Parameter not acquired by HI.This data is not acquired on FG side for the moment. 
   /*added according to ICD. not included in CM.*/
   //MMR DISCRETE WORD
   CoreSim::Vdn::A429Label        Label_271;
};

class InputsA429_MLS
{
public:
   CoreSim::Vdn::A429Receiver MLS_bus;
   //CoreSim::Vdn::A429Label        Label_310; //FG-RMPOWN MLS SEL       BOOL  1400 Parameter not acquired by HI (TBD)
   //CoreSim::Vdn::A429Label        Label_150; //FG-RMPOPP MLS SEL       BOOL  1404 Parameter not acquired by HI (TBD)
   CoreSim::Vdn::A429Label        Label_036; //MLS-MLS CHANNEL         A429  1408 Parameter not acquired by HI (TBD)
   //CoreSim::Vdn::A429Label        Label_XXX; //MLS-MLS RUNWAY HEADING  A429  1410 Parameter not acquired by HI (TBD)
   CoreSim::Vdn::A429Label        Label_173; //MLS-MLS LOC DEV         A429  1418 Parameter not acquired by HI (TBD)   
   //CoreSim::Vdn::A429Label        Label_XXX; //MLS-MLS ID 1            A429  1420 Parameter not acquired by HI (TBD)
   //CoreSim::Vdn::A429Label        Label_XXX; //MLS-MLS ID 2            A429  1428 Parameter not acquired by HI (TBD) 
   //CoreSim::Vdn::A429Label        Label_XXX; //MLS-MLSFGVAL            BOOL  1430 Parameter not acquired by HI (TBD)
   //CoreSim::Vdn::A429Label        Label_XXX; //MLS-OPPSEL              BOOL  1434 Parameter not acquired by HI (TBD)
   //CoreSim::Vdn::A429Label        Label_XXX; //MLS-LOC DEV NCD         BOOL  1438 Parameter not acquired by HI (TBD)
};

class InputsA429_IRS
{
public:
   //CoreSim::Vdn::A429Receiver IRS_bus;
   CoreSim::Vdn::A429Label        Label_314;  //TRUE HDG (DEG)       314 A429
   CoreSim::Vdn::A429Label        Label_323;  //FP ACCEL (g  )       323 A429
   CoreSim::Vdn::A429Label        Label_322;  //FP ANGLE (DEG)       322 IEEE
   CoreSim::Vdn::A429Label        Label_324;  //PITCH ANGLE (DEG)    324 IEEE
   CoreSim::Vdn::A429Label        Label_325;  //ROLL ANGLE (DEG)     325 IEEE
   CoreSim::Vdn::A429Label        Label_364;  //VERT ACCEL (g )      364 IEEE
   CoreSim::Vdn::A429Label        Label_330;  //BODY YAWRATE (DEG/S) 330 IEEE  Parameter not acquired by HI FM
   CoreSim::Vdn::A429Label        Label_361;  //INERTIAL ALT (FT )   361 IEEE
   CoreSim::Vdn::A429Label        Label_365;  //INERTIAL V/S (FT/MN) 365 IEEE
   CoreSim::Vdn::A429Label        Label_320;  //MAG HDG (DEG)        320 IEEE
   //CoreSim::Vdn::Bool             MAG_HDG_VAL;  //MAGHDGVAL              BOOL
   //CoreSim::Vdn::Bool             IRS_FG_VAL;   //IRSFGVAL               BOOL
   //CoreSim::Vdn::Bool             IRS_SOURCE;   //IRSSOURCE              ENUM   0=IRS OWN SELECTED,1=IRS3 SELECTED
};

class InputsA429_IRSOWN
{
public:
   //CoreSim::Vdn::A429Receiver     IRSOWN_bus;
   CoreSim::Vdn::A429Label        Label_175;    //HYB GROUND SPD          
   CoreSim::Vdn::A429Label        Label_247;    //GPS HORIZ FOM           
   CoreSim::Vdn::A429Label        Label_254;    //HYB PRES POS-LAT        
   CoreSim::Vdn::A429Label        Label_255;    //HYB PRES POS-LON        
   CoreSim::Vdn::A429Label        Label_264;    //HYB HORIZ FOM           
   CoreSim::Vdn::A429Label        Label_266;    //HYB N-S VEL             
   CoreSim::Vdn::A429Label        Label_267;    //HYB E-W VEL             
   CoreSim::Vdn::A429Label        Label_273;    //GPS SENSOR STATUS       
   CoreSim::Vdn::A429Label        Label_274;    //GPADIRS STATUS          
   CoreSim::Vdn::A429Label        Label_310;    //PPOS LAT OWN            
   CoreSim::Vdn::A429Label        Label_311;    //PPOS LON OWN            
   CoreSim::Vdn::A429Label        Label_366;    //N/S VEL OWN             
   CoreSim::Vdn::A429Label        Label_367;    //E/W VEL OWN             
   CoreSim::Vdn::A429Label        Label_270;    //IRS OWN DIS             
   CoreSim::Vdn::A429Label        Label_041;    //IRSOWNSETLAT            
   CoreSim::Vdn::A429Label        Label_042;    //IRSOWNSETLON            
   CoreSim::Vdn::A429Label        Label_103;    //GPS TRK ANGLE TRUE      
   CoreSim::Vdn::A429Label        Label_110;    //GPS PRES POS-LAT        
   CoreSim::Vdn::A429Label        Label_111;    //GPR PRES POS-LON        
   CoreSim::Vdn::A429Label        Label_112;    //GPS GROUND SPD          
   CoreSim::Vdn::A429Label        Label_137;    //HYB TRK ANGLE TRUE      
   CoreSim::Vdn::A429Label        Label_076;    //GPS ALTITUDE-AMSL       
   CoreSim::Vdn::A429Label        Label_130;    //GPS HORIZ INTEG LIMIT   
   CoreSim::Vdn::A429Label        Label_131;    //HYBRID HORIZ INTG LIMIT 
   CoreSim::Vdn::A429Label        Label_150;    //GPS UTC                 
   CoreSim::Vdn::A429Label        Label_162;    //PRED DEST ETA ECHO      
   CoreSim::Vdn::A429Label        Label_163;    //PRED WPT ETA ECHO       
   CoreSim::Vdn::A429Label        Label_256;    //HYBRID LAT FRACTIONS    
   CoreSim::Vdn::A429Label        Label_257;    //HYBRID LONG FRACTIONS   
   CoreSim::Vdn::A429Label        Label_260;    //GPS DATE                
   CoreSim::Vdn::A429Label        Label_261;    //HYBRID ALTITUDE         
   CoreSim::Vdn::A429Label        Label_343;    //PRED DET HIL            
   CoreSim::Vdn::A429Label        Label_347;    //PRED WPT HIL  
   //par FG
   CoreSim::Vdn::A429Label        Label_314;  //TRUE HDG (DEG)       314 A429
   CoreSim::Vdn::A429Label        Label_323;  //FP ACCEL (g  )       323 A429
   CoreSim::Vdn::A429Label        Label_322;  //FP ANGLE (DEG)       322 IEEE
   CoreSim::Vdn::A429Label        Label_324;  //PITCH ANGLE (DEG)    324 IEEE
   CoreSim::Vdn::A429Label        Label_325;  //ROLL ANGLE (DEG)     325 IEEE
   CoreSim::Vdn::A429Label        Label_364;  //VERT ACCEL (g )      364 IEEE
   CoreSim::Vdn::A429Label        Label_330;  //BODY YAWRATE (DEG/S) 330 IEEE  Parameter not acquired by HI FM
   CoreSim::Vdn::A429Label        Label_361;  //INERTIAL ALT (FT )   361 IEEE
   CoreSim::Vdn::A429Label        Label_365;  //INERTIAL V/S (FT/MN) 365 IEEE
   CoreSim::Vdn::A429Label        Label_320;  //MAG HDG (DEG)        320 IEEE
   //CoreSim::Vdn::Bool             MAG_HDG_VAL;  //MAGHDGVAL              BOOL
   //CoreSim::Vdn::Bool             IRS_FG_VAL;   //IRSFGVAL               BOOL
   //CoreSim::Vdn::Bool             IRS_SOURCE;   //IRSSOURCE              ENUM   0=IRS OWN SELECTED,1=IRS3 SELECTED
   CoreSim::Vdn::A429Label        Label_010;    //POS LAT D (BCD)          A429 C4 1100 FOR MCDU DISPLAY Parameter not acquired by HI FM APPLATB LAT
   CoreSim::Vdn::A429Label        Label_011;    //P POS LON D (BCD)        A429 C4 1108 FOR MCDU DISPLAY Parameter not acquired by HI FM APPLONB LAT
   CoreSim::Vdn::A429Label        Label_012;    //GROUND SPEED D (BCD)     A429 C4 1110 FOR MCDU DISPLAY AGDSPBC LAT
   CoreSim::Vdn::A429Label        Label_013;    //TRK ANGLE TRUE D (BCD)   A429 C4 1118 FOR MCDU DISPLAY ATTRKBC LAT
   CoreSim::Vdn::A429Label        Label_014;    //MAGNETIC HDG D (BCD)     A429 C4 1120 FOR MCDU DISPLAY AMHDGBC LAT
   CoreSim::Vdn::A429Label        Label_015;    //WIND SPEED D (BCD)       A429 C4 1128 FOR MCDU DISPLAY AWSPDBC LAT
   CoreSim::Vdn::A429Label        Label_016;    //WIND DIR TRUE D (BCD)    A429 C4 1130 FOR MCDU DISPLAY  AWTDIRB LAT
   CoreSim::Vdn::A429Label        Label_043;    //MAG HEADING D (BCD)      A429 C3 1138 Set Heading Echo  ASMHDBC LAT
   CoreSim::Vdn::A429Label        Label_044;    //TRUE HEADING D (BCD)     A429 C4 1140 FOR MCDU DISPLAY  ATHDGBC LAT
   CoreSim::Vdn::A429Label        Label_312;    //GROUND SPEED             A429 C1 1148 Parameter not acquired by HI FM ASPDGOW TOUS
   CoreSim::Vdn::A429Label        Label_334;    //PLATFORM HEADING         A429 C1 1150 Parameter not acquired by HI FM APLTHDG TOUS
   CoreSim::Vdn::A429Label        Label_350;    //IRS STATUS WORD          A429 C4 1158 Used by IRS MON page  AIRS350 LAT
   CoreSim::Vdn::A429Label        Label_275;    //IRS DISCRETE 2           A429 C3 1160
   /*added according to ICD. not include in CM.*/
   CoreSim::Vdn::A429Label        Label_301;    //VERTICAL ACCEL NON FILT
   CoreSim::Vdn::A429Label        Label_313;    //TRUE TRACK
   CoreSim::Vdn::A429Label        Label_315;    //WIND SPEED
   CoreSim::Vdn::A429Label        Label_316;    //WIND DIREC TRUE
   CoreSim::Vdn::A429Label        Label_317;    //TRACK ANGLE MAG
   CoreSim::Vdn::A429Label        Label_332;    //BODY LATERAL ACCEL
   CoreSim::Vdn::A429Label        Label_336;    //INERT PITCH RATE
   CoreSim::Vdn::A429Label        Label_337;    //INERT ROLL RATE

};

class InputsA429_IRSOPP
{
public:
   //CoreSim::Vdn::A429Receiver     IRSOPP_bus;
   CoreSim::Vdn::A429Label        Label_310; //PPOS LAT OPP             A429
   CoreSim::Vdn::A429Label        Label_311; //PPOS LON OPP             A429
   CoreSim::Vdn::A429Label        Label_366; //N/S VEL OPP              A429
   CoreSim::Vdn::A429Label        Label_367; //E/W VEL OPP              A429
   CoreSim::Vdn::A429Label        Label_270; //IRS OPP DIS              A429
   CoreSim::Vdn::A429Label        Label_041; //IRSOPPSETLAT             A429
   CoreSim::Vdn::A429Label        Label_042; //IRSOPPSETLON             A429
   CoreSim::Vdn::A429Label        Label_103; //GPS TRK ANGLE TRUE       A429
   CoreSim::Vdn::A429Label        Label_110; //GPS PRES POS-LAT         A429
   CoreSim::Vdn::A429Label        Label_111; //GPS PRES POS-LON         A429
   CoreSim::Vdn::A429Label        Label_112; //GPS GROUND SPD           A429
   CoreSim::Vdn::A429Label        Label_137; //HYB TRK ANGLE TRUE       A429
   CoreSim::Vdn::A429Label        Label_175; //HYB GROUND SPD           A429
   CoreSim::Vdn::A429Label        Label_247; //GPS HORIZ FOM            A429
   CoreSim::Vdn::A429Label        Label_254; //HYB PRES POS-LAT         A429
   CoreSim::Vdn::A429Label        Label_255; //HYB PRES POS-LON         A429
   CoreSim::Vdn::A429Label        Label_264; //HYB HORIZ FOM            A429
   CoreSim::Vdn::A429Label        Label_266; //HYB N-S VEL              A429
   CoreSim::Vdn::A429Label        Label_267; //HYB E-W VEL              A429
   CoreSim::Vdn::A429Label        Label_273; //GPS SENSOR STATUS        A429
   CoreSim::Vdn::A429Label        Label_274; //GPADIRS STATUS           A429
   CoreSim::Vdn::A429Label        Label_076; //GPS ALTITUDE-AMSL        A429
   CoreSim::Vdn::A429Label        Label_130; //GPS HORIZ INTEG LIMIT    A429
   CoreSim::Vdn::A429Label        Label_131; //HYBRID HORIZ INTG LIMIT  A429
   CoreSim::Vdn::A429Label        Label_150; //GPS UTC                  A429
   CoreSim::Vdn::A429Label        Label_162; //PRED DEST ETA ECHO       A429
   CoreSim::Vdn::A429Label        Label_163; //PRED WPT ETA ECHO        A429
   CoreSim::Vdn::A429Label        Label_256; //HYBRID LAT FRACTIONS     A429
   CoreSim::Vdn::A429Label        Label_257; //HYBRID LONG FRACTIONS    A429
   CoreSim::Vdn::A429Label        Label_260; //GPS DATE                 A429
   CoreSim::Vdn::A429Label        Label_261; //HYBRID ALTITUDE          A429
   CoreSim::Vdn::A429Label        Label_343; //PRED DET HIL             A429
   CoreSim::Vdn::A429Label        Label_347; //PRED WPT HIL             A429  
   //par FG
   CoreSim::Vdn::A429Label        Label_314;  //TRUE HDG (DEG)       314 A429
   //CoreSim::Vdn::A429Label        Label_323;  //FP ACCEL (g  )       323 A429
   //CoreSim::Vdn::A429Label        Label_322;  //FP ANGLE (DEG)       322 IEEE
   CoreSim::Vdn::A429Label        Label_324;  //PITCH ANGLE (DEG)    324 IEEE
   CoreSim::Vdn::A429Label        Label_325;  //ROLL ANGLE (DEG)     325 IEEE
   CoreSim::Vdn::A429Label        Label_364;  //VERT ACCEL (g )      364 IEEE
   CoreSim::Vdn::A429Label        Label_330;  //BODY YAWRATE (DEG/S) 330 IEEE  Parameter not acquired by HI FM
   CoreSim::Vdn::A429Label        Label_361;  //INERTIAL ALT (FT )   361 IEEE
   CoreSim::Vdn::A429Label        Label_365;  //INERTIAL V/S (FT/MN) 365 IEEE
   CoreSim::Vdn::A429Label        Label_320;  //MAG HDG (DEG)        320 IEEE
   //CoreSim::Vdn::Bool             MAG_HDG_VAL;  //MAGHDGVAL              BOOL
   //CoreSim::Vdn::Bool             IRS_FG_VAL;   //IRSFGVAL               BOOL
   //CoreSim::Vdn::Bool             IRS_SOURCE;   //IRSSOURCE              ENUM   0=IRS OWN SELECTED,1=IRS3 SELECTED
   CoreSim::Vdn::A429Label        Label_010;    //POS LAT D (BCD)          A429 C4 1100 FOR MCDU DISPLAY Parameter not acquired by HI FM APPLATB LAT
   CoreSim::Vdn::A429Label        Label_011;    //P POS LON D (BCD)        A429 C4 1108 FOR MCDU DISPLAY Parameter not acquired by HI FM APPLONB LAT
   CoreSim::Vdn::A429Label        Label_012;    //GROUND SPEED D (BCD)     A429 C4 1110 FOR MCDU DISPLAY AGDSPBC LAT
   CoreSim::Vdn::A429Label        Label_013;    //TRK ANGLE TRUE D (BCD)   A429 C4 1118 FOR MCDU DISPLAY ATTRKBC LAT
   CoreSim::Vdn::A429Label        Label_014;    //MAGNETIC HDG D (BCD)     A429 C4 1120 FOR MCDU DISPLAY AMHDGBC LAT
   CoreSim::Vdn::A429Label        Label_015;    //WIND SPEED D (BCD)       A429 C4 1128 FOR MCDU DISPLAY AWSPDBC LAT
   CoreSim::Vdn::A429Label        Label_016;    //WIND DIR TRUE D (BCD)    A429 C4 1130 FOR MCDU DISPLAY  AWTDIRB LAT
   CoreSim::Vdn::A429Label        Label_043;    //MAG HEADING D (BCD)      A429 C3 1138 Set Heading Echo  ASMHDBC LAT
   CoreSim::Vdn::A429Label        Label_044;    //TRUE HEADING D (BCD)     A429 C4 1140 FOR MCDU DISPLAY  ATHDGBC LAT
   CoreSim::Vdn::A429Label        Label_312;    //GROUND SPEED             A429 C1 1148 Parameter not acquired by HI FM ASPDGOW TOUS
   CoreSim::Vdn::A429Label        Label_334;    //PLATFORM HEADING         A429 C1 1150 Parameter not acquired by HI FM APLTHDG TOUS
   CoreSim::Vdn::A429Label        Label_350;    //IRS STATUS WORD          A429 C4 1158 Used by IRS MON page  AIRS350 LAT
   CoreSim::Vdn::A429Label        Label_275;    //IRS DISCRETE 2           A429 C3 1160
   /*added according to ICD. not include in CM.*/
   CoreSim::Vdn::A429Label        Label_301;    //VERTICAL ACCEL NON FILT
   CoreSim::Vdn::A429Label        Label_313;    //TRUE TRACK
   CoreSim::Vdn::A429Label        Label_317;    //TRACK ANGLE MAG
   CoreSim::Vdn::A429Label        Label_332;    //BODY LATERAL ACCEL
   CoreSim::Vdn::A429Label        Label_336;    //INERT PITCH RATE
   CoreSim::Vdn::A429Label        Label_337;    //INERT ROLL RATE
};

class InputsA429_IRS3
{
public:
   //CoreSim::Vdn::A429Receiver     IRS3_bus;
   CoreSim::Vdn::A429Label        Label_310;  // A429 PPOS LAT 3              
   CoreSim::Vdn::A429Label        Label_311;  // A429 PPOS LON 3              
   CoreSim::Vdn::A429Label        Label_366;  // A429 N/S VEL 3               
   CoreSim::Vdn::A429Label        Label_367;  // A429 E/W VEL 3               
   CoreSim::Vdn::A429Label        Label_270;  // A429 IRS 3 DIS               
   CoreSim::Vdn::A429Label        Label_041;  // A429 IRS3SETLAT              
   CoreSim::Vdn::A429Label        Label_042;  // A429 IRS3SETLON              
   CoreSim::Vdn::A429Label        Label_103;  // A429 GPS TRK ANGLE TRUE      
   CoreSim::Vdn::A429Label        Label_110;  // A429 GPS PRES POS-LAT        
   CoreSim::Vdn::A429Label        Label_111;  // A429 GPS PRES POS-LON        
   CoreSim::Vdn::A429Label        Label_112;  // A429 GPS GROUND SPD          
   CoreSim::Vdn::A429Label        Label_137;  // A429 HYB TRK ANGLE TRUE      
   CoreSim::Vdn::A429Label        Label_175;  // A429 HYB GROUND SPD          
   CoreSim::Vdn::A429Label        Label_247;  // A429 GPS HORIZ FOM           
   CoreSim::Vdn::A429Label        Label_254;  // A429 HYB PRES POS-LAT        
   CoreSim::Vdn::A429Label        Label_255;  // A429 HYB PRES POS-LON        
   CoreSim::Vdn::A429Label        Label_264;  // A429 HYB HORIZ FOM           
   CoreSim::Vdn::A429Label        Label_266;  // A429 HYB N-S VEL             
   CoreSim::Vdn::A429Label        Label_267;  // A429 HYB E-W VEL             
   CoreSim::Vdn::A429Label        Label_273;  // A429 GPS SENSOR STATUS       
   CoreSim::Vdn::A429Label        Label_274;  // A429 GPADIRS STATUS          
   CoreSim::Vdn::A429Label        Label_076;  // A429 GPS ALTITUDE-AMSL       
   CoreSim::Vdn::A429Label        Label_130;  // A429 GPS HORIZ INTEG LIMIT   
   CoreSim::Vdn::A429Label        Label_131;  // A429 HYBRID HORIZ INTG LIMIT 
   CoreSim::Vdn::A429Label        Label_150;  // A429 GPS UTC                 
   CoreSim::Vdn::A429Label        Label_162;  // A429 PRED DEST ETA ECHO      
   CoreSim::Vdn::A429Label        Label_163;  // A429 PRED WPT ETA ECHO       
   CoreSim::Vdn::A429Label        Label_256;  // A429 HYBRID LAT FRACTIONS    
   CoreSim::Vdn::A429Label        Label_257;  // A429 HYBRID LONG FRACTIONS   
   CoreSim::Vdn::A429Label        Label_260;  // A429 GPS DATE                
   CoreSim::Vdn::A429Label        Label_261;  // A429 HYBRID ALTITUDE         
   CoreSim::Vdn::A429Label        Label_343;  // A429 PRED DET HIL            
   CoreSim::Vdn::A429Label        Label_347;  // A429 PRED WPT HIL 
   //par FG
   CoreSim::Vdn::A429Label        Label_314;  //TRUE HDG (DEG)       314 A429
   CoreSim::Vdn::A429Label        Label_323;  //FP ACCEL (g  )       323 A429
   CoreSim::Vdn::A429Label        Label_322;  //FP ANGLE (DEG)       322 IEEE
   CoreSim::Vdn::A429Label        Label_324;  //PITCH ANGLE (DEG)    324 IEEE
   CoreSim::Vdn::A429Label        Label_325;  //ROLL ANGLE (DEG)     325 IEEE
   CoreSim::Vdn::A429Label        Label_364;  //VERT ACCEL (g )      364 IEEE
   CoreSim::Vdn::A429Label        Label_330;  //BODY YAWRATE (DEG/S) 330 IEEE  Parameter not acquired by HI FM
   CoreSim::Vdn::A429Label        Label_361;  //INERTIAL ALT (FT )   361 IEEE
   CoreSim::Vdn::A429Label        Label_365;  //INERTIAL V/S (FT/MN) 365 IEEE
   CoreSim::Vdn::A429Label        Label_320;  //MAG HDG (DEG)        320 IEEE
   //CoreSim::Vdn::Bool             MAG_HDG_VAL;  //MAGHDGVAL              BOOL
   //CoreSim::Vdn::Bool             IRS_FG_VAL;   //IRSFGVAL               BOOL
   //CoreSim::Vdn::Bool             IRS_SOURCE;   //IRSSOURCE              ENUM   0=IRS OWN SELECTED,1=IRS3 SELECTED
   /*added according to ICD. not include in CM.*/
   CoreSim::Vdn::A429Label        Label_301;    //VERTICAL ACCEL NON FILT
   CoreSim::Vdn::A429Label        Label_313;    //TRUE TRACK
   CoreSim::Vdn::A429Label        Label_315;    //WIND SPEED
   CoreSim::Vdn::A429Label        Label_316;    //WIND DIREC TRUE
   CoreSim::Vdn::A429Label        Label_317;    //TRACK ANGLE MAG
   CoreSim::Vdn::A429Label        Label_332;    //BODY LATERAL ACCEL
   CoreSim::Vdn::A429Label        Label_336;    //INERT PITCH RATE
   CoreSim::Vdn::A429Label        Label_337;    //INERT ROLL RATE
   /*irs 3 thales/smiths fm only*/
   CoreSim::Vdn::A429Label        Label_010;    //POS LAT D (BCD)         
   CoreSim::Vdn::A429Label        Label_011;    //P POS LON D (BCD)       
   CoreSim::Vdn::A429Label        Label_012;    //GROUND SPEED D (BCD)    
   CoreSim::Vdn::A429Label        Label_013;    //TRK ANGLE TRUE D (BCD)  
   CoreSim::Vdn::A429Label        Label_014;    //MAGNETIC HDG D (BCD)    
   CoreSim::Vdn::A429Label        Label_015;    //WIND SPEED D (BCD)      
   CoreSim::Vdn::A429Label        Label_016;    //WIND DIR TRUE D (BCD)   
   CoreSim::Vdn::A429Label        Label_043;    //MAG HEADING D (BCD)     
   CoreSim::Vdn::A429Label        Label_044;    //TRUE HEADING D (BCD)    
   CoreSim::Vdn::A429Label        Label_312;    //GROUND SPEED            
   CoreSim::Vdn::A429Label        Label_334;    //PLATFORM HEADING        
   CoreSim::Vdn::A429Label        Label_350;    //IRS STATUS WORD         
   CoreSim::Vdn::A429Label        Label_275;    //IRS DISCRETE 2          
};

class InputsA429_RAOWN
{
public:
    CoreSim::Vdn::A429Label        Label_164; //RADIO HEIGHT
};

class InputsA429_RAOPP
{
public:
    CoreSim::Vdn::A429Label        Label_164; //RADIO HEIGHT 
};

class InputsA429_VOR
{
public:
   //CoreSim::Vdn::A429Receiver     VOR_bus;
   CoreSim::Vdn::A429Label        Label_222; //VOR OWN BRG DEG 
   CoreSim::Vdn::A429Label        Label_034; //VOR OWN FREQ MHZ
   CoreSim::Vdn::A429Label        Label_242; //VOR OWN ID1     
   CoreSim::Vdn::A429Label        Label_244; //VOR OWN ID2        
   CoreSim::Vdn::A429Label        Label_100; //VOR OWN SEL CRS  Parameter not acquired by HI FM
   //CoreSim::Vdn::Bool             VOR_OWN_FG_VAL;
   //CoreSim::Vdn::Bool             VOR_OWN_BRG_NCD;  //Parameter not acquired by HI FM
};

class InputsA429_TCAS
{
public:
   CoreSim::Vdn::A429Label        Label_270; //tcas vert ra data word HYB
};

class InputsA429_FADECOWN
{
public:
   CoreSim::Vdn::A429Label        Label_046; //ENG_SN_1;                 //01E0  046  M  ENG S/N WD1
   CoreSim::Vdn::A429Label        Label_047; //ENG_SN_2;                 //01E8  047  M  ENG S/N WD2
   CoreSim::Vdn::A429Label        Label_133; //SELECTED_TLA_OWN;         //0208  133  C
   CoreSim::Vdn::A429Label        Label_244; //FUEL_FLOW_OWN;            //0210  244  M
   CoreSim::Vdn::A429Label        Label_271; //FAC_DIS_2;                //0218  271  M  CFM only
   CoreSim::Vdn::A429Label        Label_272; //FAC_DIS_3;                //0220  272  C
   CoreSim::Vdn::A429Label        Label_273; //FAC_DIS_4_1;              //0228  273/270  C  (CFM only)(FAC_DIS_4,273)/IAE only(FAC_DIS_1,270)  /PW only(FAC_DIS_1,270)
   CoreSim::Vdn::A429Label        Label_337; //N1_EPR_LIMIT_OWN;         //01F0  337  C  CFM only/PW only/IAE only
   CoreSim::Vdn::A429Label        Label_341; //N1_EPR_CMD_OWN;           //01F8  341  C  CFM only/PW only/IAE only
   CoreSim::Vdn::A429Label        Label_346; //N1_EPR_ACT_OWN;           //0200  346/340  C  (CFM only)(N1_ACT,346)                             /PW only(N1_ACT,340)  
   //CoreSim::Vdn::A429Label        Label_001; //FAD_OWN_FGVAL;            //0230  001  BOOL
   /*added according to ICD. not included in CM.*/
   CoreSim::Vdn::A429Label        Label_066; //FADEC DISCRETE WORD 8  DW   (CFM only)
   CoreSim::Vdn::A429Label        Label_120; //FADEC DISCRETE WORD 9  DW   (CFM only)
   CoreSim::Vdn::A429Label        Label_124; //FADEC DISCRETE WORD 10 DW   (PW  only)
   CoreSim::Vdn::A429Label        Label_145; //FADEC DISCRETE WORD 8  DW   (PW  only)
   CoreSim::Vdn::A429Label        Label_146; //FADEC DISCRETE WORD 9       (IAE only)(PW  only)
   CoreSim::Vdn::A429Label        Label_166; //N1 (TLA)  BNR 13 256 % RPM
   CoreSim::Vdn::A429Label        Label_270; //FADEC DISCRETE WORD 1
   CoreSim::Vdn::A429Label        Label_275; //FADEC DISCRETE WORD 5       (CFM only)
   CoreSim::Vdn::A429Label        Label_276; //FADEC DISCRETE WORD 6       (CFM only)
   CoreSim::Vdn::A429Label        Label_277; //FADEC DISCRETE WORD 10      (CFM only)
   CoreSim::Vdn::A429Label        Label_333; //FADEC DISCRETE WORD 8       (CFM only)
   CoreSim::Vdn::A429Label        Label_340; //EPR ACTUAL                  (IAE only)
   CoreSim::Vdn::A429Label        Label_342; //N1 LIM MAX            BNR  14 256 % RPM 
   CoreSim::Vdn::A429Label        Label_343; //N1 TARGET FEED BACK   BNR 14 256 % RPM 
   CoreSim::Vdn::A429Label        Label_355; //FADEC DISCRETE WORD 7       (IAE only)
};

class InputsA429_FADECOPP
{
public:
   CoreSim::Vdn::A429Label        Label_066; //FADEC DISCRETE WORD 7
   CoreSim::Vdn::A429Label        Label_120; //FADEC DISCRETE WORD 9
   CoreSim::Vdn::A429Label        Label_124; //FADEC DISCRETE WORD 10
   CoreSim::Vdn::A429Label        Label_133; //SELECTED_TLA; 
   CoreSim::Vdn::A429Label        Label_145; //FADEC DISCRETE WORD 8
   CoreSim::Vdn::A429Label        Label_146; //FADEC DISCRETE WORD 9
   CoreSim::Vdn::A429Label        Label_166; //N1 (TLA)
   CoreSim::Vdn::A429Label        Label_244; //FUEL_FLOW;            
   CoreSim::Vdn::A429Label        Label_270; //FADEC DISCRETE WORD 1
   CoreSim::Vdn::A429Label        Label_271; //FAC_DIS_2;            
   CoreSim::Vdn::A429Label        Label_272; //FAC_DIS_3;            
   CoreSim::Vdn::A429Label        Label_273; //FAC_DIS_4_1;          
   CoreSim::Vdn::A429Label        Label_275; //FADEC DISCRETE WORD 5 
   CoreSim::Vdn::A429Label        Label_276; //FADEC DISCRETE WORD 6 
   CoreSim::Vdn::A429Label        Label_277; //FADEC DISCRETE WORD 10
   CoreSim::Vdn::A429Label        Label_333; //FADEC DISCRETE WORD 8 
   CoreSim::Vdn::A429Label        Label_337; //N1 LIMIT; 
   CoreSim::Vdn::A429Label        Label_340; //EPR ACTUAL
   CoreSim::Vdn::A429Label        Label_341; //N1 COMMAND;           
   CoreSim::Vdn::A429Label        Label_342; //N1 LIM MAX            
   CoreSim::Vdn::A429Label        Label_343; //N1 TARGET FEED BACK   
   CoreSim::Vdn::A429Label        Label_346; //N1 ACTUAL;            
   CoreSim::Vdn::A429Label        Label_355; //FADEC DISCRETE WORD 7  
};

class InputsA429_FMGCOWN_A
{
public:
   CoreSim::Vdn::A429Label        Label_033;     //MMR MODE USAGE(ILS, MLS, GLS)  
                                                 //- ILS FREQUENCY OPP BCD
                                                 //- MLS MODE-CHANNEL OPP BCD
                                                 //- GLS MODE-CHANNEL OPP BNR
   CoreSim::Vdn::A429Label        Label_050;     //LOC SENSIBILITY CORRECTION     BNR
   CoreSim::Vdn::A429Label        Label_051;     //MANOEUVRING SPEED REFERENCE    BNR
   CoreSim::Vdn::A429Label        Label_057;     //dQ COM UPSTREAM VOTER          BNR
   CoreSim::Vdn::A429Label        Label_060;     //dP (AIL) COM UPSTREAM VOTER    BNR
   CoreSim::Vdn::A429Label        Label_061;     //dP (SPL) COM UPSTREAM VOTER    BNR
   CoreSim::Vdn::A429Label        Label_062;     //dR COM UPSTREAM VOTER          BNR
   CoreSim::Vdn::A429Label        Label_063;     //dNOSE WHEEL COM UPSTEAM VOTER  BNR
   CoreSim::Vdn::A429Label        Label_073;     //V2 SPEED                       BNR
   CoreSim::Vdn::A429Label        Label_077;     // BNR  APPROACH SPEED FM        
   CoreSim::Vdn::A429Label        Label_172;     // BNR  RUNWAY HEADING OPP       
                                                 // 131;  BNR                           
                                                 // 315;  BNR                           
   CoreSim::Vdn::A429Label        Label_355;     // HYB  COM to MON DRLB Messages 

};

//COM_FMGC_MON_IN
class InputsA429_COM_FMGC_MON
{
public:
   CoreSim::Vdn::A429Label        Label_057;  // dQ MON UPSTREAM VOTE           BNR 15 ±32 DEG C1 C1 DQUVM By COM Lane
   CoreSim::Vdn::A429Label        Label_060;  // dP (AIL) MON UPSTREAM VOTE     BNR 15 ±32 DEG C1 C1 DPAUVM By COM Lane
   CoreSim::Vdn::A429Label        Label_061;  // dP (SPL) MON UPSTREAM VOTE     BNR 15 ±64 DEG C1 C1 DPSUVM By COM Lane
   CoreSim::Vdn::A429Label        Label_062;  // dR MON UPSTREAM VOTE           BNR 15 ±32 DEG C1 C1 DRUVM By COM Lane
   CoreSim::Vdn::A429Label        Label_063;  // dNOSE WHEEL MON UPSTREAM VOTE  BNR 15 ±32 DEG C1 C1 DNWUVM By COM Lane
   CoreSim::Vdn::A429Label        Label_241;  // AOA AVERAGE                    BNR 15 ±180 DEG C1 C1 AOAHSUM By COM Lane
   CoreSim::Vdn::A429Label        Label_272;  // DW7                            DW SEE PAGE 305 C2 C2 AFGM272M By COM Lane
   CoreSim::Vdn::A429Label        Label_277;  // DW1                            DW SEE PAGE 311 C1 C1 AFGM277M By COM Lane
   CoreSim::Vdn::A429Label        Label_310;  // dP (AIL) VOTED MON             BNR 15 ±32 DEG C1 C1 DPAVM By COM Lane
   CoreSim::Vdn::A429Label        Label_311;  // dP (SPL) VOTED MON             BNR 15 ±64 DEG C1 C1 DPSVM By COM Lane
   CoreSim::Vdn::A429Label        Label_312;  // dR VOTED MON                   BNR 15 ±32 DEG C1 C1 DRVM By COM Lane
   CoreSim::Vdn::A429Label        Label_313;  // dNOSE WHEEL VOTED MON          BNR 15 ±32 DEG C1 C1 DNWVM By COM Lane
   CoreSim::Vdn::A429Label        Label_314;  // dQ VOTED MON                   BNR
   CoreSim::Vdn::A429Label        Label_350;  // SAFETY TESTS WORD              DW ASYN SFT350 By COM Lane During safety test (SYNCHRO COM/MOM)
   CoreSim::Vdn::A429Label        Label_355;  // DRLB MON TO COM                HYB
   //IAE engines
   CoreSim::Vdn::A429Label        Label_133; //THROTTLE POSITION (TLA) BNR 12 180 DEG C1 C1 OTLA By COM Lane
   CoreSim::Vdn::A429Label        Label_145; //DISCRETE WORD 10        SEE PAGE 312 C1 C1 AFG145M By COM Lane
   CoreSim::Vdn::A429Label        Label_146; //DISCRETE WORD 4         SEE PAGE 299 C1 C1 AFG146M By COM Lane
   CoreSim::Vdn::A429Label        Label_147; //DISCRETE WORD 6         SEE PAGE 407 C1 C1 AFG147M By COM Lane
   CoreSim::Vdn::A429Label        Label_166; //EPR (TLA)               BNR 15 4 EPR C2 C2 OEPRTLA By COM Lane
   CoreSim::Vdn::A429Label        Label_244; //FUEL FLOW               BNR 15 32768 Lbs/h C2 C3 AOFUELFL By FM Lane
   CoreSim::Vdn::A429Label        Label_270; //ATS DISCRETE WORD       SEE PAGE 300 C1 C1 AFG270M By COM Lane
   CoreSim::Vdn::A429Label        Label_271; //FADEC DISCRETE WORD     SEE PAGE 302 C2 C2 AOFA271 By COM & FM INCLUDING FADEC DW2 AND 4 AND 3
   CoreSim::Vdn::A429Label        Label_273; //DISCRETE WORD 3         SEE PAGE 306 C1 C1 AFG273M By COM Lane
   CoreSim::Vdn::A429Label        Label_274; //DISCRETE WORD 1         SEE PAGE 307 C1 C1 AFG274M By COM Lane
   CoreSim::Vdn::A429Label        Label_275; //DISCRETE WORD 2         SEE PAGE 308 C1 C1 AFG275M By COM Lane
   CoreSim::Vdn::A429Label        Label_276; //DISCRETE WORD 8         SEE PAGE 310 C1 C1 AFG276M By COM Lane
   CoreSim::Vdn::A429Label        Label_337;  //EPR LIMIT           BNR 15 4 EPR C2 C2 OEPRLIM By COM & FM
   CoreSim::Vdn::A429Label        Label_340;  //EPR ACTUAL          BNR 15 4 EPR C1 C1 OEPRACT By COM & FM
   CoreSim::Vdn::A429Label        Label_341;  //EPR COMMAND         BNR 15 4 EPR C2 C2 AOE PRCMD By COM Lane
   CoreSim::Vdn::A429Label        Label_342;  //EPR MAX             BNR 15 4 EPR C2 C2 OEPRMAX By COM Lane
   //CoreSim::Vdn::A429Label        Label_350;  //MAINTENANCE WORD 1  DW SEE PAGE 415 ASYN C1 MONCOM
   CoreSim::Vdn::A429Label        Label_351;  //MAINTENANCE WORD 2  DW SEE PAGE 419 ASYN C1 MONCOM
};

class InputsFG2FM
{
public:
	CoreSim::Osal::UInt16 MEM_BUSY;
	CoreSim::Osal::UInt16 FG_HLY; 
	CoreSim::Osal::UInt16 CROSS_LOAD;
	CoreSim::Osal::UInt16 SIDE_1;               //0058  TRUE=SIDE1(LM06D)
    CoreSim::Osal::UInt16 FMGC_OPP_HLY;         //005C   TRUE=HLTHY (LT02B)
    CoreSim::Osal::UInt16 AP_ENG;               //0060   TABLE 4.8.4
    CoreSim::Osal::UInt16 MAG_TRUE_SEL;         //0064   ‘True’ = TRUE SEL
    CoreSim::Osal::UInt16 DERATED_CLI;  
   CoreSim::Osal::UInt32 FPA_TGT;            
   CoreSim::Osal::UInt32 VS_TGT;             
   CoreSim::Osal::UInt32 ALT_TGT;            
   CoreSim::Osal::UInt32 SPD_TGT;            
   CoreSim::Osal::UInt32 MACH_TGT;           
   CoreSim::Osal::UInt32 FMGC_DIS_WD_1;      
   CoreSim::Osal::UInt32 FMGC_DIS_WD_2;      
   CoreSim::Osal::UInt32 FMGC_DIS_WD_3;      
   CoreSim::Osal::UInt32 FMGC_DIS_WD_4;      
   CoreSim::Osal::UInt32 FMGC_DIS_WD_5;      
   CoreSim::Osal::UInt32 FMGC_ATS_DIS_WD;    
   CoreSim::Osal::UInt16 FG_PITCH_ROLL_DIS;  
   CoreSim::Osal::UInt16 FMGC_PRIORITY;           
   CoreSim::Osal::UInt32 OPP_FMGC_DIS_WD_4;  
   CoreSim::Osal::UInt32 OUTPUT_RLIM_1;  
   CoreSim::Osal::UInt16 RMP_OWN_MLS_SEL;
   CoreSim::Osal::UInt16 RMP_OPP_MLS_SEL;
};

class InputsA429
{
   public:
   InputsA429_ADCOWN in_ADCOWN;
   InputsA429_ADCOPP in_ADCOPP;
   InputsA429_ADC3   in_ADC3;
   InputsA429_FacOwn in_FACOWN;
   InputsA429_FacOpp in_FACOPP;
   InputsA429_FADECOWN in_FADOWNA;
   InputsA429_FADECOWN in_FADOWNB;
   InputsA429_FCUOWNA in_FCUOWNA;
   InputsA429_FCUOWNB in_FCUOWNB;
   //InputsA429_COM_FMGC_MON in_COM_FMGCCOM;
   InputsA429_FQI    in_FQI;
   InputsA429_LSOWN  in_LSOWN;
   InputsA429_LSOPP  in_LSOPP;
   InputsA429_MLS    in_MLS;  //MLS  Parameter not acquired by HI.
   InputsA429_IRS    in_IRS;
   InputsA429_IRSOWN in_IRSOWN;
   InputsA429_IRSOPP in_IRSOPP;
   InputsA429_IRS3   in_IRS3;
   InputsA429_RAOWN  in_RAOWN;
   InputsA429_RAOPP  in_RAOPP;
   InputsA429_VOR    in_VOROWN;
   InputsA429_VOR    in_VOROPP;
   InputsA429_TCAS   in_TCASOWN;
   InputsA429_TCAS   in_TCASOPP;
   InputsA429_FADECOPP in_FADOPPA;
   InputsA429_FADECOPP in_FADOPPB;
   CommMemory        in_CMIO;
   CoreSim::Vdn::A429Receiver in_DME[2];
   CoreSim::Vdn::A429Receiver in_Clock;
};


//FM_FG_SHARED_MEM_FM_IN port:10184
//CommMemory m_comMemoryFM_In;
//FM_FG_SHARED_MEM_FM_OUT port:10183
//CommMemory m_comMemoryFM = {0};
//FM_FG_SHARED_MEM_IO_IN port:10183
//CommMemory m_comMemoryIO_In;
//FM_FG_SHARED_MEM_IO_OUT port:10184
//CommMemory m_comMemoryIO = {0};
//used for test only.later this variables will be sustute by m_comMemoryIO
//CommMemory m_comMemoryFMout = {0};

//CoreSim::Osal::UInt32 m_sendFrameDivisor = 30;
class FMFG
{
public:
   FMFG():
   fm_puprdy(0),
   fm_puprdy_fm(0),
   fm_hlty(0),
   sdi(0),
   code_cmp_1(0),
   code_cmp_2(0),
   code_cmp_3(0),
   code_cmp_4(0),
   code_cmp_5(0),
   code_cmp_6(0),
   //cfm_engine(0),
   //iae_engine(255),
   //pw_engine(0),
   CrosLoad_Flag(false),
   offaddr(0)
   {
   
   }
  
   //FM_PUPRDY
   CoreSim::Osal::UInt32 fm_puprdy     ;
   //FM_PUPRDY publishd by FM on Bus 114
   CoreSim::Osal::UInt32 fm_puprdy_fm  ;
   //FM HEALTHY
   CoreSim::Osal::UInt32 fm_hlty       ;
   //SDI
   CoreSim::Osal::UInt32 sdi           ;
   //FG HEALTHY
   //UINT32 fg_hlty        = 0;
   //CODE COMPARE 1
   CoreSim::Osal::UInt32 code_cmp_1    ;
   //CODE COMPARE 2
   CoreSim::Osal::UInt32 code_cmp_2    ;
   //CODE COMPARE 3
   CoreSim::Osal::UInt32 code_cmp_3    ;
   //CODE COMPARE 4
   CoreSim::Osal::UInt32 code_cmp_4    ;
   //CODE COMPARE 5
   CoreSim::Osal::UInt32 code_cmp_5    ;
   //CODE COMPARE 6
   CoreSim::Osal::UInt32 code_cmp_6    ;

   //CFM ENGINE
   //CoreSim::Osal::UInt32 cfm_engine    ;
   //IAE ENGINE
   //CoreSim::Osal::UInt32 iae_engine    ;
   //PW ENGINE
   //CoreSim::Osal::UInt32 pw_engine     ;

   //FG TO fM
   InputsFG2FM in_FG2FM;
   //Crossload_Enable Flag
   CoreSim::Osal::Bool CrosLoad_Flag;

   //Offset from CommonMemory
   CoreSim::Osal::Int64 offaddr;

   //FILE *ftest;
};