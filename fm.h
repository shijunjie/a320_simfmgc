/*****************************************************************************
*                                                                            *
*  COPYRIGHT 2018 ACCEL FLIGHT SIMULATION. ALL RIGHTS RESERVED               *
*                                                                            *
*  This file is part of A320 Neo Sim FMGC simulation                         *
*                                                                            *
*  All information and content in this document is the Property              *
*  to ACCEL (Tianjin) Flight Simulation Co., Ltd. and shall not              *
*  be disclosed, disseminated, copied, or used except for purposes           *
*  expressly authorized in writing by ACCEL. Any unauthorized use            *
*  of the content will be considered an infringement of                      *
*  ACCEL's intellectual property rights.                                     *
*                                                                            *
*                                                                            *
*  @file     fm.h                                                            *
*  @brief    Entry point for the NetVdn Application.                         *
*                                                                            *
*            This is a Sim FM example of how to use the NetVdnClient to      *
*            publish and subscribe to data on the Virtual Data Network.      *
*                                                                            *
*            This example includes publishing and subscribing to variables   *
*            as well as sending and receiving requests.                      *
*                                                                            *
*  @author   Shi, Junjie                                                     *
*  @email    junjie.shi@accelflightsimulation.com                            *
*  @version  1.0.0.0(version)                                                *
*  @date     2018/07/21                                                      *
*                                                                            *
*----------------------------------------------------------------------------*
*  Remark         : Description                                              *
*----------------------------------------------------------------------------*
*  Change History :                                                          *
*  <Date>     | <Version> | <Author>       | <Description>                   *
*----------------------------------------------------------------------------*
*  1/15/2019  | 1.0.0.0   | Shi, Junjie   | Create file                      *
*----------------------------------------------------------------------------*
*                                                                            *
*****************************************************************************/
#pragma once
#include "SimElement/Element.h"
#include "NetVdn/NetVdnClient.h"
#include "Utils/CsvFile.h"
#include "Header.h"
#include "fg.h"
#include <string.h>
#include <io.h>
using namespace std;
using namespace CoreSim;
using namespace Utils;


//define P_BUS_MSGS
#define C_P_VERSION               1
#define C_P_SIM_CLOCK             2
#define C_P_X_END_FRAME           3
#define C_P_END_FRAME             4
#define C_P_TEXT                  7
#define C_P_BUS_429              26
#define C_P_BUS_DIS              27
#define C_P_BUS_ANA              28
#define C_P_POWER                29
#define C_P_FP_LOAD              30
#define C_P_FP_SAVE              31
//define max NUM_BYTES
#define C_MAX_NUM_BYTES       16384

//define CAB
#define C_FMS_1_2                 0
#define C_FMS_1                   1
#define C_FMS_2                   2
#define C_FMS_3                   3

#define PWR_TRS                   1   //1-Long Term;    0-Very Long Term; 
#define DATALOADER                0   //1-Attached;     0-Not Attached;  /*Caution:with Dataloader attached, FMS Power on will not effect!!!*/
#define GRND_CND                  0   //1-On Ground;    0-In Air;
#define PWR_OFF                   2
#define PWR_ON                    1

//data size:the number of labels
#define C_DataGps_Size           24
#define C_DataMcdu_Size          13    //6
#define C_DataClock_Size          4
#define C_DataVorOpp_Size         5
#define C_DataVorOwn_Size         6
#define C_DataAdc_Size           12
#define C_DataAdcE_Size           9

/*According to A.4 of ICD APPENDIX-320 on page 4, Aircraft To Simulation Bus Mapping*/
//define busnumber
#define Bus_DmeOwn               43
#define Bus_DmeOpp               30
#define Bus_Printer             102
#define Bus_Clock                28
#define Bus_McduOwn               4
#define Bus_McduOpp               5
#define Bus_Dataloader          108
#define Bus_Atsu_acars           92
#define Bus_GpsOwn               13
#define Bus_GpsOpp               23
#define Bus_Ils                  93
#define Bus_Bite                114
//#define Bus_Fids                114
#define Bus_VOROwn		         49
#define Bus_VOROpp		         51
#define Bus_ADC                  45
#define Bus_IRS_GPS_Hybrid_Own		         19
#define Bus_IRS_GPS_Hybrid_Opp		         48
#define Bus_IRS_GPS_Hybrid_3  		         10
#define Bus_FCUA			        216
#define Bus_FCUB			        219
#define Bus_Mls			         18
#define Bus_FAC                 215
#define Bus_FQI                  22
#define Bus_FadecOwn            217
#define Bus_FadecOpp            218
#define Bus_FGFM_Protocol       100
//FG/FM POWER-UP
#define Bus_AL_PINS             461
#define Bus_AL_PIN1             462
#define Bus_AL_PIN2             463
#define Bus_CFM_ENG             464
#define Bus_IAE_ENG             465
#define Bus_PW_ENG              473
#define Bus_CODE_CMP_WD1        466
#define Bus_CODE_CMP_WD2        467
#define Bus_CODE_CMP_WD3        468
#define Bus_CODE_CMP_WD4        469
#define Bus_CODE_CMP_WD5        470
#define Bus_CODE_CMP_WD6        471
//Discrete Inputs
#define Bus_DIS_WD_0             66
#define Bus_DIS_WD_1             68
//FMS output bus
#define Bus_Mcdu_OWN_O            1
#define Bus_Mcdu_OPP_O            2
#define Bus_DataLoader_Sim_O    109
#define Bus_Efis_1_O             83
#define Bus_Printer_Acars_O      87
#define Bus_ADS_O               127
#define Bus_Fmgc_A_B_O           65
#define Bus_Fmgc_C_O             61
#define Bus_FMFG_Protoclol_O    114




class FM : public FMFG
{
public:
   /*FMS Message Buffers-header*/
   struct MsgBufHeader
   {
      CoreSim::Osal::UInt32 NUM_BYTES;   //MAX=16384
      CoreSim::Osal::UInt16 NUM_WORDS;   //32-bit words following in buffer including this word. MAX=4095
      CoreSim::Osal::UInt16 NUM_MSGS;   //number of variable-sized messages following in this buffer
   };

   /*FMS Message-Header*/
   #pragma pack(push,1)
   struct Msg_Header
   {																							
      CoreSim::Osal::UChar NUM_WORDS2 :4;  //bit 25-28   reverse inner byte
      CoreSim::Osal::UChar BusNum2 :4;     //bit 29-32, R
      CoreSim::Osal::UChar NUM_WORDS1 :8;  //bit 17-24
      CoreSim::Osal::UChar BusNum1 :8;	    //bit 9-16 , R
      CoreSim::Osal::UChar MSG_TYPE :6;    //bit 1-6     reverse inner byte
      CoreSim::Osal::UChar CAB :2;         //bit 7-8  , R
   };
   #pragma pack(pop)

   /*FMS Message-P_VERSION(1)*/
   #pragma pack(push,1)
   struct P_Version
   {
      //MsgBufHeader MsgBuf_Header;
      Msg_Header MSG_Header;
      CoreSim::Osal::UInt32 Version;
      CoreSim::Osal::UInt32 IMM_Version;
   };
   #pragma pack(pop)

   /*FMS Message-P_SIM_CLOCK(2)*/
   struct P_SIM_CLOCK
   {
      //MsgBufHeader MsgBuf_Header;
      Msg_Header MSG_Header;
      CoreSim::Osal::UInt32 SIM_CLOCK;
   };

   /*FMS Message-P_BUS_429(26)*/
   #pragma pack(push,1)
   struct P_BUS_429
   {
      //MsgBufHeader MsgBuf_Header;
      Msg_Header MSG_Header;
      CoreSim::Osal::UInt32 BUS_DATA; 
   };
   #pragma pack(pop)

   /*FMS Message-P_BUS_DIS(27)*/
   struct P_BUS_DIS
   {
      //MsgBufHeader MsgBuf_Header;
      Msg_Header MSG_Header;
      CoreSim::Osal::UInt32 BUS_DATA;
   };

   /*FMS Message-P_BUS_ANA(28)*/
   struct P_BUS_ANA
   {
      //MsgBufHeader MsgBuf_Header;
      Msg_Header MSG_Header;
      CoreSim::Osal::UInt32 BUS_DATA;
   };

   /*FMS SCP respond for P-FP_LOAD(30)/P-FP_SAVE(31)*/
   struct P_SCP_RESPOND
   {
      Msg_Header MSG_Header;
   };

   /*FMS P-X-END-FRAME(3)*/
   struct P_X_END_FRAME
   {
      Msg_Header MSG_Header;
   };

   /*P-END-FRAME(4)*/
   struct P_END_FRAME
   {
      Msg_Header MSG_Header;
   };

   /*P-Text(7)*/
   struct P_TEXT
   {
      Msg_Header MSG_Header;
      CoreSim::Osal::Char DATA[127*4];   //1-127
   };

   /*P-Power(29)*/
   struct P_POWER
   {
      Msg_Header MSG_Header;
      //CoreSim::Osal::UInt32 POWER;  //1=power on;2=power off
      CoreSim::Osal::UInt32 BUS_DATA;
   };

   /*P-FP_LOAD(30)*/
   struct P_FP_LOAD
   {
      Msg_Header MSG_Header;
      CoreSim::Osal::Char DATA[127*4];   //1-127
   };

   /*P-FP_SAVE(31)*/
   #pragma pack(push,1)
   typedef struct P_FP_SAVE
   {
      Msg_Header MSG_Header;
      //CoreSim::Osal::Char DATA[127*4]; 
      CoreSim::Osal::Char DATA[0]; 
   };
   #pragma pack(pop)
   
   struct FP_SAVE_Data
   {
	    MsgBufHeader m_msgBufHeader;
	    //P_FP_SAVE p_fpsave;
       P_X_END_FRAME m_x_end;
   };

   struct Pwr_Data
   {
	    MsgBufHeader m_msgBufHeader;
	    P_POWER p_power;
       P_X_END_FRAME m_x_end;
   };

   /*
   typedef struct RecvData
   {
      MsgBufHeader m_RecMsgBufHeader;
      Msg_Header m_RecMsgHeader;
      CoreSim::Osal::Char DATA[0];
      //CoreSim::Vdn::A429Label Tx_Label;
   }RecvData;
   */
   typedef struct RecvDataMsg
   {
      //MsgBufHeader m_RecMsgBufHeader;
      Msg_Header m_RecMsgHeader;
      CoreSim::Osal::Char DATA[0];
      //CoreSim::Vdn::A429Label Tx_Label;
   }RecvDataMsg;

   struct Common_Data
   {
      MsgBufHeader m_msgBufHeader; 

      P_BUS_DIS m_bus_66;   //discrete word 0
      P_BUS_DIS m_bus_68;   //discrete word 1  
   #if 1
      P_BUS_DIS m_bus_461;   //Program Pins
      P_BUS_DIS m_bus_462;   //Program Pins
      P_BUS_DIS m_bus_463;   //Program Pins
      P_BUS_DIS m_bus_464;   //cfm-engine
      P_BUS_DIS m_bus_465;   //iae-engine
      P_BUS_DIS m_bus_466;   //code compare 1
      P_BUS_DIS m_bus_467;   //code compare 2
      P_BUS_DIS m_bus_468;   //code compare 3
      P_BUS_DIS m_bus_469;   //code compare 4
      P_BUS_DIS m_bus_470;   //code compare 5
      P_BUS_DIS m_bus_471;   //code compare 6
      P_BUS_DIS m_bus_473;   //pw-engine
   #endif
      P_BUS_429 m_bus_100;   //pitch roll
   #if 0
      P_BUS_429 m_bus_19_1;   //ground speed BNR-FMS1
      P_BUS_429 m_bus_19_2;   //ground speed BNR-FMS2
   #endif
      P_BUS_429 m_bus_108;   //DATA LOADER/SIM flight freeze label 250

      //Input bus,Source: direct
      //dme own
      //CoreSim::Osal::UInt32 dme_1;
      //dme opp
      //CoreSim::Osal::UInt32 dme_2;
      //printer
      //CoreSim::Osal::UInt32 print_1;
      //gps own            13(0xd)
      P_BUS_429 gps_own[C_DataGps_Size];
      //gps opp            23(0x17)
      P_BUS_429 gps_opp[C_DataGps_Size];
      //mcdu own           4
      P_BUS_429 mcdu_own[C_DataMcdu_Size];
      //mcdu opp           5
      P_BUS_429 mcdu_opp[C_DataMcdu_Size];
      //clock              28(0x1c)
      P_BUS_429 clock[C_DataClock_Size];

      /*pack CM Data here.*/
      //VOR OWN            49(0x31)
      P_BUS_429 vor_own[C_DataVorOwn_Size];
      //VOR OPP            51(0x33)
      P_BUS_429 vor_opp[C_DataVorOpp_Size];
      //ADC                 45(0x2d)
      P_BUS_429 adc_own[C_DataAdc_Size];
       //IRS/GPS HYBRID OWN  19
      P_BUS_429 irs_own[33];  //9-12-12
      //IRS/GPS HYBRID OPP  48 
      P_BUS_429 irs_opp[33];
      //ISR/GPS HYBRID 3    10
      P_BUS_429 irs_3[33];
      //FCU_A				 216
      P_BUS_429 fcu_a[14];
      //FCU_B				 219
      P_BUS_429 fcu_b[14];
      //FAC                 215
      P_BUS_429 fac[15];

      P_X_END_FRAME m_x_end;
   };

   struct Common_Data2
   {
      MsgBufHeader m_msgBufHeader; 

      P_BUS_DIS m_bus_66;   //discrete word 0
      P_BUS_DIS m_bus_68;   //discrete word 1  
   #if 1
      P_BUS_DIS m_bus_461;   //Program Pins
      P_BUS_DIS m_bus_462;   //Program Pins
      P_BUS_DIS m_bus_463;   //Program Pins
      P_BUS_DIS m_bus_464;   //cfm-engine
      P_BUS_DIS m_bus_465;   //iae-engine
      P_BUS_DIS m_bus_466;   //code compare 1
      P_BUS_DIS m_bus_467;   //code compare 2
      P_BUS_DIS m_bus_468;   //code compare 3
      P_BUS_DIS m_bus_469;   //code compare 4
      P_BUS_DIS m_bus_470;   //code compare 5
      P_BUS_DIS m_bus_471;   //code compare 6
      P_BUS_DIS m_bus_473;   //pw-engine
   #endif
      //P_BUS_429 m_bus_100[25];   //pitch roll
      P_BUS_429 m_bus_100;
   #if 0
      P_BUS_429 m_bus_19_1;   //ground speed BNR-FMS1
      P_BUS_429 m_bus_19_2;   //ground speed BNR-FMS2
   #endif
      P_BUS_429 m_bus_108;   //DATA LOADER/SIM flight freeze label 250

      //FQI                 22
      P_BUS_429 fqi_Top;
      P_BUS_429 fqi_End[12];

      //FADEC_OWN           217
      P_BUS_429 fadec_1[11];
      //FADEC_OPP           218
      P_BUS_429 fadec_2[7];

      P_X_END_FRAME m_x_end;
   };

   struct Dis_Data
   {
      MsgBufHeader m_msgBufHeader;
      P_BUS_DIS m_bus_66;   //discrete word 0
      P_BUS_DIS m_bus_68;   //discrete word 1
      P_BUS_DIS m_bus_464;   //cfm-engine
      P_BUS_DIS m_bus_465;   //iae-engine
      P_BUS_DIS m_bus_473;   //pw-engine
      P_BUS_DIS m_bus_461;   //Program Pins
      P_BUS_DIS m_bus_462;   //Program Pins
      P_BUS_DIS m_bus_463;   //Program Pins
      P_BUS_DIS m_bus_466;   //code compare 1
      P_BUS_DIS m_bus_467;   //code compare 2
      P_BUS_DIS m_bus_468;   //code compare 3
      P_BUS_DIS m_bus_469;   //code compare 4
      P_BUS_DIS m_bus_470;   //code compare 5
      P_BUS_DIS m_bus_471;   //code compare 6

      P_BUS_DIS m_bus_100;   //fg->fm label 260 fg_healthy. This bus should be confirmed later.shijj

      P_BUS_429 m_bus_108;   //flight freeze label 250
   //   P_BUS_429 m_bus_19_1;   //ground speed BNR-FMS1
   //   P_BUS_429 m_bus_19_2;   //ground speed BNR-FMS2

   //   P_POWER p_power;

      P_X_END_FRAME m_x_end;
   };

   class InputsMcdu
   {
      public:
         //CoreSim::Vdn::A429Receiver             Mcdu_bus[2];

         //CoreSim::Vdn::A429Label                Label270;
         //CoreSim::Vdn::A429Label                Label271;
         //CoreSim::Vdn::A429Label                Label272;
         //CoreSim::Vdn::A429Label                Label350;
         //CoreSim::Vdn::A429Label                Label354;
         //CoreSim::Vdn::A429Label                Label377;

         CoreSim::Vdn::A429Label                Label270;   //DW
         CoreSim::Vdn::A429Label                Label271;   //DW
         CoreSim::Vdn::A429Label                Label272;   //DW
         CoreSim::Vdn::A429Label                Label300;   //ISO KUCH FM1SAL  
         CoreSim::Vdn::A429Label                Label301;   //ISO KUCH FM2SAL
         CoreSim::Vdn::A429Label                Label302;   //ISO KUCH ACMSSAL
         CoreSim::Vdn::A429Label                Label303;   //ISO KUCH CMSSAL
         CoreSim::Vdn::A429Label                Label304;   //ISO KUCH ACA.ATSUSAL
         CoreSim::Vdn::A429Label                Label307;   //ISO KUCH SDUSAL
         CoreSim::Vdn::A429Label                Label350;   //DW
         CoreSim::Vdn::A429Label                Label352;   //DW
         CoreSim::Vdn::A429Label                Label354;   //ISO KUCH PNSN
         CoreSim::Vdn::A429Label                Label377;   //BCD KUCD EQPTID
   };

   class InputsGps
   {
      public:
         //CoreSim::Vdn::A429Receiver             gps_bus[3];

         CoreSim::Vdn::A429Label                Label057;
         CoreSim::Vdn::A429Label                Label076;
         CoreSim::Vdn::A429Label                Label101;
         CoreSim::Vdn::A429Label                Label102;
         CoreSim::Vdn::A429Label                Label103;
         CoreSim::Vdn::A429Label                Label110;
         CoreSim::Vdn::A429Label                Label111;
         CoreSim::Vdn::A429Label                Label112;
         CoreSim::Vdn::A429Label                Label120;
         CoreSim::Vdn::A429Label                Label121;
         CoreSim::Vdn::A429Label                Label125;
         CoreSim::Vdn::A429Label                Label130;
         CoreSim::Vdn::A429Label                Label133;
         CoreSim::Vdn::A429Label                Label136;
         CoreSim::Vdn::A429Label                Label140;
         CoreSim::Vdn::A429Label                Label141;
         CoreSim::Vdn::A429Label                Label150;
         CoreSim::Vdn::A429Label                Label165;
         CoreSim::Vdn::A429Label                Label166;
         CoreSim::Vdn::A429Label                Label174;
         CoreSim::Vdn::A429Label                Label247;
         CoreSim::Vdn::A429Label                Label260;
         CoreSim::Vdn::A429Label                Label273;
         CoreSim::Vdn::A429Label                Label377;
   };

   class InputsClock
   {
      public:      
         CoreSim::Vdn::A429Receiver             clock_bus;

         CoreSim::Vdn::A429Label                Label125;
         CoreSim::Vdn::A429Label                Label150;
         CoreSim::Vdn::A429Label                Label260;
         CoreSim::Vdn::A429Label                Label377;
   };

   class Inputs
   {
      public:
      Inputs() :
         Side_1_Mon(false),
         Fuel_Poli_1(false),
         Fuel_Poli_2(false),
         Fuel_Poli_3(false),
         Adf(false),
         Fms_Printer(false),
         Fms_Acars(false),
         Mini_Acars(false),
         Ft_m(false),
         Cross_Load(false),
         Batch1(false),
         Lb_Kg(false),
         Deg_F_C(false),
         Vappr(false),
         Center_Tank(false),
         Grid_Mora_Msa(false),
         Qnh_Qfe(false),
         Rte_Retention(false),
         Gs_Before_Loc(false),
         //Fg_Ac_Ident_1(2,false),
         Kilowords_400(false),
         Speed_Prot(false),
         Fg_Ac_Ident_2(2,false),
         Lip(false),
         Aza(false),
         Fm_Perfom(false),
         Fma_Enhanced(false),
         Derated_TO(false),
         No_AP_Disc_In_F_Des(false),
         //Ap_Tcas(2,false),
         Soft_GA(false),
         AIRLINE_31(false),
         AIRLINE_32(false),
         AIRLINE_33(false),
         AIRLINE_34(false),
         AIRLINE_35(false),
         AIRLINE_36(false),
         //Jnb_Autoland(2,false),
         //Nav_In_GA(2,false),
         SPI3C(false),
         //Eng_Type_4(false),
         TRAFFIC_ADVISORY(false),
         SPI6C(false),
         Energy_Management(false),
         AUTO_BRAKE_ARMED(false),
         SPI12C(false),
         SPI13C(false),
         SPI14C(false),
         SPI16C(false),
         SPI17C(false),
         SPI18C(false),
         SPI19C(false),
         SPI20C(false),
         //Ac_Type_1(false), 
         //Ac_Type_2(false),
         //Eng_Type_1(false),
         //Eng_Type_2(false),
         //Eng_Type_3(false),
         Vmo_mmo_1(false),
         Vmo_mmo_2(false),
         Strakes(false),
         Gps_1(false),
         Gps_2(false),
         PARITY_COM(false),
         PARITY_MON(false),
         AIRLINE37(false),
         AIRLINE38(false),
         AIRLINE39(false),
         AIRLINE40(false),
         Neo_Mon(false),
         SPI9M(false),
         SPI10M(false),
         SPI11M(false),
         SPI12M(false),
         SPI13M(false),
         SPI14M(false),
         SPI15M(false),
         //Oil_Low_Press_Sw(2, false),
         //Own_Fwc_Valid(2, false),
         //Opp_Fwc_Valid(2, false),
         //Fac_Own_Hlty_Mon(2, false),
         //Fac_Opp_Hlty_Mon(2, false),
         //Fac_Own_Hlty_Com(2, false),
         //Fac_Opp_Hlty_Com(2, false),
         //Att_3_Sel(2, false),
         //Adc_3_Sel(2, false),
         //Nose_Gear_Pressed(2, false),
         //Ap_Cmd_Sw_Mon(2, false),
         //Ap_Cmd_Sw_Com(2, false),
         //Athr_Sw_Mon(2, false),
         //Athr_Sw_Com(2, false),  
         Takeover_Relay(2, false),
         //Wheel_Spd_Gt_70(2, false),
         //BSCU_COM_Valid(2, false),
         //BSCU_MON_Valid(2, false),
         DI_AP_OWN_ENGD_MON_OPP(2, false),
         DI_AP_OWN_ENGD_COM_OPP(2, false),
         //ELAC_OPP_AP_DISC_MON (2, false),
         //ELAC_OPP_AP_DISC_COM (2, false),
         //ELAC_OWN_AP_DISC_MON (2, false),
         //ELAC_OWN_AP_DISC_COM (2, false),
         //PS_SPLIT(2, false),
         ATH_INST_DISC_NC(2, false),
         ATH_INST_DISC_NO(2, false),
         ATH_OPP_ENGD_COM(2, false),
         ATH_OPP_ENGD_MON(2, false),
         AP_OPP_ENGD_MON_COM(2, false),
         AP_OPP_ENGD_COM_COM(2, false),
         FD_OPP_ENGD_COM(2, false),
         FD_OPP_ENGD_MON(2, false),
         PFD_OWN_VALID(2, false),
         PFD_OPP_VALID(2, false),
         //SIDE_1_COM(2, false),
         //NORTH_REF_PB(2, false),
         FCU_OWN_HLTY_IN(2, false),
         FCU_OPP_HLTY(2, false),
         NAV_CONTROL_OWN(2, false),
         NAV_CONTROL_OPP(2, false),
         CDU_OPP_FAIL(2, false),
         CDU_OWN_FAIL(2, false),
         BSP_CONNEX(false),
         Load_Enable(2,false),
         LB02_Power_28vdc(2,false),
         Fmgs_Reset(false),
         Flight_Freeze(false),
         Position_Freeze(false),
         Altitude_Freeze(false),
         Fuel_Freeze(false),
         Position_Set(false),
         Position_Slew(false),
         Altitude_Slew(false),
         Airspeed_Slew(false),
         Heading_Slew(false),
         Snapshot_Take(false),
         Snapshot_Recall(false),
         Speed_Times_4(false),
         ZFW_Set(false),
         Fuel_Set(false),
         Temp_Press_Set(false),
         Reposition(false),
         Wind_Set(false),
         Flight_Plan_Copy(false),
         Flight_Plan_Load(false) 
       {   
       }
      CoreSim::Vdn::Bool	Side_1_Mon;
      CoreSim::Vdn::Bool	Fuel_Poli_1;             //AIRLINE_1
      CoreSim::Vdn::Bool	Fuel_Poli_2;             //AIRLINE_2
      CoreSim::Vdn::Bool	Fuel_Poli_3;             //AIRLINE_3
      CoreSim::Vdn::Bool	Adf;                     //AIRLINE_4
      CoreSim::Vdn::Bool	Fms_Printer;             //AIRLINE_5;
      CoreSim::Vdn::Bool	Fms_Acars;               //AIRLINE_6;
      CoreSim::Vdn::Bool	Mini_Acars;              //AIRLINE_7
      CoreSim::Vdn::Bool	Ft_m;                    //AIRLINE_8;
      CoreSim::Vdn::Bool	Cross_Load;              //AIRLINE_9;
      CoreSim::Vdn::Bool	Batch1;                  //AIRLINE_10;
      CoreSim::Vdn::Bool	Lb_Kg;                   //AIRLINE_11
      CoreSim::Vdn::Bool	Deg_F_C;                 //AIRLINE_12 
      CoreSim::Vdn::Bool	Vappr;                   //AIRLINE_13;
      CoreSim::Vdn::Bool	Center_Tank;             //AIRLINE_14;
      CoreSim::Vdn::Bool	Grid_Mora_Msa;           //AIRLINE_15;
      CoreSim::Vdn::Bool	Qnh_Qfe;                 //AIRLINE_16;
      CoreSim::Vdn::Bool	Rte_Retention;           //AIRLINE_17;
      CoreSim::Vdn::Bool	Gs_Before_Loc;           //AIRLINE_18; 
      //CoreSim::Vdn::BoolArray	Fg_Ac_Ident_1;        //AIRLINE_19	;
      CoreSim::Vdn::Bool	Kilowords_400;           //AIRLINE_20;
      CoreSim::Vdn::Bool	Speed_Prot;              //AIRLINE_21;
      CoreSim::Vdn::BoolArray	Fg_Ac_Ident_2;        //AIRLINE_22	;
      CoreSim::Vdn::Bool	Lip;                     //AIRLINE_23;
      CoreSim::Vdn::Bool	Aza;                     //AIRLINE_24;
      CoreSim::Vdn::Bool	Fm_Perfom;               //AIRLINE_25;
      CoreSim::Vdn::Bool	Fma_Enhanced;            //AIRLINE_26;
      CoreSim::Vdn::Bool	Derated_TO;              //AIRLINE_27;
      CoreSim::Vdn::Bool	No_AP_Disc_In_F_Des;     //NO_AP_DISC_IN_FDES;
      //CoreSim::Vdn::BoolArray	Ap_Tcas;              //AP_TCAS_FUNCTION_COM;   //AP_TCAS_FUNCTION_MON
      CoreSim::Vdn::Bool	Soft_GA;                 //SOFT_GO_AROUND;
      CoreSim::Vdn::Bool	AIRLINE_31;
      CoreSim::Vdn::Bool	AIRLINE_32;
      CoreSim::Vdn::Bool	AIRLINE_33;
      CoreSim::Vdn::Bool	AIRLINE_34;
      CoreSim::Vdn::Bool	AIRLINE_35;
      CoreSim::Vdn::Bool	AIRLINE_36;
      //CoreSim::Vdn::BoolArray	Jnb_Autoland;         //jnb_Autoland_COM    //JNB_Autoland_MON
      //CoreSim::Vdn::BoolArray	Nav_In_GA;            //NAV_IN_GA_COM;      //NAV_IN_GA_MON
      CoreSim::Vdn::Bool	SPI3C;
      //CoreSim::Vdn::Bool	Eng_Type_4;            //NEO_COM;
      CoreSim::Vdn::Bool	TRAFFIC_ADVISORY;
      CoreSim::Vdn::Bool	SPI6C;
      CoreSim::Vdn::Bool	Energy_Management;       //SPI8C;(AC9)
      CoreSim::Vdn::Bool	AUTO_BRAKE_ARMED;
      CoreSim::Vdn::Bool	SPI12C;
      CoreSim::Vdn::Bool	SPI13C;
      CoreSim::Vdn::Bool	SPI14C;
      CoreSim::Vdn::Bool	SPI16C;
      CoreSim::Vdn::Bool	SPI17C;
      CoreSim::Vdn::Bool	SPI18C;
      CoreSim::Vdn::Bool	SPI19C;
      CoreSim::Vdn::Bool	SPI20C;
      //CoreSim::Vdn::Bool	Ac_Type_1;         //AC1;
      //CoreSim::Vdn::Bool	Ac_Type_2;         //AC2;
      //CoreSim::Vdn::Bool	Eng_Type_1;           //AC3;
      //CoreSim::Vdn::Bool	Eng_Type_2;           //AC4;
      //CoreSim::Vdn::Bool	Eng_Type_3;           //AC5;
      CoreSim::Vdn::Bool	Vmo_mmo_1;               //AC6;
      CoreSim::Vdn::Bool	Vmo_mmo_2;               //AC7;
      CoreSim::Vdn::Bool	Strakes;                 //AC8;
      CoreSim::Vdn::Bool	Gps_1;                   //AC10;
      CoreSim::Vdn::Bool	Gps_2;                   //AC11;
      CoreSim::Vdn::Bool	PARITY_COM;
      CoreSim::Vdn::Bool	PARITY_MON;
      //CoreSim::Vdn::Bool	JNB_Autoland_MON;
      //CoreSim::Vdn::Bool	NAV_IN_GA_MON;
      //CoreSim::Vdn::Bool	AP_TCAS_FUNCTION_MON	;
      CoreSim::Vdn::Bool	AIRLINE37;
      CoreSim::Vdn::Bool	AIRLINE38;
      CoreSim::Vdn::Bool	AIRLINE39;
      CoreSim::Vdn::Bool	AIRLINE40;
      CoreSim::Vdn::Bool	Neo_Mon;                 //NEO_MON;      RT01J 
      CoreSim::Vdn::Bool	SPI9M;                   //SPARE FG MON  RT02J
      CoreSim::Vdn::Bool	SPI10M;                  //SPARE FG MON  RT02K
      CoreSim::Vdn::Bool	SPI11M;                  //SPARE FG MON  RT03J
      CoreSim::Vdn::Bool	SPI12M;                  //SPARE FG MON  RT03K
      CoreSim::Vdn::Bool	SPI13M;                  //SPARE FG MON  RT04J
      CoreSim::Vdn::Bool	SPI14M;                  //SPARE FG MON  RT04K
      CoreSim::Vdn::Bool	SPI15M;                  //SPARE FG MON  RT05J
    
      //CoreSim::Vdn::BoolArray Oil_Low_Press_Sw;       //ENGINE_OWN_STOP   //ENGINE_OPP_STOP
      //CoreSim::Vdn::BoolArray Own_Fwc_Valid;          //FWC_OWN_VALID     //FWC_OPP_VALID
      //CoreSim::Vdn::BoolArray Opp_Fwc_Valid;

      //CoreSim::Vdn::BoolArray Fac_Own_Hlty_Mon;       //FAC_OWN_HLTY_MON
      //CoreSim::Vdn::BoolArray Fac_Opp_Hlty_Mon;       //FAC_OPP_HLTY_MON
      //CoreSim::Vdn::BoolArray Fac_Own_Hlty_Com;       //FAC_OWN_HLTY_COM
      //CoreSim::Vdn::BoolArray Fac_Opp_Hlty_Com;       //FAC_OPP_HLTY_COM
      //CoreSim::Vdn::BoolArray Att_3_Sel;              //ATT_3_SW
      //CoreSim::Vdn::BoolArray Adc_3_Sel;              //ADC_3_SW
      //CoreSim::Vdn::BoolArray Nose_Gear_Pressed;      //NOSE_GEAR_OW   //NOSE_GEAR_OPP
      //CoreSim::Vdn::BoolArray Ap_Cmd_Sw_Mon;          //FCU_OWN_AP_SW_MON
      //CoreSim::Vdn::BoolArray Ap_Cmd_Sw_Com;          //FCU_OWN_AP_SW_COM
      //CoreSim::Vdn::BoolArray Athr_Sw_Mon;            //FCU_OWN_ATH_SW_MON
      //CoreSim::Vdn::BoolArray Athr_Sw_Com;            //FCU_OWN_ATH_SW_COM
      CoreSim::Vdn::BoolArray Takeover_Relay;         //ATH_INST_DISC_NC  //ATH_INST_DISC_NO
      //CoreSim::Vdn::BoolArray Wheel_Spd_Gt_70;        //RIGHT_WHEEL_SPEED   //LEFT_WHEEL_SPEED
      //CoreSim::Vdn::BoolArray BSCU_COM_Valid;         //BSCU_OWN_VALID_COM   //BSCU_OPP_VALID_COM
      //CoreSim::Vdn::BoolArray BSCU_MON_Valid;         //BSCU_OWN_VALID_MON   //BSCU_OPP_VALID_OPP
      CoreSim::Vdn::BoolArray DI_AP_OWN_ENGD_MON_OPP;  //AP_OPP_ENGD_MON_MON
      CoreSim::Vdn::BoolArray DI_AP_OWN_ENGD_COM_OPP;  //AP_OPP_ENGD_COM_MON
      //CoreSim::Vdn::BoolArray ELAC_OPP_AP_DISC_MON;   //ELAC_OPP_AP_DISC_MON
      //CoreSim::Vdn::BoolArray ELAC_OPP_AP_DISC_COM;   //ELAC_OPP_AP_DISC_COM
      //CoreSim::Vdn::BoolArray ELAC_OWN_AP_DISC_MON;   //ELAC_OWN_AP_DISC_MON
      //CoreSim::Vdn::BoolArray ELAC_OWN_AP_DISC_COM;   //ELAC_OWN_AP_DISC_COM
      //CoreSim::Vdn::BoolArray PS_SPLIT;
      CoreSim::Vdn::BoolArray ATH_INST_DISC_NC;
      CoreSim::Vdn::BoolArray ATH_INST_DISC_NO;
      CoreSim::Vdn::BoolArray ATH_OPP_ENGD_COM;
      CoreSim::Vdn::BoolArray ATH_OPP_ENGD_MON;
      CoreSim::Vdn::BoolArray AP_OPP_ENGD_MON_COM;
      CoreSim::Vdn::BoolArray AP_OPP_ENGD_COM_COM;
      CoreSim::Vdn::BoolArray FD_OPP_ENGD_COM;
      CoreSim::Vdn::BoolArray FD_OPP_ENGD_MON;
      CoreSim::Vdn::BoolArray PFD_OWN_VALID;
      CoreSim::Vdn::BoolArray PFD_OPP_VALID;
      //CoreSim::Vdn::BoolArray SIDE_1_COM;
      //CoreSim::Vdn::BoolArray NORTH_REF_PB;
      CoreSim::Vdn::BoolArray FCU_OWN_HLTY_IN;     
      CoreSim::Vdn::BoolArray FCU_OPP_HLTY;
      CoreSim::Vdn::BoolArray NAV_CONTROL_OWN;
      CoreSim::Vdn::BoolArray NAV_CONTROL_OPP;
      CoreSim::Vdn::BoolArray CDU_OPP_FAIL;
      CoreSim::Vdn::BoolArray CDU_OWN_FAIL;
      CoreSim::Vdn::Bool BSP_CONNEX; 

      CoreSim::Vdn::BoolArray Load_Enable;
      CoreSim::Vdn::BoolArray LB02_Power_28vdc;
      CoreSim::Vdn::Bool Fmgs_Reset;
      //Simsoft
      CoreSim::Vdn::Bool Flight_Freeze;
      CoreSim::Vdn::Bool Position_Freeze;
      CoreSim::Vdn::Bool Altitude_Freeze;
      CoreSim::Vdn::Bool Fuel_Freeze;
      CoreSim::Vdn::Bool Position_Set;
      CoreSim::Vdn::Bool Position_Slew;
      CoreSim::Vdn::Bool Altitude_Slew;
      CoreSim::Vdn::Bool Airspeed_Slew;
      CoreSim::Vdn::Bool Heading_Slew;
      CoreSim::Vdn::Bool Snapshot_Take;
      CoreSim::Vdn::Bool Snapshot_Recall;
      CoreSim::Vdn::Bool Speed_Times_4;
      CoreSim::Vdn::Bool ZFW_Set;
      CoreSim::Vdn::Bool Fuel_Set;
      CoreSim::Vdn::Bool Temp_Press_Set;
      CoreSim::Vdn::Bool Reposition;
      CoreSim::Vdn::Bool Wind_Set;
      CoreSim::Vdn::Bool Flight_Plan_Copy;
      CoreSim::Vdn::Bool Flight_Plan_Load;
 
      CoreSim::Vdn::A429Receiver Mcdu_Eis_bus[2];     //A429_MCDU1_EIS_BGN;A429_MCDU2_EIS_BGN
      CoreSim::Vdn::A429Receiver Dme_bus[2];     //dme_own  dme_opp
      CoreSim::Vdn::A429Receiver Dldr_bus[2];    //dataload_1
      CoreSim::Vdn::A429Receiver Print_1;        //print_1
      //CoreSim::Vdn::A429Receiver Clock_bus;        //clock_1
      CoreSim::Vdn::A429Receiver Atsu_acars[2];     //mu_1
   
      CoreSim::Vdn::A429Receiver Mcdu_bus[2];    //oncdu_fm   offcdu_fm
      InputsMcdu MCDU1_Bus;
      InputsMcdu MCDU2_Bus;
      //CoreSim::Vdn::A429Receiver gps_bus;     //gps_1   gps_2   gps_3
      InputsGps GPS1_Bus;
      InputsGps GPS2_Bus;
      InputsClock Clock_bus;

      P_BUS_429 Mcdu_eis;
      P_BUS_429 Dme;
      P_BUS_429 Atsu;
      //P_BUS_429 Clock;
   };

   class OutputsA429_BusA
   {
      public:
      //FMGC OWN A (HS) LM12A-12B on page 317--FWC,FAC,FCU,DMC,FMGC,HUD
      CoreSim::Vdn::A429Label Fmgc_Label040;   //city pair - CM: airpor1 - 0X0942
      CoreSim::Vdn::A429Label Fmgc_Label041;   //city pair - CM: airpor1
      CoreSim::Vdn::A429Label Fmgc_Label042;   //city pair - CM: airpor1
      CoreSim::Vdn::A429Label Fmgc_Label233;   //flight number - CM: flightno wd1- 0xf400095a
      CoreSim::Vdn::A429Label Fmgc_Label234;   //flight number - CM: flightno wd2
      CoreSim::Vdn::A429Label Fmgc_Label235;   //flight number - CM: flightno wd3
      CoreSim::Vdn::A429Label Fmgc_Label236;   //flight number - CM: flightno wd4
      CoreSim::Vdn::A429Label Fmgc_Label237;   //flight number - CM: flightno wd5
      //CoreSim::Vdn::A429Label Fmgc_Label_145;   //DIS 5
      CoreSim::Vdn::A429Label Fmgc_Label146;   //DIS 4
      CoreSim::Vdn::A429Label Fmgc_Label147;   //DIS 6
      CoreSim::Vdn::A429Label Fmgc_Label273;   //DIS 3
      CoreSim::Vdn::A429Label Fmgc_Label274;   //DIS 1
      CoreSim::Vdn::A429Label Fmgc_Label275;   //DIS 2
      CoreSim::Vdn::A429Label Fmgc_Label276;   //DIS 6
      CoreSim::Vdn::A429Label Fmgc_Label277;   //DIS 11
      CoreSim::Vdn::A429Label Fmgc_Label327;   //DIS 8
   };

   class OutputsA429_BusB
   {
   public:
    //FMGC OWN B (LS) LM14A-14B on page 330--FAC,ELAC,FMGC
      CoreSim::Vdn::A429Label Fmgc_Label075;   //WEIGHT(BNR) - CM: WIGHT - 0X0a02
      CoreSim::Vdn::A429Label Fmgc_Label077;   //CG    (BNR) - CM: CG    - 0X0A0A
      CoreSim::Vdn::A429Label Fmgc_Label146;   //DIS 4 (BNR)
      CoreSim::Vdn::A429Label Fmgc_Label147;   //DIS 6 (BNR)
      CoreSim::Vdn::A429Label Fmgc_Label164;   //RADIO HEIGHT (BNR)
      CoreSim::Vdn::A429Label Fmgc_Label210;   //DIS 5
      CoreSim::Vdn::A429Label Fmgc_Label270;   //ATS MODES DW
      CoreSim::Vdn::A429Label Fmgc_Label274;   //DIS 1
      CoreSim::Vdn::A429Label Fmgc_Label275;   //DIS 2
      CoreSim::Vdn::A429Label Fmgc_Label300;   //V2 FM (BNR)
      CoreSim::Vdn::A429Label Fmgc_Label304;   //APPROACH SPEED TARGET (BNR)
      CoreSim::Vdn::A429Label Fmgc_Label310;   //dP (AIL) VOTED COM (BNR)
      CoreSim::Vdn::A429Label Fmgc_Label312;   //dR VOTED COM  (BNR)
      CoreSim::Vdn::A429Label Fmgc_Label345;   //N1 LEFT ENGINE  (BNR)
      CoreSim::Vdn::A429Label Fmgc_Label346;   //N1 RIGHT ENGINE  (BNR)

   };

   class OutputsA429_BusC1
   {
      public:
      //FMGC OWN C (LS) LM10A-10B on page 334   //CM 16*32 bit. only has 13 labels below,what about others?
      CoreSim::Vdn::A429Label Fmgc_Label041;   //SET LATITUDE (BCD)
      CoreSim::Vdn::A429Label Fmgc_Label042;   //SET LONGITUDE (BCD)
      CoreSim::Vdn::A429Label Fmgc_Label043;   //SET MAGNETIC HEADING (BCD)
      CoreSim::Vdn::A429Label Fmgc_Label140;   //DESTINATION QNH (BCD)
      CoreSim::Vdn::A429Label Fmgc_Label150;   //UTC (BCD)
      CoreSim::Vdn::A429Label Fmgc_Label252;   //REMAINING TIME TO DESTINATION (BNR)
      CoreSim::Vdn::A429Label Fmgc_Label253;   //REMAINING TIME TO TOP OF CLIMB (BNR)
      CoreSim::Vdn::A429Label Fmgc_Label255;   //CRZ FL (BNR)
      CoreSim::Vdn::A429Label Fmgc_Label256;   //LANDING ELEVATION (BNR)
      CoreSim::Vdn::A429Label Fmgc_Label260;   //DATE/FLIGHT LEG (BNR)
      CoreSim::Vdn::A429Label Fmgc_Label270;   //DISCRETE WORD 1 (BNR)
      CoreSim::Vdn::A429Label Fmgc_Label310;   //PRESENT POS-LAT (BNR)
      CoreSim::Vdn::A429Label Fmgc_Label311;   //PRESENT POS-LONG (BNR)
   };

   class OutputsA429_BusC2
   {
   public:
   //FMGC OWN C (LS) LM11A-11B on page 334
      CoreSim::Vdn::A429Label Fmgc_Label017;   //RUNWAY HEADING (BCD)
      CoreSim::Vdn::A429Label Fmgc_Label033;   // (HYB)
      CoreSim::Vdn::A429Label Fmgc_Label034;   //VOR FREQUENCY  (BCD)
      CoreSim::Vdn::A429Label Fmgc_Label100;   //SELECTED COURSE (BNR)
      CoreSim::Vdn::A429Label Fmgc_Label105;   //RUNWAY HEADING  (BNR)
      CoreSim::Vdn::A429Label Fmgc_Label150;   //UTC (BCD)
      CoreSim::Vdn::A429Label Fmgc_Label260;   //DATE/FLIGHT LEG (BNR)
      CoreSim::Vdn::A429Label Fmgc_Label310;   //PRESENT POS-LAT (BNR)
      CoreSim::Vdn::A429Label Fmgc_Label311;   //PRESENT POS-LONG (BNR)
      CoreSim::Vdn::A429Label Fmgc_Label320;   //BARO TEMP CORRECTED A/C ALTITUDE (BNR)
      CoreSim::Vdn::A429Label Fmgc_Label323;   //FLS AP IDENT 1 (iso 5)
      CoreSim::Vdn::A429Label Fmgc_Label324;   //FLS AP IDENT 2 (iso 5)
      CoreSim::Vdn::A429Label Fmgc_Label325;   //FLS AP LAT (BNR)
      CoreSim::Vdn::A429Label Fmgc_Label326;   //FLS AP LONG (BNR)
      CoreSim::Vdn::A429Label Fmgc_Label327;   //FLS AP ALTITUDE (BNR)
      CoreSim::Vdn::A429Label Fmgc_Label330;   //FLS BEAM SLOPE (BNR)
      CoreSim::Vdn::A429Label Fmgc_Label331;   //LOCAL MAGNETIC DEVIATION (BNR)
      CoreSim::Vdn::A429Label Fmgc_Label332;   //APPROACH IDENTIFIER (BNR)
      CoreSim::Vdn::A429Label Fmgc_Label333;   //RUNWAY THRESHOLD LAT (BNR)
      CoreSim::Vdn::A429Label Fmgc_Label334;   //RUNWAY THRESHOLD LONG (BNR)
      CoreSim::Vdn::A429Label Fmgc_Label335;   //PRESENT POSITION LAT FINE (BNR)
      CoreSim::Vdn::A429Label Fmgc_Label336;   //PRESENT POSITION LONG FINE (BNR)
      CoreSim::Vdn::A429Label Fmgc_Label351;   //DIS 3

   };

   class OutputsA429_Mcdu
   {
   public:
      //CoreSim::Vdn::A429Transmitter Mcdu_bus;
      CoreSim::Vdn::A429Label Label044;
      CoreSim::Vdn::A429Label Label125;
      CoreSim::Vdn::A429Label Label144;
      CoreSim::Vdn::A429Label Label172;
      CoreSim::Vdn::A429Label Label220;
      CoreSim::Vdn::A429Label Label221;
      CoreSim::Vdn::A429Label Label222;
      CoreSim::Vdn::A429Label Label244;
      CoreSim::Vdn::A429Label Label260;
   };

   class Outputs
   {
   public:
      Outputs():
      RTC(0),
      IsInitial(false),
      bus_Number(0),
      Label_Number(0),
      fpsave_name(""),
      Fmgc_Resets(false),
      is_Fmgc_Resets(true)
      {  

      }
      CoreSim::Vdn::Int64 RTC;
      CoreSim::Vdn::Bool  IsInitial;

      CoreSim::Vdn::Int64 bus_Number;

      CoreSim::Vdn::Int64 Label_Number;
 
      //string 
      CoreSim::Osal::String fpsave_name;
      //Resets
      CoreSim::Vdn::Bool Fmgc_Resets;
      CoreSim::Vdn::Bool is_Fmgc_Resets;
      /*
      CoreSim::Osal::UInt32 Mcdu_Data[2];

      CoreSim::Osal::UInt32 Data_Loader;

      CoreSim::Osal::UInt32 Efis_1;

      CoreSim::Osal::UInt32 Printer;

      CoreSim::Osal::UInt32 Ads;

      CoreSim::Osal::UInt32 Fmgc[2];
      */      

      OutputsA429_Mcdu Mcdu;

      CoreSim::Vdn::A429Transmitter Mcdu_bus[2];

      CoreSim::Vdn::A429Transmitter Dataload;

      CoreSim::Vdn::A429Transmitter Efis;

      CoreSim::Vdn::A429Transmitter Printer;

      CoreSim::Vdn::A429Transmitter Ads;

      OutputsA429_BusA A429_Bus_Aout;

      OutputsA429_BusB A429_Bus_Bout;

      OutputsA429_BusC1 A429_Bus_C1out;

      OutputsA429_BusC2 A429_Bus_C2out;

      CoreSim::Vdn::A429Transmitter Fmgc_A[2];   //fmgc1 own a; fmgc2 own a
  
      CoreSim::Vdn::A429Transmitter Fmgc_B[2];

      CoreSim::Vdn::A429Transmitter Fmgc1_C;

      CoreSim::Vdn::A429Transmitter Fmgc2_C;

   }; 

   class Locals
   {
   public:
      Locals():
      fm_puprdy_fm(false),
      fm_hlty_fm(false),
      al_Pins_Dis1(0),
      al_Pins_Dis2(0),
      ac_Pins_Dis(0),
      cfm_engine(false),
      iae_engine(false),
      pw_engine(false),
      di_word0_bus66(0),
      di_word1_bus68(0),
      time_usd(0)
      {  
      }
      CoreSim::Vdn::Bool fm_puprdy_fm;
      CoreSim::Vdn::Bool fm_hlty_fm;

      CoreSim::Vdn::Int64 al_Pins_Dis1;
      CoreSim::Vdn::Int64 al_Pins_Dis2;
      CoreSim::Vdn::Int64 ac_Pins_Dis;
      CoreSim::Vdn::Bool cfm_engine;
      CoreSim::Vdn::Bool iae_engine;
      CoreSim::Vdn::Bool pw_engine;
      CoreSim::Vdn::Int64 di_word0_bus66;
      CoreSim::Vdn::Int64 di_word1_bus68;
      CoreSim::Vdn::Int64 time_usd;
   
   };

   Common_Data m_commoms;

   Common_Data2 m_commoms2;

   Dis_Data m_disData;

   FP_SAVE_Data m_fpsave;

   Pwr_Data m_pwrData;

   MsgBufHeader m_RevBufSize;

   Msg_Header m_recvMsgHeader;

   P_Version p_version;

   P_SIM_CLOCK p_clock;

   P_END_FRAME p_end_frame;

   P_TEXT p_text;

   P_BUS_429 p_bus_429;

   P_BUS_DIS p_bus_dis;

   P_BUS_ANA p_bus_ana;

   P_SCP_RESPOND p_scp_respond;

   Inputs m_inputs;

   Outputs m_outputs;

   Locals m_locals;

   std::vector <CoreSim::Osal::String> cm_nameList;
   std::vector <CoreSim::Osal::Int32>  cm_busList;
   std::vector <CoreSim::Osal::String> cm_labelList;
   std::vector <CoreSim::Osal::Int32>  cm_sdiList;
   std::vector <CoreSim::Osal::String> cm_sourAddrList;
   std::vector <CoreSim::Osal::Int16>  cm_lengthList;
   std::vector <CoreSim::Osal::String> cm_commList;
   std::vector <CoreSim::Osal::String> cm_ioList;

   //TCP socket
   CoreSim::Osal::Net::TcpSocket m_txTcpSocket;
   CoreSim::Osal::Net::SocketAddress m_sendSocketAddress;

   FILE *fmlog;   //FM log file

   //CoreSim::Osal::Int32 Num_A429_MCDU1;

   int FMConnAndSendInitial(FG* fg,CoreSim::Osal::String fm_server_ip,CoreSim::Osal::UInt16 fm_server_port);
   CoreSim::Osal::Bool subscribeInputsFM(CoreSim::NetVdn::NetVdnClient& vdn);
   void packInitialDatas(Common_Data& m_commoms);
   void packInitialDatas2(Common_Data2& m_commoms2);
   void packDisDatas(Dis_Data& m_disData);
   void packPwrOffData(Pwr_Data& m_pwrData,CoreSim::Osal::UChar m_CAB);
   void packPwrOnData(Pwr_Data& m_pwrData,CoreSim::Osal::UChar m_CAB);
   //void packFPSaveData(FP_SAVE_Data& m_fpsave,CoreSim::Osal::String filename);
   int RecvDataFromFM(FG* fg);
   int unpackDatas(RecvDataMsg *dataBuf,CoreSim::Osal::Int32 num_msgs,FG* fg);
   template <typename T>
   CoreSim::Osal::Int16 packDisData(T& input,FILE* fm);
   template <typename T>
   void packMsg(T& bus,CoreSim::Osal::UChar m_msgType,CoreSim::Osal::UChar m_CAB,CoreSim::Osal::UChar m_BusNum1,CoreSim::Osal::UChar m_numWords1,CoreSim::Osal::UChar m_numWords2,CoreSim::Osal::UChar m_BusNum2);
   template <typename T>
   void packMsgBufHeader(T& bus,CoreSim::Osal::UInt32 m_numBytes,CoreSim::Osal::UInt16 m_numWORDS,CoreSim::Osal::UInt16 m_numMSGS);
   template <typename T>
   void setBusData(T& bus,CoreSim::Osal::UInt32 bus_data);
   template <typename T>
   int setBusNumber(T& bus,CoreSim::Osal::UInt32 m_busNum);
   template <typename T>
   void setCab(T& bus,CoreSim::Osal::UChar m_CAB);
   template <typename T>
   void setMsg(T& bus,CoreSim::Osal::UInt32 m_msgType);
   template <typename T>
   void setNumWords(T& bus,CoreSim::Osal::UInt32 m_numWords);
   template <typename T>
   CoreSim::Osal::Int16 packCMDatas(T& input,P_BUS_429 &bus,CoreSim::Osal::UInt32 labelnum,FILE *fm,CoreSim::Osal::UInt32 m_busNum);
   CoreSim::Osal::Int64 findAddr(CoreSim::Osal::Int32 bus,CoreSim::Osal::UInt16 Label);
   CoreSim::Osal::UInt32 getLabel(CoreSim::Osal::UInt32 Data);
   void reverseLabel(void);
   CoreSim::Osal::UInt32 bytes4ToInt(CoreSim::Osal::UInt32 RavData);
   CoreSim::Osal::UInt32 reverseBit8(CoreSim::Osal::UInt32 n);
   CoreSim::Osal::UInt32 FM::reverseMalSal(CoreSim::Osal::UInt32 n);
   CoreSim::Osal::UInt16 bytes2ToInt(CoreSim::Osal::UInt16 RavData);
   void getComMemory(CoreSim::NetVdn::NetVdnClient& vdn);
   void GetFiles(CoreSim::Osal::String path,CoreSim::Osal::String filter,vector<CoreSim::Osal::String>& filesname,vector<CoreSim::Osal::String>& files);
   void Initialization(FG* fg);
   CoreSim::Osal::Bool publishoutputs(CoreSim::NetVdn::NetVdnClient& vdn);
   void setBit(CoreSim::Osal::UInt32 &des, int bit, int value);  
   CoreSim::Osal::UInt32 getSdi(CoreSim::Osal::UInt32 A429Data);
   CoreSim::Osal::UInt32 getValue(CoreSim::Osal::UInt32 A429Data);
   CoreSim::Osal::String FM::ConfigDataFromCsv(CoreSim::Osal::String path, CoreSim::Osal::String name);
   CoreSim::Osal::Int16 FM::tcpCommunication(CoreSim::Osal::Int32 recMsgType,Common_Data& m_comdata,Common_Data2& m_comdata2,Dis_Data& m_disdat,FILE *fmlog);
   FM();
   ~FM();
};

extern InputsA429 m_inputsA429;
extern CoreSim::Osal::UInt32 al_Pins_Dis1;
extern CoreSim::Osal::UInt32 al_Pins_Dis2;
extern CoreSim::Osal::UInt32 ac_Pins_Dis;
extern CoreSim::Osal::UInt32 cfm_engine;
extern CoreSim::Osal::UInt32 iae_engine;
extern CoreSim::Osal::UInt32 pw_engine;
extern CoreSim::Osal::UInt32 di_word0_bus66;
extern CoreSim::Osal::UInt32 di_word1_bus68;