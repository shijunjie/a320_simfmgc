@echo off
@setlocal 
REM Copyright 2015-2017 Rockwell Collins. All rights reserved.
REM Create a junction called sys in the current directory which targets the VS2012 (v110) sys dir in the specified SDK directory.
REM If no command line argument is specified, the default version is used. If command line arguments are specified
REM the version is used.
REM -----------------------------------------------------------------------------
REM Usage:
REM create_sdk_link.bat <version>
REM where version is X.Y.Z (SSA-SDK-X.Y.Z or SSA-RTE-X.Y.Z)
REM -----------------------------------------------------------------------------

REM Set defaults.
SET DEFAULT_VERSION=4.4.0
SET DEFAULT_PLATFORM=110

REM Set SDK/RTE version and Visual Studio platform.
IF "%1"=="" (SET VERSION=%DEFAULT_VERSION%) ELSE (SET VERSION=%1%)
SET PLATFORM=%DEFAULT_PLATFORM%

REM If 'sys' junction already exists, remove it.
if EXIST sys (
  rmdir /s/q sys
)

REM Create a junction to the desired SDK/RTE version and Visual Studio platform.
mklink /J sys c:\RockwellCollins\CORESIM\SSA-SDK-%VERSION%\%PLATFORM%\sys
