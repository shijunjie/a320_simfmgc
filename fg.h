/*****************************************************************************
*                                                                            *
*  COPYRIGHT 2018 ACCEL FLIGHT SIMULATION. ALL RIGHTS RESERVED               *
*                                                                            *
*  This file is part of A320 Neo Sim FMGC simulation                         *
*                                                                            *
*  All information and content in this document is the Property              *
*  to ACCEL (Tianjin) Flight Simulation Co., Ltd. and shall not              *
*  be disclosed, disseminated, copied, or used except for purposes           *
*  expressly authorized in writing by ACCEL. Any unauthorized use            *
*  of the content will be considered an infringement of                      *
*  ACCEL's intellectual property rights.                                     *
*                                                                            *
*                                                                            *
*  @file     fg.h                                                            *
*  @brief    Entry point for the NetVdn Application.                         *
*                                                                            *
*            This is a Sim FM example of how to use the NetVdnClient to      *
*            publish and subscribe to data on the Virtual Data Network.      *
*                                                                            *
*            This example includes publishing and subscribing to variables   *
*            as well as sending and receiving requests.                      *
*                                                                            *
*  @author   Shi, Junjie                                                     *
*  @email    junjie.shi@accelflightsimulation.com                            *
*  @version  1.0.0.0(version)                                                *
*  @date     2018/07/21                                                      *
*                                                                            *
*----------------------------------------------------------------------------*
*  Remark         : Description                                              *
*----------------------------------------------------------------------------*
*  Change History :                                                          *
*  <Date>     | <Version> | <Author>       | <Description>                   *
*----------------------------------------------------------------------------*
*  1/15/2019  | 1.0.0.0   | Shi, Junjie   | Create file                      *
*----------------------------------------------------------------------------*
*                                                                            *
*****************************************************************************/
#pragma once
#include "SimElement/Element.h"
#include "Vdn/VdnClient.h"
#include "Osal/Threading/Mutex.h"
#include "Osal/Net/SocketAddress.h"
#include "Osal/Net/UdpSocket.h"
#include "Utils/CsvFile.h"
#include "NetVdn/NetVdnClient.h"
#include "Header.h"
#include <io.h>
#include <string.h>
using namespace CoreSim;
using namespace std;

#define UDP_SOCKET_NUMBER_DIS  19         
#define UDP_SOCKET_NUMBER_A429 12
#define UDP_SOCKET_TOTAL_DIS   34        
#define UDP_SOCKET_TOTAL_DIS_IN 130     
#define UDP_SOCKET_TOTAL_A429_IN 35

#define P_FM_FG_SHARED_MEM_FM_OUT 10183
#define P_FM_FG_SHARED_MEM_IO_OUT 10184
#define P_COMMAND_SIMSOFT_OUT_COM 10231
#define P_COMMAND_SIMSOFT_OUT_MON 10233
#define P_COMMAND_SIMSOFT_OUT_IO  10179
#define P_COM_FMGC_OWN_A_OUT      10175
#define P_COM_FMGC_OWN_B_OUT      10176
#define P_COM_FMGC_OWN_C_OUT      10178
#define P_COM_FT_GUID_OUT         10180
#define P_COM_FT_INNER_OUT        10177
#define P_MON_FMGC_MON_OUT        10181
#define P_MON_FT_MONITOR_OUT      10182

class FG : public FMFG
{
public:
   FG():
    fg_safeTest(0),
    AddrTest(false)
   {
   }

   class FGDI
   {
   public:
	   FGDI()
	   {
		   for (int i = 0; i < DummyMaxValue; i++)
		   {
			   CoreSim::Vdn::Int64 b = 0;
			   CoreSim::Osal::Net::SocketAddress adr;
			   CoreSim::Osal::String s = "";
			   diList.push_back(b);
			   addrList.push_back(adr);
			   nameList.push_back(s);
			   sizeList.push_back(b);
			   addrList2.push_back(adr);
			   nameList2.push_back(s);
		   }
	   };
	   enum DI
	   {
         RM08F_SIDE_1_MON,
         RM07G_AIRLINE_1,
         RM07H_AIRLINE_2,
         RM07J_AIRLINE_3,
         RM07K_AIRLINE_4,
         RM08G_AIRLINE_5,
         RM08H_AIRLINE_6,
         RM08J_AIRLINE_7,
         RM09C_AIRLINE_8,
         RM09D_AIRLINE_9,
         RM09E_AIRLINE_10,
         RM09F_AIRLINE_11,
         RM09G_AIRLINE_12,
         RM09H_AIRLINE_13,
         RM09J_AIRLINE_14,
         RM09K_AIRLINE_15,
         RM10C_AIRLINE_16,
         RM10D_AIRLINE_17,
         RM10E_AIRLINE_18,
         RM10F_AIRLINE_19,
         RM10G_AIRLINE_20,
         RM10H_AIRLINE_21,
         RM10J_AIRLINE_22,
         RM10K_AIRLINE_23,
         RM11E_AIRLINE_24,
         RM11F_AIRLINE_25,
         RM11G_AIRLINE_26,
         RM11H_AIRLINE_27,
         RM11J_NO_AP_DISC_IN_FDES,
         RM11K_AP_TCAS_FUNCTION_COM,
         RM06G_SOFT_GO_AROUND,
         RM06H_AIRLINE_31,
         RM06J_AIRLINE_32,
         RM06K_AIRLINE_33,
         RM08C_AIRLINE_34,
         RM08D_AIRLINE_35,
         RM08E_AIRLINE_36,
         LM01B_JNB_Autoland_COM,
         LM02B_NAV_IN_GA_COM,
         LM03A_SPI3C,
         LM04A_NEO_COM,
         LM04B_TRAFFIC_ADVISORY,
         LM05B_SPI6C,
         LM08A_SPI8C,
         LM08E_AUTO_BRAKE_ARMED,
         LM09C_SPI12C,
         LM09D_SPI13C,
         LM09E_SPI14C,
         LT04B_SPI16C,
         LT05B_SPI17C,
         LT06C_SPI18C,
         LT06D_SPI19C,
         LT06A_SPI20C,
         LM05A_AC1,
         LM06A_AC2,
         LM06B_AC3,
         LM06C_AC4,
         LM07A_AC5,
         LM07B_AC6,
         LM07C_AC7,
         LM07D_AC8,
         LM08B_AC10,
         LM08C_AC11,
         LM08D_PARITY_COM,
         RM08K_PARITY_MON,
         RM01J_JNB_Autoland_MON,
         RM02J_NAV_IN_GA_MON,
         RM02K_AP_TCAS_FUNCTION_MON,
         RM03J_AIRLINE37,
         RM03K_AIRLINE38,
         RM04K_AIRLINE39,
         RM05K_AIRLINE40,
         RT01J_NEO_MON,
         RT02J_SPI9M,
         RT02K_SPI10M,
         RT03J_SPI11M,
         RT03K_SPI12M,
         RT04J_SPI13M,
         RT04K_SPI14M,
         RT05J_SPI15M,
         LT01C_FAC_OPP_HLTY_COM,
         RT01C_FAC_OPP_HLTY_MON,
         LT01F_ATH_OPP_ENGD_COM,  
         RT01F_ATH_OPP_ENGD_MON,  
         LT02C_ELAC_OPP_AP_DISC_COM,
         RT02C_ELAC_OPP_AP_DISC_MON,
         LT03G_AP_OPP_ENGD_MON_COM,
         LT02E_AP_OPP_ENGD_COM_COM,
         LT04E_FD_OPP_ENGD_COM,
         RT04E_FD_OPP_ENGD_MON,
         LT04G_BSCU_OPP_VALID_COM,
         RT04G_BSCU_OPP_VALID_MON,
         LM01C_FAC_OWN_HLTY_COM,
         RM01C_FAC_OWN_HLTY_MON,
         LM02C_ELAC_OWN_AP_DISC_COM,
         RM02C_ELAC_OWN_AP_DISC_MON,
         LM04G_BSCU_OWN_VALID_COM,
         RM04G_BSCU_OWN_VALID_MON,
         LM05G_FCU_OWN_AP_SW_COM,
         LM01H_AP_INST_DISC_NC,
         LM05C_AP_INST_DISC_NO,
         RT02F_RIGHT_WHEEL_SPEED,
         RM02F_LEFT_WHEEL_SPEED,
         LM02G_FWC_OWN_VALID,
         LT02G_FWC_OPP_VALID,
         LM02H_PFD_OWN_VALID,
         LT02H_PFD_OPP_VALID,
         LM03B_PS_SPLIT,
         LM03D_FCU_OWN_ATH_SW_COM,
         RM05C_ATH_INST_DISC_NC,
         RM03H_ATH_INST_DISC_NO,
         RM04J_ATT_3_SW,
         LM04D_NOSE_GEAR_OWN,
         RM05J_ADC_3_SW,
         LM06D_SIDE_1_COM,
         LT01B_NORTH_REF_PB,
         LM04F_FCU_OWN_HLTY,
         RT04F_FCU_OPP_HLTY,
         RM06F_ENGINE_OWN_STOP,
         RT06F_ENGINE_OPP_STOP,
         LT02A_CDU_OPP_FAIL,
         LM07E_NAV_CONTROL_OWN,
         LT06B_NAV_CONTROL_OPP,
         LM02A_CDU_OWN_FAIL,
         LT02B_FMGC_OPP_HLTY,
         BSP_CONNEX,
         RT03G_AP_OPP_ENGD_MON_MON,
         RT02E_AP_OPP_ENGD_COM_MON,
         RM05G_FCU_OWN_AP_SW_MON,
         RM03D_FCU_OWN_ATH_SW_MON,
         COM_ADC_OWN_IN,
         COM_ADC_3_IN,
         COM_FAC_OWN_IN,
         COM_FAC_OPP_IN,
         COM_FADEC_OWN_A_IN,
         COM_FADEC_OWN_B_IN,
         COM_FCU_OWN_A_IN,
         COM_FCU_OWN_B_IN,
         COM_FMGC_MON_IN,
         COM_FMGC_OPP_IN,
         COM_FQI_IN,
         COM_ILS_OWN_IN,
         COM_ILS_OPP_IN,
         COM_IRS_OWN_IN,
         COM_IRS_OPP_IN,
         COM_IRS_3_IN,
         COM_RA_OWN_IN,
         COM_RA_OPP_IN,
         COM_TCAS_IN,
         COM_VOR_OWN_IN,
         COM_VOR_OPP_IN,
         COM_SPATIAL_1_IN,
         COM_SPATIAL_2_IN,
         MON_TCAS_OPP_IN,
         MON_ADC_OPP_IN,
         MON_FADEC_OPP_A_IN,
         MON_FADEC_OPP_B_IN,
         MON_FMGC_OWN_A_IN,
         MON_FMGC_OWN_B_IN,
         FM_FG_SHARED_MEM_IO_IN,
         FM_FG_SHARED_MEM_FM_IN,
         COMMAND_SIMSOFT,
         SNAPSHOT_IN_COM,
         SNAPSHOT_IN_MON,
         SNAPSHOT_IN_IO,
         DummyMaxValue
	   };

      std::vector<CoreSim::Vdn::Int64> diList;
	   std::vector<CoreSim::Osal::Net::SocketAddress> addrList;
      std::vector<CoreSim::Osal::String> nameList;
      std::vector<CoreSim::Osal::UInt16> sizeList;
      std::vector<CoreSim::Osal::Net::SocketAddress> addrList2;
      std::vector<CoreSim::Osal::String> nameList2;

      ~FGDI()
      {};
   };

   class FGDO
   {
   public:
	   FGDO()
	   {
		   for (int i = 0; i < DummyMaxValue; i++)
		   {
            CoreSim::Vdn::Int64 b;
			   CoreSim::Osal::Net::SocketAddress adr;
            CoreSim::Osal::String s;
            CoreSim::Osal::Bool v;
			   doList.push_back(b);
			   addrList.push_back(adr);
            nameList.push_back(s);
            listenList.push_back(s);
            sizeList.push_back(b);
            valueList.push_back(v);
            //if(i<UDP_SOCKET_NUMBER_DIS)
            {            
               clientaddrList.push_back(adr);
            }
		   }
	   };
	   enum DO
	   {
         ATH_OWN_ENGD_COM_OPP_I,
         ATH_OWN_ENGD_COM_OWN_I,
         AP_OWN_ENGD_COM_OPP_I,
         AP_OWN_ENGD_COM_OWN_I,
         FD_OWN_ENGD_COM_OPP_I,
         FD_OWN_ENGD_COM_OWN_I,
         LS_INH_I,
         FCU_OWN_HLT_I,
         FMGC_HLTY_I,
         SPO1C_I,
         SPO2C_I,
         SPO3C_I,
         SPO4C_I,
         SPO5C_I,
         SPO6C_I,
         SPO7C_I,
         SPO8C_I,
         SPO9C_I,
         SPO10C_I,
         ATH_OWN_ENGD_MON_OPP_I,
         ATH_OWN_ENGD_MON_OWN_I,
         AP_OWN_ENGD_MON_OPP_I,
         AP_OWN_ENGD_MON_OWN_I,
         FD_OWN_ENGD_MON_OPP_I,
         FD_OWN_ENGD_MON_OWN_I,
         STICK_LOCK_MON_I,
         RUDDER_LOCK_MON_I,
         SPO1M_I,
         SPO2M_I,
         SPO3M_I,
         SPO4M_I,
         SPO5M_I,
         STICK_LOCK_COM_I,
         RUDDER_LOCK_COM_I,
         COM_FMGC_OWN_A_OUT,
         COM_FMGC_OWN_B_OUT,
         COM_FMGC_OWN_C_OUT,
         COM_FT_GUID_OUT,
         COM_FT_INNER_OUT,
         MON_FMGC_MON_OUT,
         MON_FT_MONITOR_OUT,
         FM_FG_SHARED_MEM_IO_OUT,
         FM_FG_SHARED_MEM_FM_OUT,
         COMMAND_SIMSOFT_OUT_COM,
         COMMAND_SIMSOFT_OUT_MON,
         COMMAND_SIMSOFT_OUT_IO,   
         DummyMaxValue
	   };

      std::vector<CoreSim::Vdn::Int64> doList;
	   std::vector<CoreSim::Osal::Net::SocketAddress> addrList;
      std::vector<CoreSim::Osal::String> nameList;
      std::vector<CoreSim::Osal::String> listenList;
      std::vector<CoreSim::Vdn::Int64> sizeList;
      //to store the DIS value that received form FG
      std::vector<CoreSim::Osal::Bool> valueList;
      //to store the address that the DIS value received form FG
      std::vector<CoreSim::Osal::Net::SocketAddress> clientaddrList;

      ~FGDO()
      {};
   };

   class Outputs
   {
   public:
      Outputs():
      RTC(0),
      IsInitial(false),
      RM14D_FD_OWN_ENGD_MON(2,false),
      LM14D_FD_OWN_ENGD_COM(2,false),
      RT14D_AP_OWN_ENGD_MON_OPP(2,false),
      LT14B_AP_OWN_ENGD_COM_OPP(2,false),
      RM12G_AP_OWN_ENGD_MON(2,false),
      LM10D_AP_OWN_ENGD_COM(2,false),
      RM12E_ATHR_OWN_ENGD_MON(2,false),
      LM12D_ATHR_OWN_ENGD_COM(2,false),
      RM13D_RUDDER_LOCK_MON(2,false),
      LM13D_RUDDER_LOCK_COM(2,false),
      RM15C_STICK_LOCK_MON(2,false),
      LM15C_STICK_LOCK_COM(2,false),
      LT15C_FD_OWN_ENGD_COM_OPP(2,false),
      LM09A_LS_INHIB(2,false),
      LM12H_FMGC_HEALTHY(2,false),
      LM14D_ATH_OWN_ENGD_COM_OPP(2,false),
      RT15E_FD_OWN_ENGD_MON_OPP_I(2,false),
      LM09B_FCU_OWN_HLTY(2,false),
      LM13H_FMGC_CROSS_LOAD_ENABLE(2,false),
      LT14D_ATH_OWN_ENGD_COM_OPP(2,false),
      RT14F_ATH_OWN_ENGD_MON_OPP(2,false),
      FMGC_Fault_Malf(2,false),
      No_Rollout_Malf(false),
      Land_Capability_Malf(false),
      FG_Pup_Rdy(false),
      FG_SafeTest(false),
      FG_hlty_fg(false),
      FMGC_Healthy(2,false)
      {  

      }
      CoreSim::Vdn::Int64 RTC;
      CoreSim::Vdn::Bool  IsInitial;

      CoreSim::Vdn::BoolArray RM14D_FD_OWN_ENGD_MON; 
      CoreSim::Vdn::BoolArray LM14D_FD_OWN_ENGD_COM;
      CoreSim::Vdn::BoolArray RT14D_AP_OWN_ENGD_MON_OPP;
      CoreSim::Vdn::BoolArray LT14B_AP_OWN_ENGD_COM_OPP;
      CoreSim::Vdn::BoolArray RM12G_AP_OWN_ENGD_MON;
      CoreSim::Vdn::BoolArray LM10D_AP_OWN_ENGD_COM;
      CoreSim::Vdn::BoolArray RM12E_ATHR_OWN_ENGD_MON;
      CoreSim::Vdn::BoolArray LM12D_ATHR_OWN_ENGD_COM;
      CoreSim::Vdn::BoolArray RM13D_RUDDER_LOCK_MON;
      CoreSim::Vdn::BoolArray LM13D_RUDDER_LOCK_COM;
      CoreSim::Vdn::BoolArray RM15C_STICK_LOCK_MON;
      CoreSim::Vdn::BoolArray LM15C_STICK_LOCK_COM;
      CoreSim::Vdn::BoolArray LT15C_FD_OWN_ENGD_COM_OPP;
      CoreSim::Vdn::BoolArray LM09A_LS_INHIB;
      CoreSim::Vdn::BoolArray LM12H_FMGC_HEALTHY;
      CoreSim::Vdn::BoolArray LM14D_ATH_OWN_ENGD_COM_OPP;
      CoreSim::Vdn::BoolArray RT15E_FD_OWN_ENGD_MON_OPP_I;
      CoreSim::Vdn::BoolArray LM09B_FCU_OWN_HLTY;
      CoreSim::Vdn::BoolArray LM13H_FMGC_CROSS_LOAD_ENABLE;
      CoreSim::Vdn::BoolArray LT14D_ATH_OWN_ENGD_COM_OPP;
      CoreSim::Vdn::BoolArray RT14F_ATH_OWN_ENGD_MON_OPP;

      CoreSim::Vdn::Bool Test;

      //FM-FG-PHERIPH A-BUSS
      CoreSim::Vdn::A429Label Label_0040;     //CITYPAIR WD1
      CoreSim::Vdn::A429Label Label_0041;     //CITYPAIR WD2
      CoreSim::Vdn::A429Label Label_0042;     //CITYPAIR WD3
      CoreSim::Vdn::A429Label Label_0233;     //FLIGHTNO WD1
      CoreSim::Vdn::A429Label Label_0234;     //FLIGHTNO WD2
      CoreSim::Vdn::A429Label Label_0235;     //FLIGHTNO WD3
      CoreSim::Vdn::A429Label Label_0236;     //FLIGHTNO WD4
      CoreSim::Vdn::A429Label Label_0237;     //FLIGHTNO WD5
      //FM-FG-PHERIPH B-BUSS
      CoreSim::Vdn::A429Label Label_0075;     //WEIGHT
      CoreSim::Vdn::A429Label Label_0077;     //C_OF_G
      //FM-FG-PHERIPH C-BUSS
      CoreSim::Vdn::A429Label Label_0167;     //WPT ETA                          BNR
      CoreSim::Vdn::A429Label Label_0216;     //WPT LONGSEE                      BNR
      CoreSim::Vdn::A429Label Label_0230;     //NAV DATA BASE EFFECTIVE END DATE
      CoreSim::Vdn::A429Label Label_0251;     //REMAINING TIME FROM PPOS TO DEST BNR
      CoreSim::Vdn::A429Label Label_0320;     //BARO TEMP CORRECTED A/C ALTITUDE BNR
      CoreSim::Vdn::A429Label Label_0323;     //FLS AP IDENT 1                   ISO 5
      CoreSim::Vdn::A429Label Label_0324;     //FLS AP IDENT 2                   ISO 5
      CoreSim::Vdn::A429Label Label_0325;     //FLS AP LAT                       BNR                    
      CoreSim::Vdn::A429Label Label_0326;     //FLS AP LONG                      BNR
      CoreSim::Vdn::A429Label Label_0327;     //FLS AP ALTITUDE                  BNR
      CoreSim::Vdn::A429Label Label_0330;     //FLS BEAM SLOPE                   BNR
      CoreSim::Vdn::A429Label Label_0331;     //LOCAL MAGNETIC DEVIATION         BNR
      CoreSim::Vdn::A429Label Label_0333;     //RUNWAY THRESHOLD LAT             BNR
      CoreSim::Vdn::A429Label Label_0334;     //RUNWAY THRESHOLD LONG            BNR
      CoreSim::Vdn::A429Label Label_0335;     //PRESENT POSITION LAT FINE        BNR
      CoreSim::Vdn::A429Label Label_0336;     //PRESENT POSITION LONG FINE       BNR
      //TODO: NUM_C-BUSS
      //malfunction
      CoreSim::Vdn::BoolArray FMGC_Fault_Malf;
      CoreSim::Vdn::Bool No_Rollout_Malf;
      CoreSim::Vdn::Bool Land_Capability_Malf;
      //For debug
      CoreSim::Vdn::Int64 FG_Pup_Rdy;
      CoreSim::Vdn::Int64 FG_SafeTest;
      CoreSim::Vdn::Int64 FG_hlty_fg;
      CoreSim::Vdn::Int64 SIDE1;
      CoreSim::Vdn::Int64 FAC_FG_VAL;
      CoreSim::Vdn::Int64 FAC_OPP_SELECT;
      CoreSim::Vdn::Int64 ADC_FG_VAL;
      CoreSim::Vdn::Int64 ADC_SOURCE;
      CoreSim::Vdn::Int64 FAD_OWN_FG_VAL;
      CoreSim::Vdn::Int64 FAD_OPP_FG_VAL;
      CoreSim::Vdn::Int64 FCU_FG_VAL;
      CoreSim::Vdn::Int64 LS_MMR_FG_VAL;        //LS
      CoreSim::Vdn::Int64 LS_MMR_OPP_SELECT;    //LS
      CoreSim::Vdn::Int64 LS_LOC_DEV_NOT_NCD;
      CoreSim::Vdn::Int64 LS_FREQ_NOT_NCD;
      CoreSim::Vdn::Int64 LS_RWY_HDG_NOT_NCD;
      CoreSim::Vdn::Int64 LS_BILS_INST;
      CoreSim::Vdn::Int64 LS_BMLS_INST;
      CoreSim::Vdn::Int64 LS_BGLS_INST;
      CoreSim::Vdn::Int64 MLS_FG_VAL;   //MLS
      CoreSim::Vdn::Int64 MLS_OPP_SEL;
      CoreSim::Vdn::Int64 IRS_FG_VAL;       //LS
      CoreSim::Vdn::Int64 IRS_SOURCE;
      CoreSim::Vdn::Int64 VOR_OWN_FG_VAL;   //VOR
      CoreSim::Vdn::Int64 FM_HLY;
      CoreSim::Vdn::Int64 FG_HLY;
      CoreSim::Vdn::Int64 FM_PUPRDY;
      CoreSim::Vdn::Int64 CFM_ENG;
      CoreSim::Vdn::Int64 IAE_ENG;
      CoreSim::Vdn::Int64 PW_ENGINE;

      CoreSim::Vdn::BoolArray FMGC_Healthy;
      //
      CoreSim::Vdn::A429Transmitter Fmgc_OwnA[2];//FMGC-1 OWN A; FMGC-2 OWN A

   }; 

   FGDO *fgdo;

   FGDI *fgdi;

   Outputs m_outputs;

   CommMemory m_comMemoryIO;

   CommMemory m_comMemoryFM;

   //FG SAFETY TESTS
   CoreSim::Osal::UInt32 fg_safeTest;
   
   //send DIS data to FG
   CoreSim::Osal::Net::UdpSocket m_txUdpSocket;
   CoreSim::Osal::Net::UdpSocket m_txUdpSocket2;
   //send A429 data to FG
   CoreSim::Osal::Net::UdpSocket m_txSocketA429[UDP_SOCKET_TOTAL_A429_IN];
   //receive DIS data from FG
   CoreSim::Osal::Net::UdpSocket m_socketList[UDP_SOCKET_NUMBER_DIS];
   //receive A429 data from FG
   CoreSim::Osal::Net::UdpSocket m_socketA429[UDP_SOCKET_NUMBER_A429];

   CoreSim::Osal::Net::SocketAddress m_SocketAddress[130];
   //fmgc1 dis
   CoreSim::Vdn::Bool DisInFG[UDP_SOCKET_TOTAL_DIS_IN+4]; 
   //fmgc2 dis
   CoreSim::Vdn::Bool DisInFG2[UDP_SOCKET_TOTAL_DIS_IN];
   //DIS data receive from FG
   CoreSim::Vdn::Bool DisFromFG[UDP_SOCKET_TOTAL_DIS];
   //COM_FMGC_MON_OUT to COM_FMGC_MON_IN
   CoreSim::Osal::UChar m_dataMonOut[2048];
   //COM_FMGC_OWN_A_OUT  port 10175
   CoreSim::Osal::UChar m_dataFmgcOwnA[2048];
   //COM_FMGC_OWN_B_OUT  port 10176
   CoreSim::Osal::UChar m_dataFmgcOwnB[2048];

   FILE *fd_10184;
   FILE *fd_10183;
   FILE *fd_10175;   //COM_FMGC_OWN_A_OUT
   FILE *fd_10176;   //COM_FMGC_OWN_B_OUT
   FILE *fd_10178;   //COM_FMGC_OWN_C_OUT
   FILE *fd_10177;   //COM_FT_INNER_OUT
   FILE *fd_10180;   //COM_FT_GUID_OUT
   FILE *fd_10181;   //MON_FMGC_MON_OUT
   FILE *fd_10182;   //MON_FT_MONITOR_OUT

   CoreSim::Osal::Bool BVAADC  ;   //ADC OWN VALID - ADC_FG_VAL
   CoreSim::Osal::Bool BVAOADC ;   //ADC OPP VALID
   CoreSim::Osal::Bool BVAADC3 ;   //ADC 3 VALID
   CoreSim::Osal::Bool BADC3SEL;   //ADC_SOURCE

   CoreSim::Osal::Bool BVAIRS  ;   //IRS OWN VALID - IRS_FG_VAL
   CoreSim::Osal::Bool BVAOIRS ;   //IRS OPP VALID
   CoreSim::Osal::Bool BVAIRS3 ;   //IRS 3 VALID
   CoreSim::Osal::Bool BIRS3SEL;   //IRS_SOURCE
   CoreSim::Osal::Bool BVFGOFAD;   //FAD OPP FG VAL
   CoreSim::Osal::Bool BVAGFCU ;   //FCU FG VAL
   CoreSim::Osal::Bool BVAGMMR ;   //MMR FG VAL
   CoreSim::Osal::Bool BILSINST;   //BILSINST
   CoreSim::Osal::Bool BMLSINST;   //BMLSINST
   CoreSim::Osal::Bool BGLSINST;   //BGLSINST
   CoreSim::Osal::Bool BFMLSINH;   //BFMLSINH
   CoreSim::Osal::Bool BPRTYFG ;   //FMGC PRIORITY
   CoreSim::Osal::Bool BVFGFAD ;   //FAD OWN FG VAL (FADEC OWN SUPPLIED)
   CoreSim::Osal::Bool BOMMRSEL;   //MMR OPP SELECT
   CoreSim::Osal::Bool BOFACSEL;   //FACOPPSELECT
   CoreSim::Osal::Bool BINNLOC ;   //LOC DEV NOT NCD
   CoreSim::Osal::Bool AddrTest;

public:
   void GetFiles(CoreSim::Osal::String path,CoreSim::Osal::String filter,vector<CoreSim::Osal::String>& filesname,vector<CoreSim::Osal::String>& files);
   CoreSim::Osal::Bool subscribeInputsFG(FG* fg,CoreSim::NetVdn::NetVdnClient& vdn);
   void UdpConnect(FGDO *fgdo);
   void CMAddrTest();
   template <typename T>
   void packA429Datas(T& input,int socketNo,FGDI* fgdi,FILE* fd);
   void packA429DatasMonOut(CoreSim::Osal::UChar* input,int socketNo,FGDI* fgdi);
   void RecvDisDataFromFG(FGDO* fgdo,FILE* fd,CoreSim::NetVdn::NetVdnClient& vdn);
   void RecvA429DataFromFG(FGDO* fgdo,FILE* fd);
   void DecodeSharedMemIoOut(CommMemory& m_comMemoryIO, Outputs& m_outputs);
   void DecodeComFtGuidOut(FGDO* fgdo,int i,CoreSim::Osal::Int32 Recvnum,UCHAR *recvBuff,FILE* fd,FILE* fd_10183);
   void UdpCommunication(FG* fg,FILE* fd,CoreSim::NetVdn::NetVdnClient& vdn);
   void bufferInputs(FG* fg);
   CoreSim::Osal::Bool publishoutputs(CoreSim::NetVdn::NetVdnClient& vdn);
   void setBit(CoreSim::Osal::UInt32 &des, int bit, int value);
   CoreSim::Osal::Bool getBit(UINT32 value,UINT32 bit);
};

extern CoreSim::Osal::UInt32 al_Pins_Dis1;
extern CoreSim::Osal::UInt32 al_Pins_Dis2;
extern CoreSim::Osal::UInt32 ac_Pins_Dis;
extern CoreSim::Osal::UInt32 cfm_engine;
extern CoreSim::Osal::UInt32 iae_engine;
extern CoreSim::Osal::UInt32 pw_engine;
extern CoreSim::Osal::UInt32 di_word0_bus66;
extern CoreSim::Osal::UInt32 di_word1_bus68;